package ca.mcgill.sel.classdiagram.language.controller;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.classdiagram.CDAttributeMapping;
import ca.mcgill.sel.classdiagram.CDClassifierMapping;
import ca.mcgill.sel.classdiagram.CDEnumLiteralMapping;
import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.classdiagram.CDMappableElement;
import ca.mcgill.sel.classdiagram.CDOperationMapping;
import ca.mcgill.sel.classdiagram.CDParameterMapping;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.CorePackage;

/**
 * The controller for mappings in the Cdm language.
 * 
 * @author joerg
 */
public class CdmMappingsController extends BaseController {
    
    /**
     * Creates a new instance of {@link CdmMappingsController}.
     */
    protected CdmMappingsController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    /**
     * Creates a new mapping between classifiers.
     * 
     * @param modelComposition the {@link COREModelComposition} the mapping should be added to
     * @return {@link CDClassifierMapping}
     */
    public CDClassifierMapping createClassifierMapping(COREModelComposition modelComposition) {
        // create mapping
        CDClassifierMapping mapping = CdmFactory.eINSTANCE.createCDClassifierMapping();
        
        doAdd(modelComposition, CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between enums.
     * 
     * @param modelComposition the {@link COREModelComposition} the mapping should be added to
     * @return {@link CDEnumMapping}
     */
    public CDEnumMapping createEnumMapping(COREModelComposition modelComposition) {
        // create mapping
        CDEnumMapping mapping = CdmFactory.eINSTANCE.createCDEnumMapping();
        
        doAdd(modelComposition, CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, mapping);
        
        return mapping;
    }
    
 
    /**
     * Creates a new mapping between attributes.
     * 
     * @param classifierMapping the {@link CDClassifierMapping} the mapping should be added to
     * @return the newly created {@link CDAttributeMapping}
     */
    public CDAttributeMapping createAttributeMapping(CDClassifierMapping classifierMapping) {
        // create mapping
        CDAttributeMapping mapping = CdmFactory.eINSTANCE.createCDAttributeMapping();
        
        doAdd(classifierMapping, CorePackage.Literals.CORE_MAPPING__MAPPINGS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between enum literals.
     * 
     * @param enumMapping the {@link CDEnumMapping} the mapping should be added to
     * @return the newly created {@link CDEnumLiteralMapping}
     */
    public CDEnumLiteralMapping createEnumLiteralMapping(CDEnumMapping enumMapping) {
        // create mapping
        CDEnumLiteralMapping mapping = CdmFactory.eINSTANCE.createCDEnumLiteralMapping();
        
        doAdd(enumMapping, CorePackage.Literals.CORE_MAPPING__MAPPINGS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between operations.
     * 
     * @param classifierMapping the {@link CDClassifierMapping} the mapping should be added to
     * @return the newly created {@link CDOperationMapping}
     */
    public CDOperationMapping createOperationMapping(CDClassifierMapping classifierMapping) {
        // create mapping
        CDOperationMapping mapping = CdmFactory.eINSTANCE.createCDOperationMapping();
        
        doAdd(classifierMapping, CorePackage.Literals.CORE_MAPPING__MAPPINGS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between parameters.
     * 
     * @param operationMapping the {@link OperationMapping} the mapping should be added to
     */
    public void createParameterMapping(CDOperationMapping operationMapping) {
        // create mapping
        CDParameterMapping mapping = CdmFactory.eINSTANCE.createCDParameterMapping();
        
        doAdd(operationMapping, CorePackage.Literals.CORE_MAPPING__MAPPINGS, mapping);
    }
    
    /**
     * Deletes the given mapping.
     * 
     * @param mapping the {@link Mapping} that should be deleted
     */
    public void deleteMapping(COREMapping<?> mapping) {
        EditingDomain domain = EMFEditUtil.getEditingDomain(mapping);
        CompoundCommand command = new CompoundCommand();
        
        // Unset the to element specifically to force a notification for the derived property.
        command.append(SetCommand.create(domain, mapping, CorePackage.Literals.CORE_LINK__TO, SetCommand.UNSET_VALUE));
        command.append(DeleteCommand.create(domain, mapping));
        
        doExecute(domain, command);
    }
    
    /**
     * Sets the to element of the given mapping to the given element.
     * 
     * @param mapping the mapping
     * @param feature the feature to be set
     * @param mappableElement the element to be set as the to element
     */
    public void setToElement(COREMapping<?> mapping, EStructuralFeature feature, CDMappableElement mappableElement) {
        doSet(mapping, feature, mappableElement);
    }
    
    /**
     * Sets the from element of the given mapping to the given element.
     * 
     * @param mapping the mapping
     * @param feature the feature to be set
     * @param mappableElement the element to be set as the from element
     */
    public void setFromElement(COREMapping<?> mapping, EStructuralFeature feature, CDMappableElement mappableElement) {
        // uses the same functionality right now
        setToElement(mapping, feature, mappableElement);
    }
    
}
