package ca.mcgill.sel.classdiagram.language.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.MoveCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.language.controller.util.CdmLanguageAction;

/**
 * Controller for REnums.
 * @author Franz
 */
public class EnumController extends BaseController {
    
    /**
     * Creates a new instance of {@link EnumController}.
     */
    protected EnumController() {
        // prevent anyone outside this package to instantiate
    }
    
    /*
     * COREPerspective Actions Validation Methods
     */
    
    public boolean canEditEnum(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.EDIT_ENUM.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canDeleteEnum(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.DELETE_ENUM.getName())) {
                return true;
            }
        }
        
        return false;
    }
    /**
     * Removes a specified REnumLiteral from the model.
     * @param literal REnumLiteral to be removed.
     */
    public void removeLiteral(CDEnumLiteral literal) {
        // Get the editing domain
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(literal);
        
        // Execute remove command for this literal
        Command removeCommand = RemoveCommand.create(editingDomain, literal);
        doExecute(editingDomain, removeCommand);
    }
    
    /**
     * Create an REnum literal in the model.
     * @param owner REnum that will own this literal.
     * @param index Index where we should insert it.
     * @param name Name of the literal.
     * @return The created REnumLiteral.
     */
    public CDEnumLiteral createREnumLiteral(CDEnum owner, int index, String name) {
        // create the literal
        CDEnumLiteral literal = CdmFactory.eINSTANCE.createCDEnumLiteral();
        
        // Set owner and name
        literal.setName(name);
        
        // Add literal to model
        doAdd(owner, CdmPackage.Literals.CD_ENUM__LITERALS, literal, index);
        return literal;
    }
    
    /**
     * Removes the given {@link CDEnum} and its associated layout element
     *
     * @param cdEnum the Enum that should be removed
     */
    public void deleteEnum(CDEnum cdEnum) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cdEnum);
        ClassDiagram cd = (ClassDiagram) cdEnum.eContainer();
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, cd, cdEnum);
        compoundCommand.append(removeElementMapCommand);
        
        // Create command for removing enum.
        Command removeEnumCommand = RemoveCommand.create(editingDomain, cdEnum);
        compoundCommand.append(removeEnumCommand);
        
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Renames the cdEnum to newName.
     *
     * @param cdEnum the cdEnum to be renamed
     * @param newName the new name
     */
    public void renameEnum(CDEnum cdEnum, String newName) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(cdEnum, RamPackage.Literals.ASPECT);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(aspect);

        Command renameCommand = new SetCommand(editingDomain, cdEnum,
                RamPackage.Literals.NAMED_ELEMENT__NAME, newName);

        doExecute(editingDomain, renameCommand);
    }

    /**
     * Set an literal to a different index.
     *
     * @param literal the literal to be set
     * @param index the index the attribute should be set to
     */
    public void setLiteralPosition(CDEnumLiteral literal, int index) {    	
    	// Get the editing domain
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(literal);        
        
        // Execute set position command for this literal
        Command command = new MoveCommand(editingDomain, literal.eContainer(), CdmPackage.Literals.CD_ENUM__LITERALS, literal, index);
        doExecute(editingDomain, command);        
    }
    
    /**
     * Renames the literal to newName.
     *
     * @param literal the literal to be renamed
     * @param newName the new name
     */
    public void renameLiteral(CDEnumLiteral literal, String newName) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(literal, RamPackage.Literals.ASPECT);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(aspect);

        Command renameCommand = new SetCommand(editingDomain, literal,
                RamPackage.Literals.NAMED_ELEMENT__NAME, newName);

        doExecute(editingDomain, renameCommand);
    }
	
    
}
