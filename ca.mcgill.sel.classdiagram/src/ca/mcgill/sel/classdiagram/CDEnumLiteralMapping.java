/**
 */
package ca.mcgill.sel.classdiagram;

import ca.mcgill.sel.core.COREMapping;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CD Enum Literal Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDEnumLiteralMapping()
 * @model
 * @generated
 */
public interface CDEnumLiteralMapping extends COREMapping<CDEnumLiteral> {
} // CDEnumLiteralMapping
