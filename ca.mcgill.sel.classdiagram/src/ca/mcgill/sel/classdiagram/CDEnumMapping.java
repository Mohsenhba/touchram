/**
 */
package ca.mcgill.sel.classdiagram;

import ca.mcgill.sel.core.COREMapping;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CD Enum Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDEnumMapping()
 * @model
 * @generated
 */
public interface CDEnumMapping extends COREMapping<CDEnum> {
} // CDEnumMapping
