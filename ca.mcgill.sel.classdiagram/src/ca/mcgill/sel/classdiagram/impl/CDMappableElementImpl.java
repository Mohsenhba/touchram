/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.CDMappableElement;
import ca.mcgill.sel.classdiagram.CdmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CD Mappable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDMappableElementImpl extends NamedElementImpl implements CDMappableElement {
    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected CDMappableElementImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return CdmPackage.Literals.CD_MAPPABLE_ELEMENT;
	}

} //CDMappableElementImpl
