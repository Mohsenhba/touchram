/**
 */
package ca.mcgill.sel.environmentmodel.provider;


import ca.mcgill.sel.environmentmodel.EmFactory;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.environmentmodel.EnvironmentModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EnvironmentModelItemProvider 
	extends NamedElementItemProvider {
	/**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EnvironmentModelItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

	/**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

            addSystemNamePropertyDescriptor(object);
        }
        return itemPropertyDescriptors;
    }

	/**
     * This adds a property descriptor for the System Name feature.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void addSystemNamePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_EnvironmentModel_systemName_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_EnvironmentModel_systemName_feature", "_UI_EnvironmentModel_type"),
                 EmPackage.Literals.ENVIRONMENT_MODEL__SYSTEM_NAME,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

	/**
     * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
        if (childrenFeatures == null) {
            super.getChildrenFeatures(object);
            childrenFeatures.add(EmPackage.Literals.ENVIRONMENT_MODEL__ACTORS);
            childrenFeatures.add(EmPackage.Literals.ENVIRONMENT_MODEL__COMMUNICATIONS);
            childrenFeatures.add(EmPackage.Literals.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS);
            childrenFeatures.add(EmPackage.Literals.ENVIRONMENT_MODEL__ACTOR_TYPES);
            childrenFeatures.add(EmPackage.Literals.ENVIRONMENT_MODEL__PARAMETERTYPE);
            childrenFeatures.add(EmPackage.Literals.ENVIRONMENT_MODEL__MESSAGE_TYPES);
            childrenFeatures.add(EmPackage.Literals.ENVIRONMENT_MODEL__LAYOUT);
            childrenFeatures.add(EmPackage.Literals.ENVIRONMENT_MODEL__NOTES);
        }
        return childrenFeatures;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
        // Check the type of the specified child object and return the proper feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

	/**
     * This returns EnvironmentModel.gif.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/EnvironmentModel"));
    }

	/**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getText(Object object) {
        String label = ((EnvironmentModel)object).getName();
        return label == null || label.length() == 0 ?
            getString("_UI_EnvironmentModel_type") :
            getString("_UI_EnvironmentModel_type") + " " + label;
    }


	/**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(EnvironmentModel.class)) {
            case EmPackage.ENVIRONMENT_MODEL__SYSTEM_NAME:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
                return;
            case EmPackage.ENVIRONMENT_MODEL__ACTORS:
            case EmPackage.ENVIRONMENT_MODEL__COMMUNICATIONS:
            case EmPackage.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS:
            case EmPackage.ENVIRONMENT_MODEL__ACTOR_TYPES:
            case EmPackage.ENVIRONMENT_MODEL__PARAMETERTYPE:
            case EmPackage.ENVIRONMENT_MODEL__MESSAGE_TYPES:
            case EmPackage.ENVIRONMENT_MODEL__LAYOUT:
            case EmPackage.ENVIRONMENT_MODEL__NOTES:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
                return;
        }
        super.notifyChanged(notification);
    }

	/**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (EmPackage.Literals.ENVIRONMENT_MODEL__ACTORS,
                 EmFactory.eINSTANCE.createActor()));

        newChildDescriptors.add
            (createChildParameter
                (EmPackage.Literals.ENVIRONMENT_MODEL__COMMUNICATIONS,
                 EmFactory.eINSTANCE.createActorActorCommunication()));

        newChildDescriptors.add
            (createChildParameter
                (EmPackage.Literals.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS,
                 EmFactory.eINSTANCE.createTimeTriggeredEvent()));

        newChildDescriptors.add
            (createChildParameter
                (EmPackage.Literals.ENVIRONMENT_MODEL__ACTOR_TYPES,
                 EmFactory.eINSTANCE.createActorType()));

        newChildDescriptors.add
            (createChildParameter
                (EmPackage.Literals.ENVIRONMENT_MODEL__PARAMETERTYPE,
                 EmFactory.eINSTANCE.createParameterType()));

        newChildDescriptors.add
            (createChildParameter
                (EmPackage.Literals.ENVIRONMENT_MODEL__MESSAGE_TYPES,
                 EmFactory.eINSTANCE.createMessageType()));

        newChildDescriptors.add
            (createChildParameter
                (EmPackage.Literals.ENVIRONMENT_MODEL__LAYOUT,
                 EmFactory.eINSTANCE.createLayout()));

        newChildDescriptors.add
            (createChildParameter
                (EmPackage.Literals.ENVIRONMENT_MODEL__NOTES,
                 EmFactory.eINSTANCE.createNote()));
    }

}
