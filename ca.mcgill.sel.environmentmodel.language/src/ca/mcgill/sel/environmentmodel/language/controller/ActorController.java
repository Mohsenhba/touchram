package ca.mcgill.sel.environmentmodel.language.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Message;

import ca.mcgill.sel.environmentmodel.util.Multiplicity;

/**
 * Controller defining actions on use case actors
 * 
 * @author rlanguay
 *
 */
public class ActorController extends BaseController {

    /**
     * Sets the multiplicity of the given {@link Actor} according to the given lower and upper bound.
     *
     * @param em the current {@link CommunicationDiagram}
     * @param associationEnd the {@link AssociationEnd} the multiplicity should be changed for
     * @param lowerBound the lower bound of the association end
     * @param upperBound the upper bound of the association end, -1 for many
     */
    public void setMultiplicity(EnvironmentModel em, Actor actor, Multiplicity multiplicity) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(actor);
        CompoundCommand compoundCommand;
        
        compoundCommand = new CompoundCommand();
        compoundCommand.append(SetCommand.create(editingDomain, actor,
                EmPackage.Literals.ACTOR__ACTOR_LOWER_BOUND, multiplicity.getLowerBound()));
        compoundCommand.append(SetCommand.create(editingDomain, actor,
                EmPackage.Literals.ACTOR__ACTOR_UPPER_BOUND, multiplicity.getUpperBound()));
        doExecute(editingDomain, compoundCommand);
    }
    
    public void setCommunicationMultiplicity(EnvironmentModel em, Actor actor, Multiplicity multiplicity) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(actor);
        CompoundCommand compoundCommand;
        
        compoundCommand = new CompoundCommand();
        compoundCommand.append(SetCommand.create(editingDomain, actor,
                EmPackage.Literals.ACTOR__COMMUNICATION_LOWER_BOUND, multiplicity.getLowerBound()));
        compoundCommand.append(SetCommand.create(editingDomain, actor,
                EmPackage.Literals.ACTOR__COMMUNICATION_UPPER_BOUND, multiplicity.getUpperBound()));
        doExecute(editingDomain, compoundCommand);
    }

    public void removeMessage(Message message) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(message);
        
        Command removeMessageCommand = RemoveCommand.create(editingDomain, message);
        
        doExecute(editingDomain, removeMessageCommand);
    }

}
