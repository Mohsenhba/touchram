package ca.mcgill.sel.environmentmodel.language.controller;

import ca.mcgill.sel.environmentmodel.language.controller.ActorController;

/**
 * A factory to obtain controllers for environment model
 * 
 * @author hyacinthali
 *
 */
public class EnvironmentModelControllerFactory {
	
	/**
     * The singleton instance of this factory.
     */
    public static final EnvironmentModelControllerFactory INSTANCE = new EnvironmentModelControllerFactory();
    
    private EnvironmentModelController environmentModelController ;
    private ActorController actorController;
        
    /**
     * Creates a new instance of {@link EnvironmentModelControllerFactory}.
     */
    private EnvironmentModelControllerFactory() {
        
    }

	/**
	 * Returns the controller for {@link EnvironmentModelController}.
	 * 
	 * @return the environmentModelController
	 */
	public EnvironmentModelController getEnvironmentModelController() {
		if (environmentModelController == null) {
			environmentModelController = new EnvironmentModelController();
        }
		return environmentModelController;
	}
    
	/**
     * Returns the controller for {@link ca.mcgill.sel.environmentmodel.Actor}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.environmentmodel.Actor}s.
     */	
    public ActorController getActorController() {
        if (actorController == null) {
            actorController = new ActorController();
        }
        
        return actorController;
    }

}
