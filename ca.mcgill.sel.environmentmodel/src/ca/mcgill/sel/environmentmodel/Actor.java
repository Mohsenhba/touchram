/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Actor#getActorLowerBound <em>Actor Lower Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Actor#getActorUpperBound <em>Actor Upper Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Actor#getActorType <em>Actor Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Actor#getCommunicationLowerBound <em>Communication Lower Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Actor#getCommunicationUpperBound <em>Communication Upper Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Actor#getMessages <em>Messages</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getActor()
 * @model
 * @generated
 */
public interface Actor extends NamedElement {
	/**
     * Returns the value of the '<em><b>Actor Lower Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Actor Lower Bound</em>' attribute.
     * @see #setActorLowerBound(int)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getActor_ActorLowerBound()
     * @model
     * @generated
     */
	int getActorLowerBound();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Actor#getActorLowerBound <em>Actor Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Actor Lower Bound</em>' attribute.
     * @see #getActorLowerBound()
     * @generated
     */
	void setActorLowerBound(int value);

	/**
     * Returns the value of the '<em><b>Actor Upper Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Actor Upper Bound</em>' attribute.
     * @see #setActorUpperBound(int)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getActor_ActorUpperBound()
     * @model
     * @generated
     */
	int getActorUpperBound();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Actor#getActorUpperBound <em>Actor Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Actor Upper Bound</em>' attribute.
     * @see #getActorUpperBound()
     * @generated
     */
	void setActorUpperBound(int value);

	/**
     * Returns the value of the '<em><b>Actor Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Actor Type</em>' reference.
     * @see #setActorType(ActorType)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getActor_ActorType()
     * @model required="true"
     * @generated
     */
	ActorType getActorType();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Actor#getActorType <em>Actor Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Actor Type</em>' reference.
     * @see #getActorType()
     * @generated
     */
	void setActorType(ActorType value);

	/**
     * Returns the value of the '<em><b>Communication Lower Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Communication Lower Bound</em>' attribute.
     * @see #setCommunicationLowerBound(int)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getActor_CommunicationLowerBound()
     * @model
     * @generated
     */
	int getCommunicationLowerBound();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Actor#getCommunicationLowerBound <em>Communication Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Communication Lower Bound</em>' attribute.
     * @see #getCommunicationLowerBound()
     * @generated
     */
	void setCommunicationLowerBound(int value);

	/**
     * Returns the value of the '<em><b>Communication Upper Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Communication Upper Bound</em>' attribute.
     * @see #setCommunicationUpperBound(int)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getActor_CommunicationUpperBound()
     * @model
     * @generated
     */
	int getCommunicationUpperBound();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Actor#getCommunicationUpperBound <em>Communication Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Communication Upper Bound</em>' attribute.
     * @see #getCommunicationUpperBound()
     * @generated
     */
	void setCommunicationUpperBound(int value);

	/**
     * Returns the value of the '<em><b>Messages</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.Message}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Messages</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getActor_Messages()
     * @model containment="true"
     * @generated
     */
	EList<Message> getMessages();

} // Actor
