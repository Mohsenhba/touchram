/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor Actor Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.ActorActorCommunication#getParticipants <em>Participants</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getActorActorCommunication()
 * @model
 * @generated
 */
public interface ActorActorCommunication extends NamedElement {

	/**
     * Returns the value of the '<em><b>Participants</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.Actor}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Participants</em>' reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getActorActorCommunication_Participants()
     * @model lower="2" upper="2"
     * @generated
     */
	EList<Actor> getParticipants();
} // ActorActorCommunication
