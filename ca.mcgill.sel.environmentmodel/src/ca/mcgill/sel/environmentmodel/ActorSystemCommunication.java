/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor System Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.ActorSystemCommunication#getMessages <em>Messages</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getActorSystemCommunication()
 * @model
 * @generated
 */
public interface ActorSystemCommunication extends Communication {
	/**
	 * Returns the value of the '<em><b>Messages</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.environmentmodel.Message}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Messages</em>' containment reference list.
	 * @see ca.mcgill.sel.environmentmodel.EmPackage#getActorSystemCommunication_Messages()
	 * @model containment="true"
	 * @generated
	 */
	EList<Message> getMessages();

} // ActorSystemCommunication
