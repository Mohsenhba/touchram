/**
 */
package ca.mcgill.sel.environmentmodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Triggered Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getTimeTriggeredEvent()
 * @model
 * @generated
 */
public interface TimeTriggeredEvent extends NamedElement {
} // TimeTriggeredEvent
