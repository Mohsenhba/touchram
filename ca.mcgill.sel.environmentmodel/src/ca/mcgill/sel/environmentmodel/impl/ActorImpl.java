/**
 */
package ca.mcgill.sel.environmentmodel.impl;

import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.ActorType;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.Message;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.ActorImpl#getActorLowerBound <em>Actor Lower Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.ActorImpl#getActorUpperBound <em>Actor Upper Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.ActorImpl#getActorType <em>Actor Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.ActorImpl#getCommunicationLowerBound <em>Communication Lower Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.ActorImpl#getCommunicationUpperBound <em>Communication Upper Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.ActorImpl#getMessages <em>Messages</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActorImpl extends NamedElementImpl implements Actor {
	/**
     * The default value of the '{@link #getActorLowerBound() <em>Actor Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActorLowerBound()
     * @generated
     * @ordered
     */
	protected static final int ACTOR_LOWER_BOUND_EDEFAULT = 0;

	/**
     * The cached value of the '{@link #getActorLowerBound() <em>Actor Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActorLowerBound()
     * @generated
     * @ordered
     */
	protected int actorLowerBound = ACTOR_LOWER_BOUND_EDEFAULT;

	/**
     * The default value of the '{@link #getActorUpperBound() <em>Actor Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActorUpperBound()
     * @generated
     * @ordered
     */
	protected static final int ACTOR_UPPER_BOUND_EDEFAULT = 0;

	/**
     * The cached value of the '{@link #getActorUpperBound() <em>Actor Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActorUpperBound()
     * @generated
     * @ordered
     */
	protected int actorUpperBound = ACTOR_UPPER_BOUND_EDEFAULT;

	/**
     * The cached value of the '{@link #getActorType() <em>Actor Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActorType()
     * @generated
     * @ordered
     */
	protected ActorType actorType;

	/**
     * The default value of the '{@link #getCommunicationLowerBound() <em>Communication Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCommunicationLowerBound()
     * @generated
     * @ordered
     */
	protected static final int COMMUNICATION_LOWER_BOUND_EDEFAULT = 0;

	/**
     * The cached value of the '{@link #getCommunicationLowerBound() <em>Communication Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCommunicationLowerBound()
     * @generated
     * @ordered
     */
	protected int communicationLowerBound = COMMUNICATION_LOWER_BOUND_EDEFAULT;

	/**
     * The default value of the '{@link #getCommunicationUpperBound() <em>Communication Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCommunicationUpperBound()
     * @generated
     * @ordered
     */
	protected static final int COMMUNICATION_UPPER_BOUND_EDEFAULT = 0;

	/**
     * The cached value of the '{@link #getCommunicationUpperBound() <em>Communication Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCommunicationUpperBound()
     * @generated
     * @ordered
     */
	protected int communicationUpperBound = COMMUNICATION_UPPER_BOUND_EDEFAULT;

	/**
     * The cached value of the '{@link #getMessages() <em>Messages</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMessages()
     * @generated
     * @ordered
     */
	protected EList<Message> messages;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActorImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return EmPackage.Literals.ACTOR;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public int getActorLowerBound() {
        return actorLowerBound;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setActorLowerBound(int newActorLowerBound) {
        int oldActorLowerBound = actorLowerBound;
        actorLowerBound = newActorLowerBound;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.ACTOR__ACTOR_LOWER_BOUND, oldActorLowerBound, actorLowerBound));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public int getActorUpperBound() {
        return actorUpperBound;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setActorUpperBound(int newActorUpperBound) {
        int oldActorUpperBound = actorUpperBound;
        actorUpperBound = newActorUpperBound;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.ACTOR__ACTOR_UPPER_BOUND, oldActorUpperBound, actorUpperBound));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ActorType getActorType() {
        if (actorType != null && actorType.eIsProxy()) {
            InternalEObject oldActorType = (InternalEObject)actorType;
            actorType = (ActorType)eResolveProxy(oldActorType);
            if (actorType != oldActorType) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, EmPackage.ACTOR__ACTOR_TYPE, oldActorType, actorType));
            }
        }
        return actorType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ActorType basicGetActorType() {
        return actorType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setActorType(ActorType newActorType) {
        ActorType oldActorType = actorType;
        actorType = newActorType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.ACTOR__ACTOR_TYPE, oldActorType, actorType));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public int getCommunicationLowerBound() {
        return communicationLowerBound;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setCommunicationLowerBound(int newCommunicationLowerBound) {
        int oldCommunicationLowerBound = communicationLowerBound;
        communicationLowerBound = newCommunicationLowerBound;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.ACTOR__COMMUNICATION_LOWER_BOUND, oldCommunicationLowerBound, communicationLowerBound));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public int getCommunicationUpperBound() {
        return communicationUpperBound;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setCommunicationUpperBound(int newCommunicationUpperBound) {
        int oldCommunicationUpperBound = communicationUpperBound;
        communicationUpperBound = newCommunicationUpperBound;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.ACTOR__COMMUNICATION_UPPER_BOUND, oldCommunicationUpperBound, communicationUpperBound));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EList<Message> getMessages() {
        if (messages == null) {
            messages = new EObjectContainmentEList<Message>(Message.class, this, EmPackage.ACTOR__MESSAGES);
        }
        return messages;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case EmPackage.ACTOR__MESSAGES:
                return ((InternalEList<?>)getMessages()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case EmPackage.ACTOR__ACTOR_LOWER_BOUND:
                return getActorLowerBound();
            case EmPackage.ACTOR__ACTOR_UPPER_BOUND:
                return getActorUpperBound();
            case EmPackage.ACTOR__ACTOR_TYPE:
                if (resolve) return getActorType();
                return basicGetActorType();
            case EmPackage.ACTOR__COMMUNICATION_LOWER_BOUND:
                return getCommunicationLowerBound();
            case EmPackage.ACTOR__COMMUNICATION_UPPER_BOUND:
                return getCommunicationUpperBound();
            case EmPackage.ACTOR__MESSAGES:
                return getMessages();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case EmPackage.ACTOR__ACTOR_LOWER_BOUND:
                setActorLowerBound((Integer)newValue);
                return;
            case EmPackage.ACTOR__ACTOR_UPPER_BOUND:
                setActorUpperBound((Integer)newValue);
                return;
            case EmPackage.ACTOR__ACTOR_TYPE:
                setActorType((ActorType)newValue);
                return;
            case EmPackage.ACTOR__COMMUNICATION_LOWER_BOUND:
                setCommunicationLowerBound((Integer)newValue);
                return;
            case EmPackage.ACTOR__COMMUNICATION_UPPER_BOUND:
                setCommunicationUpperBound((Integer)newValue);
                return;
            case EmPackage.ACTOR__MESSAGES:
                getMessages().clear();
                getMessages().addAll((Collection<? extends Message>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case EmPackage.ACTOR__ACTOR_LOWER_BOUND:
                setActorLowerBound(ACTOR_LOWER_BOUND_EDEFAULT);
                return;
            case EmPackage.ACTOR__ACTOR_UPPER_BOUND:
                setActorUpperBound(ACTOR_UPPER_BOUND_EDEFAULT);
                return;
            case EmPackage.ACTOR__ACTOR_TYPE:
                setActorType((ActorType)null);
                return;
            case EmPackage.ACTOR__COMMUNICATION_LOWER_BOUND:
                setCommunicationLowerBound(COMMUNICATION_LOWER_BOUND_EDEFAULT);
                return;
            case EmPackage.ACTOR__COMMUNICATION_UPPER_BOUND:
                setCommunicationUpperBound(COMMUNICATION_UPPER_BOUND_EDEFAULT);
                return;
            case EmPackage.ACTOR__MESSAGES:
                getMessages().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case EmPackage.ACTOR__ACTOR_LOWER_BOUND:
                return actorLowerBound != ACTOR_LOWER_BOUND_EDEFAULT;
            case EmPackage.ACTOR__ACTOR_UPPER_BOUND:
                return actorUpperBound != ACTOR_UPPER_BOUND_EDEFAULT;
            case EmPackage.ACTOR__ACTOR_TYPE:
                return actorType != null;
            case EmPackage.ACTOR__COMMUNICATION_LOWER_BOUND:
                return communicationLowerBound != COMMUNICATION_LOWER_BOUND_EDEFAULT;
            case EmPackage.ACTOR__COMMUNICATION_UPPER_BOUND:
                return communicationUpperBound != COMMUNICATION_UPPER_BOUND_EDEFAULT;
            case EmPackage.ACTOR__MESSAGES:
                return messages != null && !messages.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (actorLowerBound: ");
        result.append(actorLowerBound);
        result.append(", actorUpperBound: ");
        result.append(actorUpperBound);
        result.append(", communicationLowerBound: ");
        result.append(communicationLowerBound);
        result.append(", communicationUpperBound: ");
        result.append(communicationUpperBound);
        result.append(')');
        return result.toString();
    }

} //ActorImpl
