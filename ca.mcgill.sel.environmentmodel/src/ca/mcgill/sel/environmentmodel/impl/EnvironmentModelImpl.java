/**
 */
package ca.mcgill.sel.environmentmodel.impl;

import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.ActorActorCommunication;
import ca.mcgill.sel.environmentmodel.ActorType;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Layout;
import ca.mcgill.sel.environmentmodel.MessageType;
import ca.mcgill.sel.environmentmodel.Note;
import ca.mcgill.sel.environmentmodel.ParameterType;
import ca.mcgill.sel.environmentmodel.TimeTriggeredEvent;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getCommunications <em>Communications</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getSystemName <em>System Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getTimeTriggeredEvents <em>Time Triggered Events</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getActorTypes <em>Actor Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getParametertype <em>Parametertype</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getMessageTypes <em>Message Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getLayout <em>Layout</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl#getNotes <em>Notes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentModelImpl extends NamedElementImpl implements EnvironmentModel {
	/**
     * The cached value of the '{@link #getActors() <em>Actors</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActors()
     * @generated
     * @ordered
     */
	protected EList<Actor> actors;

	/**
     * The cached value of the '{@link #getCommunications() <em>Communications</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCommunications()
     * @generated
     * @ordered
     */
	protected EList<ActorActorCommunication> communications;

	/**
     * The default value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSystemName()
     * @generated
     * @ordered
     */
	protected static final String SYSTEM_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSystemName()
     * @generated
     * @ordered
     */
	protected String systemName = SYSTEM_NAME_EDEFAULT;

	/**
     * The cached value of the '{@link #getTimeTriggeredEvents() <em>Time Triggered Events</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTimeTriggeredEvents()
     * @generated
     * @ordered
     */
	protected EList<TimeTriggeredEvent> timeTriggeredEvents;

	/**
     * The cached value of the '{@link #getActorTypes() <em>Actor Types</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActorTypes()
     * @generated
     * @ordered
     */
	protected EList<ActorType> actorTypes;

	/**
     * The cached value of the '{@link #getParametertype() <em>Parametertype</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getParametertype()
     * @generated
     * @ordered
     */
	protected EList<ParameterType> parametertype;

	/**
     * The cached value of the '{@link #getMessageTypes() <em>Message Types</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMessageTypes()
     * @generated
     * @ordered
     */
	protected EList<MessageType> messageTypes;

	/**
     * The cached value of the '{@link #getLayout() <em>Layout</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLayout()
     * @generated
     * @ordered
     */
	protected Layout layout;

	/**
     * The cached value of the '{@link #getNotes() <em>Notes</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNotes()
     * @generated
     * @ordered
     */
    protected EList<Note> notes;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected EnvironmentModelImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return EmPackage.Literals.ENVIRONMENT_MODEL;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EList<Actor> getActors() {
        if (actors == null) {
            actors = new EObjectContainmentEList<Actor>(Actor.class, this, EmPackage.ENVIRONMENT_MODEL__ACTORS);
        }
        return actors;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EList<ActorActorCommunication> getCommunications() {
        if (communications == null) {
            communications = new EObjectContainmentEList<ActorActorCommunication>(ActorActorCommunication.class, this, EmPackage.ENVIRONMENT_MODEL__COMMUNICATIONS);
        }
        return communications;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String getSystemName() {
        return systemName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setSystemName(String newSystemName) {
        String oldSystemName = systemName;
        systemName = newSystemName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.ENVIRONMENT_MODEL__SYSTEM_NAME, oldSystemName, systemName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EList<TimeTriggeredEvent> getTimeTriggeredEvents() {
        if (timeTriggeredEvents == null) {
            timeTriggeredEvents = new EObjectContainmentEList<TimeTriggeredEvent>(TimeTriggeredEvent.class, this, EmPackage.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS);
        }
        return timeTriggeredEvents;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EList<ActorType> getActorTypes() {
        if (actorTypes == null) {
            actorTypes = new EObjectContainmentEList<ActorType>(ActorType.class, this, EmPackage.ENVIRONMENT_MODEL__ACTOR_TYPES);
        }
        return actorTypes;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EList<ParameterType> getParametertype() {
        if (parametertype == null) {
            parametertype = new EObjectContainmentEList<ParameterType>(ParameterType.class, this, EmPackage.ENVIRONMENT_MODEL__PARAMETERTYPE);
        }
        return parametertype;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EList<MessageType> getMessageTypes() {
        if (messageTypes == null) {
            messageTypes = new EObjectContainmentEList<MessageType>(MessageType.class, this, EmPackage.ENVIRONMENT_MODEL__MESSAGE_TYPES);
        }
        return messageTypes;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Layout getLayout() {
        return layout;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetLayout(Layout newLayout, NotificationChain msgs) {
        Layout oldLayout = layout;
        layout = newLayout;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EmPackage.ENVIRONMENT_MODEL__LAYOUT, oldLayout, newLayout);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setLayout(Layout newLayout) {
        if (newLayout != layout) {
            NotificationChain msgs = null;
            if (layout != null)
                msgs = ((InternalEObject)layout).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EmPackage.ENVIRONMENT_MODEL__LAYOUT, null, msgs);
            if (newLayout != null)
                msgs = ((InternalEObject)newLayout).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EmPackage.ENVIRONMENT_MODEL__LAYOUT, null, msgs);
            msgs = basicSetLayout(newLayout, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.ENVIRONMENT_MODEL__LAYOUT, newLayout, newLayout));
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<Note> getNotes() {
        if (notes == null) {
            notes = new EObjectContainmentEList<Note>(Note.class, this, EmPackage.ENVIRONMENT_MODEL__NOTES);
        }
        return notes;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case EmPackage.ENVIRONMENT_MODEL__ACTORS:
                return ((InternalEList<?>)getActors()).basicRemove(otherEnd, msgs);
            case EmPackage.ENVIRONMENT_MODEL__COMMUNICATIONS:
                return ((InternalEList<?>)getCommunications()).basicRemove(otherEnd, msgs);
            case EmPackage.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS:
                return ((InternalEList<?>)getTimeTriggeredEvents()).basicRemove(otherEnd, msgs);
            case EmPackage.ENVIRONMENT_MODEL__ACTOR_TYPES:
                return ((InternalEList<?>)getActorTypes()).basicRemove(otherEnd, msgs);
            case EmPackage.ENVIRONMENT_MODEL__PARAMETERTYPE:
                return ((InternalEList<?>)getParametertype()).basicRemove(otherEnd, msgs);
            case EmPackage.ENVIRONMENT_MODEL__MESSAGE_TYPES:
                return ((InternalEList<?>)getMessageTypes()).basicRemove(otherEnd, msgs);
            case EmPackage.ENVIRONMENT_MODEL__LAYOUT:
                return basicSetLayout(null, msgs);
            case EmPackage.ENVIRONMENT_MODEL__NOTES:
                return ((InternalEList<?>)getNotes()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case EmPackage.ENVIRONMENT_MODEL__ACTORS:
                return getActors();
            case EmPackage.ENVIRONMENT_MODEL__COMMUNICATIONS:
                return getCommunications();
            case EmPackage.ENVIRONMENT_MODEL__SYSTEM_NAME:
                return getSystemName();
            case EmPackage.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS:
                return getTimeTriggeredEvents();
            case EmPackage.ENVIRONMENT_MODEL__ACTOR_TYPES:
                return getActorTypes();
            case EmPackage.ENVIRONMENT_MODEL__PARAMETERTYPE:
                return getParametertype();
            case EmPackage.ENVIRONMENT_MODEL__MESSAGE_TYPES:
                return getMessageTypes();
            case EmPackage.ENVIRONMENT_MODEL__LAYOUT:
                return getLayout();
            case EmPackage.ENVIRONMENT_MODEL__NOTES:
                return getNotes();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case EmPackage.ENVIRONMENT_MODEL__ACTORS:
                getActors().clear();
                getActors().addAll((Collection<? extends Actor>)newValue);
                return;
            case EmPackage.ENVIRONMENT_MODEL__COMMUNICATIONS:
                getCommunications().clear();
                getCommunications().addAll((Collection<? extends ActorActorCommunication>)newValue);
                return;
            case EmPackage.ENVIRONMENT_MODEL__SYSTEM_NAME:
                setSystemName((String)newValue);
                return;
            case EmPackage.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS:
                getTimeTriggeredEvents().clear();
                getTimeTriggeredEvents().addAll((Collection<? extends TimeTriggeredEvent>)newValue);
                return;
            case EmPackage.ENVIRONMENT_MODEL__ACTOR_TYPES:
                getActorTypes().clear();
                getActorTypes().addAll((Collection<? extends ActorType>)newValue);
                return;
            case EmPackage.ENVIRONMENT_MODEL__PARAMETERTYPE:
                getParametertype().clear();
                getParametertype().addAll((Collection<? extends ParameterType>)newValue);
                return;
            case EmPackage.ENVIRONMENT_MODEL__MESSAGE_TYPES:
                getMessageTypes().clear();
                getMessageTypes().addAll((Collection<? extends MessageType>)newValue);
                return;
            case EmPackage.ENVIRONMENT_MODEL__LAYOUT:
                setLayout((Layout)newValue);
                return;
            case EmPackage.ENVIRONMENT_MODEL__NOTES:
                getNotes().clear();
                getNotes().addAll((Collection<? extends Note>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case EmPackage.ENVIRONMENT_MODEL__ACTORS:
                getActors().clear();
                return;
            case EmPackage.ENVIRONMENT_MODEL__COMMUNICATIONS:
                getCommunications().clear();
                return;
            case EmPackage.ENVIRONMENT_MODEL__SYSTEM_NAME:
                setSystemName(SYSTEM_NAME_EDEFAULT);
                return;
            case EmPackage.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS:
                getTimeTriggeredEvents().clear();
                return;
            case EmPackage.ENVIRONMENT_MODEL__ACTOR_TYPES:
                getActorTypes().clear();
                return;
            case EmPackage.ENVIRONMENT_MODEL__PARAMETERTYPE:
                getParametertype().clear();
                return;
            case EmPackage.ENVIRONMENT_MODEL__MESSAGE_TYPES:
                getMessageTypes().clear();
                return;
            case EmPackage.ENVIRONMENT_MODEL__LAYOUT:
                setLayout((Layout)null);
                return;
            case EmPackage.ENVIRONMENT_MODEL__NOTES:
                getNotes().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case EmPackage.ENVIRONMENT_MODEL__ACTORS:
                return actors != null && !actors.isEmpty();
            case EmPackage.ENVIRONMENT_MODEL__COMMUNICATIONS:
                return communications != null && !communications.isEmpty();
            case EmPackage.ENVIRONMENT_MODEL__SYSTEM_NAME:
                return SYSTEM_NAME_EDEFAULT == null ? systemName != null : !SYSTEM_NAME_EDEFAULT.equals(systemName);
            case EmPackage.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS:
                return timeTriggeredEvents != null && !timeTriggeredEvents.isEmpty();
            case EmPackage.ENVIRONMENT_MODEL__ACTOR_TYPES:
                return actorTypes != null && !actorTypes.isEmpty();
            case EmPackage.ENVIRONMENT_MODEL__PARAMETERTYPE:
                return parametertype != null && !parametertype.isEmpty();
            case EmPackage.ENVIRONMENT_MODEL__MESSAGE_TYPES:
                return messageTypes != null && !messageTypes.isEmpty();
            case EmPackage.ENVIRONMENT_MODEL__LAYOUT:
                return layout != null;
            case EmPackage.ENVIRONMENT_MODEL__NOTES:
                return notes != null && !notes.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (systemName: ");
        result.append(systemName);
        result.append(')');
        return result.toString();
    }

} //EnvironmentModelImpl
