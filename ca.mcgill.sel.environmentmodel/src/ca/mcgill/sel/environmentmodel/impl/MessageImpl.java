/**
 */
package ca.mcgill.sel.environmentmodel.impl;

import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.MessageDirection;
import ca.mcgill.sel.environmentmodel.MessageType;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.MessageImpl#getMessageType <em>Message Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.MessageImpl#getMessageDirection <em>Message Direction</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MessageImpl extends MinimalEObjectImpl.Container implements Message {
	/**
     * The cached value of the '{@link #getMessageType() <em>Message Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMessageType()
     * @generated
     * @ordered
     */
	protected MessageType messageType;
	/**
     * The default value of the '{@link #getMessageDirection() <em>Message Direction</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMessageDirection()
     * @generated
     * @ordered
     */
	protected static final MessageDirection MESSAGE_DIRECTION_EDEFAULT = MessageDirection.INPUT;
	/**
     * The cached value of the '{@link #getMessageDirection() <em>Message Direction</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMessageDirection()
     * @generated
     * @ordered
     */
	protected MessageDirection messageDirection = MESSAGE_DIRECTION_EDEFAULT;
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected MessageImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return EmPackage.Literals.MESSAGE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MessageType getMessageType() {
        if (messageType != null && messageType.eIsProxy()) {
            InternalEObject oldMessageType = (InternalEObject)messageType;
            messageType = (MessageType)eResolveProxy(oldMessageType);
            if (messageType != oldMessageType) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, EmPackage.MESSAGE__MESSAGE_TYPE, oldMessageType, messageType));
            }
        }
        return messageType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MessageType basicGetMessageType() {
        return messageType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setMessageType(MessageType newMessageType) {
        MessageType oldMessageType = messageType;
        messageType = newMessageType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.MESSAGE__MESSAGE_TYPE, oldMessageType, messageType));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MessageDirection getMessageDirection() {
        return messageDirection;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setMessageDirection(MessageDirection newMessageDirection) {
        MessageDirection oldMessageDirection = messageDirection;
        messageDirection = newMessageDirection == null ? MESSAGE_DIRECTION_EDEFAULT : newMessageDirection;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, EmPackage.MESSAGE__MESSAGE_DIRECTION, oldMessageDirection, messageDirection));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case EmPackage.MESSAGE__MESSAGE_TYPE:
                if (resolve) return getMessageType();
                return basicGetMessageType();
            case EmPackage.MESSAGE__MESSAGE_DIRECTION:
                return getMessageDirection();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case EmPackage.MESSAGE__MESSAGE_TYPE:
                setMessageType((MessageType)newValue);
                return;
            case EmPackage.MESSAGE__MESSAGE_DIRECTION:
                setMessageDirection((MessageDirection)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case EmPackage.MESSAGE__MESSAGE_TYPE:
                setMessageType((MessageType)null);
                return;
            case EmPackage.MESSAGE__MESSAGE_DIRECTION:
                setMessageDirection(MESSAGE_DIRECTION_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case EmPackage.MESSAGE__MESSAGE_TYPE:
                return messageType != null;
            case EmPackage.MESSAGE__MESSAGE_DIRECTION:
                return messageDirection != MESSAGE_DIRECTION_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (messageDirection: ");
        result.append(messageDirection);
        result.append(')');
        return result.toString();
    }

} //MessageImpl
