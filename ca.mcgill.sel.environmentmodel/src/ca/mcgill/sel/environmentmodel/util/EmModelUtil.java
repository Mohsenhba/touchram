package ca.mcgill.sel.environmentmodel.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.controller.ModelUtil;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.ActorActorCommunication;
import ca.mcgill.sel.environmentmodel.EmFactory;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Layout;
import ca.mcgill.sel.environmentmodel.LayoutElement;


/**
 * Helper class with convenient static methods for working with evironment model objects.
 * @author hyacinthali
 *
 */
public class EmModelUtil implements ModelUtil {
	
	/**
     * Creates a new communication diagram and associated entities
     * @param name The name of the diagram
     * @return The new diagram
     */
    public static EnvironmentModel createEnvironmentModel(String name) {
    	
    	EnvironmentModel em = EmFactory.eINSTANCE.createEnvironmentModel();       
    	em.setName(name);
    	em.setSystemName(name);
    	
    	// create an empty layout
        createLayout(em);
        
        return em;
    }

    /**
     * Creates a new layout for a given {@link EnvironmentModel}.
     * The layout is the {@link ca.mcgill.sel.ram.impl.ContainerMapImpl} specifically
     * that holds all {@link LayoutElement} for children of the given {@link EnvironmentModel}.
     *
     * @param em the {@link EnvironmentModel} holding the {@link LayoutElement} for its children
     */
    private static void createLayout(EnvironmentModel em) {
        Layout layout = EmFactory.eINSTANCE.createLayout();

        // workaround used here since creating the map, adding the values and then putting it doesn't work
        // EMF somehow does some magic with the passed map instance
        layout.getContainers().put(em, new BasicEMap<EObject, LayoutElement>());

        em.setLayout(layout);
    }
    
	@Override
	public EObject createNewEmptyModel(String name) {
		return createEnvironmentModel(name);
	}

}
