package ca.mcgill.sel.operationmodel.language.controller;

import ca.mcgill.sel.core.controller.CoreBaseController;

/**
 * The controller with basic functionality that can be used by any sub-class in operation model controller.
 * @author hyacinthali
 *
 */
public class BaseController extends CoreBaseController {

}
