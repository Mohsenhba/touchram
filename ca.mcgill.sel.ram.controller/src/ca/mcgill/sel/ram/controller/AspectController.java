package ca.mcgill.sel.ram.controller;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.AbstractMessageView;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.MessageViewReference;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OriginalBehaviorExecution;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.impl.ContainerMapImpl;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * The controller for {@link Aspect}s.
 *
 * @author mschoettle
 * @author oalam
 */
public class AspectController extends BaseController {

    /**
     * Creates a new instance of {@link AspectController}.
     */
    protected AspectController() {
        // prevent anyone outside this package to instantiate
    }

    /**
     * Creates a new message view for the given operation.
     * Initializes the message view with the initial call.
     * Furthermore, creates a container map for the layout to be able to save
     * the message views layout.
     *
     * @param owner the owner of the message view
     * @param operation the operation specified by this message view
     */
    public MessageView createMessageView(Aspect owner, Operation operation) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        MessageView messageView = MessageViewUtil.createMessageView(operation);
        CompoundCommand command = createAddMessageViewCommand(editingDomain, owner, messageView,
                messageView.getSpecification());

        doExecute(editingDomain, command);
        return messageView;
    }

    /**
     * Creates a new aspect message view that advices the given operation.
     * Adds the aspect message view to the "affected by" list of the operation's message view.
     * If no message view exists:
     * <ul>
     *   <li>if the operation is in the same model, a new empty message view is created</li>
     *   <li>if the operation is mapped and that operation is partial, a new message is created as well</li>
     *   <li>if the operation is mapped and that operation is not partial, an exception is thrown</li>
     * </ul>
     * 
     * Furthermore, creates a container map for the layout to be able to save
     * the message views layout.
     *
     * @param owner the owner of the message view
     * @param operation the operation specified by this message view
     */
    public void createAspectMessageView(Aspect owner, Operation operation) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        AspectMessageView messageView = RamFactory.eINSTANCE.createAspectMessageView();
        messageView.setPointcut(operation);

        Interaction advice = MessageViewUtil.createInteraction(operation);
        messageView.setAdvice(advice);

        /**
         * Add the original behaviour execution fragment.
         */
        OriginalBehaviorExecution fragment = RamFactory.eINSTANCE.createOriginalBehaviorExecution();
        fragment.getCovered().add(advice.getLifelines().get(0));
        advice.getFragments().add(1, fragment);

        Command addMessageViewCommand = createAddMessageViewCommand(editingDomain, owner, messageView, advice);

        /**
         * Advice the message view. If it doesn't exist yet, create one.
         * If there is a message view in another model, a message view reference is required.
         */
        boolean messageViewDefined = MessageViewUtil.isMessageViewDefined(owner, operation);
        MessageView existingMessageView = MessageViewUtil.getMessageViewFor(owner, operation);
        
        Command addActualMessageView = null;
        Command adviceMessageViewCommand = null;
        
        if (existingMessageView == null) {
            /**
             * There's no message view, so let's create one.
             * This is the case for operations of the current model that weren't mapped,
             * or the mapped operation is partial.
             */
            if (!messageViewDefined) {
                existingMessageView = MessageViewUtil.createMessageView(operation);
                /**
                 * Partial operations have empty message views (i.e., no specification).
                 */
                if (operation.getPartiality() != RAMPartialityType.NONE) {
                    existingMessageView.setSpecification(null);
                    addActualMessageView = AddCommand.create(editingDomain, owner,
                            RamPackage.Literals.ASPECT__MESSAGE_VIEWS, existingMessageView);
                } else {
                    addActualMessageView = createAddMessageViewCommand(editingDomain, owner, existingMessageView,
                            existingMessageView.getSpecification());
                }
            } else {
                /**
                 * There's a message view in another model, so create a message view reference and link the two.
                 */
                Operation fromOperation = RAMModelUtil.getMappedFrom(operation);
                    
                Aspect source = EMFModelUtil.getRootContainerOfType(fromOperation, RamPackage.Literals.ASPECT);
                existingMessageView = MessageViewUtil.getMessageViewFor(source, fromOperation);
                
                if (existingMessageView != null) {
                    // Make sure there's no existing reference yet.
                    MessageViewReference reference = MessageViewUtil.getMessageViewReferenceFor(owner, fromOperation);
                    
                    if (reference == null) { 
                        reference = RamFactory.eINSTANCE.createMessageViewReference();
                        reference.setReferences(existingMessageView);
                        addActualMessageView = AddCommand.create(editingDomain, owner, 
                                RamPackage.Literals.ASPECT__MESSAGE_VIEWS, reference);
                    }
                    
                    adviceMessageViewCommand = AddCommand.create(editingDomain, reference,
                            RamPackage.Literals.ABSTRACT_MESSAGE_VIEW__AFFECTED_BY, messageView);
                }
            }
        }
        
        if (existingMessageView == null) {
            throw new IllegalArgumentException("No message view could be found to advice.\n"
                    + "If the operation is defined in another model, the message view needs to be defined there.");
        }
        
        if (adviceMessageViewCommand == null) {
            adviceMessageViewCommand = AddCommand.create(editingDomain, existingMessageView,
                    RamPackage.Literals.ABSTRACT_MESSAGE_VIEW__AFFECTED_BY, messageView);            
        }

        CompoundCommand compoundCommand = new CompoundCommand();
        if (addActualMessageView != null) {
            compoundCommand.append(addActualMessageView);
        }
        compoundCommand.append(addMessageViewCommand);
        compoundCommand.append(adviceMessageViewCommand);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Removes the given aspect message view and any links from other message views to it.
     * 
     * @param messageView the message view to remove
     */
    public void removeAspectMessageView(AspectMessageView messageView) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(messageView);
        CompoundCommand command = new CompoundCommand();
        
        Command removeCommand = createRemoveMessageViewCommand(messageView);
        
        // Remove the aspect message view from all affected message views.
        Collection<EObject> affectedMessageViews =
                EMFModelUtil.findCrossReferences(messageView, RamPackage.Literals.ABSTRACT_MESSAGE_VIEW__AFFECTED_BY);
        
        for (EObject affected : affectedMessageViews) {
            command.append(RemoveCommand.create(editingDomain, affected,
                    RamPackage.Literals.ABSTRACT_MESSAGE_VIEW__AFFECTED_BY, messageView));
        }
        
        command.append(removeCommand);
        
        doExecute(editingDomain, command);
    }

    /**
     * Creates a command to remove the given message view and its layout.
     *
     * @param messageView the message view to remove
     * @return the command that, when executed, removes the message view and its layout
     */
    protected static Command createRemoveMessageViewCommand(AbstractMessageView messageView) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(messageView, RamPackage.Literals.ASPECT);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(aspect);

        CompoundCommand compoundCommand = new CompoundCommand();

        // If there is any aspect message view, and it only advices this message view it needs to be removed.
        for (AspectMessageView affectedBy : messageView.getAffectedBy()) {
            compoundCommand.append(RemoveCommand.create(editingDomain, messageView,
                    RamPackage.Literals.ABSTRACT_MESSAGE_VIEW__AFFECTED_BY, affectedBy));
            compoundCommand.append(createRemoveMessageViewCommand(affectedBy));
        }

        compoundCommand.append(RemoveCommand.create(editingDomain, messageView));

        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(aspect.getLayout().getContainers(), messageView);
        if (layout != null) {
            compoundCommand.append(RemoveCommand.create(editingDomain, layout));
        }

        return compoundCommand;
    }

}
