package ca.mcgill.sel.ram;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.ram.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.util.Model;
import ca.mcgill.sel.ram.util.ModelResource;
import ca.mcgill.sel.ram.util.StructuralViewUtil;

//CHECKSTYLE:OFF Temporary location in controller project (therefore this package is not added to the exclusions).
public class StructuralViewUtilTest {
    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/undefined.ram") {

        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }
        
    };
    
    private Aspect aspect;
    
    // Attribute input: int attr
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_GoodInput_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        StructuralView structuralView = aspect.getStructuralView();
        
        Attribute attribute = StructuralViewUtil.createAttribute(owner, "int attr");
        Type intType = StructuralViewUtil.getTypeByName("int", structuralView);

        assertThat(attribute).hasFieldOrPropertyWithValue("name", "attr");
        assertThat(attribute).hasFieldOrPropertyWithValue("type", intType);
    }
    
    // Attribute input: int[] foo, int[] foo
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_GoodInput_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        Attribute attribute1 = StructuralViewUtil.createAttribute(owner, "int[] foo");
        Attribute attribute2 = StructuralViewUtil.createAttribute(owner, "int[] bar");

        assertThat(attribute1).hasFieldOrPropertyWithValue("name", "foo");
        assertThat(attribute2).hasFieldOrPropertyWithValue("name", "bar");
        assertThat(attribute1).hasFieldOrPropertyWithValue("type", attribute2.getType());
    }
    
    // Attribute input: intt attr
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_BadInput_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createAttribute(owner, "intt attr");
        }).withNoCause()
          .withMessageContaining("The structural view does not contain a primitive type null");
    }
    // Attribute input: int amount (duplicate)
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_BadInput_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createAttribute(owner, "int amount");
        }).withNoCause()
          .withMessageContaining("The amount attribute is not unique for attributes");
    }

    // Attribute input: ifewfew
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_BadInput_3() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createAttribute(owner, "ifewfew");
        }).withNoCause()
          .withMessageContaining("The string ifewfew is not valid syntax for attribute");
    }
    
    // Attribute input: + int foo
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_BadInput_4() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createAttribute(owner, "+ int foo");
        }).withNoCause()
          .withMessageContaining("The string + int foo is not valid syntax for attribute");
    }
    
    // Attribute input: ""
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_BadInput_5() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createAttribute(owner, "");
        }).withNoCause()
          .withMessageContaining("The string  is not valid syntax for attribute");
    }
    
    // Operation input: + void foo()
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_GoodInput_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        Operation operation = StructuralViewUtil.createOperation(owner, "+ void foo()");
        Type voidType = StructuralViewUtil.getTypeByName("void", aspect.getStructuralView());

        assertThat(operation).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PUBLIC);
        assertThat(operation).hasFieldOrPropertyWithValue("returnType", voidType);
        assertThat(operation).hasFieldOrPropertyWithValue("name", "foo");
    }
    
    // Operation input: - int foo(float param) 
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_GoodInput_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        Operation operation = StructuralViewUtil.createOperation(owner, "- int foo(int param)");
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());
        
        assertThat(operation).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PRIVATE);
        assertThat(operation).hasFieldOrPropertyWithValue("returnType", intType);
        assertThat(operation.getParameters()).hasSize(1);
        
        Parameter param = operation.getParameters().get(0);
        assertThat(param).hasFieldOrPropertyWithValue("type", intType);
        assertThat(param).hasFieldOrPropertyWithValue("name", "param");
    }
    
    // Operation input: # int foo(int param, String bar) 
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_GoodInput_3() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        Operation operation = StructuralViewUtil.createOperation(owner, "# int foo(int param, String bar)");
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());
        Type stringType = StructuralViewUtil.getTypeByName("String", aspect.getStructuralView());

        assertThat(operation).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PROTECTED);
        assertThat(operation).hasFieldOrPropertyWithValue("returnType", intType);
        assertThat(operation.getParameters()).hasSize(2);
        
        Parameter param = operation.getParameters().get(0);
        assertThat(param).hasFieldOrPropertyWithValue("type", intType);
        assertThat(param).hasFieldOrPropertyWithValue("name", "param");
        
        Parameter param2 = operation.getParameters().get(1);
        assertThat(param2).hasFieldOrPropertyWithValue("type", stringType);
        assertThat(param2).hasFieldOrPropertyWithValue("name", "bar");
    }
    
    // Operation input: - void sell(int param) (overloading) 
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_GoodInput_4() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        Operation operation = StructuralViewUtil.createOperation(owner, "- void sell(int param)");
        Type voidType = StructuralViewUtil.getTypeByName("void", aspect.getStructuralView());
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());

        assertThat(operation).hasFieldOrPropertyWithValue("name", "sell");
        assertThat(operation).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PRIVATE);
        assertThat(operation).hasFieldOrPropertyWithValue("returnType", voidType);
        assertThat(operation.getParameters()).hasSize(1);
        
        Parameter param = operation.getParameters().get(0);
        assertThat(param).hasFieldOrPropertyWithValue("type", intType);
        assertThat(param).hasFieldOrPropertyWithValue("name", "param");
    }
    
    // Operation input: + B foo()
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_GoodInput_5() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        Class b = (Class) MessageViewTestUtil.getClassifierByName(aspect, "B");
        
        Operation operation = StructuralViewUtil.createOperation(owner, "+ B foo()");
        
        assertThat(operation).hasFieldOrPropertyWithValue("returnType", b);
    }
    
    // Operation input: int foo 
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createOperation(owner, "int foo");
        }).withNoCause()
          .withMessageContaining("The string int foo does not conform to operation syntax"); 
    }
    
    // Operation input: - void sell() (duplicate)
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        Operation operation = StructuralViewUtil.createOperation(owner, "- void sell()");
        assertThat(operation).isNull(); 
    }
    
    // Operation input: int foo() 
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_3() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createOperation(owner, "int foo()");
        }).withNoCause()
          .withMessageContaining("The string int foo() does not conform to operation syntax");
    }
    
    // Operation input: + int foo(param) 
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_4() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createOperation(owner, "+ int foo(param)");
        }).withNoCause()
          .withMessageContaining("The type param does not exist in the model");
    }
    
    // Operation input: + int foo(int param int param2)
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_5() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createOperation(owner, "+ int foo(int param int param2)");
        }).withNoCause()
          .withMessageContaining("The type int param int param2 does not exist in the model"); 
    }
    
    // Operation input: = int foo()
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_6() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createOperation(owner, "= int foo()");
        }).withNoCause()
          .withMessageContaining("The string = int foo() does not conform to operation syntax"); 
    }
    
    // Operation input: fewff
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_7() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createOperation(owner, "fewff");
        }).withNoCause()
          .withMessageContaining("The string fewff does not conform to operation syntax"); 
    }
    
    // Operation input: + int foo
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_8() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createOperation(owner, "+ int foo");
        }).withNoCause()
          .withMessageContaining("The string + int foo does not conform to operation syntax"); 
    }
    
    // Operation input: ''
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_9() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createOperation(owner, "");
        }).withNoCause()
          .withMessageContaining("The string  does not conform to operation syntax");
    }
    
    // Parameter input: int param
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_GoodInput_1() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");       
        Operation owner = clazz.getOperations().get(0);
        
        Parameter param = StructuralViewUtil.createParameter(owner, "int param");  
        
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());
        assertThat(param).hasFieldOrPropertyWithValue("type", intType);
        assertThat(param).hasFieldOrPropertyWithValue("name", "param");
    }
    
    // Parameter input: intparam
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_BadInput_1() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");       
        Operation owner = clazz.getOperations().get(0);
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createParameter(owner, "intparam");
        }).withNoCause()
          .withMessageContaining("String intparam does not conform to parameter syntax");
    }
    
    // Parameter input: ''
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_BadInput_2() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");       
        Operation owner = clazz.getOperations().get(0);
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createParameter(owner, "");
        }).withNoCause()
          .withMessageContaining("String  does not conform to parameter syntax");
    }
    
    // Parameter input: param
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_BadInput_3() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");       
        Operation owner = clazz.getOperations().get(0);
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createParameter(owner, "param");
        }).withNoCause()
          .withMessageContaining("String param does not conform to parameter syntax");
    }
    
    // Parameter input: int short (duplicate)
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_BadInput_4() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Main");       
        Operation owner = clazz.getOperations().get(2);
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createParameter(owner, "int short");
        }).withNoCause()
          .withMessageContaining("Parameter names duplicate");
    }
    
    // Parameter input: type short
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_BadInput_5() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Main");       
        Operation owner = clazz.getOperations().get(2);
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createParameter(owner, "type short");
        }).withNoCause()
          .withMessageContaining("No type with that name exists");
    }

    // Parameter input: int p-o
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_BadInput_6() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Main");       
        Operation owner = clazz.getOperations().get(2);
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            StructuralViewUtil.createParameter(owner, "int p-o");
        }).withNoCause()
          .withMessageContaining("String int p-o does not conform to parameter syntax");
    }
}
