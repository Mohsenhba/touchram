package ca.mcgill.sel.ram.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.Model;
import ca.mcgill.sel.ram.util.ModelResource;
import ca.mcgill.sel.ram.util.StructuralViewUtil;

public class ClassControllerTest {
    
    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/undefined.ram") {

        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }
        
    };
    
    private static ClassController controller = ControllerFactory.INSTANCE.getClassController();
    
    private Aspect aspect;
    
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/MappedOperations.ram")
    public void testRemoveOperation_OperationWithAffectedBy_AMVRemoved() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "B");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "isPartial");
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNotNull();
        
        AspectMessageView existingAMV = MessageViewTestUtil.getAspectMessageView(aspect, operation);
        
        controller.removeOperation(operation);
        
        assertThat(messageView.eContainer()).isNull();
        assertThat(aspect.getLayout().getContainers().keySet()).doesNotContain(messageView);
        assertThat(existingAMV.eContainer()).isNull();
    }
    
    /**
     *  Checks if an abstract concern class 'A' does not change visibility when adding a public operation
     */
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void testConcernAbstractWithPublicOperation() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "A");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "sell");
        
        // Class is Concern Abstract has a private operation
        assertThat(classifier).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);
        assertThat(classifier).hasFieldOrPropertyWithValue("abstract", true);
        assertThat(operation).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);
        // Lets switch the operation to public
        controller.changeOperationAndClassVisibility(classifier, operation, RAMVisibilityType.PUBLIC);
        
        // Class is still Concern Abstract has a public operation
        assertThat(classifier).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);
        assertThat(classifier).hasFieldOrPropertyWithValue("abstract", true);
        assertThat(operation).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.PUBLIC);
    }
    
    /**
     *  Checks if concern class 'B' does change visibility when adding a public operation (old behavior)
     */
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void testConcernClassWithPublicOperation() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "B");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "buy");
        
        // Class is Concern Abstract has a private operation
        assertThat(classifier).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);
        assertThat(classifier).hasFieldOrPropertyWithValue("abstract", false);
        assertThat(operation).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);
        
        // Lets switch the operation to public
        controller.changeOperationAndClassVisibility(classifier, operation, RAMVisibilityType.PUBLIC);
        
        // Class is still Concern Abstract has a public operation
        assertThat(classifier).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.PUBLIC);
        assertThat(operation).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.PUBLIC);
    }
    
    // Remove attribute
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void removeAttribute() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        controller.removeAttribute(owner.getAttributes().get(0));
        
        assertThat(owner.getAttributes()).isEmpty();
    }
    
    // Create attribute input: int attr - rank 1
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_GoodInput_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        controller.createAttribute(owner, 1, "int attr");
        assertThat(owner.getAttributes()).hasSize(2);
        // verify rank
        assertThat(owner.getAttributes().get(1)).hasFieldOrPropertyWithValue("name", "attr");
    }
    
    // Create attribute input: int attr - rank 0
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_GoodInput_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        controller.createAttribute(owner, 0, "int attr");
        assertThat(owner.getAttributes()).hasSize(2);
        // verify rank
        assertThat(owner.getAttributes().get(0)).hasFieldOrPropertyWithValue("name", "attr");
    }
    
    // Create attribute input: attr - rank 0
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createAttribute_BadInput_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            controller.createAttribute(owner, 0, "attr");
        }).withNoCause()
          .withMessageContaining("The string attr is not valid syntax for attributes");
    }
    
    // Create operation input: + void toto() - rank 0
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_GoodInput_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        controller.createOperation(owner, 0, "+ void toto()");
        assertThat(owner.getOperations()).hasSize(2);
        // verify rank
        assertThat(owner.getOperations().get(0)).hasFieldOrPropertyWithValue("name", "toto");
    }
    
    // Create operation input: + void toto(int param, float bar) - rank 1
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_GoodInput_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        controller.createOperation(owner, 0, "+ void toto(int param, float bar)");
        assertThat(owner.getOperations()).hasSize(2);
        Operation operation = owner.getOperations().get(0);
        
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());
        Parameter param = operation.getParameters().get(0);
        assertThat(param).hasFieldOrPropertyWithValue("type", intType);
        assertThat(param).hasFieldOrPropertyWithValue("name", "param");
        
        Type floatType = StructuralViewUtil.getTypeByName("float", aspect.getStructuralView());
        Parameter param2 = operation.getParameters().get(1);
        assertThat(param2).hasFieldOrPropertyWithValue("type", floatType);
        assertThat(param2).hasFieldOrPropertyWithValue("name", "bar");
    }
    
    // Create operation input: int toto() - rank 0
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
                    
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            controller.createOperation(owner, 0, "int toto()");
        }).withNoCause()
          .withMessageContaining("The string int toto() does not conform to operation syntax");
    }
    
    // Create operation input: - void sell() (duplicate)
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createOperation_BadInput_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            controller.createOperation(owner, 0, "- void sell()");
        }).withNoCause()
          .withMessageContaining("An operation with the same signature already exists.");
    }
    
    // Create getter operation
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createGetterOperation() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        Attribute attribute = owner.getAttributes().get(0);
        controller.createGetterOperation(attribute);
        
        Operation getter = owner.getOperations().get(1);
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());
        
        assertThat(getter).hasFieldOrPropertyWithValue("name", "getAmount");
        assertThat(getter.getParameters()).hasSize(0);
        assertThat(getter).hasFieldOrPropertyWithValue("returnType", intType);
        assertThat(getter).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PUBLIC);
    }
    
    // Create setter operation
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createSetterOperation() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        Attribute attribute = owner.getAttributes().get(0);
        controller.createSetterOperation(attribute);
        
        Operation setter = owner.getOperations().get(1);
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());
        Type voidType = StructuralViewUtil.getTypeByName("void", aspect.getStructuralView());

        assertThat(setter).hasFieldOrPropertyWithValue("name", "setAmount");
        assertThat(setter.getParameters()).hasSize(1);
        assertThat(setter).hasFieldOrPropertyWithValue("returnType", voidType);
        assertThat(setter).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PUBLIC);
        
        Parameter param = setter.getParameters().get(0);
        assertThat(param).hasFieldOrPropertyWithValue("type", intType);
        assertThat(param).hasFieldOrPropertyWithValue("name", "amount");
    }
    
    // Create getter and setter operation
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createGetterSetterOperations() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        Attribute attribute = owner.getAttributes().get(0);
        controller.createGetterAndSetterOperation(attribute);
        
        Operation getter = owner.getOperations().get(1);
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());
        
        assertThat(getter).hasFieldOrPropertyWithValue("name", "getAmount");
        assertThat(getter.getParameters()).hasSize(0);
        assertThat(getter).hasFieldOrPropertyWithValue("returnType", intType);
        assertThat(getter).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PUBLIC);
        
        Operation setter = owner.getOperations().get(2);
        Type voidType = StructuralViewUtil.getTypeByName("void", aspect.getStructuralView());

        assertThat(setter).hasFieldOrPropertyWithValue("name", "setAmount");
        assertThat(setter.getParameters()).hasSize(1);
        assertThat(setter).hasFieldOrPropertyWithValue("returnType", voidType);
        assertThat(setter).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PUBLIC);
        
        Parameter param = setter.getParameters().get(0);
        assertThat(param).hasFieldOrPropertyWithValue("type", intType);
        assertThat(param).hasFieldOrPropertyWithValue("name", "amount");
    }
    
    // Create constructor operation
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createConstructorOperation_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        controller.createConstructor(owner);
        Operation constructor = owner.getOperations().get(0);

        assertThat(constructor).hasFieldOrPropertyWithValue("name", "create");
        assertThat(constructor).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PUBLIC);
    }
    
    // Create constructor operation
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createConstructorOperation_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "B");
        assertThat(owner).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);
        controller.createConstructor(owner);
        assertThat(owner).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.PUBLIC);
    }
    
    // Create destructor operation
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createDestructorOperation_1() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        controller.createDestructor(owner);
        Operation destructor = owner.getOperations().get(0);

        assertThat(destructor).hasFieldOrPropertyWithValue("name", "destroy");
        assertThat(destructor).hasFieldOrPropertyWithValue("extendedVisibility", RAMVisibilityType.PUBLIC);
    }
    
    // Create constructor operation
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createDestructorOperation_2() {
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "B");
        assertThat(owner).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);
        controller.createDestructor(owner);
        assertThat(owner).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.PUBLIC);
    }
    
    // Create parameter input: int param
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_GoodInput_1() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        Operation owner = clazz.getOperations().get(0);
        
        assertThat(owner.getParameters()).isEmpty();
        controller.createParameter(owner, 0, "int param");
        assertThat(owner.getParameters()).hasSize(1);
        
        Parameter param = owner.getParameters().get(0);
        Type intType = StructuralViewUtil.getTypeByName("int", aspect.getStructuralView());
        
        assertThat(param).hasFieldOrPropertyWithValue("name", "param");
        assertThat(param).hasFieldOrPropertyWithValue("type", intType);
    }
    
    // Create parameter input: param
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void createParameter_BadInput_1() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        Operation owner = clazz.getOperations().get(0);
        assertThat(owner.getParameters()).isEmpty();
        
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            controller.createParameter(owner, 0, "param");
        }).withNoCause()
          .withMessageContaining("String param does not conform to parameter syntax");
    }
    
    // Create parameter input: param
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void removeParameter() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Main");
        Operation owner = clazz.getOperations().get(2);
        Parameter parameter = owner.getParameters().get(0);
        controller.removeParameter(parameter);
        assertThat(owner.getParameters()).isEmpty();
    }
    
    // Add a super Type
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void addSuperType() {
        Class a = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");
        Class b = (Class) MessageViewTestUtil.getClassifierByName(aspect, "B");

        assertThat(a.getSuperTypes()).hasSize(0);
        controller.addSuperType(a, b);
        assertThat(a.getSuperTypes()).hasSize(1);
        assertThat(a.getSuperTypes()).contains(b);
    }
    
    // Removes a super Type
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void removeSuperType() {
        Class main = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Main");
        Class a = (Class) MessageViewTestUtil.getClassifierByName(aspect, "A");

        assertThat(main.getSuperTypes()).hasSize(1);
        assertThat(main.getSuperTypes().contains(a));
        
        controller.removeSuperType(a, main);
        assertThat(main.getSuperTypes()).hasSize(0);
    }
    
    // Change a Class Visibility: CONCERN to PUBLIC
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void changeClassVisibility_GoodInput_1() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "B");

        assertThat(clazz).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);

        controller.changeClassVisibility(clazz, COREVisibilityType.PUBLIC);
        
        assertThat(clazz).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.PUBLIC);
    }
    
    // Change a Class Visibility: PUBLIC to CONCERN
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void changeClassVisibility_GoodInput_2() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Hello");

        assertThat(clazz).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.PUBLIC);

        controller.changeClassVisibility(clazz, COREVisibilityType.CONCERN);
        
        assertThat(clazz).hasFieldOrPropertyWithValue("visibility", COREVisibilityType.CONCERN);
    }
    
    // Change a Class Visibility. Class can not be private (has public operation)
    @Test
    @Model(fileName = "tests/models/AbstractVisibility/AbstractVisibility.ram")
    public void changeClassVisibility_BadInput_1() {
        Class clazz = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Main");

        assertThat(clazz.getVisibility()).isEqualTo(COREVisibilityType.PUBLIC);

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            controller.changeClassVisibility(clazz, COREVisibilityType.CONCERN);
        }).withNoCause()
          .withMessageContaining("The class has one or more public operations.");
    }
}
