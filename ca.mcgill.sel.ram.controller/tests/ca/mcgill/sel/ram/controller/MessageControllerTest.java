package ca.mcgill.sel.ram.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.TemporaryProperty;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.util.ModelResource;

public class MessageControllerTest {

    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/MessageViews/Messages.ram") {

        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }

    };

    private static MessageController controller = ControllerFactory.INSTANCE.getMessageController();

    private Aspect aspect;
    private MessageView messageView;
    private Interaction interaction;

    private void loadMessageView(String className, String operationName) {
        messageView = MessageViewTestUtil.loadMessageView(aspect, className, operationName);
        interaction = messageView.getSpecification();
    }

    @Test
    public void testChangeAssignTo_TemporaryPropertyUsedElseWhere_TemporaryPropertyNotRemoved() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(2);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        MessageOccurrenceSpecification otherEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(4);
        StructuralFeature newAssignTo = otherEvent.getMessage().getAssignTo();
        
        // Change assignTo where the old assign to is still assigned to somewhere else.
        controller.changeAssignTo(message, newAssignTo);
        
        assertThat(message.getAssignTo()).isEqualTo(newAssignTo);
        assertThat(assignTo.eContainer()).isNotNull();
    }
    
    @Test
    public void testChangeAssignTo_TemporaryPropertyUnused_TemporaryPropertyRemoved() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(4);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        MessageOccurrenceSpecification otherEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(2);
        StructuralFeature newAssignTo = otherEvent.getMessage().getAssignTo();
        
        // Change assignTo where the old assign to is still assigned to somewhere else.
        controller.changeAssignTo(message, newAssignTo);
        
        assertThat(message.getAssignTo()).isEqualTo(newAssignTo);
        assertThat(assignTo.eContainer()).isNull();
    }
    
    @Test
    public void testCreateTemporaryAssignment_TemporaryPropertyUsedElseWhere_TemporaryPropertyNotRemoved() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(2);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        // Change assignTo where the old assign to is still assigned to somewhere else.
        controller.createTemporaryAssignment(message);
        
        assertThat(message.getAssignTo()).isNotEqualTo(assignTo);
        assertThat(assignTo.eContainer()).isNotNull();
    }
    
    @Test
    public void testCreateTemporaryAssignment_TemporaryPropertyUnused_TemporaryPropertyRemoved() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(4);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        // Change assignTo where the old assign to is still assigned to somewhere else.
        controller.createTemporaryAssignment(message);
        
        assertThat(message.getAssignTo()).isNotEqualTo(assignTo);
        assertThat(assignTo.eContainer()).isNull();
    }
    
    @Test
    public void testChangeAssignTo_RemoveAssignment_AssignmentUnset() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(4);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        // Remove assignTo where the property is not assigned to anywhere else.
        controller.changeAssignTo(message, null);
        
        assertThat(message.getAssignTo()).isNull();
        assertThat(assignTo.eContainer()).isNull();
    }

    @Test
    public void testChangeReturnValue_ReplyMessageWithReturnValue_ReturnValueChanged() {
        loadMessageView("Callee", "myMethod");

        Message replyMessage = interaction.getMessages().get(1);
        assertThat(replyMessage.getMessageSort()).isEqualTo(MessageSort.REPLY);
        
        ValueSpecification returns = replyMessage.getReturns();
        assertThat(returns).isNotNull();

        controller.changeReturnValue(replyMessage, null);

        assertThat(replyMessage.getReturns()).isNull();
        assertThat(returns.eContainer()).isNull();
    }

}
