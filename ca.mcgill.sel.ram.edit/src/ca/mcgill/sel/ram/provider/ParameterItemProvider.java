/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package ca.mcgill.sel.ram.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RCollection;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.controller.util.RAMReferenceUtil;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.Parameter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ParameterItemProvider
    extends TypedElementItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public ParameterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPartialityPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

    /**
	 * This adds a property descriptor for the Partiality feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addPartialityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MappableElement_partiality_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MappableElement_partiality_feature", "_UI_MappableElement_type"),
				 RamPackage.Literals.MAPPABLE_ELEMENT__PARTIALITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addTypePropertyDescriptorGen(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Parameter_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Parameter_type_feature", "_UI_Parameter_type"),
				 RamPackage.Literals.PARAMETER__TYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}
    
    /**
     * This adds a property descriptor for the Type feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addTypePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            new ItemPropertyDescriptor(
                ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_Parameter_type_feature"),
                 // CHECKSTYLE:IGNORE MultipleStringLiterals: Outcome of code generator.
                 getString("_UI_PropertyDescriptor_description", "_UI_Parameter_type_feature", "_UI_Parameter_type"),
                 RamPackage.Literals.PARAMETER__TYPE,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null) {

                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    Parameter parameter = (Parameter) object;
                    
                    Collection<?> result = RAMInterfaceUtil.getAvailableParameterTypes(parameter);  
                    
                    return result;
                }
                
                @Override
                public IItemLabelProvider getLabelProvider(Object object) {
                    // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                    return new IItemLabelProvider() {
                        
                        @Override
                        public String getText(Object object) {
                            if (object != null) {
                                Type type = (Type) object;
                                // The target is one of the parameters in the aspect.
                                // It is not necessarily the parameter we are currently looking at,
                                // but regardless it allows us to retrieve the current aspect.
                                EObject target = (EObject) getTarget();
                                Aspect currentAspect = EMFModelUtil.getRootContainerOfType(target, 
                                        RamPackage.Literals.ASPECT);
                                Aspect aspect = EMFModelUtil.getRootContainerOfType(type, RamPackage.Literals.ASPECT);
                                
                                // Only do this for types not from the current aspect.
                                if (aspect != currentAspect
                                        /**
                                         * It is possible that on removal of an parameter, 
                                         * the target is not contained anymore,
                                         * i.e., the currentAspect (container) is null, 
                                         * which would lead to the prepending of the external aspect, 
                                         * even though this cannot be guaranteed. Hence, it needs to be prevented.
                                         */
                                        && currentAspect != null
                                        && !(object instanceof RCollection) && object instanceof Classifier) {
                                    
                                    StringBuffer result = new StringBuffer();
                                    
                                    result.append(EMFEditUtil.getTypeName(type));
                                    result.append(" ");

                                    if (!(type instanceof ImplementationClass)) {
                                        result.append(EMFEditUtil.getTextFor(getAdapterFactory(), aspect));
                                        result.append(".");
                                    }
                                    result.append(EMFEditUtil.getTextFor(getAdapterFactory(), type));
                                    
                                    return result.toString();
                                }
                            }
                            
                            return itemDelegator.getText(object);
                        }
                        
                        @Override
                        public Object getImage(Object object) {
                            return itemDelegator.getImage(object);
                        }
                    };
                }
                
                @Override
                public void setPropertyValue(Object object, Object value) {
                    RAMReferenceUtil.setLocalizedPropertyValue(getEditingDomain(object), object, feature, value);
                }
                
            });
    }

    /**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RamPackage.Literals.ANNOTATABLE__ANNOTATION);
		}
		return childrenFeatures;
	}

				/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

				/**
	 * This returns Parameter.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Parameter"));
	}

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public String getText(Object object) {
        Parameter parameter = (Parameter) object;
        String label = parameter.getName();
        if (parameter.getPartiality() == RAMPartialityType.PUBLIC) {
            label = "|" + label;
        } else if (parameter.getPartiality() == RAMPartialityType.PROTECTED) {
            label = "\u00A6" + label;
        }
        return label == null || label.length() == 0 
                ? getString("_UI_Parameter_type") 
                : getString("_UI_Parameter_type") + " " + label;
    }

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Parameter.class)) {
			case RamPackage.PARAMETER__PARTIALITY:
			case RamPackage.PARAMETER__TYPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case RamPackage.PARAMETER__ANNOTATION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.ANNOTATABLE__ANNOTATION,
				 RamFactory.eINSTANCE.createAnnotation()));
	}

}
