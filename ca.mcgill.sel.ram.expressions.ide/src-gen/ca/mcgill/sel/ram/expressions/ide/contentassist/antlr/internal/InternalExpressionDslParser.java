package ca.mcgill.sel.ram.expressions.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import ca.mcgill.sel.ram.expressions.services.ExpressionDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalExpressionDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_NUMBER", "RULE_E_CHAR", "RULE_E_NULL", "RULE_E_STRING", "RULE_E_BOOLEAN", "RULE_E_FLOAT", "RULE_E_LONG", "RULE_E_DOUBLE", "RULE_E_BYTE", "RULE_WS", "'=='", "'!='", "'>='", "'<='", "'>'", "'<'", "'>>'", "'<<'", "'>>>'", "'*'", "'/'", "'%'", "'++'", "'--'", "'_'", "'?'", "':'", "'||'", "'&&'", "'+'", "'-'", "'!'", "'('", "')'", "'\\''", "'.'", "'|'", "'^'", "'&'"
    };
    public static final int RULE_E_BYTE=12;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int RULE_E_NULL=6;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int RULE_E_DOUBLE=11;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int RULE_E_CHAR=5;
    public static final int RULE_E_BOOLEAN=8;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=13;
    public static final int RULE_E_LONG=10;
    public static final int RULE_NUMBER=4;
    public static final int RULE_E_FLOAT=9;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int RULE_E_STRING=7;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalExpressionDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalExpressionDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalExpressionDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalExpressionDsl.g"; }


    	private ExpressionDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(ExpressionDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleValueSpecification"
    // InternalExpressionDsl.g:53:1: entryRuleValueSpecification : ruleValueSpecification EOF ;
    public final void entryRuleValueSpecification() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:54:1: ( ruleValueSpecification EOF )
            // InternalExpressionDsl.g:55:1: ruleValueSpecification EOF
            {
             before(grammarAccess.getValueSpecificationRule()); 
            pushFollow(FOLLOW_1);
            ruleValueSpecification();

            state._fsp--;

             after(grammarAccess.getValueSpecificationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValueSpecification"


    // $ANTLR start "ruleValueSpecification"
    // InternalExpressionDsl.g:62:1: ruleValueSpecification : ( ruleConditional ) ;
    public final void ruleValueSpecification() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:66:2: ( ( ruleConditional ) )
            // InternalExpressionDsl.g:67:2: ( ruleConditional )
            {
            // InternalExpressionDsl.g:67:2: ( ruleConditional )
            // InternalExpressionDsl.g:68:3: ruleConditional
            {
             before(grammarAccess.getValueSpecificationAccess().getConditionalParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleConditional();

            state._fsp--;

             after(grammarAccess.getValueSpecificationAccess().getConditionalParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValueSpecification"


    // $ANTLR start "entryRuleConditional"
    // InternalExpressionDsl.g:78:1: entryRuleConditional : ruleConditional EOF ;
    public final void entryRuleConditional() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:79:1: ( ruleConditional EOF )
            // InternalExpressionDsl.g:80:1: ruleConditional EOF
            {
             before(grammarAccess.getConditionalRule()); 
            pushFollow(FOLLOW_1);
            ruleConditional();

            state._fsp--;

             after(grammarAccess.getConditionalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConditional"


    // $ANTLR start "ruleConditional"
    // InternalExpressionDsl.g:87:1: ruleConditional : ( ( rule__Conditional__Group__0 ) ) ;
    public final void ruleConditional() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:91:2: ( ( ( rule__Conditional__Group__0 ) ) )
            // InternalExpressionDsl.g:92:2: ( ( rule__Conditional__Group__0 ) )
            {
            // InternalExpressionDsl.g:92:2: ( ( rule__Conditional__Group__0 ) )
            // InternalExpressionDsl.g:93:3: ( rule__Conditional__Group__0 )
            {
             before(grammarAccess.getConditionalAccess().getGroup()); 
            // InternalExpressionDsl.g:94:3: ( rule__Conditional__Group__0 )
            // InternalExpressionDsl.g:94:4: rule__Conditional__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Conditional__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConditionalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditional"


    // $ANTLR start "entryRuleOr"
    // InternalExpressionDsl.g:103:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:104:1: ( ruleOr EOF )
            // InternalExpressionDsl.g:105:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalExpressionDsl.g:112:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:116:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalExpressionDsl.g:117:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalExpressionDsl.g:117:2: ( ( rule__Or__Group__0 ) )
            // InternalExpressionDsl.g:118:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalExpressionDsl.g:119:3: ( rule__Or__Group__0 )
            // InternalExpressionDsl.g:119:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalExpressionDsl.g:128:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:129:1: ( ruleAnd EOF )
            // InternalExpressionDsl.g:130:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalExpressionDsl.g:137:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:141:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalExpressionDsl.g:142:2: ( ( rule__And__Group__0 ) )
            {
            // InternalExpressionDsl.g:142:2: ( ( rule__And__Group__0 ) )
            // InternalExpressionDsl.g:143:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalExpressionDsl.g:144:3: ( rule__And__Group__0 )
            // InternalExpressionDsl.g:144:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleBitwiseOR"
    // InternalExpressionDsl.g:153:1: entryRuleBitwiseOR : ruleBitwiseOR EOF ;
    public final void entryRuleBitwiseOR() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:154:1: ( ruleBitwiseOR EOF )
            // InternalExpressionDsl.g:155:1: ruleBitwiseOR EOF
            {
             before(grammarAccess.getBitwiseORRule()); 
            pushFollow(FOLLOW_1);
            ruleBitwiseOR();

            state._fsp--;

             after(grammarAccess.getBitwiseORRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBitwiseOR"


    // $ANTLR start "ruleBitwiseOR"
    // InternalExpressionDsl.g:162:1: ruleBitwiseOR : ( ( rule__BitwiseOR__Group__0 ) ) ;
    public final void ruleBitwiseOR() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:166:2: ( ( ( rule__BitwiseOR__Group__0 ) ) )
            // InternalExpressionDsl.g:167:2: ( ( rule__BitwiseOR__Group__0 ) )
            {
            // InternalExpressionDsl.g:167:2: ( ( rule__BitwiseOR__Group__0 ) )
            // InternalExpressionDsl.g:168:3: ( rule__BitwiseOR__Group__0 )
            {
             before(grammarAccess.getBitwiseORAccess().getGroup()); 
            // InternalExpressionDsl.g:169:3: ( rule__BitwiseOR__Group__0 )
            // InternalExpressionDsl.g:169:4: rule__BitwiseOR__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseOR__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseORAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBitwiseOR"


    // $ANTLR start "entryRuleBitwiseXOR"
    // InternalExpressionDsl.g:178:1: entryRuleBitwiseXOR : ruleBitwiseXOR EOF ;
    public final void entryRuleBitwiseXOR() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:179:1: ( ruleBitwiseXOR EOF )
            // InternalExpressionDsl.g:180:1: ruleBitwiseXOR EOF
            {
             before(grammarAccess.getBitwiseXORRule()); 
            pushFollow(FOLLOW_1);
            ruleBitwiseXOR();

            state._fsp--;

             after(grammarAccess.getBitwiseXORRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBitwiseXOR"


    // $ANTLR start "ruleBitwiseXOR"
    // InternalExpressionDsl.g:187:1: ruleBitwiseXOR : ( ( rule__BitwiseXOR__Group__0 ) ) ;
    public final void ruleBitwiseXOR() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:191:2: ( ( ( rule__BitwiseXOR__Group__0 ) ) )
            // InternalExpressionDsl.g:192:2: ( ( rule__BitwiseXOR__Group__0 ) )
            {
            // InternalExpressionDsl.g:192:2: ( ( rule__BitwiseXOR__Group__0 ) )
            // InternalExpressionDsl.g:193:3: ( rule__BitwiseXOR__Group__0 )
            {
             before(grammarAccess.getBitwiseXORAccess().getGroup()); 
            // InternalExpressionDsl.g:194:3: ( rule__BitwiseXOR__Group__0 )
            // InternalExpressionDsl.g:194:4: rule__BitwiseXOR__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseXOR__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseXORAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBitwiseXOR"


    // $ANTLR start "entryRuleBitwiseAnd"
    // InternalExpressionDsl.g:203:1: entryRuleBitwiseAnd : ruleBitwiseAnd EOF ;
    public final void entryRuleBitwiseAnd() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:204:1: ( ruleBitwiseAnd EOF )
            // InternalExpressionDsl.g:205:1: ruleBitwiseAnd EOF
            {
             before(grammarAccess.getBitwiseAndRule()); 
            pushFollow(FOLLOW_1);
            ruleBitwiseAnd();

            state._fsp--;

             after(grammarAccess.getBitwiseAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBitwiseAnd"


    // $ANTLR start "ruleBitwiseAnd"
    // InternalExpressionDsl.g:212:1: ruleBitwiseAnd : ( ( rule__BitwiseAnd__Group__0 ) ) ;
    public final void ruleBitwiseAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:216:2: ( ( ( rule__BitwiseAnd__Group__0 ) ) )
            // InternalExpressionDsl.g:217:2: ( ( rule__BitwiseAnd__Group__0 ) )
            {
            // InternalExpressionDsl.g:217:2: ( ( rule__BitwiseAnd__Group__0 ) )
            // InternalExpressionDsl.g:218:3: ( rule__BitwiseAnd__Group__0 )
            {
             before(grammarAccess.getBitwiseAndAccess().getGroup()); 
            // InternalExpressionDsl.g:219:3: ( rule__BitwiseAnd__Group__0 )
            // InternalExpressionDsl.g:219:4: rule__BitwiseAnd__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseAnd__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBitwiseAnd"


    // $ANTLR start "entryRuleEquality"
    // InternalExpressionDsl.g:228:1: entryRuleEquality : ruleEquality EOF ;
    public final void entryRuleEquality() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:229:1: ( ruleEquality EOF )
            // InternalExpressionDsl.g:230:1: ruleEquality EOF
            {
             before(grammarAccess.getEqualityRule()); 
            pushFollow(FOLLOW_1);
            ruleEquality();

            state._fsp--;

             after(grammarAccess.getEqualityRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEquality"


    // $ANTLR start "ruleEquality"
    // InternalExpressionDsl.g:237:1: ruleEquality : ( ( rule__Equality__Group__0 ) ) ;
    public final void ruleEquality() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:241:2: ( ( ( rule__Equality__Group__0 ) ) )
            // InternalExpressionDsl.g:242:2: ( ( rule__Equality__Group__0 ) )
            {
            // InternalExpressionDsl.g:242:2: ( ( rule__Equality__Group__0 ) )
            // InternalExpressionDsl.g:243:3: ( rule__Equality__Group__0 )
            {
             before(grammarAccess.getEqualityAccess().getGroup()); 
            // InternalExpressionDsl.g:244:3: ( rule__Equality__Group__0 )
            // InternalExpressionDsl.g:244:4: rule__Equality__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Equality__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEquality"


    // $ANTLR start "entryRuleComparison"
    // InternalExpressionDsl.g:253:1: entryRuleComparison : ruleComparison EOF ;
    public final void entryRuleComparison() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:254:1: ( ruleComparison EOF )
            // InternalExpressionDsl.g:255:1: ruleComparison EOF
            {
             before(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_1);
            ruleComparison();

            state._fsp--;

             after(grammarAccess.getComparisonRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalExpressionDsl.g:262:1: ruleComparison : ( ( rule__Comparison__Group__0 ) ) ;
    public final void ruleComparison() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:266:2: ( ( ( rule__Comparison__Group__0 ) ) )
            // InternalExpressionDsl.g:267:2: ( ( rule__Comparison__Group__0 ) )
            {
            // InternalExpressionDsl.g:267:2: ( ( rule__Comparison__Group__0 ) )
            // InternalExpressionDsl.g:268:3: ( rule__Comparison__Group__0 )
            {
             before(grammarAccess.getComparisonAccess().getGroup()); 
            // InternalExpressionDsl.g:269:3: ( rule__Comparison__Group__0 )
            // InternalExpressionDsl.g:269:4: rule__Comparison__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRuleBitwiseShift"
    // InternalExpressionDsl.g:278:1: entryRuleBitwiseShift : ruleBitwiseShift EOF ;
    public final void entryRuleBitwiseShift() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:279:1: ( ruleBitwiseShift EOF )
            // InternalExpressionDsl.g:280:1: ruleBitwiseShift EOF
            {
             before(grammarAccess.getBitwiseShiftRule()); 
            pushFollow(FOLLOW_1);
            ruleBitwiseShift();

            state._fsp--;

             after(grammarAccess.getBitwiseShiftRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBitwiseShift"


    // $ANTLR start "ruleBitwiseShift"
    // InternalExpressionDsl.g:287:1: ruleBitwiseShift : ( ( rule__BitwiseShift__Group__0 ) ) ;
    public final void ruleBitwiseShift() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:291:2: ( ( ( rule__BitwiseShift__Group__0 ) ) )
            // InternalExpressionDsl.g:292:2: ( ( rule__BitwiseShift__Group__0 ) )
            {
            // InternalExpressionDsl.g:292:2: ( ( rule__BitwiseShift__Group__0 ) )
            // InternalExpressionDsl.g:293:3: ( rule__BitwiseShift__Group__0 )
            {
             before(grammarAccess.getBitwiseShiftAccess().getGroup()); 
            // InternalExpressionDsl.g:294:3: ( rule__BitwiseShift__Group__0 )
            // InternalExpressionDsl.g:294:4: rule__BitwiseShift__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseShift__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseShiftAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBitwiseShift"


    // $ANTLR start "entryRulePlusOrMinus"
    // InternalExpressionDsl.g:303:1: entryRulePlusOrMinus : rulePlusOrMinus EOF ;
    public final void entryRulePlusOrMinus() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:304:1: ( rulePlusOrMinus EOF )
            // InternalExpressionDsl.g:305:1: rulePlusOrMinus EOF
            {
             before(grammarAccess.getPlusOrMinusRule()); 
            pushFollow(FOLLOW_1);
            rulePlusOrMinus();

            state._fsp--;

             after(grammarAccess.getPlusOrMinusRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePlusOrMinus"


    // $ANTLR start "rulePlusOrMinus"
    // InternalExpressionDsl.g:312:1: rulePlusOrMinus : ( ( rule__PlusOrMinus__Group__0 ) ) ;
    public final void rulePlusOrMinus() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:316:2: ( ( ( rule__PlusOrMinus__Group__0 ) ) )
            // InternalExpressionDsl.g:317:2: ( ( rule__PlusOrMinus__Group__0 ) )
            {
            // InternalExpressionDsl.g:317:2: ( ( rule__PlusOrMinus__Group__0 ) )
            // InternalExpressionDsl.g:318:3: ( rule__PlusOrMinus__Group__0 )
            {
             before(grammarAccess.getPlusOrMinusAccess().getGroup()); 
            // InternalExpressionDsl.g:319:3: ( rule__PlusOrMinus__Group__0 )
            // InternalExpressionDsl.g:319:4: rule__PlusOrMinus__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPlusOrMinusAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePlusOrMinus"


    // $ANTLR start "entryRuleMulOrDiv"
    // InternalExpressionDsl.g:328:1: entryRuleMulOrDiv : ruleMulOrDiv EOF ;
    public final void entryRuleMulOrDiv() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:329:1: ( ruleMulOrDiv EOF )
            // InternalExpressionDsl.g:330:1: ruleMulOrDiv EOF
            {
             before(grammarAccess.getMulOrDivRule()); 
            pushFollow(FOLLOW_1);
            ruleMulOrDiv();

            state._fsp--;

             after(grammarAccess.getMulOrDivRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMulOrDiv"


    // $ANTLR start "ruleMulOrDiv"
    // InternalExpressionDsl.g:337:1: ruleMulOrDiv : ( ( rule__MulOrDiv__Group__0 ) ) ;
    public final void ruleMulOrDiv() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:341:2: ( ( ( rule__MulOrDiv__Group__0 ) ) )
            // InternalExpressionDsl.g:342:2: ( ( rule__MulOrDiv__Group__0 ) )
            {
            // InternalExpressionDsl.g:342:2: ( ( rule__MulOrDiv__Group__0 ) )
            // InternalExpressionDsl.g:343:3: ( rule__MulOrDiv__Group__0 )
            {
             before(grammarAccess.getMulOrDivAccess().getGroup()); 
            // InternalExpressionDsl.g:344:3: ( rule__MulOrDiv__Group__0 )
            // InternalExpressionDsl.g:344:4: rule__MulOrDiv__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MulOrDiv__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMulOrDivAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMulOrDiv"


    // $ANTLR start "entryRulePrimary"
    // InternalExpressionDsl.g:353:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:354:1: ( rulePrimary EOF )
            // InternalExpressionDsl.g:355:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalExpressionDsl.g:362:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:366:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalExpressionDsl.g:367:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalExpressionDsl.g:367:2: ( ( rule__Primary__Alternatives ) )
            // InternalExpressionDsl.g:368:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalExpressionDsl.g:369:3: ( rule__Primary__Alternatives )
            // InternalExpressionDsl.g:369:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleAtomic"
    // InternalExpressionDsl.g:378:1: entryRuleAtomic : ruleAtomic EOF ;
    public final void entryRuleAtomic() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:379:1: ( ruleAtomic EOF )
            // InternalExpressionDsl.g:380:1: ruleAtomic EOF
            {
             before(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_1);
            ruleAtomic();

            state._fsp--;

             after(grammarAccess.getAtomicRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // InternalExpressionDsl.g:387:1: ruleAtomic : ( ( rule__Atomic__Alternatives ) ) ;
    public final void ruleAtomic() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:391:2: ( ( ( rule__Atomic__Alternatives ) ) )
            // InternalExpressionDsl.g:392:2: ( ( rule__Atomic__Alternatives ) )
            {
            // InternalExpressionDsl.g:392:2: ( ( rule__Atomic__Alternatives ) )
            // InternalExpressionDsl.g:393:3: ( rule__Atomic__Alternatives )
            {
             before(grammarAccess.getAtomicAccess().getAlternatives()); 
            // InternalExpressionDsl.g:394:3: ( rule__Atomic__Alternatives )
            // InternalExpressionDsl.g:394:4: rule__Atomic__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtomic"


    // $ANTLR start "entryRuleFeature"
    // InternalExpressionDsl.g:403:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:404:1: ( ruleFeature EOF )
            // InternalExpressionDsl.g:405:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalExpressionDsl.g:412:1: ruleFeature : ( ( rule__Feature__Alternatives ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:416:2: ( ( ( rule__Feature__Alternatives ) ) )
            // InternalExpressionDsl.g:417:2: ( ( rule__Feature__Alternatives ) )
            {
            // InternalExpressionDsl.g:417:2: ( ( rule__Feature__Alternatives ) )
            // InternalExpressionDsl.g:418:3: ( rule__Feature__Alternatives )
            {
             before(grammarAccess.getFeatureAccess().getAlternatives()); 
            // InternalExpressionDsl.g:419:3: ( rule__Feature__Alternatives )
            // InternalExpressionDsl.g:419:4: rule__Feature__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleIDENTIFIER"
    // InternalExpressionDsl.g:428:1: entryRuleIDENTIFIER : ruleIDENTIFIER EOF ;
    public final void entryRuleIDENTIFIER() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:429:1: ( ruleIDENTIFIER EOF )
            // InternalExpressionDsl.g:430:1: ruleIDENTIFIER EOF
            {
             before(grammarAccess.getIDENTIFIERRule()); 
            pushFollow(FOLLOW_1);
            ruleIDENTIFIER();

            state._fsp--;

             after(grammarAccess.getIDENTIFIERRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIDENTIFIER"


    // $ANTLR start "ruleIDENTIFIER"
    // InternalExpressionDsl.g:437:1: ruleIDENTIFIER : ( ( rule__IDENTIFIER__Group__0 ) ) ;
    public final void ruleIDENTIFIER() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:441:2: ( ( ( rule__IDENTIFIER__Group__0 ) ) )
            // InternalExpressionDsl.g:442:2: ( ( rule__IDENTIFIER__Group__0 ) )
            {
            // InternalExpressionDsl.g:442:2: ( ( rule__IDENTIFIER__Group__0 ) )
            // InternalExpressionDsl.g:443:3: ( rule__IDENTIFIER__Group__0 )
            {
             before(grammarAccess.getIDENTIFIERAccess().getGroup()); 
            // InternalExpressionDsl.g:444:3: ( rule__IDENTIFIER__Group__0 )
            // InternalExpressionDsl.g:444:4: rule__IDENTIFIER__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IDENTIFIER__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIDENTIFIERAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIDENTIFIER"


    // $ANTLR start "entryRuleE_INT"
    // InternalExpressionDsl.g:453:1: entryRuleE_INT : ruleE_INT EOF ;
    public final void entryRuleE_INT() throws RecognitionException {
        try {
            // InternalExpressionDsl.g:454:1: ( ruleE_INT EOF )
            // InternalExpressionDsl.g:455:1: ruleE_INT EOF
            {
             before(grammarAccess.getE_INTRule()); 
            pushFollow(FOLLOW_1);
            ruleE_INT();

            state._fsp--;

             after(grammarAccess.getE_INTRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleE_INT"


    // $ANTLR start "ruleE_INT"
    // InternalExpressionDsl.g:462:1: ruleE_INT : ( RULE_NUMBER ) ;
    public final void ruleE_INT() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:466:2: ( ( RULE_NUMBER ) )
            // InternalExpressionDsl.g:467:2: ( RULE_NUMBER )
            {
            // InternalExpressionDsl.g:467:2: ( RULE_NUMBER )
            // InternalExpressionDsl.g:468:3: RULE_NUMBER
            {
             before(grammarAccess.getE_INTAccess().getNUMBERTerminalRuleCall()); 
            match(input,RULE_NUMBER,FOLLOW_2); 
             after(grammarAccess.getE_INTAccess().getNUMBERTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleE_INT"


    // $ANTLR start "rule__Equality__OpAlternatives_1_1_0"
    // InternalExpressionDsl.g:477:1: rule__Equality__OpAlternatives_1_1_0 : ( ( '==' ) | ( '!=' ) );
    public final void rule__Equality__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:481:1: ( ( '==' ) | ( '!=' ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==14) ) {
                alt1=1;
            }
            else if ( (LA1_0==15) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalExpressionDsl.g:482:2: ( '==' )
                    {
                    // InternalExpressionDsl.g:482:2: ( '==' )
                    // InternalExpressionDsl.g:483:3: '=='
                    {
                     before(grammarAccess.getEqualityAccess().getOpEqualsSignEqualsSignKeyword_1_1_0_0()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getEqualityAccess().getOpEqualsSignEqualsSignKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:488:2: ( '!=' )
                    {
                    // InternalExpressionDsl.g:488:2: ( '!=' )
                    // InternalExpressionDsl.g:489:3: '!='
                    {
                     before(grammarAccess.getEqualityAccess().getOpExclamationMarkEqualsSignKeyword_1_1_0_1()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getEqualityAccess().getOpExclamationMarkEqualsSignKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__OpAlternatives_1_1_0"


    // $ANTLR start "rule__Comparison__OpAlternatives_1_1_0"
    // InternalExpressionDsl.g:498:1: rule__Comparison__OpAlternatives_1_1_0 : ( ( '>=' ) | ( '<=' ) | ( '>' ) | ( '<' ) );
    public final void rule__Comparison__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:502:1: ( ( '>=' ) | ( '<=' ) | ( '>' ) | ( '<' ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt2=1;
                }
                break;
            case 17:
                {
                alt2=2;
                }
                break;
            case 18:
                {
                alt2=3;
                }
                break;
            case 19:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalExpressionDsl.g:503:2: ( '>=' )
                    {
                    // InternalExpressionDsl.g:503:2: ( '>=' )
                    // InternalExpressionDsl.g:504:3: '>='
                    {
                     before(grammarAccess.getComparisonAccess().getOpGreaterThanSignEqualsSignKeyword_1_1_0_0()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getComparisonAccess().getOpGreaterThanSignEqualsSignKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:509:2: ( '<=' )
                    {
                    // InternalExpressionDsl.g:509:2: ( '<=' )
                    // InternalExpressionDsl.g:510:3: '<='
                    {
                     before(grammarAccess.getComparisonAccess().getOpLessThanSignEqualsSignKeyword_1_1_0_1()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getComparisonAccess().getOpLessThanSignEqualsSignKeyword_1_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionDsl.g:515:2: ( '>' )
                    {
                    // InternalExpressionDsl.g:515:2: ( '>' )
                    // InternalExpressionDsl.g:516:3: '>'
                    {
                     before(grammarAccess.getComparisonAccess().getOpGreaterThanSignKeyword_1_1_0_2()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getComparisonAccess().getOpGreaterThanSignKeyword_1_1_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalExpressionDsl.g:521:2: ( '<' )
                    {
                    // InternalExpressionDsl.g:521:2: ( '<' )
                    // InternalExpressionDsl.g:522:3: '<'
                    {
                     before(grammarAccess.getComparisonAccess().getOpLessThanSignKeyword_1_1_0_3()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getComparisonAccess().getOpLessThanSignKeyword_1_1_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__OpAlternatives_1_1_0"


    // $ANTLR start "rule__BitwiseShift__OpAlternatives_1_1_0"
    // InternalExpressionDsl.g:531:1: rule__BitwiseShift__OpAlternatives_1_1_0 : ( ( '>>' ) | ( '<<' ) | ( '>>>' ) );
    public final void rule__BitwiseShift__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:535:1: ( ( '>>' ) | ( '<<' ) | ( '>>>' ) )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt3=1;
                }
                break;
            case 21:
                {
                alt3=2;
                }
                break;
            case 22:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalExpressionDsl.g:536:2: ( '>>' )
                    {
                    // InternalExpressionDsl.g:536:2: ( '>>' )
                    // InternalExpressionDsl.g:537:3: '>>'
                    {
                     before(grammarAccess.getBitwiseShiftAccess().getOpGreaterThanSignGreaterThanSignKeyword_1_1_0_0()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getBitwiseShiftAccess().getOpGreaterThanSignGreaterThanSignKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:542:2: ( '<<' )
                    {
                    // InternalExpressionDsl.g:542:2: ( '<<' )
                    // InternalExpressionDsl.g:543:3: '<<'
                    {
                     before(grammarAccess.getBitwiseShiftAccess().getOpLessThanSignLessThanSignKeyword_1_1_0_1()); 
                    match(input,21,FOLLOW_2); 
                     after(grammarAccess.getBitwiseShiftAccess().getOpLessThanSignLessThanSignKeyword_1_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionDsl.g:548:2: ( '>>>' )
                    {
                    // InternalExpressionDsl.g:548:2: ( '>>>' )
                    // InternalExpressionDsl.g:549:3: '>>>'
                    {
                     before(grammarAccess.getBitwiseShiftAccess().getOpGreaterThanSignGreaterThanSignGreaterThanSignKeyword_1_1_0_2()); 
                    match(input,22,FOLLOW_2); 
                     after(grammarAccess.getBitwiseShiftAccess().getOpGreaterThanSignGreaterThanSignGreaterThanSignKeyword_1_1_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__OpAlternatives_1_1_0"


    // $ANTLR start "rule__PlusOrMinus__Alternatives_1_0"
    // InternalExpressionDsl.g:558:1: rule__PlusOrMinus__Alternatives_1_0 : ( ( ( rule__PlusOrMinus__Group_1_0_0__0 ) ) | ( ( rule__PlusOrMinus__Group_1_0_1__0 ) ) );
    public final void rule__PlusOrMinus__Alternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:562:1: ( ( ( rule__PlusOrMinus__Group_1_0_0__0 ) ) | ( ( rule__PlusOrMinus__Group_1_0_1__0 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==33) ) {
                alt4=1;
            }
            else if ( (LA4_0==34) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalExpressionDsl.g:563:2: ( ( rule__PlusOrMinus__Group_1_0_0__0 ) )
                    {
                    // InternalExpressionDsl.g:563:2: ( ( rule__PlusOrMinus__Group_1_0_0__0 ) )
                    // InternalExpressionDsl.g:564:3: ( rule__PlusOrMinus__Group_1_0_0__0 )
                    {
                     before(grammarAccess.getPlusOrMinusAccess().getGroup_1_0_0()); 
                    // InternalExpressionDsl.g:565:3: ( rule__PlusOrMinus__Group_1_0_0__0 )
                    // InternalExpressionDsl.g:565:4: rule__PlusOrMinus__Group_1_0_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__PlusOrMinus__Group_1_0_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPlusOrMinusAccess().getGroup_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:569:2: ( ( rule__PlusOrMinus__Group_1_0_1__0 ) )
                    {
                    // InternalExpressionDsl.g:569:2: ( ( rule__PlusOrMinus__Group_1_0_1__0 ) )
                    // InternalExpressionDsl.g:570:3: ( rule__PlusOrMinus__Group_1_0_1__0 )
                    {
                     before(grammarAccess.getPlusOrMinusAccess().getGroup_1_0_1()); 
                    // InternalExpressionDsl.g:571:3: ( rule__PlusOrMinus__Group_1_0_1__0 )
                    // InternalExpressionDsl.g:571:4: rule__PlusOrMinus__Group_1_0_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__PlusOrMinus__Group_1_0_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPlusOrMinusAccess().getGroup_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Alternatives_1_0"


    // $ANTLR start "rule__MulOrDiv__OpAlternatives_1_1_0"
    // InternalExpressionDsl.g:579:1: rule__MulOrDiv__OpAlternatives_1_1_0 : ( ( '*' ) | ( '/' ) | ( '%' ) );
    public final void rule__MulOrDiv__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:583:1: ( ( '*' ) | ( '/' ) | ( '%' ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 23:
                {
                alt5=1;
                }
                break;
            case 24:
                {
                alt5=2;
                }
                break;
            case 25:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalExpressionDsl.g:584:2: ( '*' )
                    {
                    // InternalExpressionDsl.g:584:2: ( '*' )
                    // InternalExpressionDsl.g:585:3: '*'
                    {
                     before(grammarAccess.getMulOrDivAccess().getOpAsteriskKeyword_1_1_0_0()); 
                    match(input,23,FOLLOW_2); 
                     after(grammarAccess.getMulOrDivAccess().getOpAsteriskKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:590:2: ( '/' )
                    {
                    // InternalExpressionDsl.g:590:2: ( '/' )
                    // InternalExpressionDsl.g:591:3: '/'
                    {
                     before(grammarAccess.getMulOrDivAccess().getOpSolidusKeyword_1_1_0_1()); 
                    match(input,24,FOLLOW_2); 
                     after(grammarAccess.getMulOrDivAccess().getOpSolidusKeyword_1_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionDsl.g:596:2: ( '%' )
                    {
                    // InternalExpressionDsl.g:596:2: ( '%' )
                    // InternalExpressionDsl.g:597:3: '%'
                    {
                     before(grammarAccess.getMulOrDivAccess().getOpPercentSignKeyword_1_1_0_2()); 
                    match(input,25,FOLLOW_2); 
                     after(grammarAccess.getMulOrDivAccess().getOpPercentSignKeyword_1_1_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__OpAlternatives_1_1_0"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalExpressionDsl.g:606:1: rule__Primary__Alternatives : ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__Group_1__0 ) ) | ( ( rule__Primary__Group_2__0 ) ) | ( ( rule__Primary__Group_3__0 ) ) | ( ( rule__Primary__Group_4__0 ) ) | ( ruleAtomic ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:610:1: ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__Group_1__0 ) ) | ( ( rule__Primary__Group_2__0 ) ) | ( ( rule__Primary__Group_3__0 ) ) | ( ( rule__Primary__Group_4__0 ) ) | ( ruleAtomic ) )
            int alt6=6;
            alt6 = dfa6.predict(input);
            switch (alt6) {
                case 1 :
                    // InternalExpressionDsl.g:611:2: ( ( rule__Primary__Group_0__0 ) )
                    {
                    // InternalExpressionDsl.g:611:2: ( ( rule__Primary__Group_0__0 ) )
                    // InternalExpressionDsl.g:612:3: ( rule__Primary__Group_0__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_0()); 
                    // InternalExpressionDsl.g:613:3: ( rule__Primary__Group_0__0 )
                    // InternalExpressionDsl.g:613:4: rule__Primary__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:617:2: ( ( rule__Primary__Group_1__0 ) )
                    {
                    // InternalExpressionDsl.g:617:2: ( ( rule__Primary__Group_1__0 ) )
                    // InternalExpressionDsl.g:618:3: ( rule__Primary__Group_1__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_1()); 
                    // InternalExpressionDsl.g:619:3: ( rule__Primary__Group_1__0 )
                    // InternalExpressionDsl.g:619:4: rule__Primary__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionDsl.g:623:2: ( ( rule__Primary__Group_2__0 ) )
                    {
                    // InternalExpressionDsl.g:623:2: ( ( rule__Primary__Group_2__0 ) )
                    // InternalExpressionDsl.g:624:3: ( rule__Primary__Group_2__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_2()); 
                    // InternalExpressionDsl.g:625:3: ( rule__Primary__Group_2__0 )
                    // InternalExpressionDsl.g:625:4: rule__Primary__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalExpressionDsl.g:629:2: ( ( rule__Primary__Group_3__0 ) )
                    {
                    // InternalExpressionDsl.g:629:2: ( ( rule__Primary__Group_3__0 ) )
                    // InternalExpressionDsl.g:630:3: ( rule__Primary__Group_3__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_3()); 
                    // InternalExpressionDsl.g:631:3: ( rule__Primary__Group_3__0 )
                    // InternalExpressionDsl.g:631:4: rule__Primary__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalExpressionDsl.g:635:2: ( ( rule__Primary__Group_4__0 ) )
                    {
                    // InternalExpressionDsl.g:635:2: ( ( rule__Primary__Group_4__0 ) )
                    // InternalExpressionDsl.g:636:3: ( rule__Primary__Group_4__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_4()); 
                    // InternalExpressionDsl.g:637:3: ( rule__Primary__Group_4__0 )
                    // InternalExpressionDsl.g:637:4: rule__Primary__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalExpressionDsl.g:641:2: ( ruleAtomic )
                    {
                    // InternalExpressionDsl.g:641:2: ( ruleAtomic )
                    // InternalExpressionDsl.g:642:3: ruleAtomic
                    {
                     before(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleAtomic();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Primary__OpAlternatives_3_1_0"
    // InternalExpressionDsl.g:651:1: rule__Primary__OpAlternatives_3_1_0 : ( ( '++' ) | ( '--' ) );
    public final void rule__Primary__OpAlternatives_3_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:655:1: ( ( '++' ) | ( '--' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==26) ) {
                alt7=1;
            }
            else if ( (LA7_0==27) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalExpressionDsl.g:656:2: ( '++' )
                    {
                    // InternalExpressionDsl.g:656:2: ( '++' )
                    // InternalExpressionDsl.g:657:3: '++'
                    {
                     before(grammarAccess.getPrimaryAccess().getOpPlusSignPlusSignKeyword_3_1_0_0()); 
                    match(input,26,FOLLOW_2); 
                     after(grammarAccess.getPrimaryAccess().getOpPlusSignPlusSignKeyword_3_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:662:2: ( '--' )
                    {
                    // InternalExpressionDsl.g:662:2: ( '--' )
                    // InternalExpressionDsl.g:663:3: '--'
                    {
                     before(grammarAccess.getPrimaryAccess().getOpHyphenMinusHyphenMinusKeyword_3_1_0_1()); 
                    match(input,27,FOLLOW_2); 
                     after(grammarAccess.getPrimaryAccess().getOpHyphenMinusHyphenMinusKeyword_3_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__OpAlternatives_3_1_0"


    // $ANTLR start "rule__Primary__OpAlternatives_4_2_0"
    // InternalExpressionDsl.g:672:1: rule__Primary__OpAlternatives_4_2_0 : ( ( '++' ) | ( '--' ) );
    public final void rule__Primary__OpAlternatives_4_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:676:1: ( ( '++' ) | ( '--' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==26) ) {
                alt8=1;
            }
            else if ( (LA8_0==27) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalExpressionDsl.g:677:2: ( '++' )
                    {
                    // InternalExpressionDsl.g:677:2: ( '++' )
                    // InternalExpressionDsl.g:678:3: '++'
                    {
                     before(grammarAccess.getPrimaryAccess().getOpPlusSignPlusSignKeyword_4_2_0_0()); 
                    match(input,26,FOLLOW_2); 
                     after(grammarAccess.getPrimaryAccess().getOpPlusSignPlusSignKeyword_4_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:683:2: ( '--' )
                    {
                    // InternalExpressionDsl.g:683:2: ( '--' )
                    // InternalExpressionDsl.g:684:3: '--'
                    {
                     before(grammarAccess.getPrimaryAccess().getOpHyphenMinusHyphenMinusKeyword_4_2_0_1()); 
                    match(input,27,FOLLOW_2); 
                     after(grammarAccess.getPrimaryAccess().getOpHyphenMinusHyphenMinusKeyword_4_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__OpAlternatives_4_2_0"


    // $ANTLR start "rule__Atomic__Alternatives"
    // InternalExpressionDsl.g:693:1: rule__Atomic__Alternatives : ( ( ( rule__Atomic__Group_0__0 ) ) | ( ( rule__Atomic__Group_1__0 ) ) | ( ( rule__Atomic__Group_2__0 ) ) | ( ( rule__Atomic__Group_3__0 ) ) | ( ( rule__Atomic__Group_4__0 ) ) | ( ( rule__Atomic__Group_5__0 ) ) | ( ( rule__Atomic__Group_6__0 ) ) | ( ( rule__Atomic__Group_7__0 ) ) | ( ( rule__Atomic__Group_8__0 ) ) | ( ( rule__Atomic__Group_9__0 ) ) );
    public final void rule__Atomic__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:697:1: ( ( ( rule__Atomic__Group_0__0 ) ) | ( ( rule__Atomic__Group_1__0 ) ) | ( ( rule__Atomic__Group_2__0 ) ) | ( ( rule__Atomic__Group_3__0 ) ) | ( ( rule__Atomic__Group_4__0 ) ) | ( ( rule__Atomic__Group_5__0 ) ) | ( ( rule__Atomic__Group_6__0 ) ) | ( ( rule__Atomic__Group_7__0 ) ) | ( ( rule__Atomic__Group_8__0 ) ) | ( ( rule__Atomic__Group_9__0 ) ) )
            int alt9=10;
            switch ( input.LA(1) ) {
            case RULE_NUMBER:
                {
                alt9=1;
                }
                break;
            case RULE_E_STRING:
                {
                alt9=2;
                }
                break;
            case RULE_E_CHAR:
            case 28:
                {
                alt9=3;
                }
                break;
            case RULE_E_BOOLEAN:
                {
                alt9=4;
                }
                break;
            case RULE_E_FLOAT:
                {
                alt9=5;
                }
                break;
            case RULE_E_LONG:
                {
                alt9=6;
                }
                break;
            case RULE_E_DOUBLE:
                {
                alt9=7;
                }
                break;
            case RULE_E_BYTE:
                {
                alt9=8;
                }
                break;
            case 38:
                {
                alt9=9;
                }
                break;
            case RULE_E_NULL:
                {
                alt9=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalExpressionDsl.g:698:2: ( ( rule__Atomic__Group_0__0 ) )
                    {
                    // InternalExpressionDsl.g:698:2: ( ( rule__Atomic__Group_0__0 ) )
                    // InternalExpressionDsl.g:699:3: ( rule__Atomic__Group_0__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_0()); 
                    // InternalExpressionDsl.g:700:3: ( rule__Atomic__Group_0__0 )
                    // InternalExpressionDsl.g:700:4: rule__Atomic__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:704:2: ( ( rule__Atomic__Group_1__0 ) )
                    {
                    // InternalExpressionDsl.g:704:2: ( ( rule__Atomic__Group_1__0 ) )
                    // InternalExpressionDsl.g:705:3: ( rule__Atomic__Group_1__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_1()); 
                    // InternalExpressionDsl.g:706:3: ( rule__Atomic__Group_1__0 )
                    // InternalExpressionDsl.g:706:4: rule__Atomic__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionDsl.g:710:2: ( ( rule__Atomic__Group_2__0 ) )
                    {
                    // InternalExpressionDsl.g:710:2: ( ( rule__Atomic__Group_2__0 ) )
                    // InternalExpressionDsl.g:711:3: ( rule__Atomic__Group_2__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_2()); 
                    // InternalExpressionDsl.g:712:3: ( rule__Atomic__Group_2__0 )
                    // InternalExpressionDsl.g:712:4: rule__Atomic__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalExpressionDsl.g:716:2: ( ( rule__Atomic__Group_3__0 ) )
                    {
                    // InternalExpressionDsl.g:716:2: ( ( rule__Atomic__Group_3__0 ) )
                    // InternalExpressionDsl.g:717:3: ( rule__Atomic__Group_3__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_3()); 
                    // InternalExpressionDsl.g:718:3: ( rule__Atomic__Group_3__0 )
                    // InternalExpressionDsl.g:718:4: rule__Atomic__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalExpressionDsl.g:722:2: ( ( rule__Atomic__Group_4__0 ) )
                    {
                    // InternalExpressionDsl.g:722:2: ( ( rule__Atomic__Group_4__0 ) )
                    // InternalExpressionDsl.g:723:3: ( rule__Atomic__Group_4__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_4()); 
                    // InternalExpressionDsl.g:724:3: ( rule__Atomic__Group_4__0 )
                    // InternalExpressionDsl.g:724:4: rule__Atomic__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalExpressionDsl.g:728:2: ( ( rule__Atomic__Group_5__0 ) )
                    {
                    // InternalExpressionDsl.g:728:2: ( ( rule__Atomic__Group_5__0 ) )
                    // InternalExpressionDsl.g:729:3: ( rule__Atomic__Group_5__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_5()); 
                    // InternalExpressionDsl.g:730:3: ( rule__Atomic__Group_5__0 )
                    // InternalExpressionDsl.g:730:4: rule__Atomic__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalExpressionDsl.g:734:2: ( ( rule__Atomic__Group_6__0 ) )
                    {
                    // InternalExpressionDsl.g:734:2: ( ( rule__Atomic__Group_6__0 ) )
                    // InternalExpressionDsl.g:735:3: ( rule__Atomic__Group_6__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_6()); 
                    // InternalExpressionDsl.g:736:3: ( rule__Atomic__Group_6__0 )
                    // InternalExpressionDsl.g:736:4: rule__Atomic__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_6__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalExpressionDsl.g:740:2: ( ( rule__Atomic__Group_7__0 ) )
                    {
                    // InternalExpressionDsl.g:740:2: ( ( rule__Atomic__Group_7__0 ) )
                    // InternalExpressionDsl.g:741:3: ( rule__Atomic__Group_7__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_7()); 
                    // InternalExpressionDsl.g:742:3: ( rule__Atomic__Group_7__0 )
                    // InternalExpressionDsl.g:742:4: rule__Atomic__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_7__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalExpressionDsl.g:746:2: ( ( rule__Atomic__Group_8__0 ) )
                    {
                    // InternalExpressionDsl.g:746:2: ( ( rule__Atomic__Group_8__0 ) )
                    // InternalExpressionDsl.g:747:3: ( rule__Atomic__Group_8__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_8()); 
                    // InternalExpressionDsl.g:748:3: ( rule__Atomic__Group_8__0 )
                    // InternalExpressionDsl.g:748:4: rule__Atomic__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_8__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalExpressionDsl.g:752:2: ( ( rule__Atomic__Group_9__0 ) )
                    {
                    // InternalExpressionDsl.g:752:2: ( ( rule__Atomic__Group_9__0 ) )
                    // InternalExpressionDsl.g:753:3: ( rule__Atomic__Group_9__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_9()); 
                    // InternalExpressionDsl.g:754:3: ( rule__Atomic__Group_9__0 )
                    // InternalExpressionDsl.g:754:4: rule__Atomic__Group_9__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_9__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_9()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Alternatives"


    // $ANTLR start "rule__Feature__Alternatives"
    // InternalExpressionDsl.g:762:1: rule__Feature__Alternatives : ( ( ruleIDENTIFIER ) | ( ( rule__Feature__Group_1__0 ) ) );
    public final void rule__Feature__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:766:1: ( ( ruleIDENTIFIER ) | ( ( rule__Feature__Group_1__0 ) ) )
            int alt10=2;
            alt10 = dfa10.predict(input);
            switch (alt10) {
                case 1 :
                    // InternalExpressionDsl.g:767:2: ( ruleIDENTIFIER )
                    {
                    // InternalExpressionDsl.g:767:2: ( ruleIDENTIFIER )
                    // InternalExpressionDsl.g:768:3: ruleIDENTIFIER
                    {
                     before(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleIDENTIFIER();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:773:2: ( ( rule__Feature__Group_1__0 ) )
                    {
                    // InternalExpressionDsl.g:773:2: ( ( rule__Feature__Group_1__0 ) )
                    // InternalExpressionDsl.g:774:3: ( rule__Feature__Group_1__0 )
                    {
                     before(grammarAccess.getFeatureAccess().getGroup_1()); 
                    // InternalExpressionDsl.g:775:3: ( rule__Feature__Group_1__0 )
                    // InternalExpressionDsl.g:775:4: rule__Feature__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Feature__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFeatureAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Alternatives"


    // $ANTLR start "rule__IDENTIFIER__Alternatives_0"
    // InternalExpressionDsl.g:783:1: rule__IDENTIFIER__Alternatives_0 : ( ( RULE_E_CHAR ) | ( '_' ) );
    public final void rule__IDENTIFIER__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:787:1: ( ( RULE_E_CHAR ) | ( '_' ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_E_CHAR) ) {
                alt11=1;
            }
            else if ( (LA11_0==28) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalExpressionDsl.g:788:2: ( RULE_E_CHAR )
                    {
                    // InternalExpressionDsl.g:788:2: ( RULE_E_CHAR )
                    // InternalExpressionDsl.g:789:3: RULE_E_CHAR
                    {
                     before(grammarAccess.getIDENTIFIERAccess().getE_CHARTerminalRuleCall_0_0()); 
                    match(input,RULE_E_CHAR,FOLLOW_2); 
                     after(grammarAccess.getIDENTIFIERAccess().getE_CHARTerminalRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:794:2: ( '_' )
                    {
                    // InternalExpressionDsl.g:794:2: ( '_' )
                    // InternalExpressionDsl.g:795:3: '_'
                    {
                     before(grammarAccess.getIDENTIFIERAccess().get_Keyword_0_1()); 
                    match(input,28,FOLLOW_2); 
                     after(grammarAccess.getIDENTIFIERAccess().get_Keyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IDENTIFIER__Alternatives_0"


    // $ANTLR start "rule__IDENTIFIER__Alternatives_1"
    // InternalExpressionDsl.g:804:1: rule__IDENTIFIER__Alternatives_1 : ( ( RULE_E_CHAR ) | ( '_' ) | ( RULE_NUMBER ) );
    public final void rule__IDENTIFIER__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:808:1: ( ( RULE_E_CHAR ) | ( '_' ) | ( RULE_NUMBER ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case RULE_E_CHAR:
                {
                alt12=1;
                }
                break;
            case 28:
                {
                alt12=2;
                }
                break;
            case RULE_NUMBER:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalExpressionDsl.g:809:2: ( RULE_E_CHAR )
                    {
                    // InternalExpressionDsl.g:809:2: ( RULE_E_CHAR )
                    // InternalExpressionDsl.g:810:3: RULE_E_CHAR
                    {
                     before(grammarAccess.getIDENTIFIERAccess().getE_CHARTerminalRuleCall_1_0()); 
                    match(input,RULE_E_CHAR,FOLLOW_2); 
                     after(grammarAccess.getIDENTIFIERAccess().getE_CHARTerminalRuleCall_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:815:2: ( '_' )
                    {
                    // InternalExpressionDsl.g:815:2: ( '_' )
                    // InternalExpressionDsl.g:816:3: '_'
                    {
                     before(grammarAccess.getIDENTIFIERAccess().get_Keyword_1_1()); 
                    match(input,28,FOLLOW_2); 
                     after(grammarAccess.getIDENTIFIERAccess().get_Keyword_1_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionDsl.g:821:2: ( RULE_NUMBER )
                    {
                    // InternalExpressionDsl.g:821:2: ( RULE_NUMBER )
                    // InternalExpressionDsl.g:822:3: RULE_NUMBER
                    {
                     before(grammarAccess.getIDENTIFIERAccess().getNUMBERTerminalRuleCall_1_2()); 
                    match(input,RULE_NUMBER,FOLLOW_2); 
                     after(grammarAccess.getIDENTIFIERAccess().getNUMBERTerminalRuleCall_1_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IDENTIFIER__Alternatives_1"


    // $ANTLR start "rule__Conditional__Group__0"
    // InternalExpressionDsl.g:831:1: rule__Conditional__Group__0 : rule__Conditional__Group__0__Impl rule__Conditional__Group__1 ;
    public final void rule__Conditional__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:835:1: ( rule__Conditional__Group__0__Impl rule__Conditional__Group__1 )
            // InternalExpressionDsl.g:836:2: rule__Conditional__Group__0__Impl rule__Conditional__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Conditional__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Conditional__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group__0"


    // $ANTLR start "rule__Conditional__Group__0__Impl"
    // InternalExpressionDsl.g:843:1: rule__Conditional__Group__0__Impl : ( ruleOr ) ;
    public final void rule__Conditional__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:847:1: ( ( ruleOr ) )
            // InternalExpressionDsl.g:848:1: ( ruleOr )
            {
            // InternalExpressionDsl.g:848:1: ( ruleOr )
            // InternalExpressionDsl.g:849:2: ruleOr
            {
             before(grammarAccess.getConditionalAccess().getOrParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getConditionalAccess().getOrParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group__0__Impl"


    // $ANTLR start "rule__Conditional__Group__1"
    // InternalExpressionDsl.g:858:1: rule__Conditional__Group__1 : rule__Conditional__Group__1__Impl ;
    public final void rule__Conditional__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:862:1: ( rule__Conditional__Group__1__Impl )
            // InternalExpressionDsl.g:863:2: rule__Conditional__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Conditional__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group__1"


    // $ANTLR start "rule__Conditional__Group__1__Impl"
    // InternalExpressionDsl.g:869:1: rule__Conditional__Group__1__Impl : ( ( rule__Conditional__Group_1__0 )? ) ;
    public final void rule__Conditional__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:873:1: ( ( ( rule__Conditional__Group_1__0 )? ) )
            // InternalExpressionDsl.g:874:1: ( ( rule__Conditional__Group_1__0 )? )
            {
            // InternalExpressionDsl.g:874:1: ( ( rule__Conditional__Group_1__0 )? )
            // InternalExpressionDsl.g:875:2: ( rule__Conditional__Group_1__0 )?
            {
             before(grammarAccess.getConditionalAccess().getGroup_1()); 
            // InternalExpressionDsl.g:876:2: ( rule__Conditional__Group_1__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==29) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalExpressionDsl.g:876:3: rule__Conditional__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Conditional__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConditionalAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group__1__Impl"


    // $ANTLR start "rule__Conditional__Group_1__0"
    // InternalExpressionDsl.g:885:1: rule__Conditional__Group_1__0 : rule__Conditional__Group_1__0__Impl rule__Conditional__Group_1__1 ;
    public final void rule__Conditional__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:889:1: ( rule__Conditional__Group_1__0__Impl rule__Conditional__Group_1__1 )
            // InternalExpressionDsl.g:890:2: rule__Conditional__Group_1__0__Impl rule__Conditional__Group_1__1
            {
            pushFollow(FOLLOW_3);
            rule__Conditional__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Conditional__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__0"


    // $ANTLR start "rule__Conditional__Group_1__0__Impl"
    // InternalExpressionDsl.g:897:1: rule__Conditional__Group_1__0__Impl : ( () ) ;
    public final void rule__Conditional__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:901:1: ( ( () ) )
            // InternalExpressionDsl.g:902:1: ( () )
            {
            // InternalExpressionDsl.g:902:1: ( () )
            // InternalExpressionDsl.g:903:2: ()
            {
             before(grammarAccess.getConditionalAccess().getConditionalConditionAction_1_0()); 
            // InternalExpressionDsl.g:904:2: ()
            // InternalExpressionDsl.g:904:3: 
            {
            }

             after(grammarAccess.getConditionalAccess().getConditionalConditionAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__0__Impl"


    // $ANTLR start "rule__Conditional__Group_1__1"
    // InternalExpressionDsl.g:912:1: rule__Conditional__Group_1__1 : rule__Conditional__Group_1__1__Impl rule__Conditional__Group_1__2 ;
    public final void rule__Conditional__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:916:1: ( rule__Conditional__Group_1__1__Impl rule__Conditional__Group_1__2 )
            // InternalExpressionDsl.g:917:2: rule__Conditional__Group_1__1__Impl rule__Conditional__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__Conditional__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Conditional__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__1"


    // $ANTLR start "rule__Conditional__Group_1__1__Impl"
    // InternalExpressionDsl.g:924:1: rule__Conditional__Group_1__1__Impl : ( '?' ) ;
    public final void rule__Conditional__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:928:1: ( ( '?' ) )
            // InternalExpressionDsl.g:929:1: ( '?' )
            {
            // InternalExpressionDsl.g:929:1: ( '?' )
            // InternalExpressionDsl.g:930:2: '?'
            {
             before(grammarAccess.getConditionalAccess().getQuestionMarkKeyword_1_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getConditionalAccess().getQuestionMarkKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__1__Impl"


    // $ANTLR start "rule__Conditional__Group_1__2"
    // InternalExpressionDsl.g:939:1: rule__Conditional__Group_1__2 : rule__Conditional__Group_1__2__Impl rule__Conditional__Group_1__3 ;
    public final void rule__Conditional__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:943:1: ( rule__Conditional__Group_1__2__Impl rule__Conditional__Group_1__3 )
            // InternalExpressionDsl.g:944:2: rule__Conditional__Group_1__2__Impl rule__Conditional__Group_1__3
            {
            pushFollow(FOLLOW_5);
            rule__Conditional__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Conditional__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__2"


    // $ANTLR start "rule__Conditional__Group_1__2__Impl"
    // InternalExpressionDsl.g:951:1: rule__Conditional__Group_1__2__Impl : ( ( rule__Conditional__ExpressionTAssignment_1_2 ) ) ;
    public final void rule__Conditional__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:955:1: ( ( ( rule__Conditional__ExpressionTAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:956:1: ( ( rule__Conditional__ExpressionTAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:956:1: ( ( rule__Conditional__ExpressionTAssignment_1_2 ) )
            // InternalExpressionDsl.g:957:2: ( rule__Conditional__ExpressionTAssignment_1_2 )
            {
             before(grammarAccess.getConditionalAccess().getExpressionTAssignment_1_2()); 
            // InternalExpressionDsl.g:958:2: ( rule__Conditional__ExpressionTAssignment_1_2 )
            // InternalExpressionDsl.g:958:3: rule__Conditional__ExpressionTAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Conditional__ExpressionTAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getConditionalAccess().getExpressionTAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__2__Impl"


    // $ANTLR start "rule__Conditional__Group_1__3"
    // InternalExpressionDsl.g:966:1: rule__Conditional__Group_1__3 : rule__Conditional__Group_1__3__Impl rule__Conditional__Group_1__4 ;
    public final void rule__Conditional__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:970:1: ( rule__Conditional__Group_1__3__Impl rule__Conditional__Group_1__4 )
            // InternalExpressionDsl.g:971:2: rule__Conditional__Group_1__3__Impl rule__Conditional__Group_1__4
            {
            pushFollow(FOLLOW_4);
            rule__Conditional__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Conditional__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__3"


    // $ANTLR start "rule__Conditional__Group_1__3__Impl"
    // InternalExpressionDsl.g:978:1: rule__Conditional__Group_1__3__Impl : ( ':' ) ;
    public final void rule__Conditional__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:982:1: ( ( ':' ) )
            // InternalExpressionDsl.g:983:1: ( ':' )
            {
            // InternalExpressionDsl.g:983:1: ( ':' )
            // InternalExpressionDsl.g:984:2: ':'
            {
             before(grammarAccess.getConditionalAccess().getColonKeyword_1_3()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getConditionalAccess().getColonKeyword_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__3__Impl"


    // $ANTLR start "rule__Conditional__Group_1__4"
    // InternalExpressionDsl.g:993:1: rule__Conditional__Group_1__4 : rule__Conditional__Group_1__4__Impl ;
    public final void rule__Conditional__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:997:1: ( rule__Conditional__Group_1__4__Impl )
            // InternalExpressionDsl.g:998:2: rule__Conditional__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Conditional__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__4"


    // $ANTLR start "rule__Conditional__Group_1__4__Impl"
    // InternalExpressionDsl.g:1004:1: rule__Conditional__Group_1__4__Impl : ( ( rule__Conditional__ExpressionFAssignment_1_4 ) ) ;
    public final void rule__Conditional__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1008:1: ( ( ( rule__Conditional__ExpressionFAssignment_1_4 ) ) )
            // InternalExpressionDsl.g:1009:1: ( ( rule__Conditional__ExpressionFAssignment_1_4 ) )
            {
            // InternalExpressionDsl.g:1009:1: ( ( rule__Conditional__ExpressionFAssignment_1_4 ) )
            // InternalExpressionDsl.g:1010:2: ( rule__Conditional__ExpressionFAssignment_1_4 )
            {
             before(grammarAccess.getConditionalAccess().getExpressionFAssignment_1_4()); 
            // InternalExpressionDsl.g:1011:2: ( rule__Conditional__ExpressionFAssignment_1_4 )
            // InternalExpressionDsl.g:1011:3: rule__Conditional__ExpressionFAssignment_1_4
            {
            pushFollow(FOLLOW_2);
            rule__Conditional__ExpressionFAssignment_1_4();

            state._fsp--;


            }

             after(grammarAccess.getConditionalAccess().getExpressionFAssignment_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__Group_1__4__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalExpressionDsl.g:1020:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1024:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalExpressionDsl.g:1025:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalExpressionDsl.g:1032:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1036:1: ( ( ruleAnd ) )
            // InternalExpressionDsl.g:1037:1: ( ruleAnd )
            {
            // InternalExpressionDsl.g:1037:1: ( ruleAnd )
            // InternalExpressionDsl.g:1038:2: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalExpressionDsl.g:1047:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1051:1: ( rule__Or__Group__1__Impl )
            // InternalExpressionDsl.g:1052:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalExpressionDsl.g:1058:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1062:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalExpressionDsl.g:1063:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:1063:1: ( ( rule__Or__Group_1__0 )* )
            // InternalExpressionDsl.g:1064:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalExpressionDsl.g:1065:2: ( rule__Or__Group_1__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==31) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalExpressionDsl.g:1065:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalExpressionDsl.g:1074:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1078:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalExpressionDsl.g:1079:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalExpressionDsl.g:1086:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1090:1: ( ( () ) )
            // InternalExpressionDsl.g:1091:1: ( () )
            {
            // InternalExpressionDsl.g:1091:1: ( () )
            // InternalExpressionDsl.g:1092:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalExpressionDsl.g:1093:2: ()
            // InternalExpressionDsl.g:1093:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalExpressionDsl.g:1101:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1105:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalExpressionDsl.g:1106:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalExpressionDsl.g:1113:1: rule__Or__Group_1__1__Impl : ( '||' ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1117:1: ( ( '||' ) )
            // InternalExpressionDsl.g:1118:1: ( '||' )
            {
            // InternalExpressionDsl.g:1118:1: ( '||' )
            // InternalExpressionDsl.g:1119:2: '||'
            {
             before(grammarAccess.getOrAccess().getVerticalLineVerticalLineKeyword_1_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getVerticalLineVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalExpressionDsl.g:1128:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1132:1: ( rule__Or__Group_1__2__Impl )
            // InternalExpressionDsl.g:1133:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalExpressionDsl.g:1139:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1143:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:1144:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:1144:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:1145:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:1146:2: ( rule__Or__RightAssignment_1_2 )
            // InternalExpressionDsl.g:1146:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalExpressionDsl.g:1155:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1159:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalExpressionDsl.g:1160:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalExpressionDsl.g:1167:1: rule__And__Group__0__Impl : ( ruleBitwiseOR ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1171:1: ( ( ruleBitwiseOR ) )
            // InternalExpressionDsl.g:1172:1: ( ruleBitwiseOR )
            {
            // InternalExpressionDsl.g:1172:1: ( ruleBitwiseOR )
            // InternalExpressionDsl.g:1173:2: ruleBitwiseOR
            {
             before(grammarAccess.getAndAccess().getBitwiseORParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleBitwiseOR();

            state._fsp--;

             after(grammarAccess.getAndAccess().getBitwiseORParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalExpressionDsl.g:1182:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1186:1: ( rule__And__Group__1__Impl )
            // InternalExpressionDsl.g:1187:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalExpressionDsl.g:1193:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1197:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalExpressionDsl.g:1198:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:1198:1: ( ( rule__And__Group_1__0 )* )
            // InternalExpressionDsl.g:1199:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalExpressionDsl.g:1200:2: ( rule__And__Group_1__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==32) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalExpressionDsl.g:1200:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalExpressionDsl.g:1209:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1213:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalExpressionDsl.g:1214:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalExpressionDsl.g:1221:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1225:1: ( ( () ) )
            // InternalExpressionDsl.g:1226:1: ( () )
            {
            // InternalExpressionDsl.g:1226:1: ( () )
            // InternalExpressionDsl.g:1227:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalExpressionDsl.g:1228:2: ()
            // InternalExpressionDsl.g:1228:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalExpressionDsl.g:1236:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1240:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalExpressionDsl.g:1241:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalExpressionDsl.g:1248:1: rule__And__Group_1__1__Impl : ( '&&' ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1252:1: ( ( '&&' ) )
            // InternalExpressionDsl.g:1253:1: ( '&&' )
            {
            // InternalExpressionDsl.g:1253:1: ( '&&' )
            // InternalExpressionDsl.g:1254:2: '&&'
            {
             before(grammarAccess.getAndAccess().getAmpersandAmpersandKeyword_1_1()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getAmpersandAmpersandKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalExpressionDsl.g:1263:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1267:1: ( rule__And__Group_1__2__Impl )
            // InternalExpressionDsl.g:1268:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalExpressionDsl.g:1274:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1278:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:1279:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:1279:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:1280:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:1281:2: ( rule__And__RightAssignment_1_2 )
            // InternalExpressionDsl.g:1281:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__BitwiseOR__Group__0"
    // InternalExpressionDsl.g:1290:1: rule__BitwiseOR__Group__0 : rule__BitwiseOR__Group__0__Impl rule__BitwiseOR__Group__1 ;
    public final void rule__BitwiseOR__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1294:1: ( rule__BitwiseOR__Group__0__Impl rule__BitwiseOR__Group__1 )
            // InternalExpressionDsl.g:1295:2: rule__BitwiseOR__Group__0__Impl rule__BitwiseOR__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__BitwiseOR__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseOR__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group__0"


    // $ANTLR start "rule__BitwiseOR__Group__0__Impl"
    // InternalExpressionDsl.g:1302:1: rule__BitwiseOR__Group__0__Impl : ( ruleBitwiseXOR ) ;
    public final void rule__BitwiseOR__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1306:1: ( ( ruleBitwiseXOR ) )
            // InternalExpressionDsl.g:1307:1: ( ruleBitwiseXOR )
            {
            // InternalExpressionDsl.g:1307:1: ( ruleBitwiseXOR )
            // InternalExpressionDsl.g:1308:2: ruleBitwiseXOR
            {
             before(grammarAccess.getBitwiseORAccess().getBitwiseXORParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleBitwiseXOR();

            state._fsp--;

             after(grammarAccess.getBitwiseORAccess().getBitwiseXORParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group__0__Impl"


    // $ANTLR start "rule__BitwiseOR__Group__1"
    // InternalExpressionDsl.g:1317:1: rule__BitwiseOR__Group__1 : rule__BitwiseOR__Group__1__Impl ;
    public final void rule__BitwiseOR__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1321:1: ( rule__BitwiseOR__Group__1__Impl )
            // InternalExpressionDsl.g:1322:2: rule__BitwiseOR__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseOR__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group__1"


    // $ANTLR start "rule__BitwiseOR__Group__1__Impl"
    // InternalExpressionDsl.g:1328:1: rule__BitwiseOR__Group__1__Impl : ( ( rule__BitwiseOR__Group_1__0 )* ) ;
    public final void rule__BitwiseOR__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1332:1: ( ( ( rule__BitwiseOR__Group_1__0 )* ) )
            // InternalExpressionDsl.g:1333:1: ( ( rule__BitwiseOR__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:1333:1: ( ( rule__BitwiseOR__Group_1__0 )* )
            // InternalExpressionDsl.g:1334:2: ( rule__BitwiseOR__Group_1__0 )*
            {
             before(grammarAccess.getBitwiseORAccess().getGroup_1()); 
            // InternalExpressionDsl.g:1335:2: ( rule__BitwiseOR__Group_1__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==40) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalExpressionDsl.g:1335:3: rule__BitwiseOR__Group_1__0
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__BitwiseOR__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getBitwiseORAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group__1__Impl"


    // $ANTLR start "rule__BitwiseOR__Group_1__0"
    // InternalExpressionDsl.g:1344:1: rule__BitwiseOR__Group_1__0 : rule__BitwiseOR__Group_1__0__Impl rule__BitwiseOR__Group_1__1 ;
    public final void rule__BitwiseOR__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1348:1: ( rule__BitwiseOR__Group_1__0__Impl rule__BitwiseOR__Group_1__1 )
            // InternalExpressionDsl.g:1349:2: rule__BitwiseOR__Group_1__0__Impl rule__BitwiseOR__Group_1__1
            {
            pushFollow(FOLLOW_10);
            rule__BitwiseOR__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseOR__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group_1__0"


    // $ANTLR start "rule__BitwiseOR__Group_1__0__Impl"
    // InternalExpressionDsl.g:1356:1: rule__BitwiseOR__Group_1__0__Impl : ( () ) ;
    public final void rule__BitwiseOR__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1360:1: ( ( () ) )
            // InternalExpressionDsl.g:1361:1: ( () )
            {
            // InternalExpressionDsl.g:1361:1: ( () )
            // InternalExpressionDsl.g:1362:2: ()
            {
             before(grammarAccess.getBitwiseORAccess().getLogicalOperatorLeftAction_1_0()); 
            // InternalExpressionDsl.g:1363:2: ()
            // InternalExpressionDsl.g:1363:3: 
            {
            }

             after(grammarAccess.getBitwiseORAccess().getLogicalOperatorLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group_1__0__Impl"


    // $ANTLR start "rule__BitwiseOR__Group_1__1"
    // InternalExpressionDsl.g:1371:1: rule__BitwiseOR__Group_1__1 : rule__BitwiseOR__Group_1__1__Impl rule__BitwiseOR__Group_1__2 ;
    public final void rule__BitwiseOR__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1375:1: ( rule__BitwiseOR__Group_1__1__Impl rule__BitwiseOR__Group_1__2 )
            // InternalExpressionDsl.g:1376:2: rule__BitwiseOR__Group_1__1__Impl rule__BitwiseOR__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__BitwiseOR__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseOR__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group_1__1"


    // $ANTLR start "rule__BitwiseOR__Group_1__1__Impl"
    // InternalExpressionDsl.g:1383:1: rule__BitwiseOR__Group_1__1__Impl : ( ( rule__BitwiseOR__OpAssignment_1_1 ) ) ;
    public final void rule__BitwiseOR__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1387:1: ( ( ( rule__BitwiseOR__OpAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:1388:1: ( ( rule__BitwiseOR__OpAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:1388:1: ( ( rule__BitwiseOR__OpAssignment_1_1 ) )
            // InternalExpressionDsl.g:1389:2: ( rule__BitwiseOR__OpAssignment_1_1 )
            {
             before(grammarAccess.getBitwiseORAccess().getOpAssignment_1_1()); 
            // InternalExpressionDsl.g:1390:2: ( rule__BitwiseOR__OpAssignment_1_1 )
            // InternalExpressionDsl.g:1390:3: rule__BitwiseOR__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseOR__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseORAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group_1__1__Impl"


    // $ANTLR start "rule__BitwiseOR__Group_1__2"
    // InternalExpressionDsl.g:1398:1: rule__BitwiseOR__Group_1__2 : rule__BitwiseOR__Group_1__2__Impl ;
    public final void rule__BitwiseOR__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1402:1: ( rule__BitwiseOR__Group_1__2__Impl )
            // InternalExpressionDsl.g:1403:2: rule__BitwiseOR__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseOR__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group_1__2"


    // $ANTLR start "rule__BitwiseOR__Group_1__2__Impl"
    // InternalExpressionDsl.g:1409:1: rule__BitwiseOR__Group_1__2__Impl : ( ( rule__BitwiseOR__RightAssignment_1_2 ) ) ;
    public final void rule__BitwiseOR__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1413:1: ( ( ( rule__BitwiseOR__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:1414:1: ( ( rule__BitwiseOR__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:1414:1: ( ( rule__BitwiseOR__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:1415:2: ( rule__BitwiseOR__RightAssignment_1_2 )
            {
             before(grammarAccess.getBitwiseORAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:1416:2: ( rule__BitwiseOR__RightAssignment_1_2 )
            // InternalExpressionDsl.g:1416:3: rule__BitwiseOR__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseOR__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseORAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__Group_1__2__Impl"


    // $ANTLR start "rule__BitwiseXOR__Group__0"
    // InternalExpressionDsl.g:1425:1: rule__BitwiseXOR__Group__0 : rule__BitwiseXOR__Group__0__Impl rule__BitwiseXOR__Group__1 ;
    public final void rule__BitwiseXOR__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1429:1: ( rule__BitwiseXOR__Group__0__Impl rule__BitwiseXOR__Group__1 )
            // InternalExpressionDsl.g:1430:2: rule__BitwiseXOR__Group__0__Impl rule__BitwiseXOR__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__BitwiseXOR__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseXOR__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group__0"


    // $ANTLR start "rule__BitwiseXOR__Group__0__Impl"
    // InternalExpressionDsl.g:1437:1: rule__BitwiseXOR__Group__0__Impl : ( ruleBitwiseAnd ) ;
    public final void rule__BitwiseXOR__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1441:1: ( ( ruleBitwiseAnd ) )
            // InternalExpressionDsl.g:1442:1: ( ruleBitwiseAnd )
            {
            // InternalExpressionDsl.g:1442:1: ( ruleBitwiseAnd )
            // InternalExpressionDsl.g:1443:2: ruleBitwiseAnd
            {
             before(grammarAccess.getBitwiseXORAccess().getBitwiseAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleBitwiseAnd();

            state._fsp--;

             after(grammarAccess.getBitwiseXORAccess().getBitwiseAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group__0__Impl"


    // $ANTLR start "rule__BitwiseXOR__Group__1"
    // InternalExpressionDsl.g:1452:1: rule__BitwiseXOR__Group__1 : rule__BitwiseXOR__Group__1__Impl ;
    public final void rule__BitwiseXOR__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1456:1: ( rule__BitwiseXOR__Group__1__Impl )
            // InternalExpressionDsl.g:1457:2: rule__BitwiseXOR__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseXOR__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group__1"


    // $ANTLR start "rule__BitwiseXOR__Group__1__Impl"
    // InternalExpressionDsl.g:1463:1: rule__BitwiseXOR__Group__1__Impl : ( ( rule__BitwiseXOR__Group_1__0 )* ) ;
    public final void rule__BitwiseXOR__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1467:1: ( ( ( rule__BitwiseXOR__Group_1__0 )* ) )
            // InternalExpressionDsl.g:1468:1: ( ( rule__BitwiseXOR__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:1468:1: ( ( rule__BitwiseXOR__Group_1__0 )* )
            // InternalExpressionDsl.g:1469:2: ( rule__BitwiseXOR__Group_1__0 )*
            {
             before(grammarAccess.getBitwiseXORAccess().getGroup_1()); 
            // InternalExpressionDsl.g:1470:2: ( rule__BitwiseXOR__Group_1__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==41) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalExpressionDsl.g:1470:3: rule__BitwiseXOR__Group_1__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__BitwiseXOR__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getBitwiseXORAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group__1__Impl"


    // $ANTLR start "rule__BitwiseXOR__Group_1__0"
    // InternalExpressionDsl.g:1479:1: rule__BitwiseXOR__Group_1__0 : rule__BitwiseXOR__Group_1__0__Impl rule__BitwiseXOR__Group_1__1 ;
    public final void rule__BitwiseXOR__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1483:1: ( rule__BitwiseXOR__Group_1__0__Impl rule__BitwiseXOR__Group_1__1 )
            // InternalExpressionDsl.g:1484:2: rule__BitwiseXOR__Group_1__0__Impl rule__BitwiseXOR__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__BitwiseXOR__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseXOR__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group_1__0"


    // $ANTLR start "rule__BitwiseXOR__Group_1__0__Impl"
    // InternalExpressionDsl.g:1491:1: rule__BitwiseXOR__Group_1__0__Impl : ( () ) ;
    public final void rule__BitwiseXOR__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1495:1: ( ( () ) )
            // InternalExpressionDsl.g:1496:1: ( () )
            {
            // InternalExpressionDsl.g:1496:1: ( () )
            // InternalExpressionDsl.g:1497:2: ()
            {
             before(grammarAccess.getBitwiseXORAccess().getLogicalOperatorLeftAction_1_0()); 
            // InternalExpressionDsl.g:1498:2: ()
            // InternalExpressionDsl.g:1498:3: 
            {
            }

             after(grammarAccess.getBitwiseXORAccess().getLogicalOperatorLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group_1__0__Impl"


    // $ANTLR start "rule__BitwiseXOR__Group_1__1"
    // InternalExpressionDsl.g:1506:1: rule__BitwiseXOR__Group_1__1 : rule__BitwiseXOR__Group_1__1__Impl rule__BitwiseXOR__Group_1__2 ;
    public final void rule__BitwiseXOR__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1510:1: ( rule__BitwiseXOR__Group_1__1__Impl rule__BitwiseXOR__Group_1__2 )
            // InternalExpressionDsl.g:1511:2: rule__BitwiseXOR__Group_1__1__Impl rule__BitwiseXOR__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__BitwiseXOR__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseXOR__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group_1__1"


    // $ANTLR start "rule__BitwiseXOR__Group_1__1__Impl"
    // InternalExpressionDsl.g:1518:1: rule__BitwiseXOR__Group_1__1__Impl : ( ( rule__BitwiseXOR__OpAssignment_1_1 ) ) ;
    public final void rule__BitwiseXOR__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1522:1: ( ( ( rule__BitwiseXOR__OpAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:1523:1: ( ( rule__BitwiseXOR__OpAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:1523:1: ( ( rule__BitwiseXOR__OpAssignment_1_1 ) )
            // InternalExpressionDsl.g:1524:2: ( rule__BitwiseXOR__OpAssignment_1_1 )
            {
             before(grammarAccess.getBitwiseXORAccess().getOpAssignment_1_1()); 
            // InternalExpressionDsl.g:1525:2: ( rule__BitwiseXOR__OpAssignment_1_1 )
            // InternalExpressionDsl.g:1525:3: rule__BitwiseXOR__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseXOR__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseXORAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group_1__1__Impl"


    // $ANTLR start "rule__BitwiseXOR__Group_1__2"
    // InternalExpressionDsl.g:1533:1: rule__BitwiseXOR__Group_1__2 : rule__BitwiseXOR__Group_1__2__Impl ;
    public final void rule__BitwiseXOR__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1537:1: ( rule__BitwiseXOR__Group_1__2__Impl )
            // InternalExpressionDsl.g:1538:2: rule__BitwiseXOR__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseXOR__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group_1__2"


    // $ANTLR start "rule__BitwiseXOR__Group_1__2__Impl"
    // InternalExpressionDsl.g:1544:1: rule__BitwiseXOR__Group_1__2__Impl : ( ( rule__BitwiseXOR__RightAssignment_1_2 ) ) ;
    public final void rule__BitwiseXOR__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1548:1: ( ( ( rule__BitwiseXOR__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:1549:1: ( ( rule__BitwiseXOR__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:1549:1: ( ( rule__BitwiseXOR__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:1550:2: ( rule__BitwiseXOR__RightAssignment_1_2 )
            {
             before(grammarAccess.getBitwiseXORAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:1551:2: ( rule__BitwiseXOR__RightAssignment_1_2 )
            // InternalExpressionDsl.g:1551:3: rule__BitwiseXOR__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseXOR__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseXORAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__Group_1__2__Impl"


    // $ANTLR start "rule__BitwiseAnd__Group__0"
    // InternalExpressionDsl.g:1560:1: rule__BitwiseAnd__Group__0 : rule__BitwiseAnd__Group__0__Impl rule__BitwiseAnd__Group__1 ;
    public final void rule__BitwiseAnd__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1564:1: ( rule__BitwiseAnd__Group__0__Impl rule__BitwiseAnd__Group__1 )
            // InternalExpressionDsl.g:1565:2: rule__BitwiseAnd__Group__0__Impl rule__BitwiseAnd__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__BitwiseAnd__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseAnd__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group__0"


    // $ANTLR start "rule__BitwiseAnd__Group__0__Impl"
    // InternalExpressionDsl.g:1572:1: rule__BitwiseAnd__Group__0__Impl : ( ruleEquality ) ;
    public final void rule__BitwiseAnd__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1576:1: ( ( ruleEquality ) )
            // InternalExpressionDsl.g:1577:1: ( ruleEquality )
            {
            // InternalExpressionDsl.g:1577:1: ( ruleEquality )
            // InternalExpressionDsl.g:1578:2: ruleEquality
            {
             before(grammarAccess.getBitwiseAndAccess().getEqualityParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleEquality();

            state._fsp--;

             after(grammarAccess.getBitwiseAndAccess().getEqualityParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group__0__Impl"


    // $ANTLR start "rule__BitwiseAnd__Group__1"
    // InternalExpressionDsl.g:1587:1: rule__BitwiseAnd__Group__1 : rule__BitwiseAnd__Group__1__Impl ;
    public final void rule__BitwiseAnd__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1591:1: ( rule__BitwiseAnd__Group__1__Impl )
            // InternalExpressionDsl.g:1592:2: rule__BitwiseAnd__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseAnd__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group__1"


    // $ANTLR start "rule__BitwiseAnd__Group__1__Impl"
    // InternalExpressionDsl.g:1598:1: rule__BitwiseAnd__Group__1__Impl : ( ( rule__BitwiseAnd__Group_1__0 )* ) ;
    public final void rule__BitwiseAnd__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1602:1: ( ( ( rule__BitwiseAnd__Group_1__0 )* ) )
            // InternalExpressionDsl.g:1603:1: ( ( rule__BitwiseAnd__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:1603:1: ( ( rule__BitwiseAnd__Group_1__0 )* )
            // InternalExpressionDsl.g:1604:2: ( rule__BitwiseAnd__Group_1__0 )*
            {
             before(grammarAccess.getBitwiseAndAccess().getGroup_1()); 
            // InternalExpressionDsl.g:1605:2: ( rule__BitwiseAnd__Group_1__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==42) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalExpressionDsl.g:1605:3: rule__BitwiseAnd__Group_1__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__BitwiseAnd__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getBitwiseAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group__1__Impl"


    // $ANTLR start "rule__BitwiseAnd__Group_1__0"
    // InternalExpressionDsl.g:1614:1: rule__BitwiseAnd__Group_1__0 : rule__BitwiseAnd__Group_1__0__Impl rule__BitwiseAnd__Group_1__1 ;
    public final void rule__BitwiseAnd__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1618:1: ( rule__BitwiseAnd__Group_1__0__Impl rule__BitwiseAnd__Group_1__1 )
            // InternalExpressionDsl.g:1619:2: rule__BitwiseAnd__Group_1__0__Impl rule__BitwiseAnd__Group_1__1
            {
            pushFollow(FOLLOW_14);
            rule__BitwiseAnd__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseAnd__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group_1__0"


    // $ANTLR start "rule__BitwiseAnd__Group_1__0__Impl"
    // InternalExpressionDsl.g:1626:1: rule__BitwiseAnd__Group_1__0__Impl : ( () ) ;
    public final void rule__BitwiseAnd__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1630:1: ( ( () ) )
            // InternalExpressionDsl.g:1631:1: ( () )
            {
            // InternalExpressionDsl.g:1631:1: ( () )
            // InternalExpressionDsl.g:1632:2: ()
            {
             before(grammarAccess.getBitwiseAndAccess().getLogicalOperatorLeftAction_1_0()); 
            // InternalExpressionDsl.g:1633:2: ()
            // InternalExpressionDsl.g:1633:3: 
            {
            }

             after(grammarAccess.getBitwiseAndAccess().getLogicalOperatorLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group_1__0__Impl"


    // $ANTLR start "rule__BitwiseAnd__Group_1__1"
    // InternalExpressionDsl.g:1641:1: rule__BitwiseAnd__Group_1__1 : rule__BitwiseAnd__Group_1__1__Impl rule__BitwiseAnd__Group_1__2 ;
    public final void rule__BitwiseAnd__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1645:1: ( rule__BitwiseAnd__Group_1__1__Impl rule__BitwiseAnd__Group_1__2 )
            // InternalExpressionDsl.g:1646:2: rule__BitwiseAnd__Group_1__1__Impl rule__BitwiseAnd__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__BitwiseAnd__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseAnd__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group_1__1"


    // $ANTLR start "rule__BitwiseAnd__Group_1__1__Impl"
    // InternalExpressionDsl.g:1653:1: rule__BitwiseAnd__Group_1__1__Impl : ( ( rule__BitwiseAnd__OpAssignment_1_1 ) ) ;
    public final void rule__BitwiseAnd__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1657:1: ( ( ( rule__BitwiseAnd__OpAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:1658:1: ( ( rule__BitwiseAnd__OpAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:1658:1: ( ( rule__BitwiseAnd__OpAssignment_1_1 ) )
            // InternalExpressionDsl.g:1659:2: ( rule__BitwiseAnd__OpAssignment_1_1 )
            {
             before(grammarAccess.getBitwiseAndAccess().getOpAssignment_1_1()); 
            // InternalExpressionDsl.g:1660:2: ( rule__BitwiseAnd__OpAssignment_1_1 )
            // InternalExpressionDsl.g:1660:3: rule__BitwiseAnd__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseAnd__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseAndAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group_1__1__Impl"


    // $ANTLR start "rule__BitwiseAnd__Group_1__2"
    // InternalExpressionDsl.g:1668:1: rule__BitwiseAnd__Group_1__2 : rule__BitwiseAnd__Group_1__2__Impl ;
    public final void rule__BitwiseAnd__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1672:1: ( rule__BitwiseAnd__Group_1__2__Impl )
            // InternalExpressionDsl.g:1673:2: rule__BitwiseAnd__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseAnd__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group_1__2"


    // $ANTLR start "rule__BitwiseAnd__Group_1__2__Impl"
    // InternalExpressionDsl.g:1679:1: rule__BitwiseAnd__Group_1__2__Impl : ( ( rule__BitwiseAnd__RightAssignment_1_2 ) ) ;
    public final void rule__BitwiseAnd__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1683:1: ( ( ( rule__BitwiseAnd__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:1684:1: ( ( rule__BitwiseAnd__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:1684:1: ( ( rule__BitwiseAnd__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:1685:2: ( rule__BitwiseAnd__RightAssignment_1_2 )
            {
             before(grammarAccess.getBitwiseAndAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:1686:2: ( rule__BitwiseAnd__RightAssignment_1_2 )
            // InternalExpressionDsl.g:1686:3: rule__BitwiseAnd__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseAnd__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__Group_1__2__Impl"


    // $ANTLR start "rule__Equality__Group__0"
    // InternalExpressionDsl.g:1695:1: rule__Equality__Group__0 : rule__Equality__Group__0__Impl rule__Equality__Group__1 ;
    public final void rule__Equality__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1699:1: ( rule__Equality__Group__0__Impl rule__Equality__Group__1 )
            // InternalExpressionDsl.g:1700:2: rule__Equality__Group__0__Impl rule__Equality__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Equality__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equality__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group__0"


    // $ANTLR start "rule__Equality__Group__0__Impl"
    // InternalExpressionDsl.g:1707:1: rule__Equality__Group__0__Impl : ( ruleComparison ) ;
    public final void rule__Equality__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1711:1: ( ( ruleComparison ) )
            // InternalExpressionDsl.g:1712:1: ( ruleComparison )
            {
            // InternalExpressionDsl.g:1712:1: ( ruleComparison )
            // InternalExpressionDsl.g:1713:2: ruleComparison
            {
             before(grammarAccess.getEqualityAccess().getComparisonParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleComparison();

            state._fsp--;

             after(grammarAccess.getEqualityAccess().getComparisonParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group__0__Impl"


    // $ANTLR start "rule__Equality__Group__1"
    // InternalExpressionDsl.g:1722:1: rule__Equality__Group__1 : rule__Equality__Group__1__Impl ;
    public final void rule__Equality__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1726:1: ( rule__Equality__Group__1__Impl )
            // InternalExpressionDsl.g:1727:2: rule__Equality__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equality__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group__1"


    // $ANTLR start "rule__Equality__Group__1__Impl"
    // InternalExpressionDsl.g:1733:1: rule__Equality__Group__1__Impl : ( ( rule__Equality__Group_1__0 )* ) ;
    public final void rule__Equality__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1737:1: ( ( ( rule__Equality__Group_1__0 )* ) )
            // InternalExpressionDsl.g:1738:1: ( ( rule__Equality__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:1738:1: ( ( rule__Equality__Group_1__0 )* )
            // InternalExpressionDsl.g:1739:2: ( rule__Equality__Group_1__0 )*
            {
             before(grammarAccess.getEqualityAccess().getGroup_1()); 
            // InternalExpressionDsl.g:1740:2: ( rule__Equality__Group_1__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=14 && LA19_0<=15)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalExpressionDsl.g:1740:3: rule__Equality__Group_1__0
            	    {
            	    pushFollow(FOLLOW_17);
            	    rule__Equality__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getEqualityAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group__1__Impl"


    // $ANTLR start "rule__Equality__Group_1__0"
    // InternalExpressionDsl.g:1749:1: rule__Equality__Group_1__0 : rule__Equality__Group_1__0__Impl rule__Equality__Group_1__1 ;
    public final void rule__Equality__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1753:1: ( rule__Equality__Group_1__0__Impl rule__Equality__Group_1__1 )
            // InternalExpressionDsl.g:1754:2: rule__Equality__Group_1__0__Impl rule__Equality__Group_1__1
            {
            pushFollow(FOLLOW_16);
            rule__Equality__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equality__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__0"


    // $ANTLR start "rule__Equality__Group_1__0__Impl"
    // InternalExpressionDsl.g:1761:1: rule__Equality__Group_1__0__Impl : ( () ) ;
    public final void rule__Equality__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1765:1: ( ( () ) )
            // InternalExpressionDsl.g:1766:1: ( () )
            {
            // InternalExpressionDsl.g:1766:1: ( () )
            // InternalExpressionDsl.g:1767:2: ()
            {
             before(grammarAccess.getEqualityAccess().getEqualityLeftAction_1_0()); 
            // InternalExpressionDsl.g:1768:2: ()
            // InternalExpressionDsl.g:1768:3: 
            {
            }

             after(grammarAccess.getEqualityAccess().getEqualityLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__0__Impl"


    // $ANTLR start "rule__Equality__Group_1__1"
    // InternalExpressionDsl.g:1776:1: rule__Equality__Group_1__1 : rule__Equality__Group_1__1__Impl rule__Equality__Group_1__2 ;
    public final void rule__Equality__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1780:1: ( rule__Equality__Group_1__1__Impl rule__Equality__Group_1__2 )
            // InternalExpressionDsl.g:1781:2: rule__Equality__Group_1__1__Impl rule__Equality__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__Equality__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equality__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__1"


    // $ANTLR start "rule__Equality__Group_1__1__Impl"
    // InternalExpressionDsl.g:1788:1: rule__Equality__Group_1__1__Impl : ( ( rule__Equality__OpAssignment_1_1 ) ) ;
    public final void rule__Equality__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1792:1: ( ( ( rule__Equality__OpAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:1793:1: ( ( rule__Equality__OpAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:1793:1: ( ( rule__Equality__OpAssignment_1_1 ) )
            // InternalExpressionDsl.g:1794:2: ( rule__Equality__OpAssignment_1_1 )
            {
             before(grammarAccess.getEqualityAccess().getOpAssignment_1_1()); 
            // InternalExpressionDsl.g:1795:2: ( rule__Equality__OpAssignment_1_1 )
            // InternalExpressionDsl.g:1795:3: rule__Equality__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Equality__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__1__Impl"


    // $ANTLR start "rule__Equality__Group_1__2"
    // InternalExpressionDsl.g:1803:1: rule__Equality__Group_1__2 : rule__Equality__Group_1__2__Impl ;
    public final void rule__Equality__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1807:1: ( rule__Equality__Group_1__2__Impl )
            // InternalExpressionDsl.g:1808:2: rule__Equality__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equality__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__2"


    // $ANTLR start "rule__Equality__Group_1__2__Impl"
    // InternalExpressionDsl.g:1814:1: rule__Equality__Group_1__2__Impl : ( ( rule__Equality__RightAssignment_1_2 ) ) ;
    public final void rule__Equality__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1818:1: ( ( ( rule__Equality__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:1819:1: ( ( rule__Equality__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:1819:1: ( ( rule__Equality__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:1820:2: ( rule__Equality__RightAssignment_1_2 )
            {
             before(grammarAccess.getEqualityAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:1821:2: ( rule__Equality__RightAssignment_1_2 )
            // InternalExpressionDsl.g:1821:3: rule__Equality__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Equality__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__2__Impl"


    // $ANTLR start "rule__Comparison__Group__0"
    // InternalExpressionDsl.g:1830:1: rule__Comparison__Group__0 : rule__Comparison__Group__0__Impl rule__Comparison__Group__1 ;
    public final void rule__Comparison__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1834:1: ( rule__Comparison__Group__0__Impl rule__Comparison__Group__1 )
            // InternalExpressionDsl.g:1835:2: rule__Comparison__Group__0__Impl rule__Comparison__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Comparison__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group__0"


    // $ANTLR start "rule__Comparison__Group__0__Impl"
    // InternalExpressionDsl.g:1842:1: rule__Comparison__Group__0__Impl : ( ruleBitwiseShift ) ;
    public final void rule__Comparison__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1846:1: ( ( ruleBitwiseShift ) )
            // InternalExpressionDsl.g:1847:1: ( ruleBitwiseShift )
            {
            // InternalExpressionDsl.g:1847:1: ( ruleBitwiseShift )
            // InternalExpressionDsl.g:1848:2: ruleBitwiseShift
            {
             before(grammarAccess.getComparisonAccess().getBitwiseShiftParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleBitwiseShift();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getBitwiseShiftParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group__0__Impl"


    // $ANTLR start "rule__Comparison__Group__1"
    // InternalExpressionDsl.g:1857:1: rule__Comparison__Group__1 : rule__Comparison__Group__1__Impl ;
    public final void rule__Comparison__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1861:1: ( rule__Comparison__Group__1__Impl )
            // InternalExpressionDsl.g:1862:2: rule__Comparison__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group__1"


    // $ANTLR start "rule__Comparison__Group__1__Impl"
    // InternalExpressionDsl.g:1868:1: rule__Comparison__Group__1__Impl : ( ( rule__Comparison__Group_1__0 )* ) ;
    public final void rule__Comparison__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1872:1: ( ( ( rule__Comparison__Group_1__0 )* ) )
            // InternalExpressionDsl.g:1873:1: ( ( rule__Comparison__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:1873:1: ( ( rule__Comparison__Group_1__0 )* )
            // InternalExpressionDsl.g:1874:2: ( rule__Comparison__Group_1__0 )*
            {
             before(grammarAccess.getComparisonAccess().getGroup_1()); 
            // InternalExpressionDsl.g:1875:2: ( rule__Comparison__Group_1__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=16 && LA20_0<=19)) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalExpressionDsl.g:1875:3: rule__Comparison__Group_1__0
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__Comparison__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getComparisonAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group__1__Impl"


    // $ANTLR start "rule__Comparison__Group_1__0"
    // InternalExpressionDsl.g:1884:1: rule__Comparison__Group_1__0 : rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1 ;
    public final void rule__Comparison__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1888:1: ( rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1 )
            // InternalExpressionDsl.g:1889:2: rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1
            {
            pushFollow(FOLLOW_18);
            rule__Comparison__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__0"


    // $ANTLR start "rule__Comparison__Group_1__0__Impl"
    // InternalExpressionDsl.g:1896:1: rule__Comparison__Group_1__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1900:1: ( ( () ) )
            // InternalExpressionDsl.g:1901:1: ( () )
            {
            // InternalExpressionDsl.g:1901:1: ( () )
            // InternalExpressionDsl.g:1902:2: ()
            {
             before(grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0()); 
            // InternalExpressionDsl.g:1903:2: ()
            // InternalExpressionDsl.g:1903:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__0__Impl"


    // $ANTLR start "rule__Comparison__Group_1__1"
    // InternalExpressionDsl.g:1911:1: rule__Comparison__Group_1__1 : rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2 ;
    public final void rule__Comparison__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1915:1: ( rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2 )
            // InternalExpressionDsl.g:1916:2: rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__Comparison__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__1"


    // $ANTLR start "rule__Comparison__Group_1__1__Impl"
    // InternalExpressionDsl.g:1923:1: rule__Comparison__Group_1__1__Impl : ( ( rule__Comparison__OpAssignment_1_1 ) ) ;
    public final void rule__Comparison__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1927:1: ( ( ( rule__Comparison__OpAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:1928:1: ( ( rule__Comparison__OpAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:1928:1: ( ( rule__Comparison__OpAssignment_1_1 ) )
            // InternalExpressionDsl.g:1929:2: ( rule__Comparison__OpAssignment_1_1 )
            {
             before(grammarAccess.getComparisonAccess().getOpAssignment_1_1()); 
            // InternalExpressionDsl.g:1930:2: ( rule__Comparison__OpAssignment_1_1 )
            // InternalExpressionDsl.g:1930:3: rule__Comparison__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__1__Impl"


    // $ANTLR start "rule__Comparison__Group_1__2"
    // InternalExpressionDsl.g:1938:1: rule__Comparison__Group_1__2 : rule__Comparison__Group_1__2__Impl ;
    public final void rule__Comparison__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1942:1: ( rule__Comparison__Group_1__2__Impl )
            // InternalExpressionDsl.g:1943:2: rule__Comparison__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__2"


    // $ANTLR start "rule__Comparison__Group_1__2__Impl"
    // InternalExpressionDsl.g:1949:1: rule__Comparison__Group_1__2__Impl : ( ( rule__Comparison__RightAssignment_1_2 ) ) ;
    public final void rule__Comparison__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1953:1: ( ( ( rule__Comparison__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:1954:1: ( ( rule__Comparison__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:1954:1: ( ( rule__Comparison__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:1955:2: ( rule__Comparison__RightAssignment_1_2 )
            {
             before(grammarAccess.getComparisonAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:1956:2: ( rule__Comparison__RightAssignment_1_2 )
            // InternalExpressionDsl.g:1956:3: rule__Comparison__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__2__Impl"


    // $ANTLR start "rule__BitwiseShift__Group__0"
    // InternalExpressionDsl.g:1965:1: rule__BitwiseShift__Group__0 : rule__BitwiseShift__Group__0__Impl rule__BitwiseShift__Group__1 ;
    public final void rule__BitwiseShift__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1969:1: ( rule__BitwiseShift__Group__0__Impl rule__BitwiseShift__Group__1 )
            // InternalExpressionDsl.g:1970:2: rule__BitwiseShift__Group__0__Impl rule__BitwiseShift__Group__1
            {
            pushFollow(FOLLOW_20);
            rule__BitwiseShift__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseShift__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group__0"


    // $ANTLR start "rule__BitwiseShift__Group__0__Impl"
    // InternalExpressionDsl.g:1977:1: rule__BitwiseShift__Group__0__Impl : ( rulePlusOrMinus ) ;
    public final void rule__BitwiseShift__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1981:1: ( ( rulePlusOrMinus ) )
            // InternalExpressionDsl.g:1982:1: ( rulePlusOrMinus )
            {
            // InternalExpressionDsl.g:1982:1: ( rulePlusOrMinus )
            // InternalExpressionDsl.g:1983:2: rulePlusOrMinus
            {
             before(grammarAccess.getBitwiseShiftAccess().getPlusOrMinusParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePlusOrMinus();

            state._fsp--;

             after(grammarAccess.getBitwiseShiftAccess().getPlusOrMinusParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group__0__Impl"


    // $ANTLR start "rule__BitwiseShift__Group__1"
    // InternalExpressionDsl.g:1992:1: rule__BitwiseShift__Group__1 : rule__BitwiseShift__Group__1__Impl ;
    public final void rule__BitwiseShift__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:1996:1: ( rule__BitwiseShift__Group__1__Impl )
            // InternalExpressionDsl.g:1997:2: rule__BitwiseShift__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseShift__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group__1"


    // $ANTLR start "rule__BitwiseShift__Group__1__Impl"
    // InternalExpressionDsl.g:2003:1: rule__BitwiseShift__Group__1__Impl : ( ( rule__BitwiseShift__Group_1__0 )* ) ;
    public final void rule__BitwiseShift__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2007:1: ( ( ( rule__BitwiseShift__Group_1__0 )* ) )
            // InternalExpressionDsl.g:2008:1: ( ( rule__BitwiseShift__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:2008:1: ( ( rule__BitwiseShift__Group_1__0 )* )
            // InternalExpressionDsl.g:2009:2: ( rule__BitwiseShift__Group_1__0 )*
            {
             before(grammarAccess.getBitwiseShiftAccess().getGroup_1()); 
            // InternalExpressionDsl.g:2010:2: ( rule__BitwiseShift__Group_1__0 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0>=20 && LA21_0<=22)) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalExpressionDsl.g:2010:3: rule__BitwiseShift__Group_1__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__BitwiseShift__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getBitwiseShiftAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group__1__Impl"


    // $ANTLR start "rule__BitwiseShift__Group_1__0"
    // InternalExpressionDsl.g:2019:1: rule__BitwiseShift__Group_1__0 : rule__BitwiseShift__Group_1__0__Impl rule__BitwiseShift__Group_1__1 ;
    public final void rule__BitwiseShift__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2023:1: ( rule__BitwiseShift__Group_1__0__Impl rule__BitwiseShift__Group_1__1 )
            // InternalExpressionDsl.g:2024:2: rule__BitwiseShift__Group_1__0__Impl rule__BitwiseShift__Group_1__1
            {
            pushFollow(FOLLOW_20);
            rule__BitwiseShift__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseShift__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group_1__0"


    // $ANTLR start "rule__BitwiseShift__Group_1__0__Impl"
    // InternalExpressionDsl.g:2031:1: rule__BitwiseShift__Group_1__0__Impl : ( () ) ;
    public final void rule__BitwiseShift__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2035:1: ( ( () ) )
            // InternalExpressionDsl.g:2036:1: ( () )
            {
            // InternalExpressionDsl.g:2036:1: ( () )
            // InternalExpressionDsl.g:2037:2: ()
            {
             before(grammarAccess.getBitwiseShiftAccess().getShiftLeftAction_1_0()); 
            // InternalExpressionDsl.g:2038:2: ()
            // InternalExpressionDsl.g:2038:3: 
            {
            }

             after(grammarAccess.getBitwiseShiftAccess().getShiftLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group_1__0__Impl"


    // $ANTLR start "rule__BitwiseShift__Group_1__1"
    // InternalExpressionDsl.g:2046:1: rule__BitwiseShift__Group_1__1 : rule__BitwiseShift__Group_1__1__Impl rule__BitwiseShift__Group_1__2 ;
    public final void rule__BitwiseShift__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2050:1: ( rule__BitwiseShift__Group_1__1__Impl rule__BitwiseShift__Group_1__2 )
            // InternalExpressionDsl.g:2051:2: rule__BitwiseShift__Group_1__1__Impl rule__BitwiseShift__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__BitwiseShift__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BitwiseShift__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group_1__1"


    // $ANTLR start "rule__BitwiseShift__Group_1__1__Impl"
    // InternalExpressionDsl.g:2058:1: rule__BitwiseShift__Group_1__1__Impl : ( ( rule__BitwiseShift__OpAssignment_1_1 ) ) ;
    public final void rule__BitwiseShift__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2062:1: ( ( ( rule__BitwiseShift__OpAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:2063:1: ( ( rule__BitwiseShift__OpAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:2063:1: ( ( rule__BitwiseShift__OpAssignment_1_1 ) )
            // InternalExpressionDsl.g:2064:2: ( rule__BitwiseShift__OpAssignment_1_1 )
            {
             before(grammarAccess.getBitwiseShiftAccess().getOpAssignment_1_1()); 
            // InternalExpressionDsl.g:2065:2: ( rule__BitwiseShift__OpAssignment_1_1 )
            // InternalExpressionDsl.g:2065:3: rule__BitwiseShift__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseShift__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseShiftAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group_1__1__Impl"


    // $ANTLR start "rule__BitwiseShift__Group_1__2"
    // InternalExpressionDsl.g:2073:1: rule__BitwiseShift__Group_1__2 : rule__BitwiseShift__Group_1__2__Impl ;
    public final void rule__BitwiseShift__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2077:1: ( rule__BitwiseShift__Group_1__2__Impl )
            // InternalExpressionDsl.g:2078:2: rule__BitwiseShift__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseShift__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group_1__2"


    // $ANTLR start "rule__BitwiseShift__Group_1__2__Impl"
    // InternalExpressionDsl.g:2084:1: rule__BitwiseShift__Group_1__2__Impl : ( ( rule__BitwiseShift__RightAssignment_1_2 ) ) ;
    public final void rule__BitwiseShift__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2088:1: ( ( ( rule__BitwiseShift__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:2089:1: ( ( rule__BitwiseShift__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:2089:1: ( ( rule__BitwiseShift__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:2090:2: ( rule__BitwiseShift__RightAssignment_1_2 )
            {
             before(grammarAccess.getBitwiseShiftAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:2091:2: ( rule__BitwiseShift__RightAssignment_1_2 )
            // InternalExpressionDsl.g:2091:3: rule__BitwiseShift__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseShift__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseShiftAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__Group_1__2__Impl"


    // $ANTLR start "rule__PlusOrMinus__Group__0"
    // InternalExpressionDsl.g:2100:1: rule__PlusOrMinus__Group__0 : rule__PlusOrMinus__Group__0__Impl rule__PlusOrMinus__Group__1 ;
    public final void rule__PlusOrMinus__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2104:1: ( rule__PlusOrMinus__Group__0__Impl rule__PlusOrMinus__Group__1 )
            // InternalExpressionDsl.g:2105:2: rule__PlusOrMinus__Group__0__Impl rule__PlusOrMinus__Group__1
            {
            pushFollow(FOLLOW_22);
            rule__PlusOrMinus__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group__0"


    // $ANTLR start "rule__PlusOrMinus__Group__0__Impl"
    // InternalExpressionDsl.g:2112:1: rule__PlusOrMinus__Group__0__Impl : ( ruleMulOrDiv ) ;
    public final void rule__PlusOrMinus__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2116:1: ( ( ruleMulOrDiv ) )
            // InternalExpressionDsl.g:2117:1: ( ruleMulOrDiv )
            {
            // InternalExpressionDsl.g:2117:1: ( ruleMulOrDiv )
            // InternalExpressionDsl.g:2118:2: ruleMulOrDiv
            {
             before(grammarAccess.getPlusOrMinusAccess().getMulOrDivParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleMulOrDiv();

            state._fsp--;

             after(grammarAccess.getPlusOrMinusAccess().getMulOrDivParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group__0__Impl"


    // $ANTLR start "rule__PlusOrMinus__Group__1"
    // InternalExpressionDsl.g:2127:1: rule__PlusOrMinus__Group__1 : rule__PlusOrMinus__Group__1__Impl ;
    public final void rule__PlusOrMinus__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2131:1: ( rule__PlusOrMinus__Group__1__Impl )
            // InternalExpressionDsl.g:2132:2: rule__PlusOrMinus__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group__1"


    // $ANTLR start "rule__PlusOrMinus__Group__1__Impl"
    // InternalExpressionDsl.g:2138:1: rule__PlusOrMinus__Group__1__Impl : ( ( rule__PlusOrMinus__Group_1__0 )* ) ;
    public final void rule__PlusOrMinus__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2142:1: ( ( ( rule__PlusOrMinus__Group_1__0 )* ) )
            // InternalExpressionDsl.g:2143:1: ( ( rule__PlusOrMinus__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:2143:1: ( ( rule__PlusOrMinus__Group_1__0 )* )
            // InternalExpressionDsl.g:2144:2: ( rule__PlusOrMinus__Group_1__0 )*
            {
             before(grammarAccess.getPlusOrMinusAccess().getGroup_1()); 
            // InternalExpressionDsl.g:2145:2: ( rule__PlusOrMinus__Group_1__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0>=33 && LA22_0<=34)) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalExpressionDsl.g:2145:3: rule__PlusOrMinus__Group_1__0
            	    {
            	    pushFollow(FOLLOW_23);
            	    rule__PlusOrMinus__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getPlusOrMinusAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group__1__Impl"


    // $ANTLR start "rule__PlusOrMinus__Group_1__0"
    // InternalExpressionDsl.g:2154:1: rule__PlusOrMinus__Group_1__0 : rule__PlusOrMinus__Group_1__0__Impl rule__PlusOrMinus__Group_1__1 ;
    public final void rule__PlusOrMinus__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2158:1: ( rule__PlusOrMinus__Group_1__0__Impl rule__PlusOrMinus__Group_1__1 )
            // InternalExpressionDsl.g:2159:2: rule__PlusOrMinus__Group_1__0__Impl rule__PlusOrMinus__Group_1__1
            {
            pushFollow(FOLLOW_4);
            rule__PlusOrMinus__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1__0"


    // $ANTLR start "rule__PlusOrMinus__Group_1__0__Impl"
    // InternalExpressionDsl.g:2166:1: rule__PlusOrMinus__Group_1__0__Impl : ( ( rule__PlusOrMinus__Alternatives_1_0 ) ) ;
    public final void rule__PlusOrMinus__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2170:1: ( ( ( rule__PlusOrMinus__Alternatives_1_0 ) ) )
            // InternalExpressionDsl.g:2171:1: ( ( rule__PlusOrMinus__Alternatives_1_0 ) )
            {
            // InternalExpressionDsl.g:2171:1: ( ( rule__PlusOrMinus__Alternatives_1_0 ) )
            // InternalExpressionDsl.g:2172:2: ( rule__PlusOrMinus__Alternatives_1_0 )
            {
             before(grammarAccess.getPlusOrMinusAccess().getAlternatives_1_0()); 
            // InternalExpressionDsl.g:2173:2: ( rule__PlusOrMinus__Alternatives_1_0 )
            // InternalExpressionDsl.g:2173:3: rule__PlusOrMinus__Alternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Alternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getPlusOrMinusAccess().getAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1__0__Impl"


    // $ANTLR start "rule__PlusOrMinus__Group_1__1"
    // InternalExpressionDsl.g:2181:1: rule__PlusOrMinus__Group_1__1 : rule__PlusOrMinus__Group_1__1__Impl ;
    public final void rule__PlusOrMinus__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2185:1: ( rule__PlusOrMinus__Group_1__1__Impl )
            // InternalExpressionDsl.g:2186:2: rule__PlusOrMinus__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1__1"


    // $ANTLR start "rule__PlusOrMinus__Group_1__1__Impl"
    // InternalExpressionDsl.g:2192:1: rule__PlusOrMinus__Group_1__1__Impl : ( ( rule__PlusOrMinus__RightAssignment_1_1 ) ) ;
    public final void rule__PlusOrMinus__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2196:1: ( ( ( rule__PlusOrMinus__RightAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:2197:1: ( ( rule__PlusOrMinus__RightAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:2197:1: ( ( rule__PlusOrMinus__RightAssignment_1_1 ) )
            // InternalExpressionDsl.g:2198:2: ( rule__PlusOrMinus__RightAssignment_1_1 )
            {
             before(grammarAccess.getPlusOrMinusAccess().getRightAssignment_1_1()); 
            // InternalExpressionDsl.g:2199:2: ( rule__PlusOrMinus__RightAssignment_1_1 )
            // InternalExpressionDsl.g:2199:3: rule__PlusOrMinus__RightAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__RightAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getPlusOrMinusAccess().getRightAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1__1__Impl"


    // $ANTLR start "rule__PlusOrMinus__Group_1_0_0__0"
    // InternalExpressionDsl.g:2208:1: rule__PlusOrMinus__Group_1_0_0__0 : rule__PlusOrMinus__Group_1_0_0__0__Impl rule__PlusOrMinus__Group_1_0_0__1 ;
    public final void rule__PlusOrMinus__Group_1_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2212:1: ( rule__PlusOrMinus__Group_1_0_0__0__Impl rule__PlusOrMinus__Group_1_0_0__1 )
            // InternalExpressionDsl.g:2213:2: rule__PlusOrMinus__Group_1_0_0__0__Impl rule__PlusOrMinus__Group_1_0_0__1
            {
            pushFollow(FOLLOW_24);
            rule__PlusOrMinus__Group_1_0_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group_1_0_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1_0_0__0"


    // $ANTLR start "rule__PlusOrMinus__Group_1_0_0__0__Impl"
    // InternalExpressionDsl.g:2220:1: rule__PlusOrMinus__Group_1_0_0__0__Impl : ( () ) ;
    public final void rule__PlusOrMinus__Group_1_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2224:1: ( ( () ) )
            // InternalExpressionDsl.g:2225:1: ( () )
            {
            // InternalExpressionDsl.g:2225:1: ( () )
            // InternalExpressionDsl.g:2226:2: ()
            {
             before(grammarAccess.getPlusOrMinusAccess().getPlusLeftAction_1_0_0_0()); 
            // InternalExpressionDsl.g:2227:2: ()
            // InternalExpressionDsl.g:2227:3: 
            {
            }

             after(grammarAccess.getPlusOrMinusAccess().getPlusLeftAction_1_0_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1_0_0__0__Impl"


    // $ANTLR start "rule__PlusOrMinus__Group_1_0_0__1"
    // InternalExpressionDsl.g:2235:1: rule__PlusOrMinus__Group_1_0_0__1 : rule__PlusOrMinus__Group_1_0_0__1__Impl ;
    public final void rule__PlusOrMinus__Group_1_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2239:1: ( rule__PlusOrMinus__Group_1_0_0__1__Impl )
            // InternalExpressionDsl.g:2240:2: rule__PlusOrMinus__Group_1_0_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group_1_0_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1_0_0__1"


    // $ANTLR start "rule__PlusOrMinus__Group_1_0_0__1__Impl"
    // InternalExpressionDsl.g:2246:1: rule__PlusOrMinus__Group_1_0_0__1__Impl : ( '+' ) ;
    public final void rule__PlusOrMinus__Group_1_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2250:1: ( ( '+' ) )
            // InternalExpressionDsl.g:2251:1: ( '+' )
            {
            // InternalExpressionDsl.g:2251:1: ( '+' )
            // InternalExpressionDsl.g:2252:2: '+'
            {
             before(grammarAccess.getPlusOrMinusAccess().getPlusSignKeyword_1_0_0_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getPlusOrMinusAccess().getPlusSignKeyword_1_0_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1_0_0__1__Impl"


    // $ANTLR start "rule__PlusOrMinus__Group_1_0_1__0"
    // InternalExpressionDsl.g:2262:1: rule__PlusOrMinus__Group_1_0_1__0 : rule__PlusOrMinus__Group_1_0_1__0__Impl rule__PlusOrMinus__Group_1_0_1__1 ;
    public final void rule__PlusOrMinus__Group_1_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2266:1: ( rule__PlusOrMinus__Group_1_0_1__0__Impl rule__PlusOrMinus__Group_1_0_1__1 )
            // InternalExpressionDsl.g:2267:2: rule__PlusOrMinus__Group_1_0_1__0__Impl rule__PlusOrMinus__Group_1_0_1__1
            {
            pushFollow(FOLLOW_22);
            rule__PlusOrMinus__Group_1_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group_1_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1_0_1__0"


    // $ANTLR start "rule__PlusOrMinus__Group_1_0_1__0__Impl"
    // InternalExpressionDsl.g:2274:1: rule__PlusOrMinus__Group_1_0_1__0__Impl : ( () ) ;
    public final void rule__PlusOrMinus__Group_1_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2278:1: ( ( () ) )
            // InternalExpressionDsl.g:2279:1: ( () )
            {
            // InternalExpressionDsl.g:2279:1: ( () )
            // InternalExpressionDsl.g:2280:2: ()
            {
             before(grammarAccess.getPlusOrMinusAccess().getMinusLeftAction_1_0_1_0()); 
            // InternalExpressionDsl.g:2281:2: ()
            // InternalExpressionDsl.g:2281:3: 
            {
            }

             after(grammarAccess.getPlusOrMinusAccess().getMinusLeftAction_1_0_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1_0_1__0__Impl"


    // $ANTLR start "rule__PlusOrMinus__Group_1_0_1__1"
    // InternalExpressionDsl.g:2289:1: rule__PlusOrMinus__Group_1_0_1__1 : rule__PlusOrMinus__Group_1_0_1__1__Impl ;
    public final void rule__PlusOrMinus__Group_1_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2293:1: ( rule__PlusOrMinus__Group_1_0_1__1__Impl )
            // InternalExpressionDsl.g:2294:2: rule__PlusOrMinus__Group_1_0_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PlusOrMinus__Group_1_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1_0_1__1"


    // $ANTLR start "rule__PlusOrMinus__Group_1_0_1__1__Impl"
    // InternalExpressionDsl.g:2300:1: rule__PlusOrMinus__Group_1_0_1__1__Impl : ( '-' ) ;
    public final void rule__PlusOrMinus__Group_1_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2304:1: ( ( '-' ) )
            // InternalExpressionDsl.g:2305:1: ( '-' )
            {
            // InternalExpressionDsl.g:2305:1: ( '-' )
            // InternalExpressionDsl.g:2306:2: '-'
            {
             before(grammarAccess.getPlusOrMinusAccess().getHyphenMinusKeyword_1_0_1_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getPlusOrMinusAccess().getHyphenMinusKeyword_1_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__Group_1_0_1__1__Impl"


    // $ANTLR start "rule__MulOrDiv__Group__0"
    // InternalExpressionDsl.g:2316:1: rule__MulOrDiv__Group__0 : rule__MulOrDiv__Group__0__Impl rule__MulOrDiv__Group__1 ;
    public final void rule__MulOrDiv__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2320:1: ( rule__MulOrDiv__Group__0__Impl rule__MulOrDiv__Group__1 )
            // InternalExpressionDsl.g:2321:2: rule__MulOrDiv__Group__0__Impl rule__MulOrDiv__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__MulOrDiv__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MulOrDiv__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group__0"


    // $ANTLR start "rule__MulOrDiv__Group__0__Impl"
    // InternalExpressionDsl.g:2328:1: rule__MulOrDiv__Group__0__Impl : ( rulePrimary ) ;
    public final void rule__MulOrDiv__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2332:1: ( ( rulePrimary ) )
            // InternalExpressionDsl.g:2333:1: ( rulePrimary )
            {
            // InternalExpressionDsl.g:2333:1: ( rulePrimary )
            // InternalExpressionDsl.g:2334:2: rulePrimary
            {
             before(grammarAccess.getMulOrDivAccess().getPrimaryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getMulOrDivAccess().getPrimaryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group__0__Impl"


    // $ANTLR start "rule__MulOrDiv__Group__1"
    // InternalExpressionDsl.g:2343:1: rule__MulOrDiv__Group__1 : rule__MulOrDiv__Group__1__Impl ;
    public final void rule__MulOrDiv__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2347:1: ( rule__MulOrDiv__Group__1__Impl )
            // InternalExpressionDsl.g:2348:2: rule__MulOrDiv__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MulOrDiv__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group__1"


    // $ANTLR start "rule__MulOrDiv__Group__1__Impl"
    // InternalExpressionDsl.g:2354:1: rule__MulOrDiv__Group__1__Impl : ( ( rule__MulOrDiv__Group_1__0 )* ) ;
    public final void rule__MulOrDiv__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2358:1: ( ( ( rule__MulOrDiv__Group_1__0 )* ) )
            // InternalExpressionDsl.g:2359:1: ( ( rule__MulOrDiv__Group_1__0 )* )
            {
            // InternalExpressionDsl.g:2359:1: ( ( rule__MulOrDiv__Group_1__0 )* )
            // InternalExpressionDsl.g:2360:2: ( rule__MulOrDiv__Group_1__0 )*
            {
             before(grammarAccess.getMulOrDivAccess().getGroup_1()); 
            // InternalExpressionDsl.g:2361:2: ( rule__MulOrDiv__Group_1__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( ((LA23_0>=23 && LA23_0<=25)) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalExpressionDsl.g:2361:3: rule__MulOrDiv__Group_1__0
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__MulOrDiv__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getMulOrDivAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group__1__Impl"


    // $ANTLR start "rule__MulOrDiv__Group_1__0"
    // InternalExpressionDsl.g:2370:1: rule__MulOrDiv__Group_1__0 : rule__MulOrDiv__Group_1__0__Impl rule__MulOrDiv__Group_1__1 ;
    public final void rule__MulOrDiv__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2374:1: ( rule__MulOrDiv__Group_1__0__Impl rule__MulOrDiv__Group_1__1 )
            // InternalExpressionDsl.g:2375:2: rule__MulOrDiv__Group_1__0__Impl rule__MulOrDiv__Group_1__1
            {
            pushFollow(FOLLOW_25);
            rule__MulOrDiv__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MulOrDiv__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group_1__0"


    // $ANTLR start "rule__MulOrDiv__Group_1__0__Impl"
    // InternalExpressionDsl.g:2382:1: rule__MulOrDiv__Group_1__0__Impl : ( () ) ;
    public final void rule__MulOrDiv__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2386:1: ( ( () ) )
            // InternalExpressionDsl.g:2387:1: ( () )
            {
            // InternalExpressionDsl.g:2387:1: ( () )
            // InternalExpressionDsl.g:2388:2: ()
            {
             before(grammarAccess.getMulOrDivAccess().getMulDivModLeftAction_1_0()); 
            // InternalExpressionDsl.g:2389:2: ()
            // InternalExpressionDsl.g:2389:3: 
            {
            }

             after(grammarAccess.getMulOrDivAccess().getMulDivModLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group_1__0__Impl"


    // $ANTLR start "rule__MulOrDiv__Group_1__1"
    // InternalExpressionDsl.g:2397:1: rule__MulOrDiv__Group_1__1 : rule__MulOrDiv__Group_1__1__Impl rule__MulOrDiv__Group_1__2 ;
    public final void rule__MulOrDiv__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2401:1: ( rule__MulOrDiv__Group_1__1__Impl rule__MulOrDiv__Group_1__2 )
            // InternalExpressionDsl.g:2402:2: rule__MulOrDiv__Group_1__1__Impl rule__MulOrDiv__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__MulOrDiv__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MulOrDiv__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group_1__1"


    // $ANTLR start "rule__MulOrDiv__Group_1__1__Impl"
    // InternalExpressionDsl.g:2409:1: rule__MulOrDiv__Group_1__1__Impl : ( ( rule__MulOrDiv__OpAssignment_1_1 ) ) ;
    public final void rule__MulOrDiv__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2413:1: ( ( ( rule__MulOrDiv__OpAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:2414:1: ( ( rule__MulOrDiv__OpAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:2414:1: ( ( rule__MulOrDiv__OpAssignment_1_1 ) )
            // InternalExpressionDsl.g:2415:2: ( rule__MulOrDiv__OpAssignment_1_1 )
            {
             before(grammarAccess.getMulOrDivAccess().getOpAssignment_1_1()); 
            // InternalExpressionDsl.g:2416:2: ( rule__MulOrDiv__OpAssignment_1_1 )
            // InternalExpressionDsl.g:2416:3: rule__MulOrDiv__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__MulOrDiv__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getMulOrDivAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group_1__1__Impl"


    // $ANTLR start "rule__MulOrDiv__Group_1__2"
    // InternalExpressionDsl.g:2424:1: rule__MulOrDiv__Group_1__2 : rule__MulOrDiv__Group_1__2__Impl ;
    public final void rule__MulOrDiv__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2428:1: ( rule__MulOrDiv__Group_1__2__Impl )
            // InternalExpressionDsl.g:2429:2: rule__MulOrDiv__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MulOrDiv__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group_1__2"


    // $ANTLR start "rule__MulOrDiv__Group_1__2__Impl"
    // InternalExpressionDsl.g:2435:1: rule__MulOrDiv__Group_1__2__Impl : ( ( rule__MulOrDiv__RightAssignment_1_2 ) ) ;
    public final void rule__MulOrDiv__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2439:1: ( ( ( rule__MulOrDiv__RightAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:2440:1: ( ( rule__MulOrDiv__RightAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:2440:1: ( ( rule__MulOrDiv__RightAssignment_1_2 ) )
            // InternalExpressionDsl.g:2441:2: ( rule__MulOrDiv__RightAssignment_1_2 )
            {
             before(grammarAccess.getMulOrDivAccess().getRightAssignment_1_2()); 
            // InternalExpressionDsl.g:2442:2: ( rule__MulOrDiv__RightAssignment_1_2 )
            // InternalExpressionDsl.g:2442:3: rule__MulOrDiv__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__MulOrDiv__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getMulOrDivAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__Group_1__2__Impl"


    // $ANTLR start "rule__Primary__Group_0__0"
    // InternalExpressionDsl.g:2451:1: rule__Primary__Group_0__0 : rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 ;
    public final void rule__Primary__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2455:1: ( rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 )
            // InternalExpressionDsl.g:2456:2: rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1
            {
            pushFollow(FOLLOW_27);
            rule__Primary__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0"


    // $ANTLR start "rule__Primary__Group_0__0__Impl"
    // InternalExpressionDsl.g:2463:1: rule__Primary__Group_0__0__Impl : ( () ) ;
    public final void rule__Primary__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2467:1: ( ( () ) )
            // InternalExpressionDsl.g:2468:1: ( () )
            {
            // InternalExpressionDsl.g:2468:1: ( () )
            // InternalExpressionDsl.g:2469:2: ()
            {
             before(grammarAccess.getPrimaryAccess().getNotAction_0_0()); 
            // InternalExpressionDsl.g:2470:2: ()
            // InternalExpressionDsl.g:2470:3: 
            {
            }

             after(grammarAccess.getPrimaryAccess().getNotAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0__Impl"


    // $ANTLR start "rule__Primary__Group_0__1"
    // InternalExpressionDsl.g:2478:1: rule__Primary__Group_0__1 : rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 ;
    public final void rule__Primary__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2482:1: ( rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 )
            // InternalExpressionDsl.g:2483:2: rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2
            {
            pushFollow(FOLLOW_4);
            rule__Primary__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1"


    // $ANTLR start "rule__Primary__Group_0__1__Impl"
    // InternalExpressionDsl.g:2490:1: rule__Primary__Group_0__1__Impl : ( '!' ) ;
    public final void rule__Primary__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2494:1: ( ( '!' ) )
            // InternalExpressionDsl.g:2495:1: ( '!' )
            {
            // InternalExpressionDsl.g:2495:1: ( '!' )
            // InternalExpressionDsl.g:2496:2: '!'
            {
             before(grammarAccess.getPrimaryAccess().getExclamationMarkKeyword_0_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getExclamationMarkKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__2"
    // InternalExpressionDsl.g:2505:1: rule__Primary__Group_0__2 : rule__Primary__Group_0__2__Impl ;
    public final void rule__Primary__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2509:1: ( rule__Primary__Group_0__2__Impl )
            // InternalExpressionDsl.g:2510:2: rule__Primary__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2"


    // $ANTLR start "rule__Primary__Group_0__2__Impl"
    // InternalExpressionDsl.g:2516:1: rule__Primary__Group_0__2__Impl : ( ( rule__Primary__ExpressionAssignment_0_2 ) ) ;
    public final void rule__Primary__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2520:1: ( ( ( rule__Primary__ExpressionAssignment_0_2 ) ) )
            // InternalExpressionDsl.g:2521:1: ( ( rule__Primary__ExpressionAssignment_0_2 ) )
            {
            // InternalExpressionDsl.g:2521:1: ( ( rule__Primary__ExpressionAssignment_0_2 ) )
            // InternalExpressionDsl.g:2522:2: ( rule__Primary__ExpressionAssignment_0_2 )
            {
             before(grammarAccess.getPrimaryAccess().getExpressionAssignment_0_2()); 
            // InternalExpressionDsl.g:2523:2: ( rule__Primary__ExpressionAssignment_0_2 )
            // InternalExpressionDsl.g:2523:3: rule__Primary__ExpressionAssignment_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Primary__ExpressionAssignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getExpressionAssignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2__Impl"


    // $ANTLR start "rule__Primary__Group_1__0"
    // InternalExpressionDsl.g:2532:1: rule__Primary__Group_1__0 : rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 ;
    public final void rule__Primary__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2536:1: ( rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 )
            // InternalExpressionDsl.g:2537:2: rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1
            {
            pushFollow(FOLLOW_28);
            rule__Primary__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0"


    // $ANTLR start "rule__Primary__Group_1__0__Impl"
    // InternalExpressionDsl.g:2544:1: rule__Primary__Group_1__0__Impl : ( () ) ;
    public final void rule__Primary__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2548:1: ( ( () ) )
            // InternalExpressionDsl.g:2549:1: ( () )
            {
            // InternalExpressionDsl.g:2549:1: ( () )
            // InternalExpressionDsl.g:2550:2: ()
            {
             before(grammarAccess.getPrimaryAccess().getUnaryMinusAction_1_0()); 
            // InternalExpressionDsl.g:2551:2: ()
            // InternalExpressionDsl.g:2551:3: 
            {
            }

             after(grammarAccess.getPrimaryAccess().getUnaryMinusAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0__Impl"


    // $ANTLR start "rule__Primary__Group_1__1"
    // InternalExpressionDsl.g:2559:1: rule__Primary__Group_1__1 : rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 ;
    public final void rule__Primary__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2563:1: ( rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 )
            // InternalExpressionDsl.g:2564:2: rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__Primary__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1"


    // $ANTLR start "rule__Primary__Group_1__1__Impl"
    // InternalExpressionDsl.g:2571:1: rule__Primary__Group_1__1__Impl : ( '-' ) ;
    public final void rule__Primary__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2575:1: ( ( '-' ) )
            // InternalExpressionDsl.g:2576:1: ( '-' )
            {
            // InternalExpressionDsl.g:2576:1: ( '-' )
            // InternalExpressionDsl.g:2577:2: '-'
            {
             before(grammarAccess.getPrimaryAccess().getHyphenMinusKeyword_1_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getHyphenMinusKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1__Impl"


    // $ANTLR start "rule__Primary__Group_1__2"
    // InternalExpressionDsl.g:2586:1: rule__Primary__Group_1__2 : rule__Primary__Group_1__2__Impl ;
    public final void rule__Primary__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2590:1: ( rule__Primary__Group_1__2__Impl )
            // InternalExpressionDsl.g:2591:2: rule__Primary__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2"


    // $ANTLR start "rule__Primary__Group_1__2__Impl"
    // InternalExpressionDsl.g:2597:1: rule__Primary__Group_1__2__Impl : ( ( rule__Primary__ExpressionAssignment_1_2 ) ) ;
    public final void rule__Primary__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2601:1: ( ( ( rule__Primary__ExpressionAssignment_1_2 ) ) )
            // InternalExpressionDsl.g:2602:1: ( ( rule__Primary__ExpressionAssignment_1_2 ) )
            {
            // InternalExpressionDsl.g:2602:1: ( ( rule__Primary__ExpressionAssignment_1_2 ) )
            // InternalExpressionDsl.g:2603:2: ( rule__Primary__ExpressionAssignment_1_2 )
            {
             before(grammarAccess.getPrimaryAccess().getExpressionAssignment_1_2()); 
            // InternalExpressionDsl.g:2604:2: ( rule__Primary__ExpressionAssignment_1_2 )
            // InternalExpressionDsl.g:2604:3: rule__Primary__ExpressionAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Primary__ExpressionAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getExpressionAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2__Impl"


    // $ANTLR start "rule__Primary__Group_2__0"
    // InternalExpressionDsl.g:2613:1: rule__Primary__Group_2__0 : rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 ;
    public final void rule__Primary__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2617:1: ( rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 )
            // InternalExpressionDsl.g:2618:2: rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1
            {
            pushFollow(FOLLOW_4);
            rule__Primary__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0"


    // $ANTLR start "rule__Primary__Group_2__0__Impl"
    // InternalExpressionDsl.g:2625:1: rule__Primary__Group_2__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2629:1: ( ( '(' ) )
            // InternalExpressionDsl.g:2630:1: ( '(' )
            {
            // InternalExpressionDsl.g:2630:1: ( '(' )
            // InternalExpressionDsl.g:2631:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0__Impl"


    // $ANTLR start "rule__Primary__Group_2__1"
    // InternalExpressionDsl.g:2640:1: rule__Primary__Group_2__1 : rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2 ;
    public final void rule__Primary__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2644:1: ( rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2 )
            // InternalExpressionDsl.g:2645:2: rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2
            {
            pushFollow(FOLLOW_29);
            rule__Primary__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1"


    // $ANTLR start "rule__Primary__Group_2__1__Impl"
    // InternalExpressionDsl.g:2652:1: rule__Primary__Group_2__1__Impl : ( ruleValueSpecification ) ;
    public final void rule__Primary__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2656:1: ( ( ruleValueSpecification ) )
            // InternalExpressionDsl.g:2657:1: ( ruleValueSpecification )
            {
            // InternalExpressionDsl.g:2657:1: ( ruleValueSpecification )
            // InternalExpressionDsl.g:2658:2: ruleValueSpecification
            {
             before(grammarAccess.getPrimaryAccess().getValueSpecificationParserRuleCall_2_1()); 
            pushFollow(FOLLOW_2);
            ruleValueSpecification();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getValueSpecificationParserRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1__Impl"


    // $ANTLR start "rule__Primary__Group_2__2"
    // InternalExpressionDsl.g:2667:1: rule__Primary__Group_2__2 : rule__Primary__Group_2__2__Impl ;
    public final void rule__Primary__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2671:1: ( rule__Primary__Group_2__2__Impl )
            // InternalExpressionDsl.g:2672:2: rule__Primary__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__2"


    // $ANTLR start "rule__Primary__Group_2__2__Impl"
    // InternalExpressionDsl.g:2678:1: rule__Primary__Group_2__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2682:1: ( ( ')' ) )
            // InternalExpressionDsl.g:2683:1: ( ')' )
            {
            // InternalExpressionDsl.g:2683:1: ( ')' )
            // InternalExpressionDsl.g:2684:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_2_2()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__2__Impl"


    // $ANTLR start "rule__Primary__Group_3__0"
    // InternalExpressionDsl.g:2694:1: rule__Primary__Group_3__0 : rule__Primary__Group_3__0__Impl rule__Primary__Group_3__1 ;
    public final void rule__Primary__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2698:1: ( rule__Primary__Group_3__0__Impl rule__Primary__Group_3__1 )
            // InternalExpressionDsl.g:2699:2: rule__Primary__Group_3__0__Impl rule__Primary__Group_3__1
            {
            pushFollow(FOLLOW_30);
            rule__Primary__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__0"


    // $ANTLR start "rule__Primary__Group_3__0__Impl"
    // InternalExpressionDsl.g:2706:1: rule__Primary__Group_3__0__Impl : ( () ) ;
    public final void rule__Primary__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2710:1: ( ( () ) )
            // InternalExpressionDsl.g:2711:1: ( () )
            {
            // InternalExpressionDsl.g:2711:1: ( () )
            // InternalExpressionDsl.g:2712:2: ()
            {
             before(grammarAccess.getPrimaryAccess().getPreIncrementOrDecrementAction_3_0()); 
            // InternalExpressionDsl.g:2713:2: ()
            // InternalExpressionDsl.g:2713:3: 
            {
            }

             after(grammarAccess.getPrimaryAccess().getPreIncrementOrDecrementAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__0__Impl"


    // $ANTLR start "rule__Primary__Group_3__1"
    // InternalExpressionDsl.g:2721:1: rule__Primary__Group_3__1 : rule__Primary__Group_3__1__Impl rule__Primary__Group_3__2 ;
    public final void rule__Primary__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2725:1: ( rule__Primary__Group_3__1__Impl rule__Primary__Group_3__2 )
            // InternalExpressionDsl.g:2726:2: rule__Primary__Group_3__1__Impl rule__Primary__Group_3__2
            {
            pushFollow(FOLLOW_31);
            rule__Primary__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__1"


    // $ANTLR start "rule__Primary__Group_3__1__Impl"
    // InternalExpressionDsl.g:2733:1: rule__Primary__Group_3__1__Impl : ( ( rule__Primary__OpAssignment_3_1 ) ) ;
    public final void rule__Primary__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2737:1: ( ( ( rule__Primary__OpAssignment_3_1 ) ) )
            // InternalExpressionDsl.g:2738:1: ( ( rule__Primary__OpAssignment_3_1 ) )
            {
            // InternalExpressionDsl.g:2738:1: ( ( rule__Primary__OpAssignment_3_1 ) )
            // InternalExpressionDsl.g:2739:2: ( rule__Primary__OpAssignment_3_1 )
            {
             before(grammarAccess.getPrimaryAccess().getOpAssignment_3_1()); 
            // InternalExpressionDsl.g:2740:2: ( rule__Primary__OpAssignment_3_1 )
            // InternalExpressionDsl.g:2740:3: rule__Primary__OpAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Primary__OpAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getOpAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__1__Impl"


    // $ANTLR start "rule__Primary__Group_3__2"
    // InternalExpressionDsl.g:2748:1: rule__Primary__Group_3__2 : rule__Primary__Group_3__2__Impl ;
    public final void rule__Primary__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2752:1: ( rule__Primary__Group_3__2__Impl )
            // InternalExpressionDsl.g:2753:2: rule__Primary__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__2"


    // $ANTLR start "rule__Primary__Group_3__2__Impl"
    // InternalExpressionDsl.g:2759:1: rule__Primary__Group_3__2__Impl : ( ( rule__Primary__ExpressionAssignment_3_2 ) ) ;
    public final void rule__Primary__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2763:1: ( ( ( rule__Primary__ExpressionAssignment_3_2 ) ) )
            // InternalExpressionDsl.g:2764:1: ( ( rule__Primary__ExpressionAssignment_3_2 ) )
            {
            // InternalExpressionDsl.g:2764:1: ( ( rule__Primary__ExpressionAssignment_3_2 ) )
            // InternalExpressionDsl.g:2765:2: ( rule__Primary__ExpressionAssignment_3_2 )
            {
             before(grammarAccess.getPrimaryAccess().getExpressionAssignment_3_2()); 
            // InternalExpressionDsl.g:2766:2: ( rule__Primary__ExpressionAssignment_3_2 )
            // InternalExpressionDsl.g:2766:3: rule__Primary__ExpressionAssignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__Primary__ExpressionAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getExpressionAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_3__2__Impl"


    // $ANTLR start "rule__Primary__Group_4__0"
    // InternalExpressionDsl.g:2775:1: rule__Primary__Group_4__0 : rule__Primary__Group_4__0__Impl rule__Primary__Group_4__1 ;
    public final void rule__Primary__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2779:1: ( rule__Primary__Group_4__0__Impl rule__Primary__Group_4__1 )
            // InternalExpressionDsl.g:2780:2: rule__Primary__Group_4__0__Impl rule__Primary__Group_4__1
            {
            pushFollow(FOLLOW_31);
            rule__Primary__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_4__0"


    // $ANTLR start "rule__Primary__Group_4__0__Impl"
    // InternalExpressionDsl.g:2787:1: rule__Primary__Group_4__0__Impl : ( () ) ;
    public final void rule__Primary__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2791:1: ( ( () ) )
            // InternalExpressionDsl.g:2792:1: ( () )
            {
            // InternalExpressionDsl.g:2792:1: ( () )
            // InternalExpressionDsl.g:2793:2: ()
            {
             before(grammarAccess.getPrimaryAccess().getPostIncrementOrDecrementAction_4_0()); 
            // InternalExpressionDsl.g:2794:2: ()
            // InternalExpressionDsl.g:2794:3: 
            {
            }

             after(grammarAccess.getPrimaryAccess().getPostIncrementOrDecrementAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_4__0__Impl"


    // $ANTLR start "rule__Primary__Group_4__1"
    // InternalExpressionDsl.g:2802:1: rule__Primary__Group_4__1 : rule__Primary__Group_4__1__Impl rule__Primary__Group_4__2 ;
    public final void rule__Primary__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2806:1: ( rule__Primary__Group_4__1__Impl rule__Primary__Group_4__2 )
            // InternalExpressionDsl.g:2807:2: rule__Primary__Group_4__1__Impl rule__Primary__Group_4__2
            {
            pushFollow(FOLLOW_30);
            rule__Primary__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_4__1"


    // $ANTLR start "rule__Primary__Group_4__1__Impl"
    // InternalExpressionDsl.g:2814:1: rule__Primary__Group_4__1__Impl : ( ( rule__Primary__ExpressionAssignment_4_1 ) ) ;
    public final void rule__Primary__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2818:1: ( ( ( rule__Primary__ExpressionAssignment_4_1 ) ) )
            // InternalExpressionDsl.g:2819:1: ( ( rule__Primary__ExpressionAssignment_4_1 ) )
            {
            // InternalExpressionDsl.g:2819:1: ( ( rule__Primary__ExpressionAssignment_4_1 ) )
            // InternalExpressionDsl.g:2820:2: ( rule__Primary__ExpressionAssignment_4_1 )
            {
             before(grammarAccess.getPrimaryAccess().getExpressionAssignment_4_1()); 
            // InternalExpressionDsl.g:2821:2: ( rule__Primary__ExpressionAssignment_4_1 )
            // InternalExpressionDsl.g:2821:3: rule__Primary__ExpressionAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Primary__ExpressionAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getExpressionAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_4__1__Impl"


    // $ANTLR start "rule__Primary__Group_4__2"
    // InternalExpressionDsl.g:2829:1: rule__Primary__Group_4__2 : rule__Primary__Group_4__2__Impl ;
    public final void rule__Primary__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2833:1: ( rule__Primary__Group_4__2__Impl )
            // InternalExpressionDsl.g:2834:2: rule__Primary__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_4__2"


    // $ANTLR start "rule__Primary__Group_4__2__Impl"
    // InternalExpressionDsl.g:2840:1: rule__Primary__Group_4__2__Impl : ( ( rule__Primary__OpAssignment_4_2 ) ) ;
    public final void rule__Primary__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2844:1: ( ( ( rule__Primary__OpAssignment_4_2 ) ) )
            // InternalExpressionDsl.g:2845:1: ( ( rule__Primary__OpAssignment_4_2 ) )
            {
            // InternalExpressionDsl.g:2845:1: ( ( rule__Primary__OpAssignment_4_2 ) )
            // InternalExpressionDsl.g:2846:2: ( rule__Primary__OpAssignment_4_2 )
            {
             before(grammarAccess.getPrimaryAccess().getOpAssignment_4_2()); 
            // InternalExpressionDsl.g:2847:2: ( rule__Primary__OpAssignment_4_2 )
            // InternalExpressionDsl.g:2847:3: rule__Primary__OpAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Primary__OpAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getOpAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_4__2__Impl"


    // $ANTLR start "rule__Atomic__Group_0__0"
    // InternalExpressionDsl.g:2856:1: rule__Atomic__Group_0__0 : rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1 ;
    public final void rule__Atomic__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2860:1: ( rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1 )
            // InternalExpressionDsl.g:2861:2: rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1
            {
            pushFollow(FOLLOW_32);
            rule__Atomic__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__0"


    // $ANTLR start "rule__Atomic__Group_0__0__Impl"
    // InternalExpressionDsl.g:2868:1: rule__Atomic__Group_0__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2872:1: ( ( () ) )
            // InternalExpressionDsl.g:2873:1: ( () )
            {
            // InternalExpressionDsl.g:2873:1: ( () )
            // InternalExpressionDsl.g:2874:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralIntegerAction_0_0()); 
            // InternalExpressionDsl.g:2875:2: ()
            // InternalExpressionDsl.g:2875:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralIntegerAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__0__Impl"


    // $ANTLR start "rule__Atomic__Group_0__1"
    // InternalExpressionDsl.g:2883:1: rule__Atomic__Group_0__1 : rule__Atomic__Group_0__1__Impl ;
    public final void rule__Atomic__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2887:1: ( rule__Atomic__Group_0__1__Impl )
            // InternalExpressionDsl.g:2888:2: rule__Atomic__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__1"


    // $ANTLR start "rule__Atomic__Group_0__1__Impl"
    // InternalExpressionDsl.g:2894:1: rule__Atomic__Group_0__1__Impl : ( ( rule__Atomic__ValueAssignment_0_1 ) ) ;
    public final void rule__Atomic__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2898:1: ( ( ( rule__Atomic__ValueAssignment_0_1 ) ) )
            // InternalExpressionDsl.g:2899:1: ( ( rule__Atomic__ValueAssignment_0_1 ) )
            {
            // InternalExpressionDsl.g:2899:1: ( ( rule__Atomic__ValueAssignment_0_1 ) )
            // InternalExpressionDsl.g:2900:2: ( rule__Atomic__ValueAssignment_0_1 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_0_1()); 
            // InternalExpressionDsl.g:2901:2: ( rule__Atomic__ValueAssignment_0_1 )
            // InternalExpressionDsl.g:2901:3: rule__Atomic__ValueAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__1__Impl"


    // $ANTLR start "rule__Atomic__Group_1__0"
    // InternalExpressionDsl.g:2910:1: rule__Atomic__Group_1__0 : rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1 ;
    public final void rule__Atomic__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2914:1: ( rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1 )
            // InternalExpressionDsl.g:2915:2: rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1
            {
            pushFollow(FOLLOW_33);
            rule__Atomic__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__0"


    // $ANTLR start "rule__Atomic__Group_1__0__Impl"
    // InternalExpressionDsl.g:2922:1: rule__Atomic__Group_1__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2926:1: ( ( () ) )
            // InternalExpressionDsl.g:2927:1: ( () )
            {
            // InternalExpressionDsl.g:2927:1: ( () )
            // InternalExpressionDsl.g:2928:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralStringAction_1_0()); 
            // InternalExpressionDsl.g:2929:2: ()
            // InternalExpressionDsl.g:2929:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralStringAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__0__Impl"


    // $ANTLR start "rule__Atomic__Group_1__1"
    // InternalExpressionDsl.g:2937:1: rule__Atomic__Group_1__1 : rule__Atomic__Group_1__1__Impl ;
    public final void rule__Atomic__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2941:1: ( rule__Atomic__Group_1__1__Impl )
            // InternalExpressionDsl.g:2942:2: rule__Atomic__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__1"


    // $ANTLR start "rule__Atomic__Group_1__1__Impl"
    // InternalExpressionDsl.g:2948:1: rule__Atomic__Group_1__1__Impl : ( ( rule__Atomic__ValueAssignment_1_1 ) ) ;
    public final void rule__Atomic__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2952:1: ( ( ( rule__Atomic__ValueAssignment_1_1 ) ) )
            // InternalExpressionDsl.g:2953:1: ( ( rule__Atomic__ValueAssignment_1_1 ) )
            {
            // InternalExpressionDsl.g:2953:1: ( ( rule__Atomic__ValueAssignment_1_1 ) )
            // InternalExpressionDsl.g:2954:2: ( rule__Atomic__ValueAssignment_1_1 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_1_1()); 
            // InternalExpressionDsl.g:2955:2: ( rule__Atomic__ValueAssignment_1_1 )
            // InternalExpressionDsl.g:2955:3: rule__Atomic__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__1__Impl"


    // $ANTLR start "rule__Atomic__Group_2__0"
    // InternalExpressionDsl.g:2964:1: rule__Atomic__Group_2__0 : rule__Atomic__Group_2__0__Impl rule__Atomic__Group_2__1 ;
    public final void rule__Atomic__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2968:1: ( rule__Atomic__Group_2__0__Impl rule__Atomic__Group_2__1 )
            // InternalExpressionDsl.g:2969:2: rule__Atomic__Group_2__0__Impl rule__Atomic__Group_2__1
            {
            pushFollow(FOLLOW_34);
            rule__Atomic__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__0"


    // $ANTLR start "rule__Atomic__Group_2__0__Impl"
    // InternalExpressionDsl.g:2976:1: rule__Atomic__Group_2__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2980:1: ( ( () ) )
            // InternalExpressionDsl.g:2981:1: ( () )
            {
            // InternalExpressionDsl.g:2981:1: ( () )
            // InternalExpressionDsl.g:2982:2: ()
            {
             before(grammarAccess.getAtomicAccess().getOpaqueExpressionAction_2_0()); 
            // InternalExpressionDsl.g:2983:2: ()
            // InternalExpressionDsl.g:2983:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getOpaqueExpressionAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__0__Impl"


    // $ANTLR start "rule__Atomic__Group_2__1"
    // InternalExpressionDsl.g:2991:1: rule__Atomic__Group_2__1 : rule__Atomic__Group_2__1__Impl ;
    public final void rule__Atomic__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:2995:1: ( rule__Atomic__Group_2__1__Impl )
            // InternalExpressionDsl.g:2996:2: rule__Atomic__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__1"


    // $ANTLR start "rule__Atomic__Group_2__1__Impl"
    // InternalExpressionDsl.g:3002:1: rule__Atomic__Group_2__1__Impl : ( ( rule__Atomic__BodyAssignment_2_1 ) ) ;
    public final void rule__Atomic__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3006:1: ( ( ( rule__Atomic__BodyAssignment_2_1 ) ) )
            // InternalExpressionDsl.g:3007:1: ( ( rule__Atomic__BodyAssignment_2_1 ) )
            {
            // InternalExpressionDsl.g:3007:1: ( ( rule__Atomic__BodyAssignment_2_1 ) )
            // InternalExpressionDsl.g:3008:2: ( rule__Atomic__BodyAssignment_2_1 )
            {
             before(grammarAccess.getAtomicAccess().getBodyAssignment_2_1()); 
            // InternalExpressionDsl.g:3009:2: ( rule__Atomic__BodyAssignment_2_1 )
            // InternalExpressionDsl.g:3009:3: rule__Atomic__BodyAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__BodyAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getBodyAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__1__Impl"


    // $ANTLR start "rule__Atomic__Group_3__0"
    // InternalExpressionDsl.g:3018:1: rule__Atomic__Group_3__0 : rule__Atomic__Group_3__0__Impl rule__Atomic__Group_3__1 ;
    public final void rule__Atomic__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3022:1: ( rule__Atomic__Group_3__0__Impl rule__Atomic__Group_3__1 )
            // InternalExpressionDsl.g:3023:2: rule__Atomic__Group_3__0__Impl rule__Atomic__Group_3__1
            {
            pushFollow(FOLLOW_35);
            rule__Atomic__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_3__0"


    // $ANTLR start "rule__Atomic__Group_3__0__Impl"
    // InternalExpressionDsl.g:3030:1: rule__Atomic__Group_3__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3034:1: ( ( () ) )
            // InternalExpressionDsl.g:3035:1: ( () )
            {
            // InternalExpressionDsl.g:3035:1: ( () )
            // InternalExpressionDsl.g:3036:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralBooleanAction_3_0()); 
            // InternalExpressionDsl.g:3037:2: ()
            // InternalExpressionDsl.g:3037:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralBooleanAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_3__0__Impl"


    // $ANTLR start "rule__Atomic__Group_3__1"
    // InternalExpressionDsl.g:3045:1: rule__Atomic__Group_3__1 : rule__Atomic__Group_3__1__Impl ;
    public final void rule__Atomic__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3049:1: ( rule__Atomic__Group_3__1__Impl )
            // InternalExpressionDsl.g:3050:2: rule__Atomic__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_3__1"


    // $ANTLR start "rule__Atomic__Group_3__1__Impl"
    // InternalExpressionDsl.g:3056:1: rule__Atomic__Group_3__1__Impl : ( ( rule__Atomic__ValueAssignment_3_1 ) ) ;
    public final void rule__Atomic__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3060:1: ( ( ( rule__Atomic__ValueAssignment_3_1 ) ) )
            // InternalExpressionDsl.g:3061:1: ( ( rule__Atomic__ValueAssignment_3_1 ) )
            {
            // InternalExpressionDsl.g:3061:1: ( ( rule__Atomic__ValueAssignment_3_1 ) )
            // InternalExpressionDsl.g:3062:2: ( rule__Atomic__ValueAssignment_3_1 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_3_1()); 
            // InternalExpressionDsl.g:3063:2: ( rule__Atomic__ValueAssignment_3_1 )
            // InternalExpressionDsl.g:3063:3: rule__Atomic__ValueAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_3__1__Impl"


    // $ANTLR start "rule__Atomic__Group_4__0"
    // InternalExpressionDsl.g:3072:1: rule__Atomic__Group_4__0 : rule__Atomic__Group_4__0__Impl rule__Atomic__Group_4__1 ;
    public final void rule__Atomic__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3076:1: ( rule__Atomic__Group_4__0__Impl rule__Atomic__Group_4__1 )
            // InternalExpressionDsl.g:3077:2: rule__Atomic__Group_4__0__Impl rule__Atomic__Group_4__1
            {
            pushFollow(FOLLOW_36);
            rule__Atomic__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_4__0"


    // $ANTLR start "rule__Atomic__Group_4__0__Impl"
    // InternalExpressionDsl.g:3084:1: rule__Atomic__Group_4__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3088:1: ( ( () ) )
            // InternalExpressionDsl.g:3089:1: ( () )
            {
            // InternalExpressionDsl.g:3089:1: ( () )
            // InternalExpressionDsl.g:3090:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralFloatAction_4_0()); 
            // InternalExpressionDsl.g:3091:2: ()
            // InternalExpressionDsl.g:3091:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralFloatAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_4__0__Impl"


    // $ANTLR start "rule__Atomic__Group_4__1"
    // InternalExpressionDsl.g:3099:1: rule__Atomic__Group_4__1 : rule__Atomic__Group_4__1__Impl ;
    public final void rule__Atomic__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3103:1: ( rule__Atomic__Group_4__1__Impl )
            // InternalExpressionDsl.g:3104:2: rule__Atomic__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_4__1"


    // $ANTLR start "rule__Atomic__Group_4__1__Impl"
    // InternalExpressionDsl.g:3110:1: rule__Atomic__Group_4__1__Impl : ( ( rule__Atomic__ValueAssignment_4_1 ) ) ;
    public final void rule__Atomic__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3114:1: ( ( ( rule__Atomic__ValueAssignment_4_1 ) ) )
            // InternalExpressionDsl.g:3115:1: ( ( rule__Atomic__ValueAssignment_4_1 ) )
            {
            // InternalExpressionDsl.g:3115:1: ( ( rule__Atomic__ValueAssignment_4_1 ) )
            // InternalExpressionDsl.g:3116:2: ( rule__Atomic__ValueAssignment_4_1 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_4_1()); 
            // InternalExpressionDsl.g:3117:2: ( rule__Atomic__ValueAssignment_4_1 )
            // InternalExpressionDsl.g:3117:3: rule__Atomic__ValueAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_4__1__Impl"


    // $ANTLR start "rule__Atomic__Group_5__0"
    // InternalExpressionDsl.g:3126:1: rule__Atomic__Group_5__0 : rule__Atomic__Group_5__0__Impl rule__Atomic__Group_5__1 ;
    public final void rule__Atomic__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3130:1: ( rule__Atomic__Group_5__0__Impl rule__Atomic__Group_5__1 )
            // InternalExpressionDsl.g:3131:2: rule__Atomic__Group_5__0__Impl rule__Atomic__Group_5__1
            {
            pushFollow(FOLLOW_37);
            rule__Atomic__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_5__0"


    // $ANTLR start "rule__Atomic__Group_5__0__Impl"
    // InternalExpressionDsl.g:3138:1: rule__Atomic__Group_5__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3142:1: ( ( () ) )
            // InternalExpressionDsl.g:3143:1: ( () )
            {
            // InternalExpressionDsl.g:3143:1: ( () )
            // InternalExpressionDsl.g:3144:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralLongAction_5_0()); 
            // InternalExpressionDsl.g:3145:2: ()
            // InternalExpressionDsl.g:3145:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralLongAction_5_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_5__0__Impl"


    // $ANTLR start "rule__Atomic__Group_5__1"
    // InternalExpressionDsl.g:3153:1: rule__Atomic__Group_5__1 : rule__Atomic__Group_5__1__Impl ;
    public final void rule__Atomic__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3157:1: ( rule__Atomic__Group_5__1__Impl )
            // InternalExpressionDsl.g:3158:2: rule__Atomic__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_5__1"


    // $ANTLR start "rule__Atomic__Group_5__1__Impl"
    // InternalExpressionDsl.g:3164:1: rule__Atomic__Group_5__1__Impl : ( ( rule__Atomic__ValueAssignment_5_1 ) ) ;
    public final void rule__Atomic__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3168:1: ( ( ( rule__Atomic__ValueAssignment_5_1 ) ) )
            // InternalExpressionDsl.g:3169:1: ( ( rule__Atomic__ValueAssignment_5_1 ) )
            {
            // InternalExpressionDsl.g:3169:1: ( ( rule__Atomic__ValueAssignment_5_1 ) )
            // InternalExpressionDsl.g:3170:2: ( rule__Atomic__ValueAssignment_5_1 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_5_1()); 
            // InternalExpressionDsl.g:3171:2: ( rule__Atomic__ValueAssignment_5_1 )
            // InternalExpressionDsl.g:3171:3: rule__Atomic__ValueAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_5__1__Impl"


    // $ANTLR start "rule__Atomic__Group_6__0"
    // InternalExpressionDsl.g:3180:1: rule__Atomic__Group_6__0 : rule__Atomic__Group_6__0__Impl rule__Atomic__Group_6__1 ;
    public final void rule__Atomic__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3184:1: ( rule__Atomic__Group_6__0__Impl rule__Atomic__Group_6__1 )
            // InternalExpressionDsl.g:3185:2: rule__Atomic__Group_6__0__Impl rule__Atomic__Group_6__1
            {
            pushFollow(FOLLOW_38);
            rule__Atomic__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_6__0"


    // $ANTLR start "rule__Atomic__Group_6__0__Impl"
    // InternalExpressionDsl.g:3192:1: rule__Atomic__Group_6__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3196:1: ( ( () ) )
            // InternalExpressionDsl.g:3197:1: ( () )
            {
            // InternalExpressionDsl.g:3197:1: ( () )
            // InternalExpressionDsl.g:3198:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralDoubleAction_6_0()); 
            // InternalExpressionDsl.g:3199:2: ()
            // InternalExpressionDsl.g:3199:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralDoubleAction_6_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_6__0__Impl"


    // $ANTLR start "rule__Atomic__Group_6__1"
    // InternalExpressionDsl.g:3207:1: rule__Atomic__Group_6__1 : rule__Atomic__Group_6__1__Impl ;
    public final void rule__Atomic__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3211:1: ( rule__Atomic__Group_6__1__Impl )
            // InternalExpressionDsl.g:3212:2: rule__Atomic__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_6__1"


    // $ANTLR start "rule__Atomic__Group_6__1__Impl"
    // InternalExpressionDsl.g:3218:1: rule__Atomic__Group_6__1__Impl : ( ( rule__Atomic__ValueAssignment_6_1 ) ) ;
    public final void rule__Atomic__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3222:1: ( ( ( rule__Atomic__ValueAssignment_6_1 ) ) )
            // InternalExpressionDsl.g:3223:1: ( ( rule__Atomic__ValueAssignment_6_1 ) )
            {
            // InternalExpressionDsl.g:3223:1: ( ( rule__Atomic__ValueAssignment_6_1 ) )
            // InternalExpressionDsl.g:3224:2: ( rule__Atomic__ValueAssignment_6_1 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_6_1()); 
            // InternalExpressionDsl.g:3225:2: ( rule__Atomic__ValueAssignment_6_1 )
            // InternalExpressionDsl.g:3225:3: rule__Atomic__ValueAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_6__1__Impl"


    // $ANTLR start "rule__Atomic__Group_7__0"
    // InternalExpressionDsl.g:3234:1: rule__Atomic__Group_7__0 : rule__Atomic__Group_7__0__Impl rule__Atomic__Group_7__1 ;
    public final void rule__Atomic__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3238:1: ( rule__Atomic__Group_7__0__Impl rule__Atomic__Group_7__1 )
            // InternalExpressionDsl.g:3239:2: rule__Atomic__Group_7__0__Impl rule__Atomic__Group_7__1
            {
            pushFollow(FOLLOW_39);
            rule__Atomic__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_7__0"


    // $ANTLR start "rule__Atomic__Group_7__0__Impl"
    // InternalExpressionDsl.g:3246:1: rule__Atomic__Group_7__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3250:1: ( ( () ) )
            // InternalExpressionDsl.g:3251:1: ( () )
            {
            // InternalExpressionDsl.g:3251:1: ( () )
            // InternalExpressionDsl.g:3252:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralByteAction_7_0()); 
            // InternalExpressionDsl.g:3253:2: ()
            // InternalExpressionDsl.g:3253:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralByteAction_7_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_7__0__Impl"


    // $ANTLR start "rule__Atomic__Group_7__1"
    // InternalExpressionDsl.g:3261:1: rule__Atomic__Group_7__1 : rule__Atomic__Group_7__1__Impl ;
    public final void rule__Atomic__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3265:1: ( rule__Atomic__Group_7__1__Impl )
            // InternalExpressionDsl.g:3266:2: rule__Atomic__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_7__1"


    // $ANTLR start "rule__Atomic__Group_7__1__Impl"
    // InternalExpressionDsl.g:3272:1: rule__Atomic__Group_7__1__Impl : ( ( rule__Atomic__ValueAssignment_7_1 ) ) ;
    public final void rule__Atomic__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3276:1: ( ( ( rule__Atomic__ValueAssignment_7_1 ) ) )
            // InternalExpressionDsl.g:3277:1: ( ( rule__Atomic__ValueAssignment_7_1 ) )
            {
            // InternalExpressionDsl.g:3277:1: ( ( rule__Atomic__ValueAssignment_7_1 ) )
            // InternalExpressionDsl.g:3278:2: ( rule__Atomic__ValueAssignment_7_1 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_7_1()); 
            // InternalExpressionDsl.g:3279:2: ( rule__Atomic__ValueAssignment_7_1 )
            // InternalExpressionDsl.g:3279:3: rule__Atomic__ValueAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_7__1__Impl"


    // $ANTLR start "rule__Atomic__Group_8__0"
    // InternalExpressionDsl.g:3288:1: rule__Atomic__Group_8__0 : rule__Atomic__Group_8__0__Impl rule__Atomic__Group_8__1 ;
    public final void rule__Atomic__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3292:1: ( rule__Atomic__Group_8__0__Impl rule__Atomic__Group_8__1 )
            // InternalExpressionDsl.g:3293:2: rule__Atomic__Group_8__0__Impl rule__Atomic__Group_8__1
            {
            pushFollow(FOLLOW_40);
            rule__Atomic__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_8__0"


    // $ANTLR start "rule__Atomic__Group_8__0__Impl"
    // InternalExpressionDsl.g:3300:1: rule__Atomic__Group_8__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3304:1: ( ( () ) )
            // InternalExpressionDsl.g:3305:1: ( () )
            {
            // InternalExpressionDsl.g:3305:1: ( () )
            // InternalExpressionDsl.g:3306:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralCharAction_8_0()); 
            // InternalExpressionDsl.g:3307:2: ()
            // InternalExpressionDsl.g:3307:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralCharAction_8_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_8__0__Impl"


    // $ANTLR start "rule__Atomic__Group_8__1"
    // InternalExpressionDsl.g:3315:1: rule__Atomic__Group_8__1 : rule__Atomic__Group_8__1__Impl rule__Atomic__Group_8__2 ;
    public final void rule__Atomic__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3319:1: ( rule__Atomic__Group_8__1__Impl rule__Atomic__Group_8__2 )
            // InternalExpressionDsl.g:3320:2: rule__Atomic__Group_8__1__Impl rule__Atomic__Group_8__2
            {
            pushFollow(FOLLOW_41);
            rule__Atomic__Group_8__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_8__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_8__1"


    // $ANTLR start "rule__Atomic__Group_8__1__Impl"
    // InternalExpressionDsl.g:3327:1: rule__Atomic__Group_8__1__Impl : ( '\\'' ) ;
    public final void rule__Atomic__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3331:1: ( ( '\\'' ) )
            // InternalExpressionDsl.g:3332:1: ( '\\'' )
            {
            // InternalExpressionDsl.g:3332:1: ( '\\'' )
            // InternalExpressionDsl.g:3333:2: '\\''
            {
             before(grammarAccess.getAtomicAccess().getApostropheKeyword_8_1()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getApostropheKeyword_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_8__1__Impl"


    // $ANTLR start "rule__Atomic__Group_8__2"
    // InternalExpressionDsl.g:3342:1: rule__Atomic__Group_8__2 : rule__Atomic__Group_8__2__Impl rule__Atomic__Group_8__3 ;
    public final void rule__Atomic__Group_8__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3346:1: ( rule__Atomic__Group_8__2__Impl rule__Atomic__Group_8__3 )
            // InternalExpressionDsl.g:3347:2: rule__Atomic__Group_8__2__Impl rule__Atomic__Group_8__3
            {
            pushFollow(FOLLOW_40);
            rule__Atomic__Group_8__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_8__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_8__2"


    // $ANTLR start "rule__Atomic__Group_8__2__Impl"
    // InternalExpressionDsl.g:3354:1: rule__Atomic__Group_8__2__Impl : ( ( rule__Atomic__ValueAssignment_8_2 ) ) ;
    public final void rule__Atomic__Group_8__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3358:1: ( ( ( rule__Atomic__ValueAssignment_8_2 ) ) )
            // InternalExpressionDsl.g:3359:1: ( ( rule__Atomic__ValueAssignment_8_2 ) )
            {
            // InternalExpressionDsl.g:3359:1: ( ( rule__Atomic__ValueAssignment_8_2 ) )
            // InternalExpressionDsl.g:3360:2: ( rule__Atomic__ValueAssignment_8_2 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_8_2()); 
            // InternalExpressionDsl.g:3361:2: ( rule__Atomic__ValueAssignment_8_2 )
            // InternalExpressionDsl.g:3361:3: rule__Atomic__ValueAssignment_8_2
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_8_2();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_8_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_8__2__Impl"


    // $ANTLR start "rule__Atomic__Group_8__3"
    // InternalExpressionDsl.g:3369:1: rule__Atomic__Group_8__3 : rule__Atomic__Group_8__3__Impl ;
    public final void rule__Atomic__Group_8__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3373:1: ( rule__Atomic__Group_8__3__Impl )
            // InternalExpressionDsl.g:3374:2: rule__Atomic__Group_8__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_8__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_8__3"


    // $ANTLR start "rule__Atomic__Group_8__3__Impl"
    // InternalExpressionDsl.g:3380:1: rule__Atomic__Group_8__3__Impl : ( '\\'' ) ;
    public final void rule__Atomic__Group_8__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3384:1: ( ( '\\'' ) )
            // InternalExpressionDsl.g:3385:1: ( '\\'' )
            {
            // InternalExpressionDsl.g:3385:1: ( '\\'' )
            // InternalExpressionDsl.g:3386:2: '\\''
            {
             before(grammarAccess.getAtomicAccess().getApostropheKeyword_8_3()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getApostropheKeyword_8_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_8__3__Impl"


    // $ANTLR start "rule__Atomic__Group_9__0"
    // InternalExpressionDsl.g:3396:1: rule__Atomic__Group_9__0 : rule__Atomic__Group_9__0__Impl rule__Atomic__Group_9__1 ;
    public final void rule__Atomic__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3400:1: ( rule__Atomic__Group_9__0__Impl rule__Atomic__Group_9__1 )
            // InternalExpressionDsl.g:3401:2: rule__Atomic__Group_9__0__Impl rule__Atomic__Group_9__1
            {
            pushFollow(FOLLOW_31);
            rule__Atomic__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_9__0"


    // $ANTLR start "rule__Atomic__Group_9__0__Impl"
    // InternalExpressionDsl.g:3408:1: rule__Atomic__Group_9__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3412:1: ( ( () ) )
            // InternalExpressionDsl.g:3413:1: ( () )
            {
            // InternalExpressionDsl.g:3413:1: ( () )
            // InternalExpressionDsl.g:3414:2: ()
            {
             before(grammarAccess.getAtomicAccess().getLiteralNullAction_9_0()); 
            // InternalExpressionDsl.g:3415:2: ()
            // InternalExpressionDsl.g:3415:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getLiteralNullAction_9_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_9__0__Impl"


    // $ANTLR start "rule__Atomic__Group_9__1"
    // InternalExpressionDsl.g:3423:1: rule__Atomic__Group_9__1 : rule__Atomic__Group_9__1__Impl ;
    public final void rule__Atomic__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3427:1: ( rule__Atomic__Group_9__1__Impl )
            // InternalExpressionDsl.g:3428:2: rule__Atomic__Group_9__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_9__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_9__1"


    // $ANTLR start "rule__Atomic__Group_9__1__Impl"
    // InternalExpressionDsl.g:3434:1: rule__Atomic__Group_9__1__Impl : ( RULE_E_NULL ) ;
    public final void rule__Atomic__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3438:1: ( ( RULE_E_NULL ) )
            // InternalExpressionDsl.g:3439:1: ( RULE_E_NULL )
            {
            // InternalExpressionDsl.g:3439:1: ( RULE_E_NULL )
            // InternalExpressionDsl.g:3440:2: RULE_E_NULL
            {
             before(grammarAccess.getAtomicAccess().getE_NULLTerminalRuleCall_9_1()); 
            match(input,RULE_E_NULL,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getE_NULLTerminalRuleCall_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_9__1__Impl"


    // $ANTLR start "rule__Feature__Group_1__0"
    // InternalExpressionDsl.g:3450:1: rule__Feature__Group_1__0 : rule__Feature__Group_1__0__Impl rule__Feature__Group_1__1 ;
    public final void rule__Feature__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3454:1: ( rule__Feature__Group_1__0__Impl rule__Feature__Group_1__1 )
            // InternalExpressionDsl.g:3455:2: rule__Feature__Group_1__0__Impl rule__Feature__Group_1__1
            {
            pushFollow(FOLLOW_42);
            rule__Feature__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__0"


    // $ANTLR start "rule__Feature__Group_1__0__Impl"
    // InternalExpressionDsl.g:3462:1: rule__Feature__Group_1__0__Impl : ( ruleIDENTIFIER ) ;
    public final void rule__Feature__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3466:1: ( ( ruleIDENTIFIER ) )
            // InternalExpressionDsl.g:3467:1: ( ruleIDENTIFIER )
            {
            // InternalExpressionDsl.g:3467:1: ( ruleIDENTIFIER )
            // InternalExpressionDsl.g:3468:2: ruleIDENTIFIER
            {
             before(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIDENTIFIER();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__0__Impl"


    // $ANTLR start "rule__Feature__Group_1__1"
    // InternalExpressionDsl.g:3477:1: rule__Feature__Group_1__1 : rule__Feature__Group_1__1__Impl rule__Feature__Group_1__2 ;
    public final void rule__Feature__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3481:1: ( rule__Feature__Group_1__1__Impl rule__Feature__Group_1__2 )
            // InternalExpressionDsl.g:3482:2: rule__Feature__Group_1__1__Impl rule__Feature__Group_1__2
            {
            pushFollow(FOLLOW_34);
            rule__Feature__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__1"


    // $ANTLR start "rule__Feature__Group_1__1__Impl"
    // InternalExpressionDsl.g:3489:1: rule__Feature__Group_1__1__Impl : ( '.' ) ;
    public final void rule__Feature__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3493:1: ( ( '.' ) )
            // InternalExpressionDsl.g:3494:1: ( '.' )
            {
            // InternalExpressionDsl.g:3494:1: ( '.' )
            // InternalExpressionDsl.g:3495:2: '.'
            {
             before(grammarAccess.getFeatureAccess().getFullStopKeyword_1_1()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getFeatureAccess().getFullStopKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__1__Impl"


    // $ANTLR start "rule__Feature__Group_1__2"
    // InternalExpressionDsl.g:3504:1: rule__Feature__Group_1__2 : rule__Feature__Group_1__2__Impl ;
    public final void rule__Feature__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3508:1: ( rule__Feature__Group_1__2__Impl )
            // InternalExpressionDsl.g:3509:2: rule__Feature__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__2"


    // $ANTLR start "rule__Feature__Group_1__2__Impl"
    // InternalExpressionDsl.g:3515:1: rule__Feature__Group_1__2__Impl : ( ruleIDENTIFIER ) ;
    public final void rule__Feature__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3519:1: ( ( ruleIDENTIFIER ) )
            // InternalExpressionDsl.g:3520:1: ( ruleIDENTIFIER )
            {
            // InternalExpressionDsl.g:3520:1: ( ruleIDENTIFIER )
            // InternalExpressionDsl.g:3521:2: ruleIDENTIFIER
            {
             before(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_1_2()); 
            pushFollow(FOLLOW_2);
            ruleIDENTIFIER();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__2__Impl"


    // $ANTLR start "rule__IDENTIFIER__Group__0"
    // InternalExpressionDsl.g:3531:1: rule__IDENTIFIER__Group__0 : rule__IDENTIFIER__Group__0__Impl rule__IDENTIFIER__Group__1 ;
    public final void rule__IDENTIFIER__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3535:1: ( rule__IDENTIFIER__Group__0__Impl rule__IDENTIFIER__Group__1 )
            // InternalExpressionDsl.g:3536:2: rule__IDENTIFIER__Group__0__Impl rule__IDENTIFIER__Group__1
            {
            pushFollow(FOLLOW_43);
            rule__IDENTIFIER__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IDENTIFIER__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IDENTIFIER__Group__0"


    // $ANTLR start "rule__IDENTIFIER__Group__0__Impl"
    // InternalExpressionDsl.g:3543:1: rule__IDENTIFIER__Group__0__Impl : ( ( rule__IDENTIFIER__Alternatives_0 ) ) ;
    public final void rule__IDENTIFIER__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3547:1: ( ( ( rule__IDENTIFIER__Alternatives_0 ) ) )
            // InternalExpressionDsl.g:3548:1: ( ( rule__IDENTIFIER__Alternatives_0 ) )
            {
            // InternalExpressionDsl.g:3548:1: ( ( rule__IDENTIFIER__Alternatives_0 ) )
            // InternalExpressionDsl.g:3549:2: ( rule__IDENTIFIER__Alternatives_0 )
            {
             before(grammarAccess.getIDENTIFIERAccess().getAlternatives_0()); 
            // InternalExpressionDsl.g:3550:2: ( rule__IDENTIFIER__Alternatives_0 )
            // InternalExpressionDsl.g:3550:3: rule__IDENTIFIER__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__IDENTIFIER__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getIDENTIFIERAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IDENTIFIER__Group__0__Impl"


    // $ANTLR start "rule__IDENTIFIER__Group__1"
    // InternalExpressionDsl.g:3558:1: rule__IDENTIFIER__Group__1 : rule__IDENTIFIER__Group__1__Impl ;
    public final void rule__IDENTIFIER__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3562:1: ( rule__IDENTIFIER__Group__1__Impl )
            // InternalExpressionDsl.g:3563:2: rule__IDENTIFIER__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IDENTIFIER__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IDENTIFIER__Group__1"


    // $ANTLR start "rule__IDENTIFIER__Group__1__Impl"
    // InternalExpressionDsl.g:3569:1: rule__IDENTIFIER__Group__1__Impl : ( ( rule__IDENTIFIER__Alternatives_1 )* ) ;
    public final void rule__IDENTIFIER__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3573:1: ( ( ( rule__IDENTIFIER__Alternatives_1 )* ) )
            // InternalExpressionDsl.g:3574:1: ( ( rule__IDENTIFIER__Alternatives_1 )* )
            {
            // InternalExpressionDsl.g:3574:1: ( ( rule__IDENTIFIER__Alternatives_1 )* )
            // InternalExpressionDsl.g:3575:2: ( rule__IDENTIFIER__Alternatives_1 )*
            {
             before(grammarAccess.getIDENTIFIERAccess().getAlternatives_1()); 
            // InternalExpressionDsl.g:3576:2: ( rule__IDENTIFIER__Alternatives_1 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( ((LA24_0>=RULE_NUMBER && LA24_0<=RULE_E_CHAR)||LA24_0==28) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalExpressionDsl.g:3576:3: rule__IDENTIFIER__Alternatives_1
            	    {
            	    pushFollow(FOLLOW_44);
            	    rule__IDENTIFIER__Alternatives_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getIDENTIFIERAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IDENTIFIER__Group__1__Impl"


    // $ANTLR start "rule__Conditional__ExpressionTAssignment_1_2"
    // InternalExpressionDsl.g:3585:1: rule__Conditional__ExpressionTAssignment_1_2 : ( ruleConditional ) ;
    public final void rule__Conditional__ExpressionTAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3589:1: ( ( ruleConditional ) )
            // InternalExpressionDsl.g:3590:2: ( ruleConditional )
            {
            // InternalExpressionDsl.g:3590:2: ( ruleConditional )
            // InternalExpressionDsl.g:3591:3: ruleConditional
            {
             before(grammarAccess.getConditionalAccess().getExpressionTConditionalParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleConditional();

            state._fsp--;

             after(grammarAccess.getConditionalAccess().getExpressionTConditionalParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__ExpressionTAssignment_1_2"


    // $ANTLR start "rule__Conditional__ExpressionFAssignment_1_4"
    // InternalExpressionDsl.g:3600:1: rule__Conditional__ExpressionFAssignment_1_4 : ( ruleConditional ) ;
    public final void rule__Conditional__ExpressionFAssignment_1_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3604:1: ( ( ruleConditional ) )
            // InternalExpressionDsl.g:3605:2: ( ruleConditional )
            {
            // InternalExpressionDsl.g:3605:2: ( ruleConditional )
            // InternalExpressionDsl.g:3606:3: ruleConditional
            {
             before(grammarAccess.getConditionalAccess().getExpressionFConditionalParserRuleCall_1_4_0()); 
            pushFollow(FOLLOW_2);
            ruleConditional();

            state._fsp--;

             after(grammarAccess.getConditionalAccess().getExpressionFConditionalParserRuleCall_1_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conditional__ExpressionFAssignment_1_4"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalExpressionDsl.g:3615:1: rule__Or__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3619:1: ( ( ruleAnd ) )
            // InternalExpressionDsl.g:3620:2: ( ruleAnd )
            {
            // InternalExpressionDsl.g:3620:2: ( ruleAnd )
            // InternalExpressionDsl.g:3621:3: ruleAnd
            {
             before(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalExpressionDsl.g:3630:1: rule__And__RightAssignment_1_2 : ( ruleBitwiseOR ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3634:1: ( ( ruleBitwiseOR ) )
            // InternalExpressionDsl.g:3635:2: ( ruleBitwiseOR )
            {
            // InternalExpressionDsl.g:3635:2: ( ruleBitwiseOR )
            // InternalExpressionDsl.g:3636:3: ruleBitwiseOR
            {
             before(grammarAccess.getAndAccess().getRightBitwiseORParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleBitwiseOR();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightBitwiseORParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__BitwiseOR__OpAssignment_1_1"
    // InternalExpressionDsl.g:3645:1: rule__BitwiseOR__OpAssignment_1_1 : ( ( '|' ) ) ;
    public final void rule__BitwiseOR__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3649:1: ( ( ( '|' ) ) )
            // InternalExpressionDsl.g:3650:2: ( ( '|' ) )
            {
            // InternalExpressionDsl.g:3650:2: ( ( '|' ) )
            // InternalExpressionDsl.g:3651:3: ( '|' )
            {
             before(grammarAccess.getBitwiseORAccess().getOpVerticalLineKeyword_1_1_0()); 
            // InternalExpressionDsl.g:3652:3: ( '|' )
            // InternalExpressionDsl.g:3653:4: '|'
            {
             before(grammarAccess.getBitwiseORAccess().getOpVerticalLineKeyword_1_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getBitwiseORAccess().getOpVerticalLineKeyword_1_1_0()); 

            }

             after(grammarAccess.getBitwiseORAccess().getOpVerticalLineKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__OpAssignment_1_1"


    // $ANTLR start "rule__BitwiseOR__RightAssignment_1_2"
    // InternalExpressionDsl.g:3664:1: rule__BitwiseOR__RightAssignment_1_2 : ( ruleBitwiseXOR ) ;
    public final void rule__BitwiseOR__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3668:1: ( ( ruleBitwiseXOR ) )
            // InternalExpressionDsl.g:3669:2: ( ruleBitwiseXOR )
            {
            // InternalExpressionDsl.g:3669:2: ( ruleBitwiseXOR )
            // InternalExpressionDsl.g:3670:3: ruleBitwiseXOR
            {
             before(grammarAccess.getBitwiseORAccess().getRightBitwiseXORParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleBitwiseXOR();

            state._fsp--;

             after(grammarAccess.getBitwiseORAccess().getRightBitwiseXORParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseOR__RightAssignment_1_2"


    // $ANTLR start "rule__BitwiseXOR__OpAssignment_1_1"
    // InternalExpressionDsl.g:3679:1: rule__BitwiseXOR__OpAssignment_1_1 : ( ( '^' ) ) ;
    public final void rule__BitwiseXOR__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3683:1: ( ( ( '^' ) ) )
            // InternalExpressionDsl.g:3684:2: ( ( '^' ) )
            {
            // InternalExpressionDsl.g:3684:2: ( ( '^' ) )
            // InternalExpressionDsl.g:3685:3: ( '^' )
            {
             before(grammarAccess.getBitwiseXORAccess().getOpCircumflexAccentKeyword_1_1_0()); 
            // InternalExpressionDsl.g:3686:3: ( '^' )
            // InternalExpressionDsl.g:3687:4: '^'
            {
             before(grammarAccess.getBitwiseXORAccess().getOpCircumflexAccentKeyword_1_1_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getBitwiseXORAccess().getOpCircumflexAccentKeyword_1_1_0()); 

            }

             after(grammarAccess.getBitwiseXORAccess().getOpCircumflexAccentKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__OpAssignment_1_1"


    // $ANTLR start "rule__BitwiseXOR__RightAssignment_1_2"
    // InternalExpressionDsl.g:3698:1: rule__BitwiseXOR__RightAssignment_1_2 : ( ruleBitwiseAnd ) ;
    public final void rule__BitwiseXOR__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3702:1: ( ( ruleBitwiseAnd ) )
            // InternalExpressionDsl.g:3703:2: ( ruleBitwiseAnd )
            {
            // InternalExpressionDsl.g:3703:2: ( ruleBitwiseAnd )
            // InternalExpressionDsl.g:3704:3: ruleBitwiseAnd
            {
             before(grammarAccess.getBitwiseXORAccess().getRightBitwiseAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleBitwiseAnd();

            state._fsp--;

             after(grammarAccess.getBitwiseXORAccess().getRightBitwiseAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseXOR__RightAssignment_1_2"


    // $ANTLR start "rule__BitwiseAnd__OpAssignment_1_1"
    // InternalExpressionDsl.g:3713:1: rule__BitwiseAnd__OpAssignment_1_1 : ( ( '&' ) ) ;
    public final void rule__BitwiseAnd__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3717:1: ( ( ( '&' ) ) )
            // InternalExpressionDsl.g:3718:2: ( ( '&' ) )
            {
            // InternalExpressionDsl.g:3718:2: ( ( '&' ) )
            // InternalExpressionDsl.g:3719:3: ( '&' )
            {
             before(grammarAccess.getBitwiseAndAccess().getOpAmpersandKeyword_1_1_0()); 
            // InternalExpressionDsl.g:3720:3: ( '&' )
            // InternalExpressionDsl.g:3721:4: '&'
            {
             before(grammarAccess.getBitwiseAndAccess().getOpAmpersandKeyword_1_1_0()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getBitwiseAndAccess().getOpAmpersandKeyword_1_1_0()); 

            }

             after(grammarAccess.getBitwiseAndAccess().getOpAmpersandKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__OpAssignment_1_1"


    // $ANTLR start "rule__BitwiseAnd__RightAssignment_1_2"
    // InternalExpressionDsl.g:3732:1: rule__BitwiseAnd__RightAssignment_1_2 : ( ruleEquality ) ;
    public final void rule__BitwiseAnd__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3736:1: ( ( ruleEquality ) )
            // InternalExpressionDsl.g:3737:2: ( ruleEquality )
            {
            // InternalExpressionDsl.g:3737:2: ( ruleEquality )
            // InternalExpressionDsl.g:3738:3: ruleEquality
            {
             before(grammarAccess.getBitwiseAndAccess().getRightEqualityParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEquality();

            state._fsp--;

             after(grammarAccess.getBitwiseAndAccess().getRightEqualityParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseAnd__RightAssignment_1_2"


    // $ANTLR start "rule__Equality__OpAssignment_1_1"
    // InternalExpressionDsl.g:3747:1: rule__Equality__OpAssignment_1_1 : ( ( rule__Equality__OpAlternatives_1_1_0 ) ) ;
    public final void rule__Equality__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3751:1: ( ( ( rule__Equality__OpAlternatives_1_1_0 ) ) )
            // InternalExpressionDsl.g:3752:2: ( ( rule__Equality__OpAlternatives_1_1_0 ) )
            {
            // InternalExpressionDsl.g:3752:2: ( ( rule__Equality__OpAlternatives_1_1_0 ) )
            // InternalExpressionDsl.g:3753:3: ( rule__Equality__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getEqualityAccess().getOpAlternatives_1_1_0()); 
            // InternalExpressionDsl.g:3754:3: ( rule__Equality__OpAlternatives_1_1_0 )
            // InternalExpressionDsl.g:3754:4: rule__Equality__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Equality__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__OpAssignment_1_1"


    // $ANTLR start "rule__Equality__RightAssignment_1_2"
    // InternalExpressionDsl.g:3762:1: rule__Equality__RightAssignment_1_2 : ( ruleComparison ) ;
    public final void rule__Equality__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3766:1: ( ( ruleComparison ) )
            // InternalExpressionDsl.g:3767:2: ( ruleComparison )
            {
            // InternalExpressionDsl.g:3767:2: ( ruleComparison )
            // InternalExpressionDsl.g:3768:3: ruleComparison
            {
             before(grammarAccess.getEqualityAccess().getRightComparisonParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleComparison();

            state._fsp--;

             after(grammarAccess.getEqualityAccess().getRightComparisonParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__RightAssignment_1_2"


    // $ANTLR start "rule__Comparison__OpAssignment_1_1"
    // InternalExpressionDsl.g:3777:1: rule__Comparison__OpAssignment_1_1 : ( ( rule__Comparison__OpAlternatives_1_1_0 ) ) ;
    public final void rule__Comparison__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3781:1: ( ( ( rule__Comparison__OpAlternatives_1_1_0 ) ) )
            // InternalExpressionDsl.g:3782:2: ( ( rule__Comparison__OpAlternatives_1_1_0 ) )
            {
            // InternalExpressionDsl.g:3782:2: ( ( rule__Comparison__OpAlternatives_1_1_0 ) )
            // InternalExpressionDsl.g:3783:3: ( rule__Comparison__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getComparisonAccess().getOpAlternatives_1_1_0()); 
            // InternalExpressionDsl.g:3784:3: ( rule__Comparison__OpAlternatives_1_1_0 )
            // InternalExpressionDsl.g:3784:4: rule__Comparison__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__OpAssignment_1_1"


    // $ANTLR start "rule__Comparison__RightAssignment_1_2"
    // InternalExpressionDsl.g:3792:1: rule__Comparison__RightAssignment_1_2 : ( ruleBitwiseShift ) ;
    public final void rule__Comparison__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3796:1: ( ( ruleBitwiseShift ) )
            // InternalExpressionDsl.g:3797:2: ( ruleBitwiseShift )
            {
            // InternalExpressionDsl.g:3797:2: ( ruleBitwiseShift )
            // InternalExpressionDsl.g:3798:3: ruleBitwiseShift
            {
             before(grammarAccess.getComparisonAccess().getRightBitwiseShiftParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleBitwiseShift();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getRightBitwiseShiftParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__RightAssignment_1_2"


    // $ANTLR start "rule__BitwiseShift__OpAssignment_1_1"
    // InternalExpressionDsl.g:3807:1: rule__BitwiseShift__OpAssignment_1_1 : ( ( rule__BitwiseShift__OpAlternatives_1_1_0 ) ) ;
    public final void rule__BitwiseShift__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3811:1: ( ( ( rule__BitwiseShift__OpAlternatives_1_1_0 ) ) )
            // InternalExpressionDsl.g:3812:2: ( ( rule__BitwiseShift__OpAlternatives_1_1_0 ) )
            {
            // InternalExpressionDsl.g:3812:2: ( ( rule__BitwiseShift__OpAlternatives_1_1_0 ) )
            // InternalExpressionDsl.g:3813:3: ( rule__BitwiseShift__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getBitwiseShiftAccess().getOpAlternatives_1_1_0()); 
            // InternalExpressionDsl.g:3814:3: ( rule__BitwiseShift__OpAlternatives_1_1_0 )
            // InternalExpressionDsl.g:3814:4: rule__BitwiseShift__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__BitwiseShift__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getBitwiseShiftAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__OpAssignment_1_1"


    // $ANTLR start "rule__BitwiseShift__RightAssignment_1_2"
    // InternalExpressionDsl.g:3822:1: rule__BitwiseShift__RightAssignment_1_2 : ( rulePlusOrMinus ) ;
    public final void rule__BitwiseShift__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3826:1: ( ( rulePlusOrMinus ) )
            // InternalExpressionDsl.g:3827:2: ( rulePlusOrMinus )
            {
            // InternalExpressionDsl.g:3827:2: ( rulePlusOrMinus )
            // InternalExpressionDsl.g:3828:3: rulePlusOrMinus
            {
             before(grammarAccess.getBitwiseShiftAccess().getRightPlusOrMinusParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePlusOrMinus();

            state._fsp--;

             after(grammarAccess.getBitwiseShiftAccess().getRightPlusOrMinusParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BitwiseShift__RightAssignment_1_2"


    // $ANTLR start "rule__PlusOrMinus__RightAssignment_1_1"
    // InternalExpressionDsl.g:3837:1: rule__PlusOrMinus__RightAssignment_1_1 : ( ruleMulOrDiv ) ;
    public final void rule__PlusOrMinus__RightAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3841:1: ( ( ruleMulOrDiv ) )
            // InternalExpressionDsl.g:3842:2: ( ruleMulOrDiv )
            {
            // InternalExpressionDsl.g:3842:2: ( ruleMulOrDiv )
            // InternalExpressionDsl.g:3843:3: ruleMulOrDiv
            {
             before(grammarAccess.getPlusOrMinusAccess().getRightMulOrDivParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleMulOrDiv();

            state._fsp--;

             after(grammarAccess.getPlusOrMinusAccess().getRightMulOrDivParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PlusOrMinus__RightAssignment_1_1"


    // $ANTLR start "rule__MulOrDiv__OpAssignment_1_1"
    // InternalExpressionDsl.g:3852:1: rule__MulOrDiv__OpAssignment_1_1 : ( ( rule__MulOrDiv__OpAlternatives_1_1_0 ) ) ;
    public final void rule__MulOrDiv__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3856:1: ( ( ( rule__MulOrDiv__OpAlternatives_1_1_0 ) ) )
            // InternalExpressionDsl.g:3857:2: ( ( rule__MulOrDiv__OpAlternatives_1_1_0 ) )
            {
            // InternalExpressionDsl.g:3857:2: ( ( rule__MulOrDiv__OpAlternatives_1_1_0 ) )
            // InternalExpressionDsl.g:3858:3: ( rule__MulOrDiv__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getMulOrDivAccess().getOpAlternatives_1_1_0()); 
            // InternalExpressionDsl.g:3859:3: ( rule__MulOrDiv__OpAlternatives_1_1_0 )
            // InternalExpressionDsl.g:3859:4: rule__MulOrDiv__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__MulOrDiv__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getMulOrDivAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__OpAssignment_1_1"


    // $ANTLR start "rule__MulOrDiv__RightAssignment_1_2"
    // InternalExpressionDsl.g:3867:1: rule__MulOrDiv__RightAssignment_1_2 : ( rulePrimary ) ;
    public final void rule__MulOrDiv__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3871:1: ( ( rulePrimary ) )
            // InternalExpressionDsl.g:3872:2: ( rulePrimary )
            {
            // InternalExpressionDsl.g:3872:2: ( rulePrimary )
            // InternalExpressionDsl.g:3873:3: rulePrimary
            {
             before(grammarAccess.getMulOrDivAccess().getRightPrimaryParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getMulOrDivAccess().getRightPrimaryParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MulOrDiv__RightAssignment_1_2"


    // $ANTLR start "rule__Primary__ExpressionAssignment_0_2"
    // InternalExpressionDsl.g:3882:1: rule__Primary__ExpressionAssignment_0_2 : ( rulePrimary ) ;
    public final void rule__Primary__ExpressionAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3886:1: ( ( rulePrimary ) )
            // InternalExpressionDsl.g:3887:2: ( rulePrimary )
            {
            // InternalExpressionDsl.g:3887:2: ( rulePrimary )
            // InternalExpressionDsl.g:3888:3: rulePrimary
            {
             before(grammarAccess.getPrimaryAccess().getExpressionPrimaryParserRuleCall_0_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getExpressionPrimaryParserRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ExpressionAssignment_0_2"


    // $ANTLR start "rule__Primary__ExpressionAssignment_1_2"
    // InternalExpressionDsl.g:3897:1: rule__Primary__ExpressionAssignment_1_2 : ( rulePrimary ) ;
    public final void rule__Primary__ExpressionAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3901:1: ( ( rulePrimary ) )
            // InternalExpressionDsl.g:3902:2: ( rulePrimary )
            {
            // InternalExpressionDsl.g:3902:2: ( rulePrimary )
            // InternalExpressionDsl.g:3903:3: rulePrimary
            {
             before(grammarAccess.getPrimaryAccess().getExpressionPrimaryParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getExpressionPrimaryParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ExpressionAssignment_1_2"


    // $ANTLR start "rule__Primary__OpAssignment_3_1"
    // InternalExpressionDsl.g:3912:1: rule__Primary__OpAssignment_3_1 : ( ( rule__Primary__OpAlternatives_3_1_0 ) ) ;
    public final void rule__Primary__OpAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3916:1: ( ( ( rule__Primary__OpAlternatives_3_1_0 ) ) )
            // InternalExpressionDsl.g:3917:2: ( ( rule__Primary__OpAlternatives_3_1_0 ) )
            {
            // InternalExpressionDsl.g:3917:2: ( ( rule__Primary__OpAlternatives_3_1_0 ) )
            // InternalExpressionDsl.g:3918:3: ( rule__Primary__OpAlternatives_3_1_0 )
            {
             before(grammarAccess.getPrimaryAccess().getOpAlternatives_3_1_0()); 
            // InternalExpressionDsl.g:3919:3: ( rule__Primary__OpAlternatives_3_1_0 )
            // InternalExpressionDsl.g:3919:4: rule__Primary__OpAlternatives_3_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Primary__OpAlternatives_3_1_0();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getOpAlternatives_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__OpAssignment_3_1"


    // $ANTLR start "rule__Primary__ExpressionAssignment_3_2"
    // InternalExpressionDsl.g:3927:1: rule__Primary__ExpressionAssignment_3_2 : ( ruleAtomic ) ;
    public final void rule__Primary__ExpressionAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3931:1: ( ( ruleAtomic ) )
            // InternalExpressionDsl.g:3932:2: ( ruleAtomic )
            {
            // InternalExpressionDsl.g:3932:2: ( ruleAtomic )
            // InternalExpressionDsl.g:3933:3: ruleAtomic
            {
             before(grammarAccess.getPrimaryAccess().getExpressionAtomicParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAtomic();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getExpressionAtomicParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ExpressionAssignment_3_2"


    // $ANTLR start "rule__Primary__ExpressionAssignment_4_1"
    // InternalExpressionDsl.g:3942:1: rule__Primary__ExpressionAssignment_4_1 : ( ruleAtomic ) ;
    public final void rule__Primary__ExpressionAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3946:1: ( ( ruleAtomic ) )
            // InternalExpressionDsl.g:3947:2: ( ruleAtomic )
            {
            // InternalExpressionDsl.g:3947:2: ( ruleAtomic )
            // InternalExpressionDsl.g:3948:3: ruleAtomic
            {
             before(grammarAccess.getPrimaryAccess().getExpressionAtomicParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAtomic();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getExpressionAtomicParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ExpressionAssignment_4_1"


    // $ANTLR start "rule__Primary__OpAssignment_4_2"
    // InternalExpressionDsl.g:3957:1: rule__Primary__OpAssignment_4_2 : ( ( rule__Primary__OpAlternatives_4_2_0 ) ) ;
    public final void rule__Primary__OpAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3961:1: ( ( ( rule__Primary__OpAlternatives_4_2_0 ) ) )
            // InternalExpressionDsl.g:3962:2: ( ( rule__Primary__OpAlternatives_4_2_0 ) )
            {
            // InternalExpressionDsl.g:3962:2: ( ( rule__Primary__OpAlternatives_4_2_0 ) )
            // InternalExpressionDsl.g:3963:3: ( rule__Primary__OpAlternatives_4_2_0 )
            {
             before(grammarAccess.getPrimaryAccess().getOpAlternatives_4_2_0()); 
            // InternalExpressionDsl.g:3964:3: ( rule__Primary__OpAlternatives_4_2_0 )
            // InternalExpressionDsl.g:3964:4: rule__Primary__OpAlternatives_4_2_0
            {
            pushFollow(FOLLOW_2);
            rule__Primary__OpAlternatives_4_2_0();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getOpAlternatives_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__OpAssignment_4_2"


    // $ANTLR start "rule__Atomic__ValueAssignment_0_1"
    // InternalExpressionDsl.g:3972:1: rule__Atomic__ValueAssignment_0_1 : ( ruleE_INT ) ;
    public final void rule__Atomic__ValueAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3976:1: ( ( ruleE_INT ) )
            // InternalExpressionDsl.g:3977:2: ( ruleE_INT )
            {
            // InternalExpressionDsl.g:3977:2: ( ruleE_INT )
            // InternalExpressionDsl.g:3978:3: ruleE_INT
            {
             before(grammarAccess.getAtomicAccess().getValueE_INTParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleE_INT();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getValueE_INTParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_0_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_1_1"
    // InternalExpressionDsl.g:3987:1: rule__Atomic__ValueAssignment_1_1 : ( RULE_E_STRING ) ;
    public final void rule__Atomic__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:3991:1: ( ( RULE_E_STRING ) )
            // InternalExpressionDsl.g:3992:2: ( RULE_E_STRING )
            {
            // InternalExpressionDsl.g:3992:2: ( RULE_E_STRING )
            // InternalExpressionDsl.g:3993:3: RULE_E_STRING
            {
             before(grammarAccess.getAtomicAccess().getValueE_STRINGTerminalRuleCall_1_1_0()); 
            match(input,RULE_E_STRING,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueE_STRINGTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_1_1"


    // $ANTLR start "rule__Atomic__BodyAssignment_2_1"
    // InternalExpressionDsl.g:4002:1: rule__Atomic__BodyAssignment_2_1 : ( ruleFeature ) ;
    public final void rule__Atomic__BodyAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:4006:1: ( ( ruleFeature ) )
            // InternalExpressionDsl.g:4007:2: ( ruleFeature )
            {
            // InternalExpressionDsl.g:4007:2: ( ruleFeature )
            // InternalExpressionDsl.g:4008:3: ruleFeature
            {
             before(grammarAccess.getAtomicAccess().getBodyFeatureParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getBodyFeatureParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__BodyAssignment_2_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_3_1"
    // InternalExpressionDsl.g:4017:1: rule__Atomic__ValueAssignment_3_1 : ( RULE_E_BOOLEAN ) ;
    public final void rule__Atomic__ValueAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:4021:1: ( ( RULE_E_BOOLEAN ) )
            // InternalExpressionDsl.g:4022:2: ( RULE_E_BOOLEAN )
            {
            // InternalExpressionDsl.g:4022:2: ( RULE_E_BOOLEAN )
            // InternalExpressionDsl.g:4023:3: RULE_E_BOOLEAN
            {
             before(grammarAccess.getAtomicAccess().getValueE_BOOLEANTerminalRuleCall_3_1_0()); 
            match(input,RULE_E_BOOLEAN,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueE_BOOLEANTerminalRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_3_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_4_1"
    // InternalExpressionDsl.g:4032:1: rule__Atomic__ValueAssignment_4_1 : ( RULE_E_FLOAT ) ;
    public final void rule__Atomic__ValueAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:4036:1: ( ( RULE_E_FLOAT ) )
            // InternalExpressionDsl.g:4037:2: ( RULE_E_FLOAT )
            {
            // InternalExpressionDsl.g:4037:2: ( RULE_E_FLOAT )
            // InternalExpressionDsl.g:4038:3: RULE_E_FLOAT
            {
             before(grammarAccess.getAtomicAccess().getValueE_FLOATTerminalRuleCall_4_1_0()); 
            match(input,RULE_E_FLOAT,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueE_FLOATTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_4_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_5_1"
    // InternalExpressionDsl.g:4047:1: rule__Atomic__ValueAssignment_5_1 : ( RULE_E_LONG ) ;
    public final void rule__Atomic__ValueAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:4051:1: ( ( RULE_E_LONG ) )
            // InternalExpressionDsl.g:4052:2: ( RULE_E_LONG )
            {
            // InternalExpressionDsl.g:4052:2: ( RULE_E_LONG )
            // InternalExpressionDsl.g:4053:3: RULE_E_LONG
            {
             before(grammarAccess.getAtomicAccess().getValueE_LONGTerminalRuleCall_5_1_0()); 
            match(input,RULE_E_LONG,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueE_LONGTerminalRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_5_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_6_1"
    // InternalExpressionDsl.g:4062:1: rule__Atomic__ValueAssignment_6_1 : ( RULE_E_DOUBLE ) ;
    public final void rule__Atomic__ValueAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:4066:1: ( ( RULE_E_DOUBLE ) )
            // InternalExpressionDsl.g:4067:2: ( RULE_E_DOUBLE )
            {
            // InternalExpressionDsl.g:4067:2: ( RULE_E_DOUBLE )
            // InternalExpressionDsl.g:4068:3: RULE_E_DOUBLE
            {
             before(grammarAccess.getAtomicAccess().getValueE_DOUBLETerminalRuleCall_6_1_0()); 
            match(input,RULE_E_DOUBLE,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueE_DOUBLETerminalRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_6_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_7_1"
    // InternalExpressionDsl.g:4077:1: rule__Atomic__ValueAssignment_7_1 : ( RULE_E_BYTE ) ;
    public final void rule__Atomic__ValueAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:4081:1: ( ( RULE_E_BYTE ) )
            // InternalExpressionDsl.g:4082:2: ( RULE_E_BYTE )
            {
            // InternalExpressionDsl.g:4082:2: ( RULE_E_BYTE )
            // InternalExpressionDsl.g:4083:3: RULE_E_BYTE
            {
             before(grammarAccess.getAtomicAccess().getValueE_BYTETerminalRuleCall_7_1_0()); 
            match(input,RULE_E_BYTE,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueE_BYTETerminalRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_7_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_8_2"
    // InternalExpressionDsl.g:4092:1: rule__Atomic__ValueAssignment_8_2 : ( RULE_E_CHAR ) ;
    public final void rule__Atomic__ValueAssignment_8_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionDsl.g:4096:1: ( ( RULE_E_CHAR ) )
            // InternalExpressionDsl.g:4097:2: ( RULE_E_CHAR )
            {
            // InternalExpressionDsl.g:4097:2: ( RULE_E_CHAR )
            // InternalExpressionDsl.g:4098:3: RULE_E_CHAR
            {
             before(grammarAccess.getAtomicAccess().getValueE_CHARTerminalRuleCall_8_2_0()); 
            match(input,RULE_E_CHAR,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueE_CHARTerminalRuleCall_8_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_8_2"

    // Delegated rules


    protected DFA6 dfa6 = new DFA6(this);
    protected DFA10 dfa10 = new DFA10(this);
    static final String dfa_1s = "\35\uffff";
    static final String dfa_2s = "\5\uffff\11\21\1\uffff\1\21\2\uffff\3\21\2\uffff\6\21";
    static final String dfa_3s = "\1\4\4\uffff\2\16\2\4\5\16\1\5\1\16\2\uffff\3\4\1\5\1\46\2\4\1\16\3\4";
    static final String dfa_4s = "\1\46\4\uffff\11\52\1\5\1\52\2\uffff\3\52\1\34\1\46\6\52";
    static final String dfa_5s = "\1\uffff\1\1\1\2\1\3\1\4\13\uffff\1\5\1\6\13\uffff";
    static final String dfa_6s = "\35\uffff}>";
    static final String[] dfa_7s = {
            "\1\5\1\7\1\17\1\6\1\11\1\12\1\13\1\14\1\15\15\uffff\2\4\1\10\5\uffff\1\2\1\1\1\3\1\uffff\1\16",
            "",
            "",
            "",
            "",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "\1\24\1\22\10\uffff\14\21\2\20\1\23\6\21\2\uffff\1\21\1\uffff\1\25\3\21",
            "\1\24\1\22\10\uffff\14\21\2\20\1\23\6\21\2\uffff\1\21\1\uffff\1\25\3\21",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "\1\26",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "",
            "",
            "\1\24\1\22\10\uffff\14\21\2\20\1\23\6\21\2\uffff\1\21\1\uffff\1\25\3\21",
            "\1\24\1\22\10\uffff\14\21\2\20\1\23\6\21\2\uffff\1\21\1\uffff\1\25\3\21",
            "\1\24\1\22\10\uffff\14\21\2\20\1\23\6\21\2\uffff\1\21\1\uffff\1\25\3\21",
            "\1\27\26\uffff\1\30",
            "\1\31",
            "\1\34\1\32\10\uffff\14\21\2\20\1\33\6\21\2\uffff\1\21\2\uffff\3\21",
            "\1\34\1\32\10\uffff\14\21\2\20\1\33\6\21\2\uffff\1\21\2\uffff\3\21",
            "\14\21\2\20\1\uffff\6\21\2\uffff\1\21\2\uffff\3\21",
            "\1\34\1\32\10\uffff\14\21\2\20\1\33\6\21\2\uffff\1\21\2\uffff\3\21",
            "\1\34\1\32\10\uffff\14\21\2\20\1\33\6\21\2\uffff\1\21\2\uffff\3\21",
            "\1\34\1\32\10\uffff\14\21\2\20\1\33\6\21\2\uffff\1\21\2\uffff\3\21"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "606:1: rule__Primary__Alternatives : ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__Group_1__0 ) ) | ( ( rule__Primary__Group_2__0 ) ) | ( ( rule__Primary__Group_3__0 ) ) | ( ( rule__Primary__Group_4__0 ) ) | ( ruleAtomic ) );";
        }
    }
    static final String dfa_8s = "\10\uffff";
    static final String dfa_9s = "\1\uffff\5\7\2\uffff";
    static final String dfa_10s = "\1\5\5\4\2\uffff";
    static final String dfa_11s = "\1\34\5\52\2\uffff";
    static final String dfa_12s = "\6\uffff\1\2\1\1";
    static final String dfa_13s = "\10\uffff}>";
    static final String[] dfa_14s = {
            "\1\1\26\uffff\1\2",
            "\1\5\1\3\10\uffff\16\7\1\4\6\7\2\uffff\1\7\1\uffff\1\6\3\7",
            "\1\5\1\3\10\uffff\16\7\1\4\6\7\2\uffff\1\7\1\uffff\1\6\3\7",
            "\1\5\1\3\10\uffff\16\7\1\4\6\7\2\uffff\1\7\1\uffff\1\6\3\7",
            "\1\5\1\3\10\uffff\16\7\1\4\6\7\2\uffff\1\7\1\uffff\1\6\3\7",
            "\1\5\1\3\10\uffff\16\7\1\4\6\7\2\uffff\1\7\1\uffff\1\6\3\7",
            "",
            ""
    };

    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final char[] dfa_11 = DFA.unpackEncodedStringToUnsignedChars(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[] dfa_13 = DFA.unpackEncodedString(dfa_13s);
    static final short[][] dfa_14 = unpackEncodedStringArray(dfa_14s);

    class DFA10 extends DFA {

        public DFA10(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 10;
            this.eot = dfa_8;
            this.eof = dfa_9;
            this.min = dfa_10;
            this.max = dfa_11;
            this.accept = dfa_12;
            this.special = dfa_13;
            this.transition = dfa_14;
        }
        public String getDescription() {
            return "762:1: rule__Feature__Alternatives : ( ( ruleIDENTIFIER ) | ( ( rule__Feature__Group_1__0 ) ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000005C1C001FF0L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000010000000002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000020000000002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x000000000000C002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x00000000000F0000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x00000000000F0002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000700000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000700002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000600000002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000003800000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000003800002L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000004010001FF0L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000010000020L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000010000030L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000010000032L});

}