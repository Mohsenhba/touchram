package ca.mcgill.sel.ram.expressions.tests

import static extension org.junit.Assert.*

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith
import ca.mcgill.sel.ram.ValueSpecification
import org.xtext.example.mydsl.tests.ExpressionsDslInjectorProvider
import org.eclipse.xtext.testing.validation.ValidationTestHelper

@RunWith(XtextRunner)
@InjectWith(ExpressionsDslInjectorProvider)
class ExpressionParserTest {
    @Inject extension ParseHelper<ValueSpecification> parseHelper
    @Inject extension ValidationTestHelper
    extension TestUtils utils = new TestUtils

    /**
     * Testing that none of the letters are used as hidden terminals
     */
    @Test
    def void testHiddenTerminals() {
        "abcdefghijklmnopqrstuvwxyz".toCharArray().forEach[e|'''«e»'''.parse.assertNoErrors]
        "abcdefghijklmnopqrstuvwxyz".toUpperCase().toCharArray().forEach[e|'''«e»'''.parse.assertNoErrors]
    }

    @Test
    def void testIdentifierWithKeywords(){
        "testString".assertRepr("testString")
        "teststring".assertRepr("teststring")
        "testr".assertRepr("testr")
        "testfa".assertRepr("testfa")
        "testnu".assertRepr("testnu")
    }
    
    @Test
    def void testBasicParsingOperations() {
        '''null'''.parse.assertNoErrors
        '''1'''.parse.assertNoErrors
        '''1'''.parse.assertNoErrors
        '''12132L'''.parse.assertNoErrors
        '''1.0f'''.parse.assertNoErrors
        '''1.0F'''.parse.assertNoErrors
        '''1F'''.parse.assertNoErrors
        '''1.0d'''.parse.assertNoErrors
        '''1.0D'''.parse.assertNoErrors
        '''1D'''.parse.assertNoErrors
        '''"some characters"'''.parse.assertNoErrors
        '''true'''.parse.assertNoErrors

        '''2b'''.parse.assertNoErrors
        '''123b'''.parse.assertNoErrors
        
        '''2+3'''.parse.assertNoErrors
        '''2-3'''.parse.assertNoErrors
        '''3*4'''.parse.assertNoErrors
        '''2/3'''.parse.assertNoErrors
        '''1 > 0'''.parse.assertNoErrors
        '''0 >= 0'''.parse.assertNoErrors
        '''2 <= 2'''.parse.assertNoErrors
        '''2 < 3'''.parse.assertNoErrors
        '''5 == 5'''.parse.assertNoErrors
        '''5 != 3'''.parse.assertNoErrors
        '''true && false'''.parse.assertNoErrors
        '''true || false'''.parse.assertNoErrors
    }

    @Test
    def void testAssociativity() {
        "1 + 3 + 4".assertRepr("((1 + 3) + 4)")
        "1 - 3 + 4".assertRepr("((1 - 3) + 4)")
    }

    @Test def void testBitShfits() {
        "3 >> 4".assertRepr("(3 >> 4)")
        "3 << 4".assertRepr("(3 << 4)")
        "3 >>> 4".assertRepr("(3 >>> 4)")
    }
    
    @Test def void testBitwiseLogicOperators() {
        "3 & 4".assertRepr("(3 & 4)")
        "3 | 4".assertRepr("(3 | 4)")
        "3 ^ 4".assertRepr("(3 ^ 4)")
    }
    
    @Test def void testBitwiseOperatorPresendence() {
        "a | 4 + c >> d & 7".assertRepr("(a | (((4 + c) >> d) & 7))")
    }

    @Test def void testModulusOperator() {
        "2 % 3".assertRepr("(2 % 3)")
    }
    
    @Test def void testConditionalTernaryOperator() {
        "!true?1:1+3".assertRepr("((!true) ? 1 : (1 + 3))")
        "true?13:12".assertRepr("(true ? 13 : 12)")
        "true?13:false?123:as".assertRepr("(true ? 13 : (false ? 123 : as))")
        "true?false?123:as:12".assertRepr("(true ? (false ? 123 : as) : 12)")
        "true?1 < 3?123:as:12".assertRepr("(true ? ((1 < 3) ? 123 : as) : 12)")
    }

    @Test def void testPreIncrementOrDecrement() {
        "++a".assertRepr("++a")
        "--a".assertRepr("--a")
    }

    @Test def void testPostIncrementOrDecrement() {
        "a++".assertRepr("a++")
        "c--".assertRepr("c--")
        "3--".assertRepr("3--")
    }

    @Test def void testPrecedence() {
        "!true||false&&1>>3>(1/3+5*2)".assertRepr("((!true) || (false && ((1 >> 3) > ((1 / 3) + (5 * 2)))))")
    }

    def private assertRepr(CharSequence input, CharSequence expected) {
        expected.assertEquals((input).parse.stringRepr);
    }
}
