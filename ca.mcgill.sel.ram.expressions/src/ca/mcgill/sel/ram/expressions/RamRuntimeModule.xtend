package ca.mcgill.sel.ram.expressions

import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule
import com.google.inject.Inject
import org.eclipse.xtext.resource.IContainer
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.resource.IResourceDescriptions
import org.eclipse.xtext.naming.SimpleNameProvider

class RamRuntimeModule extends AbstractGenericResourceRuntimeModule {
    
   @Inject IContainer.Manager manager
 
    def void printVisibleResources(Resource resource, IResourceDescriptions index) {
        val descr = index.getResourceDescription(resource.URI)
        for (visibleContainer : manager.getVisibleContainers(descr, index)) {
            for (visibleResourceDesc : visibleContainer.resourceDescriptions) {
                println(visibleResourceDesc.URI)
            }
        }
    }
    override protected getFileExtensions() {
        'ram'
    }
    
    override protected getLanguageName() {
        'ca.mcgill.sel.ram.ram.Ram'
    }
    
    override bindIQualifiedNameProvider() {
//        DefaultDeclarativeQualifiedNameProvider
        SimpleNameProvider
    }
    
}