package ca.mcgill.sel.ram.expressions

import com.google.inject.Guice
import com.google.inject.Inject
import org.eclipse.xtext.ISetup
import org.eclipse.xtext.resource.FileExtensionProvider
import org.eclipse.xtext.resource.IResourceServiceProvider

class RamStandaloneSetup implements ISetup {
    
    @Inject
    FileExtensionProvider fileExtensionProvider;

    @Inject
    IResourceServiceProvider resourceServiceProvider;

    @Inject
    IResourceServiceProvider.Registry registry;
    
    def static void doSetup() {
        new RamStandaloneSetup().createInjectorAndDoEMFRegistration()
    }
    
    override createInjectorAndDoEMFRegistration() {
        val injector = Guice.createInjector(new RamRuntimeModule)
        injector.injectMembers(this)
        for(fileExt: fileExtensionProvider.fileExtensions) 
            registry.extensionToFactoryMap.put(fileExt, resourceServiceProvider)
        return injector
    }
}