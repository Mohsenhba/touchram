package ca.mcgill.sel.ram.expressions.conversion;

import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractLexerBasedConverter;
import org.eclipse.xtext.nodemodel.INode;

/**
 * ValueConverter for a LiteralLong.
 * 
 * @author Rohit Verma
 */
public class LONGValueConverter extends AbstractLexerBasedConverter<Long> {

    /**
     * Value of a LiteralLong.
     */
    private String intPart;
    
    /**
     * Extracts the integer part from the value of the LiteralLong, e.g., returns 1 if the string is "1L".
     * @param str the given input.
     * @param node the parsed node.
     * @return the new {@code int}.
     * @throws ValueConverterException if the given input is {@code null}, empty or does not
     *  represent an integer number.
     */
    @Override public Long toValue(String str, INode node) throws ValueConverterException {
        if (str.isEmpty()) {
            throw new ValueConverterException("Couldn't convert empty string to long.", node, null);
        }
        if (str.contains("L")) {
            String[] strArray = str.split("L");
            intPart = strArray[0];
        }
        try {
            return Long.parseLong(intPart, 10);
        } catch (NumberFormatException e) {
            throw parsingError(str, node, e);
        }
    }

    /**
     * Throws Exception if the ValueConverter failed to do its job.
     * @param str the given input.
     * @param node the parsed node.
     * @param cause of the exception
     * @return the new {@code int}.
     * @throws ValueConverterException if the given input is {@code null}, empty or does not
     *  represent an integer number.
     */
    private ValueConverterException parsingError(String str, INode node, Exception cause) {
        return new ValueConverterException("Couldn't convert '" + str + "' to long.", node, cause);
    }
}