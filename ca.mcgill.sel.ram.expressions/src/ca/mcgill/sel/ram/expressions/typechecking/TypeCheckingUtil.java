package ca.mcgill.sel.ram.expressions.typechecking;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.mcgill.sel.ram.Comparison;
import ca.mcgill.sel.ram.Equality;
import ca.mcgill.sel.ram.Minus;
import ca.mcgill.sel.ram.MulDivMod;
import ca.mcgill.sel.ram.Plus;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * A utility class that contains information about typechecking logic to be accessible by all Node types.
 *
 * @author Rohit Verma
 */
public final class TypeCheckingUtil {

    /**
     * List of operand types allowed to be used with the {@link Plus} operator.
     */
    public static final List<Types> PLUS_OPERANDS = Arrays.asList(Types.doubleType, Types.floatType,
            Types.longType, Types.stringType, Types.intType, Types.referenceIntType, Types.charType, Types.byteType);

    /**
     * List of operand types allowed to be used with the {@link Minus} operator.
     */
    public static final List<Types> MINUS_OPERANDS = Arrays.asList(Types.doubleType, Types.floatType, Types.longType,
            Types.stringType, Types.intType, Types.referenceIntType, Types.charType, Types.byteType);

    /**
     * List of operand types allowed to be used with the {@link MulDivMod} operator.
     */
    public static final List<Types> MUL_DIV_MOD_OPERAND = Arrays.asList(Types.doubleType, Types.floatType,
            Types.longType, Types.intType, Types.charType, Types.referenceIntType);

    /**
     * List of operand types allowed to be used with the {@link Comparison} operator.
     */
    public static final List<Types> COMPARISON_OPERANDS = Arrays.asList(Types.doubleType, Types.floatType,
            Types.longType, Types.intType, Types.referenceIntType, Types.charType, Types.byteType);

    // can also be applied to short type
    /**
     * List of operand types allowed to be used with the BitwiseShift operator.
     */
    public static final List<Types> BITWISESHIFT_OPERANDS =
            Arrays.asList(Types.longType, Types.intType, Types.referenceIntType, Types.charType, Types.byteType);

    /**
     * List of operand types allowed to be used with the {@link Equality} operator.
     */
    public static final List<Types> EQUALITY_OPERANDS = Arrays.asList(Types.doubleType, Types.floatType, Types.longType,
            Types.intType, Types.referenceIntType, Types.charType, Types.byteType,
            Types.nullType, Types.userDefinedType);

    /**
     * List of operand types allowed to be used with the operators: And, Or, Not.
     */
    public static final List<Types> BOOLEAN_TYPE_OPERANDS = Arrays.asList(Types.booleanType);

    /**
     * It stores value for each type.
     */
    @SuppressWarnings("serial")
    public static final Map<Types, Integer> TYPE_PROMOTION_MAP = new HashMap<Types, Integer>() {
        {
            put(Types.stringType, TypeCheckingUtil.STRING_PRIORITY);
            put(Types.doubleType, TypeCheckingUtil.DOUBLE_PRIORITY);
            put(Types.floatType, TypeCheckingUtil.FLOAT_PRIORITY);
            put(Types.longType, TypeCheckingUtil.LONG_PRIORITY);
            put(Types.intType, TypeCheckingUtil.INT_PRIORITY);
            put(Types.referenceIntType, TypeCheckingUtil.INT_PRIORITY);
            put(Types.charType, TypeCheckingUtil.CHAR_PRIORITY);
            put(Types.byteType, TypeCheckingUtil.BYTE_PRIORITY);
        }
    };

    /**
     * Stores priorities for different data types required for type promotion
     * mechanism.
     */
    /**
     * Priority for a String type.
     */
    public static final int STRING_PRIORITY = 1;
    /**
     * Priority for a Double type.
     */
    public static final int DOUBLE_PRIORITY = 2;
    /**
     * Priority for a Float type.
     */
    public static final int FLOAT_PRIORITY = 3;
    /**
     * Priority for a Long type.
     */
    public static final int LONG_PRIORITY = 4;
    /**
     * Priority for an Integer type.
     */
    public static final int INT_PRIORITY = 5;
    /**
     * Priority for a Char type.
     */
    public static final int CHAR_PRIORITY = 6;
    /**
     * Priority for a Byte type. Byte has the smallest size so it has the highest priority in type promotion.
     */
    public static final int BYTE_PRIORITY = 7;

    /**
     * Overriding the default public constructor so that the class
     * can't be instantiated from elsewhere in the code.
     */
    private TypeCheckingUtil() {
        // Do nothing
    }

    /**
     * This method is used for widening conversion.
     * It returns a targetType if the sourceType is smaller in size than a targetType.
     * We can only move down the priority not up.
     * 
     * promoteToType(int, float) -> float
     * 
     * @param sourceType current type of the expression.
     * @param targetType type that we are attempting to promote to.
     * @return type to promote to
     */
    public static Types promoteToType(Types sourceType, Types targetType) {
        return TYPE_PROMOTION_MAP.get(sourceType) < TYPE_PROMOTION_MAP.get(targetType) ? sourceType : targetType;
    }
}
