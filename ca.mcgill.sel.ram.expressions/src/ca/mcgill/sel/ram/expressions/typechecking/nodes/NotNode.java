package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * A class that contains the typechecking logic for a node representing "NOT" Operator..
 * 
 * @author Rohit Verma
 */
public final class NotNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private NotNode() {
    }

    /**
     * Method that implements the typechecking logic for the node representing "NOT" Operator.
     * 
     * @param expression : expression of NotNode
     * 
     * @return The type that NotNode should return
     */
    public static Types typeCheck(ValueSpecification expression) {

        Types shouldReturnType = null;
        Types expType = TypeChecker.resolveType(expression);

        if (expType != null) {
            if (expType.equals(Types.booleanType)) {
                shouldReturnType = Types.booleanType;
            } else {
                TypeChecker.addIssue(EMFEditUtil.getText(expression) + " is not a valid type for the NOT operation ");
            }
        }
        return shouldReturnType;
    }
}
