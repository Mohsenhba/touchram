package ca.mcgill.sel.classdiagram.ui.views;

import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionTitleView;
import ca.mcgill.sel.ram.ui.views.CompositionView;

/**
 * A subclass of CompositionTitleView that provides the Cdm-specific buttons for compositions.
 * 
 * @author joerg
 */
public class CdmCompositionTitleView extends CompositionTitleView implements ActionListener {

    /**
     * Button to add Classifier Mapping.
     */
    private RamButton buttonAddClassifierMapping;

    /**
     * Button to add Enum Mapping.
     */
    private RamButton buttonAddEnumMapping;

    /**
     * Creates a new title view.
     * 
     * @param container the composition container view that contains all the compositions
     * @param compositionView the {@link CompositionView} the title view is for
     * @param reuse - the {@link COREReuse} the composition is for.
     * @param detailedViewOn whether the detailed view should be enabled
     */
    public CdmCompositionTitleView(CompositionContainerView container,
            CompositionView compositionView, COREReuse reuse, boolean detailedViewOn) {
        super(container, compositionView, reuse, detailedViewOn);
    }

    @Override
    protected void destroyComponent() {
        super.destroyComponent();
        buttonAddClassifierMapping.destroy();
        buttonAddEnumMapping.destroy();
    }
   
    /**
     * Hides all buttons and shows the expand button.
     */
    @Override
    public void hideButtons() {
        super.hideButtons();
        this.removeChild(buttonAddClassifierMapping);
        this.removeChild(buttonAddEnumMapping);
    }
    
    /**
     * Shows all buttons including a collapse button.
     */
    @Override
    public void showButtons() {
        super.showButtons();        
        this.addChild(buttonAddClassifierMapping);
        this.addChild(buttonAddEnumMapping);
    }

    @Override
    public void addCustomButtons(boolean detailedViewOn) {
        RamImageComponent addClassifierImage = new RamImageComponent(Icons.ICON_CLASSIFIER_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        addClassifierImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        addClassifierImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonAddClassifierMapping = new RamButton(addClassifierImage);
        buttonAddClassifierMapping.setActionCommand(CdmCompositionContainerView.ACTION_CLASSIFIER_MAPPING_ADD);
        buttonAddClassifierMapping.addActionListener(this);
        if (detailedViewOn) {
            addChild(buttonAddClassifierMapping);
        }

        RamImageComponent addEnumImage = new RamImageComponent(Icons.ICON_ENUM_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        addEnumImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        addEnumImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonAddEnumMapping = new RamButton(addEnumImage);
        buttonAddEnumMapping.setActionCommand(CdmCompositionContainerView.ACTION_ENUM_MAPPING_ADD);
        buttonAddEnumMapping.addActionListener(this);
        if (detailedViewOn) {
            addChild(buttonAddEnumMapping);
        }        
    }
}
