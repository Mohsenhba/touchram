package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.classdiagram.CDEnumLiteralMapping;
import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;

/**
 * This view contains a EnumMappingView and all mapping related container views for a enum mapping.
 * 
 * @author joerg
 */
public class CdmEnumMappingContainerView extends RamRoundedRectangleComponent implements UINotifyChangedListener {
    // It is useful to keep the child as a reference for hiding/showing purposes
    private CdmEnumMappingView myCDEnumMappingView;

    // Enum Mapping information
    private CDEnumMapping myCDEnumMapping;

    // All the enum literal mapping related views will be in this view:
    private RamRectangleComponent hideableEnumLiteralContainer;

    /**
     * Creates a new mapping container view for enum mapopings.
     * 
     * @param enumMapping - {@link EnumMapping} which we want to create a {@link EnumMappingContainerView} for.
     */
    public CdmEnumMappingContainerView(CDEnumMapping enumMapping) {
        super(0);
        setNoStroke(false);
        setStrokeWeight(3);
        setNoFill(false);
        setFillColor(Colors.MAPPING_CONTAINER_VIEW_FILL_COLOR);

        myCDEnumMappingView = new CdmEnumMappingView(this, enumMapping);
        this.addChild(myCDEnumMappingView);

        myCDEnumMapping = enumMapping;

        hideableEnumLiteralContainer = new RamRectangleComponent();
        hideableEnumLiteralContainer.setLayout(new VerticalLayout(0));
        hideableEnumLiteralContainer.setFillColor(Colors.ATTRIBUTE_MAPPING_VIEW_FILL_COLOR);
        hideableEnumLiteralContainer.setNoFill(false);
        hideableEnumLiteralContainer.setBufferSize(Cardinal.EAST, 0);

        this.addChild(hideableEnumLiteralContainer);

        setLayout(new VerticalLayout());

        // setBufferSize(Cardinal.EAST, 0);
        setBuffers(0);
        // add all the operation and attribute mappings related to this classifier mapping.
        addAllEnumLiteralMappings();

        EMFEditUtil.addListenerFor(myCDEnumMapping, this);
    }

    /**
     * Adds all the enum literal mappings which belongs to the enum mapping.
     */
    private void addAllEnumLiteralMappings() {
        EList<COREMapping<?>> coreMappings = myCDEnumMapping.getMappings();
        for (COREMapping<?> mapping : coreMappings) {
            if (mapping instanceof CDEnumLiteralMapping) {
                addEnumLiteralMappingView((CDEnumLiteralMapping) mapping);
            }
        }
    }

    /**
     * Adds an {@link EnumLiteralMappingView} to the mapping container view (inside of hideableEnumLiteralContainer).
     * 
     * @param newEnumLiteralMapping the {@link EnumLiteralMapping} to add a view for
     */
    private void addEnumLiteralMappingView(CDEnumLiteralMapping newEnumLiteralMapping) {
        CdmEnumLiteralMappingView enumLiteralMappingView = new CdmEnumLiteralMappingView(this, newEnumLiteralMapping);
        hideableEnumLiteralContainer.addChild(enumLiteralMappingView);
    }

    /**
     * Deletes an {@link CdmEnumLiteralMappingView} from the 
     * mapping container view (inside the hideableEnumLiteralContainer).
     * 
     * @param deletedEnumLiteralMapping the {@link CDEnumLiteralMapping} to remove the view for
     */
    private void deleteEnumLiteralMappingView(CDEnumLiteralMapping deletedEnumLiteralMapping) {
        MTComponent[] enumLiteralMappingViews = hideableEnumLiteralContainer.getChildren();
        for (MTComponent view : enumLiteralMappingViews) {
            if (view instanceof CdmEnumLiteralMappingView) {
                CdmEnumLiteralMappingView enumLiteralMappingView = (CdmEnumLiteralMappingView) view;
                if (enumLiteralMappingView.getEnumLiteralMapping() == deletedEnumLiteralMapping) {
                    hideableEnumLiteralContainer.removeChild(enumLiteralMappingView);
                }
            }
        }
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(myCDEnumMapping, this);
        super.destroy();
    }

    /**
     * Getter for CDEnumMapping information of the view.
     * 
     * @return {@link CDEnumMapping}
     */
    public CDEnumMapping getClassifierMapping() {
        return myCDEnumMapping;
    }

    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == myCDEnumMapping) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MAPPING__MAPPINGS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:

                        CDEnumLiteralMapping newEnumLiteralMapping = (CDEnumLiteralMapping) notification.getNewValue();
                        addEnumLiteralMappingView(newEnumLiteralMapping);
                        break;

                    case Notification.REMOVE:
                        CDEnumLiteralMapping oldEnumLiteralMapping = (CDEnumLiteralMapping) notification.getOldValue();
                        deleteEnumLiteralMappingView(oldEnumLiteralMapping);
                        break;
                }
            }
        }

    }
}
