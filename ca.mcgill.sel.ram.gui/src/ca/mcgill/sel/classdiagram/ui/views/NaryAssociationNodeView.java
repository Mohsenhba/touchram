package ca.mcgill.sel.classdiagram.ui.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.ui.views.CDEnd.Position;
import ca.mcgill.sel.classdiagram.ui.views.handler.INaryAssociationNodeViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.RamApp.DisplayMode;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.utils.Colors;

/**
 * This view draws the representation of an association node. It's used as a connection center for n-ary associations
 * @author yhattab
 *
 */
public class NaryAssociationNodeView extends LinkableView<INaryAssociationNodeViewHandler> {

    /**
     * The stroke weight for drawing this view.
     */
    public static final float STROKE_WEIGHT = 2.0f;
    
    /**
     * The stroke color for drawing this view.
     */
    public static final MTColor STROKE_COLOR = Colors.CLASS_VIEW_DEFAULT_STROKE_COLOR;
    
    /**
     * The fill color for drawing this view.
     */
    public static final MTColor FILL_COLOR = Colors.CLASS_VIEW_DEFAULT_FILL_COLOR;
    
    /**
     * The minimum width of the view.
     */
    protected static final float MINIMUM_WIDTH = 25f;    

    /**
     * The distance from the node to create association classes at.
     */
    protected static final int DISTANCE_ASSOCIATION_CLASS = 150;
    
    /**
     * Reference to the represented {@link Association}.
     */
    protected Association association;
    
    /**
     * List of {@link NaryAssociationView} linked to this node.
     */
    protected List<NaryAssociationView> associationViews;
    
    /**
     * Link to the association class.
     */
    protected NaryAssociationView associationClassLink;
    
    /**
     * Number of ends the represented association has.
     */
    protected int numberOfEnds;
    
    /**
     * The polygon used for displaying the association node.
     */
    protected NaryNodeDiamond diamond;

    /**
     * Constructor for a the diamond central node of an Nary Association.
     * 
     * @param classDiagramView that will contain this view.
     * @param assoc Association represented by this view.
     * @param layoutElement of this view.
     */
    protected NaryAssociationNodeView(ClassDiagramView classDiagramView, Association assoc, 
            LayoutElement layoutElement) {
        super(classDiagramView, assoc, layoutElement);
        
        this.classDiagramView = classDiagramView;
        this.association = assoc;
        this.numberOfEnds = assoc.getEnds().size();
        this.associationViews = new ArrayList<NaryAssociationView>();
        
        this.setNoFill(true);
        this.setNoStroke(true);
        
        //set a fixed size
        this.setMinimumSize(MINIMUM_WIDTH, MINIMUM_WIDTH);
        this.setMaximumSize(MINIMUM_WIDTH, MINIMUM_WIDTH);
        this.inputOverlay.setMinimumSize(MINIMUM_WIDTH, MINIMUM_WIDTH);
        this.inputOverlay.setMaximumSize(MINIMUM_WIDTH, MINIMUM_WIDTH);
        
        if (layoutElement != null) {
            setLayoutElement(layoutElement);
        }
        
        buildDiamond();
        
        //if only binary association with an association class, we don't show the diamond
        if (numberOfEnds == 2) {
            this.diamond.setNoFill(true);
            this.diamond.setStrokeColor(Colors.BACKGROUND_COLOR);
        }
        
    }

    /**
     * Polygon representing a diamond.
     *
     * @author yhattab
     */
    protected class NaryNodeDiamond extends MTPolygon {

        /**
         * Creates a nary node polygon.
         *
         * @param x the X position of the upper left tip
         * @param y the Y position of the upper left tip
         * @param width of the diamond
         */
        public NaryNodeDiamond(float x, float y, float width) {
            super(RamApp.getApplication(), new Vertex[] {});
            
            // vertices should be set first
            float halfSize = width / 2.0f;
            Vertex left = new Vertex(x - halfSize, y);
            Vertex top = new Vertex(x, y - halfSize);
            Vertex right = new Vertex(x + halfSize, y);
            Vertex bottom = new Vertex(x, y + halfSize);

            this.setVertices(new Vertex[] {left, top, right, bottom, left});
            
            
            this.setNoStroke(false);
            this.setNoFill(false);
            this.setStrokeColor(STROKE_COLOR);
            this.setFillColor(FILL_COLOR);
            this.setEnabled(false);
            this.setPickable(false);
        }
    }
    
    /**
     * Builds and draws the Polygon representing a diamond for nary associations at the center of the association node.
     */
    private void buildDiamond() {
        float x = getCenterPointLocal().getX();
        float y = getCenterPointLocal().getY();
        
        diamond = new NaryNodeDiamond(x, y, MINIMUM_WIDTH);
        
        this.addChild(diamond, false);
    }

    /**
     * Returns the association represented by this view.
     * @return {@link Association}
     */
    public Association getAssociation() {
        return association;
    }
    
    /**
     * Returns the position with the least relationship ends and with some offset to create an association class on.
     * @return {@link Vector3D} approximatly least crowded position to create the association class at
     */
    public Vector3D getAssociationClassCreationPosition() {
        int smallestCount = Integer.MAX_VALUE;
        Position smallestPosition = Position.TOP;
        for (Position p : relationshipEndByPosition.keySet()) {
            if (p != Position.OFFSCREEN) {
                int count = relationshipEndByPosition.get(p).size();
                if (count < smallestCount) {
                    smallestCount = count;
                    smallestPosition = p;
                }
            }
        }
        
        float associationClassX = getX();
        float associationClassY = getY();
        
        //approximate least crowded area to create the new class in
        switch (smallestPosition) {
            case TOP:
                associationClassY = associationClassY - DISTANCE_ASSOCIATION_CLASS;
                break;
            case RIGHT:
                associationClassX = associationClassX + DISTANCE_ASSOCIATION_CLASS;
                break;
            case BOTTOM:
                associationClassY = associationClassY + DISTANCE_ASSOCIATION_CLASS;
                break;
            case LEFT:
                associationClassX = associationClassX - DISTANCE_ASSOCIATION_CLASS;
                break;
        }
            
        return new Vector3D(associationClassX, associationClassY);
    }
   
    /**
     * Overriding relation ship ends positioning approach to simplify it and adhere to the diamond edges locations.
     */
    @Override
    protected void setCorrectPosition(List<CDEnd<?, ? extends RamRectangleComponent>> list, Position position) {
        // to speed things up
        if (!list.isEmpty()) {
         // the top left position is needed to determine the new ends position along the side of the
            // class
            Vector3D classLocation = getLocalVecToParentRelativeSpace(this, getBounds().getVectorsLocal()[0]);

            //avalaible space is only half the height/width to avoid misaligning with diamond corners when possible
            float availableSpace = 0;
            switch (position) {
                case TOP:
                case BOTTOM:
                    availableSpace = getWidthXY(TransformSpace.RELATIVE_TO_PARENT) / 2f;
                    break;
                case LEFT:
                case RIGHT:
                    availableSpace = getHeightXY(TransformSpace.RELATIVE_TO_PARENT) / 2f;
                    break;
                default:
            }

            if (RamApp.getDisplayMode() == DisplayMode.PRETTY) {
                // we want a pretty drawing, reorder the ends so they're less likely to overlap
                Comparator<CDEnd<?, ?>> comparator;
                switch (position) {
                    case TOP:
                    case BOTTOM:
                        comparator = CDEnd.HORIZONTAL_COMPARATOR;
                        break;
                    case LEFT:
                    case RIGHT:
                        comparator = CDEnd.VERTICAL_COMPARATOR;
                        break;
                    case OFFSCREEN:
                        // set as null
                    default:
                        comparator = null;
                }
                if (comparator != null) {
                    Collections.sort(list, comparator);
                }
            }

            int actualNumOfSlots = calculateNumberOfSlots(list, position);   
            float dividerDistance = availableSpace / (actualNumOfSlots + 1);
     
            int currentSlot = 0;
            for (int index = 0; index < list.size(); index++) {
                CDEnd<?, ?> associationEnd = list.get(index);
                changePosition(associationEnd, classLocation, currentSlot, dividerDistance);
                currentSlot++;
            }
        }
    }

    @Override
    public void setSizeLocal(float width, float height) {
        super.setSizeLocal(width, height);
        updateRelationships();
    }

    @Override
    public void translate(Vector3D dirVect) {
        super.translate(dirVect);
        updateRelationships();
    }
    
    @Override
    protected void changePosition(CDEnd<?, ?> end, Vector3D classLocation, float number, float space) {
        Vector3D center = getCenterPointRelativeToParent();
        
        //if binary association with Association Class, hide polygon
        if (numberOfEnds == 2) {
            end.setLocation(center);
            //alternate position and spacing around diamond corner for aesthetical purposes
            int alternatingNumber = (int) ((number % 2 == 0) 
                    ? -1 * (((int) number) / 2) : (((int) number) / 2) + number % 2);
            switch (end.getPosition()) {
                case TOP:
                    end.setLocation(new Vector3D(center.getX() + alternatingNumber * space, center.getY()));
                    break;
                case BOTTOM:
                    end.setLocation(new Vector3D(center.getX() + alternatingNumber * space, center.getY()));
                    break;
                case RIGHT:
                    end.setLocation(new Vector3D(center.getX(), center.getY() + alternatingNumber * space));
                    break;
                case LEFT:
                    end.setLocation(new Vector3D(center.getX(), center.getY() + alternatingNumber * space));
                    break;
            }
        } else {
            //alternate position and spacing around diamond corner for aesthetical purposes
            int alternatingNumber = (int) ((number % 2 == 0) 
                    ? -1 * (((int) number) / 2) : (((int) number) / 2) + number % 2);
            switch (end.getPosition()) {
                case TOP:
                    end.setLocation(new Vector3D(center.getX() + alternatingNumber * space,
                            classLocation.getY()));
                    break;
                case BOTTOM:
                    end.setLocation(new Vector3D(center.getX() + alternatingNumber * space,
                            classLocation.getY() + getHeightXY(TransformSpace.RELATIVE_TO_PARENT)));
                    break;
                case RIGHT:
                    end.setLocation(new Vector3D(classLocation.getX() + getWidthXY(TransformSpace.RELATIVE_TO_PARENT),
                            center.getY() + alternatingNumber * space));
                    break;
                case LEFT:
                    end.setLocation(new Vector3D(classLocation.getX(), center.getY() + alternatingNumber * space));
                    break;
            }
        }
    }

    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        Class associationClass = null;
        if (notification.getNotifier() == association) {
            if (notification.getFeature() == CdmPackage.Literals.ASSOCIATION__ASSOCIATION_CLASS) {
                Association notificationAssociation = (Association) notification.getNotifier();
                if (association.getAssociationClass() == null) {
                    //Removing association class
                    removeAssociationClassLink();
                } else {
                    //Adding association class
                    associationClass = (Class) notificationAssociation.getAssociationClass();
                    classDiagramView.addAssociationClass(this, associationClass);  
                } 
            }
        }
    }

    /**
     * Returns the NaryAssociationView (link) to the association class.
     * @return {@link NaryAssociationView}
     */
    public NaryAssociationView getAssociationClassLink() {
        return associationClassLink;
    }

    /**
     * Sets the NaryAssociationView (link) to the association class.
     * @param associationClassLink the {@link NaryAssociationView} to the association class
     */
    public void setAssociationClassLink(NaryAssociationView associationClassLink) {
        this.associationClassLink = associationClassLink;
    }
    
    /**
     * Removes and destroys the NaryAssociationView (link) to the association class if it exists.
     */
    public void removeAssociationClassLink() {
        if (associationClassLink != null) {
            CDEnd<NamedElement, LinkableView<?>> toEnd = associationClassLink.getToEnd();
            LinkableView<?> classView = toEnd.getComponentView();
            
            classDiagramView.removeChild(associationClassLink);
            
            classView.removeRelationshipEnd(toEnd);
            this.removeRelationshipEnd(associationClassLink.getFromEnd());
            
            this.associationClassLink.destroy();
            this.associationClassLink = null;
        }
    }

    /**
     * Adds an NaryAssociationView (link) that is linked to this node.
     * @param view the {@link NaryAssociationView} linking this node to a class
     */
    public void addAssociationView(NaryAssociationView view) {
        if (!associationViews.contains(view)) {
            associationViews.add(view);
        }
    }
    
    /**
     * Returns all the NaryAssociationViews of this node.
     * @return list of this node's {@link NaryAssociationView}s
     */
    public  List<NaryAssociationView> getAssociationViews() {
        return associationViews;
    }
}
