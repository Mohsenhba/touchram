package ca.mcgill.sel.classdiagram.ui.views.handler;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.ICompositionViewHandler;

/**
 * This interface can be implemented by a handler which handles the operations of a composition view  for the
 * classdiagram language.
 * 
 * @author joerg
 */
public interface ICdmCompositionViewHandler extends ICompositionViewHandler {
    
    /**
     * This function is used to add a classifier mapping view.
     * 
     * @param composition the composition
     */
    void addClassifierMapping(COREModelComposition composition);
    
    /**
     * This function is used to add an enum mapping view.
     * 
     * @param composition the composition
     */
    void addEnumMapping(COREModelComposition composition);    
    
    /**
     * Loads the extended or reused classdiagram and displays it.
     * 
     * @param myCompositionView the composition
     */
    void showExternalModelOfComposition(CompositionView<ICdmCompositionViewHandler> myCompositionView);
}
