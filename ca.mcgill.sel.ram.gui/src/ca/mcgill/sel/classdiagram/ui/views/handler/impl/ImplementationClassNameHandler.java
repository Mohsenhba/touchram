package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.ValidatingTextViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.IImplementationClassNameHandler;
import ca.mcgill.sel.ram.util.MetamodelRegex;

/**
 * Handler for the name field of an ImplementationClassView. When double tapped, it toggles between class name and
 * instance class name.
 * 
 * @author Franz
 * @author yhattab
 * 
 */
public class ImplementationClassNameHandler extends ValidatingTextViewHandler implements
        IImplementationClassNameHandler {
    
    private boolean nameToggle;
    
    /**
     * Creates a new instance of {@link ImplementationClassNameHandler}.
     */
    public ImplementationClassNameHandler() {
        super(MetamodelRegex.REGEX_CLASS_NAME);
        nameToggle = false;
    }
    
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            TextView target = (TextView) tapEvent.getTarget();
            nameToggle = !nameToggle;
            if (nameToggle) {
                target.setFeature(CdmPackage.Literals.IMPLEMENTATION_CLASS__INSTANCE_CLASS_NAME, false);
            } else {
                target.setFeature(CdmPackage.Literals.NAMED_ELEMENT__NAME, false);
            }
        }
        
        return true;
    }
    
    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        return true;
    }
    
}
