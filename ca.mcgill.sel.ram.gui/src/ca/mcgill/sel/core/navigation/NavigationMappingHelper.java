package ca.mcgill.sel.core.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.provider.INotifyChangedListener;

import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.InterLanguageMapping;
import ca.mcgill.sel.core.InterLanguageMappingEnd;
import ca.mcgill.sel.core.IntraLanguageMapping;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.NavigationMapping;
import ca.mcgill.sel.core.navigation.IntraLanguageMappingTree.ILMNode;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;

public class NavigationMappingHelper {
    
    private static NavigationMappingHelper instance = new NavigationMappingHelper();
    
    private CorePackage corePackage = CorePackage.eINSTANCE;
        
    private List<IntraLanguageMapping> allILM;
    
    private boolean aspectModelsCreated;
    
    private Map<COREPerspective, List<COREModelElementMapping>> modelMappingsForPerspective;
    
    private Map<EObject, IntraLanguageMappingTree> aspectTrees;
    
    public NavigationMappingHelper() {
        aspectTrees = new HashMap<EObject, IntraLanguageMappingTree>();
        allILM = new ArrayList<IntraLanguageMapping>();
        modelMappingsForPerspective = new HashMap<COREPerspective, List<COREModelElementMapping>>();
    }
    
    public static NavigationMappingHelper getInstance() {
        return instance;
    }
    
    public List<IntraLanguageMapping> getAllILM() {
        return allILM;
    }
    
    public IntraLanguageMappingTree getTreeForObject(EObject object) {
        return aspectTrees.get(object);
    }
    
    public void createBaseMappings() {
        createBaseFeatureModelMappings();
        createBaseImpactModelMappings();
        createBaseReuseMappings();
    }
    
    /**
     * Should automatically name the intra language mappings based on the name of the final hop making up the mapping.
     * @param mappings the mappings to be named.
     */
    public static void nameMappings(List<IntraLanguageMapping> mappings) {
        for (IntraLanguageMapping ilm : mappings) {
            List<EReference> hops = ilm.getHops();
            String name = hops.get(hops.size() - 1).getName();
            ilm.setName(StringUtil.toUpperCaseFirst(name));
        }
    }
    
    
    private void createBaseFeatureModelMappings() {
        IntraLanguageMapping concernFeaturesMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping featureExcludesMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping featureRequiresMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping concernImpactNodeMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping featureRealizationModelMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        
        EClass concernEClass = corePackage.getCOREConcern();
        EClass featureClass = corePackage.getCOREFeature();
        EReference featureModel = corePackage.getCOREConcern_FeatureModel();
        EReference features = corePackage.getCOREFeatureModel_Features();
        EReference excludes = corePackage.getCOREFeature_Excludes();
        EReference requires = corePackage.getCOREFeature_Requires();
        EReference impactModel = corePackage.getCOREConcern_ImpactModel();
        EReference impactNodes = corePackage.getCOREImpactModel_ImpactModelElements();
        EReference featureScene = corePackage.getCOREFeature_RealizedBy();
        EReference sceneArtefact = corePackage.getCOREScene_Artefacts();
        
        concernFeaturesMapping.setFrom(concernEClass);
        concernFeaturesMapping.getHops().add(featureModel);
        concernFeaturesMapping.getHops().add(features);
        featureExcludesMapping.setFrom(featureClass);
        featureExcludesMapping.getHops().add(excludes);
        featureRequiresMapping.setFrom(featureClass);
        featureRequiresMapping.getHops().add(requires);
        concernImpactNodeMapping.setFrom(concernEClass);
        concernImpactNodeMapping.getHops().add(impactModel);
        concernImpactNodeMapping.getHops().add(impactNodes);
        featureRealizationModelMapping.setFrom(featureClass);
        featureRealizationModelMapping.getHops().add(featureScene);
        featureRealizationModelMapping.getHops().add(sceneArtefact);
        
        List<IntraLanguageMapping> featureModelILM = new ArrayList<IntraLanguageMapping>();
        
        featureModelILM.add(concernFeaturesMapping);
        featureModelILM.add(featureExcludesMapping);
        featureModelILM.add(featureRequiresMapping);
        featureModelILM.add(concernImpactNodeMapping);
        featureModelILM.add(featureRealizationModelMapping);
        
        nameMappings(featureModelILM);
        
        concernFeaturesMapping.setName("Features");
        
        allILM.addAll(featureModelILM);
        
    }
    
    private void createBaseImpactModelMappings() {
        IntraLanguageMapping impactModelImpactNodeMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        
        EClass impactModelEClass = corePackage.getCOREImpactModel();
        EReference impactModelNodeMapping = corePackage.getCOREImpactModel_ImpactModelElements();
        
        impactModelImpactNodeMapping.setFrom(impactModelEClass);
        impactModelImpactNodeMapping.getHops().add(impactModelNodeMapping);
        
        List<IntraLanguageMapping> impactModelILM = new ArrayList<IntraLanguageMapping>();
        impactModelILM.add(impactModelImpactNodeMapping);
        
        nameMappings(impactModelILM);
        
        allILM.addAll(impactModelILM);
    }
    
    private void createBaseReuseMappings() {
        IntraLanguageMapping concernReuseMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping artifactModelReuseMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping artifactModelExtendsMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        
        EClass concernEClass = corePackage.getCOREConcern();
        EClass artifactEClass = corePackage.getCOREArtefact();
        
        EReference concernReuse = corePackage.getCOREConcern_Reuses();
        EReference reuseConcern = corePackage.getCOREReuse_ReusedConcern();
        EReference artifactReuse = corePackage.getCOREArtefact_ModelReuses();
        EReference artifactExtensions = corePackage.getCOREArtefact_ModelExtensions();
        EReference modelCompArtifact = corePackage.getCOREModelComposition_Source();

        
        concernReuseMapping.setFrom(concernEClass);
        concernReuseMapping.getHops().add(concernReuse);
        concernReuseMapping.getHops().add(reuseConcern);
        
        artifactModelReuseMapping.setFrom(artifactEClass);
        artifactModelReuseMapping.getHops().add(artifactReuse);
        artifactModelReuseMapping.getHops().add(modelCompArtifact);
        
        artifactModelExtendsMapping.setFrom(artifactEClass);
        artifactModelExtendsMapping.getHops().add(artifactExtensions);
        artifactModelExtendsMapping.getHops().add(modelCompArtifact);
        
        concernReuseMapping.setReuse(true);
        artifactModelExtendsMapping.setReuse(true);
        artifactModelReuseMapping.setReuse(true);
        
        concernReuseMapping.setChangeModel(true);
        artifactModelExtendsMapping.setChangeModel(true);
        artifactModelReuseMapping.setChangeModel(true);
        
        concernReuseMapping.setName("Concern Reuse");
        artifactModelExtendsMapping.setName("Extends");
        artifactModelReuseMapping.setName("Artifact Reuse");
        
        allILM.add(concernReuseMapping);
        allILM.add(artifactModelReuseMapping);
        allILM.add(artifactModelExtendsMapping);
    }
    
    private void createBaseAspectMappings(COREPerspective perspective) {
        IntraLanguageMapping aspectClassifierMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping structuralViewClassifierMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping classOperationsMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping classSuperClassMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping aspectInteractionMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping aspectMessageViewMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping messageViewInteractionMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping interactionLifelineMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping interactionMessageMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        
        EClass aspectEClass = RamPackage.eINSTANCE.getAspect();
        EClass classifierEClass = RamPackage.eINSTANCE.getClassifier();
        EClass interactionEClass = RamPackage.eINSTANCE.getInteraction();
        EClass structrualViewEClass = RamPackage.eINSTANCE.getStructuralView();
        EClass messageViewEClass = RamPackage.eINSTANCE.getMessageView();
        
        EReference aspectStructuralView = RamPackage.eINSTANCE.getAspect_StructuralView();
        EReference structuralViewClass = RamPackage.eINSTANCE.getStructuralView_Classes();
        EReference classOperations = RamPackage.eINSTANCE.getClassifier_Operations();
        EReference classSuperClasses = RamPackage.eINSTANCE.getClassifier_SuperTypes();
        EReference aspectMessageView = RamPackage.eINSTANCE.getAspect_MessageViews();
        EReference messageViewInteraction = RamPackage.eINSTANCE.getMessageView_Specification();
        EReference interactionLifeline = RamPackage.eINSTANCE.getInteraction_Lifelines();
        EReference interactionMessage = RamPackage.eINSTANCE.getInteraction_Messages();
        
        aspectClassifierMapping.setFrom(aspectEClass);
        aspectClassifierMapping.getHops().add(aspectStructuralView);
        aspectClassifierMapping.getHops().add(structuralViewClass);
        
        structuralViewClassifierMapping.setFrom(structrualViewEClass);
        structuralViewClassifierMapping.getHops().add(structuralViewClass);
        
        classOperationsMapping.setFrom(classifierEClass);
        classOperationsMapping.getHops().add(classOperations);
        
        classSuperClassMapping.setFrom(classifierEClass);
        classSuperClassMapping.getHops().add(classSuperClasses);
        classSuperClassMapping.setClosure(true);
        
        aspectMessageViewMapping.setFrom(aspectEClass);
        aspectMessageViewMapping.getHops().add(aspectMessageView);
        aspectMessageViewMapping.setIncreaseDepth(true);
        aspectMessageViewMapping.setChangeModel(true);
        
        aspectInteractionMapping.setFrom(aspectEClass);
        aspectInteractionMapping.getHops().add(aspectMessageView);
        aspectInteractionMapping.getHops().add(messageViewInteraction);
        
        messageViewInteractionMapping.setFrom(messageViewEClass);
        messageViewInteractionMapping.getHops().add(messageViewInteraction);
        
        interactionLifelineMapping.setFrom(interactionEClass);
        interactionLifelineMapping.getHops().add(interactionLifeline);
        
        interactionMessageMapping.setFrom(interactionEClass);
        interactionMessageMapping.getHops().add(interactionMessage);
        
        List<IntraLanguageMapping> aspectILM = new ArrayList<IntraLanguageMapping>();
        aspectILM.add(aspectClassifierMapping);
        aspectILM.add(classOperationsMapping);
        aspectILM.add(classSuperClassMapping);
        aspectILM.add(aspectMessageViewMapping);
        aspectILM.add(interactionLifelineMapping);
        aspectILM.add(interactionMessageMapping);
        
        List<IntraLanguageMapping> structuralViewILM = new ArrayList<IntraLanguageMapping>();
        structuralViewILM.add(structuralViewClassifierMapping);
        structuralViewILM.add(classOperationsMapping);
        structuralViewILM.add(classSuperClassMapping);
        
        List<IntraLanguageMapping> messageViewILM = new ArrayList<IntraLanguageMapping>();
        messageViewILM.add(messageViewInteractionMapping);
        messageViewILM.add(interactionLifelineMapping);
        messageViewILM.add(interactionMessageMapping);
        
        nameMappings(aspectILM);
        nameMappings(structuralViewILM);
        nameMappings(messageViewILM);
        aspectILM.add(structuralViewClassifierMapping);
        aspectILM.add(messageViewInteractionMapping);
        
        allILM.addAll(aspectILM);
                
//        perspective.getNavigationMappings().addAll(aspectILM);
//        for (NavigationMapping nmap : aspectILM) {
//            if (!perspective.getNavigationMappings().contains(nmap)) {
//                perspective.getNavigationMappings().add(nmap);
//            }
//        }

    }
    
    private void createInterLanguageMappings(COREPerspective perspective) {
        EList<CORELanguageElementMapping> perspectiveMappings = perspective.getMappings();
        
        List<InterLanguageMapping> interLanguageMappings = new ArrayList<InterLanguageMapping>();
        
        for (CORELanguageElementMapping mapping : perspectiveMappings) {
            InterLanguageMapping curMapping = CoreFactory.eINSTANCE.createInterLanguageMapping();
            
            curMapping.setCoreLanguageElementMapping(mapping);
            
            interLanguageMappings.add(curMapping);
            
            for (MappingEnd end : mapping.getMappingEnds()) {
                InterLanguageMappingEnd nameForMapping = CoreFactory.eINSTANCE.createInterLanguageMappingEnd();
                nameForMapping.setMappingEnd(end);
                nameForMapping.setOrigin(true);
                nameForMapping.setDestination(true);
                curMapping.getInterLanguageMappingEnds().add(nameForMapping);
            }
            
        }
        perspective.getNavigationMappings().addAll(interLanguageMappings);
        
    }
    
    private void getInterLanguageMappings() {
        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
        
        
        createInterLanguageMappings(perspective);

    }
    
    protected EList<EObject> performHop(EObject currentElement, EReference ref) {
        if (currentElement.eGet(ref) instanceof EList) {
            EList<EObject> responses = (EList<EObject>) currentElement.eGet(ref);
            return responses;
        } else if (currentElement.eGet(ref) instanceof EObject) {
            EObject response = (EObject) currentElement.eGet(ref);
            EList<EObject> responses = new BasicEList<EObject>();
            responses.add(response);
            return responses;
        } else {
            return null;
        }
        
    }
    
    private List<EObject> connectMappingForModel(EObject model, EClass modelEClass, List<IntraLanguageMapping> 
        mappingsForModel, IntraLanguageMappingTree modelTree, EObject closureObject) {
        
        List<EObject> overallList = new ArrayList<EObject>();
        
        for (IntraLanguageMapping mapping : mappingsForModel) {
            EList<EClass> objectSuperClasses = modelEClass.getESuperTypes();
            boolean mappingClosure = mapping.isClosure();
            if (mapping.getFrom().equals(modelEClass)) {
                EList<EReference> mappingHops = mapping.getHops();
                List<EObject> currentObjects = new ArrayList<EObject>();
                currentObjects.add(model);
                int numberOfHopsMade = 0;

                
                while (numberOfHopsMade < mappingHops.size()) {
                    EReference hop = mappingHops.get(numberOfHopsMade);
                    List<EObject> currentResponses = new ArrayList<EObject>();
                    for (EObject currentObj : currentObjects) {
                        EList<EObject> responses = performHop(currentObj, hop);
                        currentResponses.addAll(responses);
                    }
                    
                    currentObjects = currentResponses;
                    overallList.addAll(currentResponses);
                    numberOfHopsMade++;
                }
                for (EObject object : currentObjects) {
                    ILMNode findNode = modelTree.findObjectNode(object);
                    if (closureObject != null) {
                        if (findNode == null) {
                            modelTree.addNode(object, closureObject, mapping);
                        } else if (!findNode.getILM().equals(mapping)) {
                            modelTree.addNode(object, closureObject, mapping);
                        } else if (!findNode.getParentNode().getNodeModelElement().equals(model)) {
                            modelTree.addNode(object, model, mapping);
                        } else {
                            continue;
                        }
                    } else {
                        if (findNode == null) {
                            modelTree.addNode(object, model, mapping);
                        } else if (!findNode.getILM().equals(mapping)) {
                            modelTree.addNode(object, model, mapping);
                        } else if (!findNode.getParentNode().getNodeModelElement().equals(model)) {
                            modelTree.addNode(object, model, mapping);
                        } else {
                            continue;
                        }
                    }
                    EClass currentObjectEClass = object.eClass();
                    List<EObject> connectionsForObject = null;
                    if (mappingClosure) {
                        List<IntraLanguageMapping> mappingsForClosure = new ArrayList<IntraLanguageMapping>();
                        mappingsForClosure.add(mapping);
                        connectionsForObject = connectMappingForModel(object, currentObjectEClass, 
                                mappingsForClosure, modelTree, model);
                    } else if (!mapping.isChangeModel()) {
                        connectionsForObject = connectMappingForModel(object, currentObjectEClass, 
                                mappingsForModel, modelTree, null);
                    }
                    
                    if (connectionsForObject != null && connectionsForObject.size() > 0) {
                        overallList.addAll(connectionsForObject);
                    }
                }
            } else {
                EList<EReference> mappingHops = mapping.getHops();
                EReference firstHop = mappingHops.get(0);
                for (EClass superType : objectSuperClasses) {
                    EList<EReference> refsForSuper = superType.getEReferences();
                    if (mapping.getFrom().equals(superType) && refsForSuper.contains(firstHop)) {
                        List<EObject> currentObjects = new ArrayList<EObject>();
                        currentObjects.add(model);
                        int numberOfHopsMade = 0;
                        while (numberOfHopsMade < mappingHops.size()) {
                            EReference hop = mappingHops.get(numberOfHopsMade);
                            List<EObject> currentResponses = new ArrayList<EObject>();
                            for (EObject currentObj : currentObjects) {
                                EList<EObject> responses = performHop(currentObj, hop);
                                currentResponses.addAll(responses);
                            }
                            currentObjects = currentResponses;
                            overallList.addAll(currentResponses);
                            numberOfHopsMade++;
                        }
                        for (EObject object : currentObjects) {
                            ILMNode findNode = modelTree.findObjectNode(object);
                            if (closureObject != null) {
                                if (findNode == null) {
                                    modelTree.addNode(object, closureObject, mapping);
                                } else if (!findNode.getILM().equals(mapping)) {
                                    modelTree.addNode(object, closureObject, mapping);
                                } else if (!findNode.getParentNode().getNodeModelElement().equals(model)) {
                                    modelTree.addNode(object, model, mapping);
                                } else {
                                    continue;
                                }
                            } else {
                                if (findNode == null) {
                                    modelTree.addNode(object, model, mapping);
                                } else if (!findNode.getILM().equals(mapping)) {
                                    modelTree.addNode(object, model, mapping);
                                } else if (!findNode.getParentNode().getNodeModelElement().equals(model)) {
                                    modelTree.addNode(object, model, mapping);
                                } else {
                                    continue;
                                }
                            }
                            
                            EClass currentObjectEClass = object.eClass();
                            List<EObject> connectionsForObject = null;
                            if (mappingClosure) {
                                List<IntraLanguageMapping> mappingsForClosure = new ArrayList<IntraLanguageMapping>();
                                mappingsForClosure.add(mapping);
                                connectionsForObject = connectMappingForModel(object, currentObjectEClass, 
                                        mappingsForClosure, modelTree, model);
                            } else if (!mapping.isChangeModel()) {
                                connectionsForObject = connectMappingForModel(object, currentObjectEClass, 
                                        mappingsForModel, modelTree, null);
                            }
                            if (connectionsForObject != null && connectionsForObject.size() > 0) {
                                overallList.addAll(connectionsForObject);
                            }
                        }
                    }
                }
            }
        }
        return overallList;
    }
    
    private String getRoleNameForModel(EObject objectForRole) {
        COREExternalArtefact art = COREArtefactUtil.getReferencingExternalArtefact(objectForRole);
        
        COREScene scene = art.getScene();
        
        EMap<String, EList<COREArtefact>> sceneArts = scene.getArtefacts();
        
        String roleName = "";
        
        for (String key : sceneArts.keySet()) {
            EList<COREArtefact> arts = sceneArts.get(key);
            if (arts.contains(art)) {
                roleName = key;
            }
        }
        return roleName;
    }
    
    private boolean findIfDestination(EObject obj, InterLanguageMapping ilMapping) {
        boolean destination = false;
        for (InterLanguageMappingEnd mappingName : ilMapping.getInterLanguageMappingEnds()) {
            if (mappingName.getMappingEnd().getRoleName().equals(getRoleNameForModel(obj))) {
                if (mappingName.isDestination()) {
                    destination = true;
                }
            }
        }
        return destination;
    }
    
    /**
     * Should add inter language mappings to the input node based on the COREModelElementMappings that have been 
     * defined. Names for the mappings are currently given so that they may be displayed clearly within the navigation
     * bar.
     * @param node the node which will have inter language mappings added as its children.
     * @param tree the current tree where the node exists.
     * @param rootModelElementMappings the mappings present within current scene.
     * @param treeAlreadyVisible whether this is an addition after the nav bar section is already open
     */
    protected void addInterLanguageMappingsToNode(ILMNode node, IntraLanguageMappingTree tree, 
            EList<COREModelElementMapping> rootModelElementMappings, boolean treeAlreadyVisible) {
        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
        
        String roleName = getRoleNameForModel(tree.getRootModelElement());
        
        EObject nodeObj = node.getNodeModelElement();
        for (COREModelElementMapping rootModelMapping : rootModelElementMappings) {
            if (rootModelMapping.getModelElements().contains(nodeObj)) {
                List<EObject> currentMappingObjects = rootModelMapping.getModelElements();
                
                InterLanguageMapping ilMapping = null;
                int lEMid = rootModelMapping.getLEMid();
                
                for (NavigationMapping nMap : perspective.getNavigationMappings()) {
                    if (nMap instanceof IntraLanguageMapping) {
                        continue;
                    } else {
                        InterLanguageMapping nowInterMap = (InterLanguageMapping) nMap;
                        int identifier = nowInterMap.getCoreLanguageElementMapping().getIdentifier();
                        if (identifier == lEMid) {
                            ilMapping = nowInterMap;
                            break;
                        }
                    }
                }
                
                boolean origin = false;
                
                List<InterLanguageMappingEnd> ilMappingEnds = new ArrayList<InterLanguageMappingEnd>();
                
                if (ilMapping == null) {
                    continue;
                } else {
                    List<InterLanguageMappingEnd> ends = ilMapping.getInterLanguageMappingEnds();
                    for (InterLanguageMappingEnd end : ends) {
                        if (end.getMappingEnd().getRoleName().equals(roleName)) {
                            origin = end.isOrigin();
                        } else {
                            ilMappingEnds.add(end);
                        }
                    }
                }
                
                if (!origin) {
                    continue;
                }
                
                for (EObject obj : currentMappingObjects) {
                    if (obj == null) {
                        continue;
                    }
                    if (obj == nodeObj) {
                        continue;
                    }
                    
                    boolean isDestination = findIfDestination(obj, ilMapping);
                    if (!isDestination) {
                        continue;
                    }
                    
                    
                    ILMNode findNode = tree.findObjectNode(obj);
                    if (findNode != null && findNode.getParentNode() != null) {
                        if (findNode.getParentNode().getNodeModelElement().equals(node.getNodeModelElement())) {
                            continue;
                        }
                    }
                    if (!treeAlreadyVisible) {
                        ILMNode newNode = tree.addNode(obj, node.getNodeModelElement(), ilMapping);
                        String name = null;
                        for (InterLanguageMappingEnd mappingName : ilMapping.getInterLanguageMappingEnds()) {
                            if (mappingName.getMappingEnd().getLanguageElement().getLanguageElement().equals(
                                    obj.eClass())) {
                                name = mappingName.getName(node.getNodeModelElement().eClass());
                            }
                        }
                        newNode.setInterMappingName(name);
                    } else {
                        String name = null;
                        for (InterLanguageMappingEnd mappingName : ilMapping.getInterLanguageMappingEnds()) {
                            if (mappingName.getMappingEnd().getLanguageElement().getLanguageElement().equals(
                                    obj.eClass())) {
                                name = mappingName.getName(node.getNodeModelElement().eClass());
                            }
                        }
                        tree.checkMappingForAddition(node, obj, ilMapping, name);
                    }
                }
            }
        }
    }
    
    /**
     * Should be called after intra language mappings have been added in order to add inter language mappings as leaves
     * for the tree.
     * @param tree the tree which has been created and populated with intra language mappings
     * @param treeAlreadyVisible whether this is an addition after the nav bar section is already open
     */
    protected void addInterLanguageMappingsToTree(IntraLanguageMappingTree tree, boolean treeAlreadyVisible) {
        
        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
                        
        EObject rootObject = tree.getRootModelElement();
        
        COREScene rootScene = COREArtefactUtil.getReferencingExternalArtefact(rootObject).getScene();
        
        if (rootScene == null) {
            return;
        }
        
        EList<COREModelElementMapping> rootModelElementMappings = rootScene.getElementMappings();
        
        modelMappingsForPerspective.put(perspective, rootModelElementMappings);
        
        
        List<ILMNode> nodes = tree.getAllNodes();
        for (ILMNode node : nodes) {
            addInterLanguageMappingsToNode(node, tree, rootModelElementMappings, treeAlreadyVisible);
        }
        
        if (!treeAlreadyVisible) {
            addListenerToElementMappings(rootScene, tree);
        } 
        
    }
    
    /**
     * Adds a listener for a newly created model element mapping as they often don't have eobjects until slightly after
     * they are created. Kind of a hack that was created because the way Perspective mappings are defined is changing
     * frequently and thus leading me to be a bit behind. May need to be changed again in final implementation.
     * @param mapping the newly created mapping
     * @param tree the mapping tree for this model
     */
    private void addListenerToNewMapping(COREModelElementMapping mapping, IntraLanguageMappingTree tree) {
        EMFEditUtil.addListenerFor(mapping, new INotifyChangedListener() {
            
            @Override
            public void notifyChanged(Notification notification) {
                for (EObject obj : mapping.getModelElements()) {
                    ILMNode nodeForMapping = tree.getNodeWithLowestDepth(obj);
                    if (nodeForMapping != null) {
                        if (nodeForMapping.getIntraLanguageMappingTree() instanceof InterLanguageMapping) {
                            continue;
                        }
                        EObject rootObject = tree.getRootModelElement();
                        COREScene rootScene = COREArtefactUtil.getReferencingExternalArtefact(rootObject)
                                .getScene();
                        EList<COREModelElementMapping> rootModelElementMappings = rootScene
                                .getElementMappings();
                        addInterLanguageMappingsToNode(nodeForMapping, tree, rootModelElementMappings, 
                                true);
                        break;
                    }
                }
                
            }
        });
    }
    
    /**
     * Adds a listener to the scene's model element mappings. Should recognize when a new one is created and add the 
     * inter-language mapping to the nav bar if that is applicable for the new mapping.
     * @param scene the current scene
     * @param tree the tree for the current model.
     */
    private void addListenerToElementMappings(COREScene scene, IntraLanguageMappingTree tree) {
        EReference modelMappingsReference = corePackage.getCOREScene_ElementMappings();
        EMFEditUtil.addListenerFor(scene, new INotifyChangedListener() {
            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature().equals(modelMappingsReference)) {
                    if (notification.getEventType() == Notification.ADD) {
                        if (notification.getNewValue() instanceof COREModelElementMapping) {
                            COREModelElementMapping newMapping = (COREModelElementMapping) notification.getNewValue();
                            if (newMapping.getModelElements().isEmpty()) {
                                addListenerToNewMapping(newMapping, tree); 
                            }
                            for (EObject obj : newMapping.getModelElements()) {
                                ILMNode nodeForMapping = tree.getNodeWithLowestDepth(obj);
                                if (nodeForMapping != null) {
                                    if (nodeForMapping.getIntraLanguageMappingTree() instanceof InterLanguageMapping) {
                                        continue;
                                    }
                                    EObject rootObject = tree.getRootModelElement();
                                    COREScene rootScene = COREArtefactUtil.getReferencingExternalArtefact(rootObject)
                                            .getScene();
                                    EList<COREModelElementMapping> rootModelElementMappings = rootScene
                                            .getElementMappings();
                                    addInterLanguageMappingsToNode(nodeForMapping, tree, rootModelElementMappings, 
                                            true);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            
        });
    }
    
    /**
     * Used to create listeners for specific case of model reuse and extension where the source of the mapping is the 
     * artefact of the model rather than the model itself. There is a need for this special case as technically this 
     * is across two different languages which makes it not work in the normal way that listeners for ILM work.
     * This is very much a patch rather than what I would like it to have to look like but given the nature of these 
     * mappings I'm just happy it actually works. 
     * Basically used for COREModel Reuses and Extensions.
     * @param node the node to add listeners for (generally this will be the root node)
     * @param mapping the mapping which should be recognized
     * @author ian
     */
    private void addModelReuseExtensionListeners(ILMNode node, IntraLanguageMapping mapping) {
        EObject nodeObject = node.getNodeModelElement();

        COREExternalArtefact extArtefact = COREArtefactUtil.getReferencingExternalArtefact(nodeObject);

        if (extArtefact == null) {
            return;
        }
        
        EList<EReference> hops = mapping.getHops();
        
        EReference hop = hops.get(0);
        
        EMFEditUtil.addListenerFor(extArtefact, new INotifyChangedListener() {
            public void notifyChanged(Notification notification) {
                if (notification.getFeature().equals(hop)) {
                    List<EObject> currentObjects = new ArrayList<EObject>();                   
                    EList<EObject> responses = NavigationMappingHelper.getInstance().performHop(extArtefact, hop);
                    for (EObject resp : responses) {
                        if (resp.equals(notification.getNewValue())) {
                            currentObjects.add(resp);
                        }
                    }
                    int numberOfHopsMade = 1;
                    while (numberOfHopsMade < hops.size()) {
                        EReference hop = hops.get(numberOfHopsMade);
                        numberOfHopsMade++;
                        List<EObject> currentResponses = new ArrayList<EObject>();
                        for (EObject currentObj : currentObjects) {
                            EList<EObject> resps = NavigationMappingHelper.getInstance().performHop(currentObj, hop);
                            currentResponses.addAll(resps);
                            if (numberOfHopsMade == hops.size()) {
                                if (notification.getEventType() == Notification.ADD) {
                                    node.getIntraLanguageMappingTree().checkMappingForAddition(node, 
                                            resps.get(0), mapping, hop.getName());
                                }
                            }
                        }
                    }
                    if (notification.getEventType() == Notification.REMOVE) {
                        currentObjects = new ArrayList<EObject>();
                        currentObjects.add((EObject) notification.getOldValue());
                        numberOfHopsMade = 1;
                        while (numberOfHopsMade < hops.size()) {
                            EReference hop = hops.get(numberOfHopsMade);
                            numberOfHopsMade++;
                            List<EObject> currentResponses = new ArrayList<EObject>();
                            for (EObject currentObj : currentObjects) {
                                EList<EObject> resps = NavigationMappingHelper.getInstance().performHop(currentObj,
                                        hop);
                                currentResponses.addAll(resps);
                                if (numberOfHopsMade == hops.size()) {
                                    List<ILMNode> childs = node.getChildren();
                                    ILMNode nodeToRemove = null;
                                    for (ILMNode child : childs) {
                                        if (child.getNodeModelElement().equals(resps.get(0))) {
                                            nodeToRemove = child;
                                            
                                        }
                                    }
                                    if (nodeToRemove != null) {
                                        node.getIntraLanguageMappingTree().removeElementFromTree(node, nodeToRemove.
                                                getNodeModelElement(), mapping);
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
        });
        
        
    }
    
    
    public IntraLanguageMappingTree getMappingsForModel(EObject currentModel) {
        IntraLanguageMappingTree currentModelTree = null;
        if (aspectTrees.containsKey(currentModel)) {
            currentModelTree = aspectTrees.get(currentModel);
        } else if (currentModel instanceof COREConcern || currentModel instanceof COREImpactModel) {
            EClass modelEClass = currentModel.eClass();
            List<IntraLanguageMapping> mappingsForModel = allILM;
            
            currentModelTree = new IntraLanguageMappingTree(currentModel);
            
            connectMappingForModel(currentModel, modelEClass, mappingsForModel, currentModelTree, null);
            
            currentModelTree.makeChildMappingLists(mappingsForModel);
                        
            aspectTrees.put(currentModel, currentModelTree);
        } else {
            COREPerspective persp = NavigationBar.getInstance().getCurrentPerspective();
            
            //here because the current perspective version for aspect is not fully implemented so I need to add
            //the mappings myself. Should be removed later.
            if (currentModel instanceof Aspect && !aspectModelsCreated) {
                createBaseAspectMappings(persp);
                aspectModelsCreated = true;
            }
            
            List<IntraLanguageMapping> allILMs = new ArrayList<IntraLanguageMapping>();
            if (persp != null) {
                for (NavigationMapping m : persp.getNavigationMappings()) {
                    if (m instanceof IntraLanguageMapping) {
                        allILMs.add((IntraLanguageMapping) m);
                    }
                }
            }
            nameMappings(allILMs);
            allILM.addAll(allILMs);
            EClass modelEClass = currentModel.eClass();
            
            currentModelTree = new IntraLanguageMappingTree(currentModel);
            
            connectMappingForModel(currentModel, modelEClass, allILM, currentModelTree, null);
            
            COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentModel);
            
            if (!(artefact == null)) {
                EClass artefactEClass = artefact.eClass();
                connectMappingForModel(artefact, artefactEClass, allILM, currentModelTree, null);
                
                for (IntraLanguageMapping ilm : allILM) {
                    if (ilm.isReuse() && ilm.getFrom().equals(CorePackage.eINSTANCE.getCOREArtefact())) {
                        addModelReuseExtensionListeners(currentModelTree.getRootNode(), ilm);
                    }
                }
            }

            if (persp != null) {
                getInterLanguageMappings();
            }
            
            addInterLanguageMappingsToTree(currentModelTree, false);
            currentModelTree.makeChildMappingLists(allILM);
                        
            aspectTrees.put(currentModel, currentModelTree);
        }
        return currentModelTree;
    }
    
    
}
