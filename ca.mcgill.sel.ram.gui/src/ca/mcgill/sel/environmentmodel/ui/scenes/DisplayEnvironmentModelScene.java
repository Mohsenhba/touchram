package ca.mcgill.sel.environmentmodel.ui.scenes;

import java.util.Collection;
import java.util.HashMap;
import java.util.Stack;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.MTComponent;
import org.mt4j.components.visibleComponents.shapes.MTRectangle.PositionAnchor;

import ca.mcgill.sel.environmentmodel.impl.ContainerMapImpl;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.CommunicationDiagramView;
import ca.mcgill.sel.environmentmodel.ui.views.MessageDetailView;
import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup.OptionType;
import ca.mcgill.sel.ram.ui.components.listeners.RamPanelListener;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;


public class DisplayEnvironmentModelScene extends RamAbstractScene<IDisplaySceneHandler>
        implements INotifyChangedListener, RamPanelListener {

    // Actions for the scene
    private static final String ACTION_BACK = "display.back";
    // private static final String ACTION_OPEN_VALIDATOR = "display.validator";
    private static final String ACTION_CONCERN_BACK = "display.concern.back";
    private static final String ACTION_OPEN_TRACING = "display.tracing";
    private static final String ACTION_LAYOUT = "display.layout";
    private static final String ACTION_TOGGLE_NOTES = "display.toggle.notes";
    private static final String ACTION_GENERATE = "display.generate";

    // Name of the submenus
    private static final String SUBMENU_GOTO = "sub.goto";
    private static final String SUBMENU_OTHER = "sub.oth";

    private CommunicationDiagramView communicationDiagramView;

    private GraphicalUpdater graphicalUpdater;

    private EnvironmentModel em;
    
    private COREFeature featureForScene;

    private MTComponent viewContainer;
    private Stack<MTComponent> previousViews;
    private MTComponent currentView;
    
    private HashMap<Message, MessageDetailView> messageToDetailViewMap;

    public DisplayEnvironmentModelScene(RamApp app, COREExternalArtefact artefact,
            EnvironmentModel em, String name) {
        super(app, name, artefact);
        this.em = em;

        this.handler = EmHandlerFactory.INSTANCE.getCommunicationDiagramDisplaySceneHandler();
        
        this.messageToDetailViewMap = new HashMap<Message, MessageDetailView>(); 

        // views are added into this layer
        viewContainer = new MTComponent(app, "view container");
        getContainerLayer().addChild(viewContainer);

        // Graphical Updater linked to this aspect
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(em);

        this.buildViews();

        EMFEditUtil.addListenerFor(em, this);

        // the following registers the artefact to listen to all changes to it's content, which
        // is all the changes to contained COREModelExtensions, COREModelReuses, and CORELinks
        // whenever a new mapping is created, deleted, or when the "to" element of a mapping changes,
        // the graphical updated is sent a referenceEvent so it can trigger updateStyle changes on all
        // registered views so that they can change to their "ghost" background color, if needed
        this.artefact.eAdapters().add(new EContentAdapter() {
            @Override
            public void notifyChanged(Notification notification) {
                super.notifyChanged(notification);
                if (notification.getFeature() == CorePackage.Literals.CORE_LINK__TO) {
                    // the model element of a "to" mapping was changed
                    // we need to trigger the updateStyle in all views representing the old element and the new
                    // element
                    if (notification.getOldValue() != null) {
                        graphicalUpdater.referenceEvent((EObject) notification.getOldValue());
                    }
                    if (notification.getNewValue() != null) {
                        graphicalUpdater.referenceEvent((EObject) notification.getNewValue());
                    }
                }
            }
        });

        setCommandStackListener(em);

        repushSections();
    }

    @Override
    public void panelClosed(RamPanelComponent panel) {
        // TODO Validation
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__CORE_CONCERN
                && notification.getEventType() == Notification.SET
                && notification.getNewValue() == null) {
            // Go back to the concern if the link with it is undone.
            handler.switchToConcern(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (handler != null) {
            String actionCommand = event.getActionCommand();
            if (ACTION_BACK.equals(actionCommand)) {
                handler.back(this);
            } else if (ACTION_CONCERN_BACK.equals(actionCommand)) {
                handler.switchToConcern(this);
            } else if (ACTION_OPEN_TRACING.equals(actionCommand)) {
                handler.showTracing(this);
            } else if (ACTION_LAYOUT.equals(actionCommand)) {
                handler.layout(this);
//            } else if (ACTION_TOGGLE_NOTES.equals(actionCommand)) {
//                menu.toggleAction(((DisplayEnvironmentModelSceneHandler) handler).toggleNotes(this),
//                        ACTION_TOGGLE_NOTES);
            } else if (ACTION_GENERATE.equals(actionCommand)) {
                handler.generate(this);
            } else {
                super.actionPerformed(event);
            }
        } else {
            LoggerUtils.warn("No handler set for " + this.getClass().getName());
        }
    }

    @Override
    protected void initMenu() {
        menu.addSubMenu(2, SUBMENU_OTHER);
        menu.addSubMenu(1, SUBMENU_GOTO);
    }

    @Override
    protected EObject getElementToSave() {
        return em;
    }

    @Override
    public boolean destroy() {

        EMFEditUtil.removeListenerFor(em, this);

        /**
         * Not all views are currently children, so we need to destroy them explicitly.
         * I.e., only the last active view was a children of the view container.
         */
        communicationDiagramView.destroy();
        for (MessageDetailView view : messageToDetailViewMap.values()) {
            view.destroy();
        }
        
        if (this.getPreviousScene() == null) {
            navbar.returnNormalViewFromSplit();
            navbar.concernSelectMode();
            RamApp.getActiveScene().getCanvas().addChild(navbar);
        }
        return super.destroy();
    }

    public EnvironmentModel getEnvironmentModel() {
        return this.em;
    }

    public CommunicationDiagramView getCommunicationDiagramView() {
        return this.communicationDiagramView;
    }

    /**
     * Returns the current view that is displayed.
     *
     * @return the current view
     */
    public MTComponent getCurrentView() {
        return currentView;
    }

    /**
     * Displays the view that was displayed previously.
     *
     * @see #switchToView(MTComponent)
     */
    public void switchToPreviousView() {
        if (!previousViews.isEmpty()) {
            switchToView(previousViews.pop(), false);
        }
        navbar.popSection();

        // If back brings us back to a detail view, we have to re-push it to the navbar.
//        if (currentView instanceof MessageDetailView) {
//            Message view = ((MessageDetailView) currentView).getMessage();
//            navbar.pushSectionJumpUC(
//                    Icons.ICON_MENU_MESSAGE_VIEW, ((Message) view).getMessageType().getName(), null, (Message) view);
//        }
    }

    /**
     * Replaces the current view with the given view. Remembers the old current view in case the user wants to go back
     * to it.
     *
     * @param view
     *            the view to display
     * @param saveCurrent - whether we want to remember the current view as the previous one.
     * @see #switchToPreviousView()
     */
    public void switchToView(MTComponent view, boolean saveCurrent) {
        
        if (saveCurrent) {
            previousViews.push(currentView);
        }   
        
        // If moving from a detail to the diagram, we want to force the details to regenerate
        if (currentView instanceof MessageDetailView && !(view instanceof MessageDetailView)) {
            for (MessageDetailView detailView : messageToDetailViewMap.values()) {
                detailView.destroy();
            }
            messageToDetailViewMap.clear();
        }

        currentView = view;

        viewContainer.removeAllChildren();
        viewContainer.addChild(currentView);

        if (currentView instanceof CommunicationDiagramView) {
            if (menu.getAction(ACTION_TOGGLE_NOTES) == null) {
                this.getMenu().addAction(Strings.MENU_TOGGLE_NOTES, Strings.MENU_TOGGLE_NOTES,
                    Icons.ICON_MENU_SHOW_NOTES, Icons.ICON_MENU_HIDE_NOTES, ACTION_TOGGLE_NOTES, this, SUBMENU_OTHER,
                    true, true);
            }
        } else {
            menu.removeAction(ACTION_TOGGLE_NOTES);
        }


        if (menu.getAction(ACTION_GENERATE) == null) {
            this.getMenu().addAction(Strings.MENU_GENERATE, Icons.ICON_MENU_GENERATE, ACTION_GENERATE,
                    this, SUBMENU_OTHER, true);
        }
        

        clearTemporaryComponents();
    }

    /**
     * Shows a confirm popup for the given aspect to ask the user whether the aspect should be saved.
     *
     * @param parent
     *            the scene where the popup should be displayed, usually the current scene
     * @param listener
     *            the listener to inform which option the user selected
     */
    public void showCloseConfirmPopup(RamAbstractScene<?> parent, ConfirmPopup.SelectionListener listener) {
        showCloseConfirmPopup(parent, listener, OptionType.YES_NO_CANCEL);
    }

    /**
     * Shows a confirm popup for the given class diagram to ask the user whether it should be saved.
     *
     * @param parent the scene where the popup should be displayed, usually the current scene
     * @param listener the listener to inform which option the user selected
     * @param options the buttons to display in the popup
     */
    public void showCloseConfirmPopup(RamAbstractScene<?> parent, ConfirmPopup.SelectionListener listener,
            OptionType options) {
        String message = Strings.MODEL_ASPECT + " " + em.getName() + Strings.POPUP_MODIFIED_SAVE;
        ConfirmPopup saveConfirmPopup = new ConfirmPopup(message, options);
        saveConfirmPopup.setListener(listener);

        parent.displayPopup(saveConfirmPopup);
    }

    /**
     * Builds the views associated with this scene.
     */
    private void buildViews() {
        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(em.getLayout().getContainers(), em);
        communicationDiagramView = new CommunicationDiagramView(em, layout, getWidth(), getHeight());

        // we will get position of the upper left corner of the view instead of center.
        communicationDiagramView.setAnchor(PositionAnchor.UPPER_LEFT);

        previousViews = new Stack<MTComponent>();
        switchToView(communicationDiagramView, true);

        communicationDiagramView.setHandler(EmHandlerFactory.INSTANCE.getCommunicationDiagramViewHandler());
    }

    /**
     * Used to refresh the sections in the navigation bar whenever the scene is called.
     */
    public void repushSections() {
        if (artefact.getScene() != null && artefact.getScene().getRealizes().size() > 0) {
            
            COREFeature feature = artefact.getScene().getRealizes().get(0);
            
            COREScene scene = artefact.getScene();
            
            if (SceneCreationAndChangeFactory.getFactory().getSceneToFeature().containsKey(scene)) {
                feature = SceneCreationAndChangeFactory.getFactory().getSceneToFeature().get(scene);
            }
            
            if (featureForScene != null) {
                feature = featureForScene;
            }
            
            featureForScene = feature;
            
            if (scene.getRealizes().size() > 1) {
                navbar.addConflictResolutionFeatureNamer(scene, feature);
            } else {
                navbar.pushSectionJumpGeneric(Icons.ICON_NAVIGATION_FEATURE, feature.getName(), null, feature);
            }
            
            navbar.pushSectionEnvironmentModel(Icons.ICON_NAVIGATION_ASPECT, em.getName(), navbar.getGenericNamerBase(),
                    em);
        } 
    }
    
    /**
     * Displays a view for the message view.
     * Creates a new view if it hasn't been created before, otherwise it uses the existing view.
     *
     * @param message the {@link Message} to display its {@link MessageDetailView} for
     */
    public void showMessageDetail(Message message) {
        MessageDetailView view = messageToDetailViewMap.get(message);

        if (view == null) {
            view = new MessageDetailView(message, communicationDiagramView, getWidth(), getHeight());
            messageToDetailViewMap.put(message, view);
        }
        boolean fromDiagram = currentView instanceof CommunicationDiagramView;

        switchToView(view, true);

        view.setHandler(EmHandlerFactory.INSTANCE.getMessageDetailViewHandler());

        handleViewSections(message, fromDiagram);
    }
    
    /**
     * Examines the view and pushes relative section inside the Bar.
     * 
     * @param view the view to examine
     * @param fromDiagram Flag to say if we are coming from the diagram view.
     */
    private void handleViewSections(EObject view, boolean fromDiagram) {
        if (view instanceof Message) {
            if (fromDiagram) {
                navbar.pushSection(Icons.ICON_MENU_MESSAGE_VIEW,
                        ((Message) view).getMessageType().getName(), null, (Message) view);
            } else {
                navbar.pushSection(Icons.ICON_MENU_MESSAGE_VIEW,
                        ((Message) view).getMessageType().getName(), null, (Message) view);
            }

        }
    }
}
