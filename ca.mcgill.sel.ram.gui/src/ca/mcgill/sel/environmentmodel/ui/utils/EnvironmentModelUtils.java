package ca.mcgill.sel.environmentmodel.ui.utils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.components.MTComponent;
import org.mt4j.sceneManagement.Iscene;

import ca.mcgill.sel.classdiagram.ui.views.OperationView;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamKeyboard;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent.Cardinal;
import ca.mcgill.sel.ram.ui.components.listeners.DefaultRamKeyboardListener;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.util.MetamodelRegex;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EmFactory;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.MessageType;
import ca.mcgill.sel.environmentmodel.ParameterType;
import ca.mcgill.sel.environmentmodel.impl.EmFactoryImpl;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.scenes.DisplayEnvironmentModelScene;
import ca.mcgill.sel.environmentmodel.ui.views.CommunicationDiagramView;
import ca.mcgill.sel.environmentmodel.ui.views.MessageView;
import ca.mcgill.sel.environmentmodel.ui.views.ParameterView;
import ca.mcgill.sel.environmentmodel.util.EmModelUtil;
import ca.mcgill.sel.environmentmodel.util.Multiplicity;

/**
 * Utility methods for use case diagrams in the UI.
 * @author rlanguay
 *
 */
public final class EnvironmentModelUtils {    
    private static final int UPPER_BOUND_INDEX = 3;
        
    /**
     * Creates a new instance with default pretty printers.
     */
    private EnvironmentModelUtils() {
    }

    /**
     * Retrieves the class diagram view from the RamApp if viewing a class diagram.
     * @param app currently running RamApp
     * @return class diagram view currently displayed {@link ClassDiagramView}
     */
    public static CommunicationDiagramView getCommunicationDiagramViewFromApp(RamApp app) {
        final Iscene scene = RamApp.getActiveScene();
        if (scene != null && scene instanceof DisplayEnvironmentModelScene) {
            final DisplayEnvironmentModelScene emScene = (DisplayEnvironmentModelScene) scene;
            return emScene.getCommunicationDiagramView();
        }
        return null;
    }
    
    /**
     * Gets the multiplicity of an actor, as displayed in the UI.
     * @param actor The actor
     * @return The multiplicity string to display
     */
    public static String getActorMultiplicity(Actor actor) {
        String upperBound;
        
        if (actor.getActorUpperBound() <= 0) {
            upperBound = "*";
            if (actor.getActorLowerBound() <= 0) {
                return upperBound;
            }               
        } else {
            upperBound = String.valueOf(actor.getActorUpperBound());
            if (actor.getActorLowerBound() == actor.getActorUpperBound()) {
                return upperBound.toString();                             
            }
        }

        StringBuilder builder = new StringBuilder();

        builder.append(actor.getActorLowerBound());
        builder.append("..");
        builder.append(upperBound);

        return builder.toString();

    }
    
    /**
     * Gets the multiplicity of an actor communication, as displayed in the UI.
     * @param actor The actor
     * @return The multiplicity string to display
     */
    public static String getActorCommunicationMultiplicity(Actor actor) {
        String upperBound;
     
        if (actor.getCommunicationUpperBound() <= 0) {
            upperBound = "*";
            if (actor.getCommunicationLowerBound() <= 0) {
                return upperBound;
            }               
        } else {
            upperBound = String.valueOf(actor.getCommunicationUpperBound());
            if (actor.getCommunicationLowerBound() == actor.getCommunicationUpperBound()) {
                return upperBound.toString();                             
            }
        }

        StringBuilder builder = new StringBuilder();

        builder.append(actor.getCommunicationLowerBound());
        builder.append("..");
        builder.append(upperBound);

        return builder.toString();
    }
    
    public static Multiplicity parseMultiplicity(String text) {
        // For example: 0..* --> group(0) = 0..*, group(1) = 0, group(2)= .. ,group(3) = *
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_GROUP_MULTIPLICITY).matcher(text);
        if (!matcher.matches()) {
            return null;
        }

        // it may be null because allowed inputs can be : number OR number..number OR number..*
        String upperBoundString = matcher.group(UPPER_BOUND_INDEX);
        int upperBound;
        int lowerBound = Integer.parseInt(matcher.group(1));
        if (upperBoundString == null) {
            upperBound = lowerBound;
        } else {
            if ("*".equals(upperBoundString)) {
                upperBound = -1;
            } else {
                upperBound = Integer.parseInt(upperBoundString);
                if (upperBound < lowerBound) {
                    return null;
                }
            }
        }
        return new Multiplicity(lowerBound, upperBound);
    }

    public static float getMaxComponentWidth() {
        return RamApp.getApplication().width * 0.7f;
    }    

    /**
     * Add a parameter at the end of an operation.
     * It add the placeholder in the view and in the model
     *
     * @param messageView - the related {@link MessageView}
     */
    public static void createParameterEndMessageType(final MessageView messageView) {
        final RamTextComponent textRow = new RamTextComponent();
        textRow.setBufferSize(Cardinal.SOUTH, OperationView.BUFFER_BOTTOM);
        textRow.setPlaceholderText(Strings.PH_PARAM);

        final RamRectangleComponent parameterContainer = messageView.getParameterContainer();
        int visualIndex = parameterContainer.getChildCount() - 1;
        parameterContainer.addChild(visualIndex, textRow);

        final MessageType messageType = messageView.getMessage().getMessageType();
        final int nbParameters = messageType.getParameters().size();

        // visual index for delimiter; after if it is the first, before otherwise
        final int delimiterIndex = visualIndex;

        if (nbParameters > 0) {
            messageView.addDelimiter(delimiterIndex);
        }
        final MTComponent delimiter = parameterContainer.getChildByIndex(delimiterIndex);

        RamKeyboard keyboard = new RamKeyboard();

        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                parameterContainer.removeChild(textRow);
                if (delimiter != null) {
                    parameterContainer.removeChild(delimiter);
                }
            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    int nbParameters = messageType.getParameters().size();
                    createParameterInModel(messageType, nbParameters, textRow.getText());
                } catch (final IllegalArgumentException e) {
                    return false;
                }
                if (delimiter != null) {
                    parameterContainer.removeChild(delimiter);
                }
                parameterContainer.removeChild(textRow);

                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }
    
    /**
     * Creates a new parameter in the model.
     *
     * @param owner the MessageType the parameter should be added to
     * @param index the index in the list of parameters where the parameter should be added at
     * @param parameterString the string representation of the parameter (e.g., "&lt;type> &lt;parameterName>)
     */
    public static void createParameterInModel(MessageType owner, int index, String parameterString) {
        Matcher matcher =
                Pattern.compile("^" + MetamodelRegex.REGEX_PARAMETER_DECLARATION + "$").matcher(parameterString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The string " + parameterString
                    + " does not conform to parameter syntax");
        }

        String typeString = matcher.group(1);
        String nameString = matcher.group(3);
        
        ParameterType type = getParameterTypeByName(typeString);       
        
        if (nameString == null) {
            throw new IllegalArgumentException("Parameter name did not match naming syntax");
        }
        
        if (type == null) {
            EnvironmentModel em = (EnvironmentModel)owner.eContainer();
            type = EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().createParameterType(em, em.getParametertype().size(), typeString);           
        }

        
        EStructuralFeature containingFeature = EmPackage.Literals.MESSAGE_TYPE__PARAMETERS;
//        if (!EmModelUtil.isUnique(owner, containingFeature, nameString, type)) {
//            throw new IllegalArgumentException("Parameter names duplicate");
//        }

        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().createParameter(owner, index, nameString, 
                type);
    }

    private static ParameterType getParameterTypeByName(String name) {
       
        return getParameterTypeByName(name, ((DisplayEnvironmentModelScene) RamApp.getActiveScene()).getEnvironmentModel());
    }

    private static ParameterType getParameterTypeByName(String name, EnvironmentModel em) {
  
        for (ParameterType parameterType : em.getParametertype()) {
            if (parameterType.getName().equals(name))
                return parameterType;
        }

        return null;
    }

    public static List<MessageType> getMessageTypes(MessageType type) {
        EnvironmentModel em = (EnvironmentModel) type.eContainer();
        return em.getMessageTypes();
    }

}
