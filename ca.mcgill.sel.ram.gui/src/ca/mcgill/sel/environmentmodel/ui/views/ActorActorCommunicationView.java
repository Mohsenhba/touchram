package ca.mcgill.sel.environmentmodel.ui.views;

import org.mt4j.components.TransformSpace;

import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.ActorActorCommunication;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;

public class ActorActorCommunicationView extends RelationshipView<Actor, ActorView> {    
    
    private static final int SCALING_CONSTANT = 180;
    private static final float TEXT_OFFSET = 10.0f;
    
    private GraphicalUpdater graphicalUpdater;
    
    private RamTextComponent nameField;
    private ActorActorCommunication represented;
    
    private float currentTextRotation;

    public ActorActorCommunicationView(ActorActorCommunication communication, Actor from, 
            ActorView fromView, Actor to, ActorView toView) {
        
        super(from, fromView, to, toView);
        this.represented = communication;
        
        this.lineStyle = LineStyle.SOLID;
        
        EnvironmentModel em = EMFModelUtil.getRootContainerOfType(from, EmPackage.Literals.ENVIRONMENT_MODEL);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(em);
        graphicalUpdater.addGUListener(this.represented, this);
    }
    
    @Override
    protected void update() {
        drawAllLines(); 
        if (nameField == null) {
            createNameField();
            if (represented.getName() == null) {
                nameField.showKeyboard();
            }
        }
        moveNameField();
    }
    
    /**
     * Draws the line between the center of the note and the center of the annotated element (not using the CDEnds).
     */
    @Override
    protected void drawAllLines() {
        float fromX = fromEnd.getLocation().getX();
        float fromY = fromEnd.getLocation().getY();
        float toX = toEnd.getLocation().getX();
        float toY = toEnd.getLocation().getY();

        drawLine(fromX, fromY, null, toX, toY);
    }
    
    
    /**
     * Updates the line layout for the view.
     */
    @Override
    public void updateLines() {
        RamRectangleComponent viewFrom = fromEnd.getComponentView();
        RamRectangleComponent viewTo = toEnd.getComponentView();

        // get the top left corner coordinates of the classes for all calculations
        Vector3D topLeftFrom = viewFrom.getPosition(TransformSpace.GLOBAL);
        Vector3D topLeftTo = viewTo.getPosition(TransformSpace.GLOBAL);

        float heightFrom = viewFrom.getHeightXY(TransformSpace.GLOBAL);
        float widthFrom = viewFrom.getWidthXY(TransformSpace.GLOBAL);
        float heightTo = viewTo.getHeightXY(TransformSpace.GLOBAL);
        float widthTo = viewTo.getWidthXY(TransformSpace.GLOBAL);

        // calculate the outer coordinates and center of each class
        float leftXFrom = topLeftFrom.getX();
        float rightXFrom = topLeftFrom.getX() + widthFrom;
        float topYFrom = topLeftFrom.getY();
        float bottomYFrom = topLeftFrom.getY() + heightFrom;
        float leftXTo = topLeftTo.getX();
        float rightXTo = topLeftTo.getX() + widthTo;
        float topYTo = topLeftTo.getY();
        float bottomYTo = topLeftTo.getY() + heightTo;

        Position fromEndPosition = null;
        Position toEndPosition = null;
        
        if (rightXFrom < leftXTo) {
            // depending on the two classes get the position of the ends
            // where they should be drawn
            // from class is on the left side of to class

            // from class is above to class
            if (bottomYFrom < topYTo) {
                fromEndPosition = Position.BOTTOM;
                toEndPosition = Position.LEFT;
            } else if (bottomYTo < topYFrom) {
                // to class is above from class
                fromEndPosition = Position.RIGHT;
                toEndPosition = Position.BOTTOM;
            } else {
                // classes are next each other (neither above nor below)
                fromEndPosition = Position.RIGHT;
                toEndPosition = Position.LEFT;
            }
        } else if (rightXTo < leftXFrom) {
            // to class is on the left side of from class
            // from class is above to class
            if (bottomYFrom < topYTo) {
                fromEndPosition = Position.LEFT;
                toEndPosition = Position.TOP;
            } else if (bottomYTo < topYFrom || Math.abs(bottomYTo - topYFrom) < 0.5) {
                // to class is above from class
                // And also handles an edge case where the classes are very close on the y-axis,
                // which causes problems if the ends are positioned somewhere else.
                // See issue #36.
                fromEndPosition = Position.LEFT;
                toEndPosition = Position.BOTTOM;
            } else {
                // classes are next each other (neither above nor below)
                fromEndPosition = Position.LEFT;
                toEndPosition = Position.RIGHT;
            }
        } else if (bottomYFrom < topYTo) {
            // classes neither on the left nor on the right of each other
            // from class is above to class
            fromEndPosition = Position.BOTTOM;
            toEndPosition = Position.TOP;
        } else if (bottomYTo < topYFrom) {
            // classes neither on the left nor on the right of each other
            // to class is above from class
            fromEndPosition = Position.TOP;
            toEndPosition = Position.BOTTOM;
        } else {
            fromEndPosition = Position.OFFSCREEN;
            toEndPosition = Position.OFFSCREEN;
        }
        
        updateRelationshipEnds(viewFrom, viewTo, fromEndPosition, toEndPosition);
    }

    @Override
    protected void updateRelationshipEnds(RamRectangleComponent viewFrom, RamRectangleComponent viewTo,
            Position fromEndPosition, Position toEndPosition) {
        // now we set the positions.

        // process from end
        LinkableView<?> classViewFrom = (LinkableView<?>) viewFrom;
        LinkableView<?> classViewTo = (LinkableView<?>) viewTo;

        // if previous and current positions are different
        // also if fromEnd.getPosition() is null
        if (fromEndPosition != fromEnd.getPosition()) {
            classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
            classViewFrom.updateLayout();
        } else {
            // if position is the same reset the positions
            // for all ends on the same edge of this view.
            classViewFrom.setCorrectPosition(fromEnd);
        }

        // process to end
        if (toEndPosition != toEnd.getPosition()) {
            classViewTo.moveRelationshipEnd(toEnd, toEndPosition);
            classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
            classViewTo.updateLayout();
        } else {
            classViewTo.setCorrectPosition(toEnd);
        }
    }
    
    public ActorActorCommunication getCommunication() {
        return this.represented;
    }
    
    private void createNameField() {
        ITextViewHandler nameHandler = EmHandlerFactory.INSTANCE.getTextViewHandler();
        nameField = new TextView(getCommunication(), EmPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setPlaceholderText(Strings.PH_ENTER_COMMUNICATION_NAME);
//        ((TextView) nameField).setHandler(nameHandler);
        ((TextView) nameField).registerTapProcessor(nameHandler);

        addChild(nameField);
    }
    
    private void moveNameField() {
        float fromX = fromEnd.getLocation().getX();
        float fromY = fromEnd.getLocation().getY();
        float toX = toEnd.getLocation().getX();
        float toY = toEnd.getLocation().getY();
        
        float x;
        float y;
        
        x = (fromX + toX) / 2;
        y = (fromY + toY) / 2;
       
        float angle = (float) Math.atan2(fromY - toY, fromX - toX);
        
        if (fromX == toX) {
            angle = 0;
        }
        
        if (angle < -Math.PI / 2) {
            angle = angle + (float) Math.PI;
        } else if (angle > Math.PI / 2) {
            angle = angle - (float) Math.PI;
        }
        
        nameField.setAnchor(PositionAnchor.CENTER);
        if (angle != 0) {
            x += TEXT_OFFSET * Math.sin(angle);
            y -= TEXT_OFFSET * Math.cos(angle);
        } else if (fromX == toX) {
            nameField.setAnchor(PositionAnchor.LOWER_LEFT);
        } else {
            y -= TEXT_OFFSET;
        }
       
        nameField.setPositionRelativeToParent(new Vector3D(x, y));
        
        float angleToRotate = angle - currentTextRotation;      
        nameField.rotateZ(new Vector3D(x, y), (float) (angleToRotate * SCALING_CONSTANT / Math.PI));
        
        currentTextRotation = angle;
        
    }
 
}