package ca.mcgill.sel.environmentmodel.ui.views;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.MessageDirection;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;


public class ActorMessagesView extends RelationshipView<EObject, LinkableView<?>> implements
        INotifyChangedListener {

    /**
     * The offset of text from the association.
     */
    private static final float TEXT_OFFSET_FROM_ASSOCIATION = 2.0f;
    private static final float TEXT_OFFSET_FROM_CLASSIFIER = 1.0f;
        
    private static final int ARROW_SIZE = 20;
    private static final int ROTATION = 90;
    
    private static final float TEXT_OFFSET_FROM_TEXT = 20.0f;     
    
    private static final float CONTAINER_OFFSET_FROM_LINE = ARROW_SIZE + 10.0f;
    private static final float CONTAINER_OFFSET_FROM_ACTOR = 10.0f;
    
    private static final float MIN_ARROW_LEN = 10.0f;  

    private Actor actor;   

    private MultiplicityTextView multiplicityView;
    
    protected Map<Message, MessageView> messageLocations; 

    private MTPolygon inArrowPolygon;
    private MTPolygon outArrowPolygon;   
    
    private int inputMessageNum;
    private int outputMessageNum;
    
    private float maxMessageLen;
    
    /**
     * Creates a new ActorMessagesView.
     *
     * @param actor
     *            the actor{@link Actor}
     * @param actorView
     *            the actor view {@link ActorView}
     * @param em
     *            the environment model {@link EnvironmentModel}
     * @param systemBoxView
     *            the system box view {@link SystemBoxView}
     * @see RelationshipView#RelationshipView(ca.mcgill.sel.ram.NamedElement, LinkableView,
     *      ca.mcgill.sel.ram.NamedElement, LinkableView)
     */
    public ActorMessagesView(Actor actor, ActorView actorView, EnvironmentModel em, SystemBoxView systemBoxView) {
       
        super(actor, actorView, em, systemBoxView);

        this.actor = actor;

        this.lineStyle = LineStyle.SOLID;
        
        messageLocations = new HashMap<Message, MessageView>();
        
        inputMessageNum = 0;
        outputMessageNum = 0;
        
        maxMessageLen = MIN_ARROW_LEN;
        initializeMessages();
        
        // We do not need to add a listener for the "to" side, this listener already listens on the whole class
        EMFEditUtil.addListenerFor(actor, this);
    }

    @Override
    protected void update() {     
        maxMessageLen = MIN_ARROW_LEN;
        
        if (multiplicityView == null) {
            createMultiplicityView();
        }
        
        moveMultiplicityView();
        moveMessages();
        
        drawAllLines(); 
        
    }   
   
    /**
     * Draws the line between the center of the note and the center of the annotated element (not using the CDEnds).
     */
    @Override
    protected void drawAllLines() {
        float fromX = fromEnd.getLocation().getX();
        float fromY = fromEnd.getLocation().getY();
        Position positionFrom = fromEnd.getPosition();
        float toX = toEnd.getLocation().getX();
        float toY = toEnd.getLocation().getY();
        Position positionTo = toEnd.getPosition();        

        if (positionFrom == Position.BOTTOM && positionTo == Position.TOP
                || positionFrom == Position.TOP && positionTo == Position.BOTTOM
                || positionFrom == Position.LEFT && positionTo == Position.RIGHT
                || positionFrom == Position.RIGHT && positionTo == Position.LEFT) {
            // if the ends are the opposite of each other
            // (e.g. LEFT and RIGHT which means the two classes are next to each other)
            // we need three lines
            // therefore create one from each end that goes to the middle of the two classes
            // the third line will then connect the two lines
            boolean isFromAlone = fromEnd.isAlone();
            boolean isToAlone = toEnd.isAlone();

            boolean lineDrawn = false;
            
            // if both ends are alone on their side of the class, draw one line in the center
            if (isFromAlone && isToAlone) {
                if (positionFrom == Position.RIGHT || positionFrom == Position.LEFT) {
                    float centerY = fromY + (toY - fromY) / 2.0f;
                    drawLine(fromX, centerY, null, toX, centerY);
                    lineDrawn = true;
                } else {
                    float centerX = fromX + (toX - fromX) / 2.0f;
                    drawLine(centerX, fromY, null, centerX, toY);
                    lineDrawn = true;
                }
            } else if (isFromAlone) {
                // if the "from" end is alone on its side of the class, draw the line straight from the "to" position
                Vector3D fromClassLocation = getLocalVecToParentRelativeSpace(fromEnd.getComponentView(),
                        fromEnd.getComponentView().getBounds().getVectorsLocal()[0]);
                float fromTop = fromClassLocation.getY();
                float fromBottom = fromTop + fromEnd.getComponentView().getHeightXY(TransformSpace.RELATIVE_TO_PARENT);
                if (positionFrom == Position.RIGHT || positionFrom == Position.LEFT) {
                    if (toY > fromTop && toY < fromBottom) {
                        fromEnd.setLocation(new Vector3D(fromX, toY));
                        drawLine(fromX, toY, null, toX, toY);
                        lineDrawn = true;
                    }
                }
                float fromLeft = fromClassLocation.getX();
                float fromRight = fromLeft + fromEnd.getComponentView().getWidthXY(TransformSpace.RELATIVE_TO_PARENT);
                if (toX > fromLeft && toX < fromRight) {
                    fromEnd.setLocation(new Vector3D(toX, fromY));
                    drawLine(toX, fromY, null, toX, toY);
                    lineDrawn = true;
                }
            } else if (isToAlone) {
                // if the "to" end is alone on its side of the class, draw the line straight from the "from" position
                Vector3D toClassLocation = getLocalVecToParentRelativeSpace(toEnd.getComponentView(),
                        toEnd.getComponentView().getBounds().getVectorsLocal()[0]);
                float toTop = toClassLocation.getY();
                float toBottom = toTop + toEnd.getComponentView().getHeightXY(TransformSpace.RELATIVE_TO_PARENT);
                if (positionFrom == Position.RIGHT || positionFrom == Position.LEFT) {
                    if (fromY > toTop && fromY < toBottom) {
                        toEnd.setLocation(new Vector3D(toX, fromY));
                        drawLine(fromX, fromY, null, toX, fromY);
                        lineDrawn = true;
                    }
                }
                float toLeft = toClassLocation.getX();
                float toRight = toLeft + toEnd.getComponentView().getWidthXY(TransformSpace.RELATIVE_TO_PARENT);
                if (fromX > toLeft && fromX < toRight) {
                    toEnd.setLocation(new Vector3D(fromX, toY));
                    drawLine(fromX, fromY, null, fromX, toY);
                    lineDrawn = true;
                }
            }

            // otherwise draw 3 lines
            if (!lineDrawn) {
                float fromCenterX = fromX + (toX - fromX) / 2.0f;
                float fromCenterY = fromY + (toY - fromY) / 2.0f;
                float toCenterX = toX + (fromX - toX) / 2.0f;
                float toCenterY = toY + (fromY - toY) / 2.0f;
                drawLine(fromX, fromY, positionFrom, fromCenterX, fromCenterY);
                drawLine(toX, toY, positionTo, toCenterX, toCenterY);
                // Revert the position according to the current one.
                // Since drawLine does the same for TOP and BOTTOM
                // it is not necessary to distinguish between the two cases.
                // If the classes are next to each the y value stays the same and the x center is used.
                if (positionFrom == Position.RIGHT || positionFrom == Position.LEFT) {
                    drawLine(fromCenterX, fromY, Position.TOP, toCenterX, toY);
                    // otherwise x stays the same and the y center is used
                } else {
                    drawLine(fromX, fromCenterY, Position.LEFT, toX, toCenterY);
                }
            }
        } else {
            // two lines are sufficient
            // so just draw them starting one from each end
            drawLine(fromX, fromY, positionFrom, toX, toY);
            drawLine(toX, toY, positionTo, fromX, fromY);
        }
        
        drawArrows();
        
    }
    
    
    public void drawArrows() {
        float xIn = fromEnd.getLocation().getX();
        float yIn = fromEnd.getLocation().getY();
        float xOut = fromEnd.getLocation().getX();
        float yOut = fromEnd.getLocation().getY();
        
        Position positionFrom = fromEnd.getPosition();
        Position positionTo;
        switch (positionFrom) {
            case LEFT:
                positionTo = Position.RIGHT;
                break;
            case RIGHT:
                positionTo = Position.LEFT;
                break;
            case TOP:
                positionTo = Position.BOTTOM;
                break;
            case BOTTOM:
                positionTo = Position.TOP;
                break;
            default:
                positionTo = Position.OFFSCREEN;
                break;
        }
              
        switch (fromEnd.getPosition()) {
            case BOTTOM:                
                xIn -= ARROW_SIZE;
                yIn += CONTAINER_OFFSET_FROM_ACTOR + TEXT_OFFSET_FROM_TEXT;
                xOut += ARROW_SIZE;
                yOut += CONTAINER_OFFSET_FROM_ACTOR + TEXT_OFFSET_FROM_TEXT;
                break;
            case TOP:
                xIn -= ARROW_SIZE;
                yIn -= CONTAINER_OFFSET_FROM_ACTOR + TEXT_OFFSET_FROM_TEXT;
                xOut += ARROW_SIZE;
                yOut -= CONTAINER_OFFSET_FROM_ACTOR + TEXT_OFFSET_FROM_TEXT;
                break;
            case LEFT:
                xIn -= CONTAINER_OFFSET_FROM_ACTOR + multiplicityView.getWidth();
                yIn -= CONTAINER_OFFSET_FROM_LINE;
                xOut -= CONTAINER_OFFSET_FROM_ACTOR + multiplicityView.getWidth();
                yOut += CONTAINER_OFFSET_FROM_LINE;
                break;
            case RIGHT:
                xIn += CONTAINER_OFFSET_FROM_ACTOR + multiplicityView.getWidth();
                yIn -= CONTAINER_OFFSET_FROM_LINE;
                xOut += CONTAINER_OFFSET_FROM_ACTOR + multiplicityView.getWidth();
                yOut += CONTAINER_OFFSET_FROM_LINE;
                break;
        }        
        
        float arrowLength;
        if (fromEnd.getPosition() == Position.RIGHT || fromEnd.getPosition() == Position.LEFT) {    
            arrowLength = maxMessageLen;
            if (fromEnd.getPosition() == Position.LEFT) {
                arrowLength = -arrowLength;
            }
            
            if (inputMessageNum > 0) {
                drawLine(xIn, yIn, null, xIn + arrowLength, yIn);
                if (!containsChild(inArrowPolygon)) {
                    inArrowPolygon = getShiftedRotatedArrowPolygon(xIn + arrowLength, yIn, drawColor, positionTo);
                    addChild(inArrowPolygon);
                }
            }
            if (outputMessageNum > 0) {
                drawLine(xOut, yOut, null, xOut + arrowLength, yOut);
                if (!containsChild(outArrowPolygon)) {
                    outArrowPolygon = getShiftedRotatedArrowPolygon(xOut, yOut, drawColor, positionFrom);
                    addChild(outArrowPolygon);
                }
            }
        } else if (positionFrom == Position.TOP || positionFrom == Position.BOTTOM) {
            arrowLength = Math.max(inputMessageNum, outputMessageNum) * TEXT_OFFSET_FROM_TEXT;          
            if (positionFrom == Position.TOP) {
                arrowLength = -arrowLength;
            }
            if (inputMessageNum > 0) {
                drawLine(xIn, yIn, null, xIn, yIn + arrowLength);
                inArrowPolygon = getShiftedRotatedArrowPolygon(xIn, yIn + arrowLength, drawColor, positionTo);
                addChild(inArrowPolygon);
            }
            if (outputMessageNum > 0) {
                drawLine(xOut, yOut, null, xOut, yOut + arrowLength);
                outArrowPolygon = getShiftedRotatedArrowPolygon(xOut, yOut, drawColor, positionFrom);
                addChild(outArrowPolygon);
            }
        }
        
    }
    
    private void moveMessages() {
        float xIn = fromEnd.getLocation().getX();
        float yIn = fromEnd.getLocation().getY();
        float xOut = fromEnd.getLocation().getX();
        float yOut = fromEnd.getLocation().getY();
              
        switch (fromEnd.getPosition()) {
            case BOTTOM:                
                xIn -= CONTAINER_OFFSET_FROM_LINE;
                yIn += CONTAINER_OFFSET_FROM_ACTOR;
                xOut += CONTAINER_OFFSET_FROM_LINE;
                yOut += CONTAINER_OFFSET_FROM_ACTOR;
                break;
            case TOP:
                xIn -= CONTAINER_OFFSET_FROM_LINE;
                yIn -= CONTAINER_OFFSET_FROM_ACTOR;
                xOut += CONTAINER_OFFSET_FROM_LINE;
                yOut -= CONTAINER_OFFSET_FROM_ACTOR;
                break;
            case LEFT:
                xIn -= CONTAINER_OFFSET_FROM_ACTOR + multiplicityView.getWidth();
                yIn -= CONTAINER_OFFSET_FROM_LINE;
                xOut -= CONTAINER_OFFSET_FROM_ACTOR + multiplicityView.getWidth();
                yOut += CONTAINER_OFFSET_FROM_LINE;
                break;
            case RIGHT:
                xIn += CONTAINER_OFFSET_FROM_ACTOR + multiplicityView.getWidth();
                yIn -= CONTAINER_OFFSET_FROM_LINE;
                xOut += CONTAINER_OFFSET_FROM_ACTOR + multiplicityView.getWidth();
                yOut += CONTAINER_OFFSET_FROM_LINE;
                break;
        }        
      
        for (Message message : actor.getMessages()) {
            MessageView messageView = messageLocations.get(message);
            
            if (fromEnd.getPosition() == Position.LEFT) {               
                if (message.getMessageDirection() == MessageDirection.INPUT) {
                    messageView.setAnchor(PositionAnchor.LOWER_RIGHT);
                    yIn -= TEXT_OFFSET_FROM_TEXT;
                    messageView.setPositionRelativeToParent(new Vector3D(xIn, yIn)); 
                } else {
                    messageView.setAnchor(PositionAnchor.UPPER_RIGHT);
                    yOut += TEXT_OFFSET_FROM_TEXT;
                    messageView.setPositionRelativeToParent(new Vector3D(xOut, yOut));
                }
            } else if (fromEnd.getPosition() == Position.RIGHT) {                
                if (message.getMessageDirection() == MessageDirection.INPUT) {
                    messageView.setAnchor(PositionAnchor.LOWER_LEFT);
                    yIn -= TEXT_OFFSET_FROM_TEXT;
                    messageView.setPositionRelativeToParent(new Vector3D(xIn, yIn)); 
                } else {
                    messageView.setAnchor(PositionAnchor.UPPER_LEFT);
                    yOut += TEXT_OFFSET_FROM_TEXT;
                    messageView.setPositionRelativeToParent(new Vector3D(xOut, yOut));
                }
            } else if (fromEnd.getPosition() == Position.BOTTOM) {                
                if (message.getMessageDirection() == MessageDirection.INPUT) {
                    messageView.setAnchor(PositionAnchor.UPPER_RIGHT);
                    yIn += TEXT_OFFSET_FROM_TEXT;
                    messageView.setPositionRelativeToParent(new Vector3D(xIn, yIn)); 
                } else {
                    messageView.setAnchor(PositionAnchor.UPPER_LEFT);
                    yOut += TEXT_OFFSET_FROM_TEXT;
                    messageView.setPositionRelativeToParent(new Vector3D(xOut, yOut));
                }
            } else if (fromEnd.getPosition() == Position.TOP) {           
                if (message.getMessageDirection() == MessageDirection.INPUT) {
                    messageView.setAnchor(PositionAnchor.LOWER_RIGHT);
                    yIn -= TEXT_OFFSET_FROM_TEXT;
                    messageView.setPositionRelativeToParent(new Vector3D(xIn, yIn)); 
                } else {
                    messageView.setAnchor(PositionAnchor.LOWER_LEFT);
                    yOut -= TEXT_OFFSET_FROM_TEXT;
                    messageView.setPositionRelativeToParent(new Vector3D(xOut, yOut));
                }
            }
            maxMessageLen = Math.max(maxMessageLen, messageView.getWidth());

        }      
    }        
    
    /**
     * Updates the line layout for the view.
     */
    @Override
    public void updateLines() {
        RamRectangleComponent viewFrom = fromEnd.getComponentView();
        RamRectangleComponent viewTo = toEnd.getComponentView();

        // get the top left corner coordinates of the classes for all calculations
        Vector3D topLeftFrom = viewFrom.getPosition(TransformSpace.GLOBAL);
        Vector3D topLeftTo = viewTo.getPosition(TransformSpace.GLOBAL);

        float heightFrom = viewFrom.getHeightXY(TransformSpace.GLOBAL);
        float widthFrom = viewFrom.getWidthXY(TransformSpace.GLOBAL);
        float heightTo = viewTo.getHeightXY(TransformSpace.GLOBAL);
        float widthTo = viewTo.getWidthXY(TransformSpace.GLOBAL);

        // calculate the outer coordinates and center of each class
        float leftXFrom = topLeftFrom.getX();
        float rightXFrom = topLeftFrom.getX() + widthFrom;
        float topYFrom = topLeftFrom.getY();
        float bottomYFrom = topLeftFrom.getY() + heightFrom;
        float leftXTo = topLeftTo.getX();
        float rightXTo = topLeftTo.getX() + widthTo;
        float topYTo = topLeftTo.getY();
        float bottomYTo = topLeftTo.getY() + heightTo;

        Position fromEndPosition = null;
        Position toEndPosition = null;
        
        if (topYTo > bottomYFrom) {
            toEndPosition = Position.TOP;
            if (rightXFrom <= leftXTo) {
                fromEndPosition = Position.RIGHT;
            } else if (leftXFrom >= rightXTo) {
                fromEndPosition = Position.LEFT;
            } else {
                fromEndPosition = Position.BOTTOM;
            }
        } else if (bottomYTo < topYFrom) {         
            toEndPosition = Position.BOTTOM;
            if (rightXFrom <= leftXTo) {
                fromEndPosition = Position.RIGHT;
            } else if (leftXFrom >= rightXTo) {
                fromEndPosition = Position.LEFT;
            } else {
                fromEndPosition = Position.TOP;
            }
        } else if (rightXFrom < leftXTo) {
            fromEndPosition = Position.RIGHT;
            toEndPosition = Position.LEFT;
        } else if (leftXFrom > rightXTo) {
            fromEndPosition = Position.LEFT;
            toEndPosition = Position.RIGHT;
        } else {
            fromEndPosition = Position.OFFSCREEN;
            toEndPosition = Position.OFFSCREEN;
        }
        
        updateRelationshipEnds(viewFrom, viewTo, fromEndPosition, toEndPosition);
    }


    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == actor) {
            if (notification.getFeature() == EmPackage.Literals.ACTOR__MESSAGES) {
                switch (notification.getEventType()) {
                    case Notification.REMOVE:
                        Message message = (Message) notification.getOldValue();
                        removeMessage(message);
                        break;
                    case Notification.ADD:
                        message = (Message) notification.getNewValue();
                        addMessageView(message);
                        this.shouldUpdate();
                        break;
//                    case Notification.MOVE:
//                        event = (TimeTriggeredEvent) notification.getNewValue();
//                        setTimeTriggeredEventPosition(event, notification.getPosition());
//                        break;    
                }
            }
        }
        
    }

    
    private void removeMessage(Message message) {     
        if (messageLocations.containsKey(message)) {
            RamApp.getApplication().invokeLater(new Runnable() {
                @Override
                public void run() {
                    MessageView messageView = messageLocations.remove(message);
                    removeChild(messageView);
                    messageView.destroy();                    
                    if (message.getMessageDirection() == MessageDirection.INPUT) {
                        inputMessageNum--;
                    } else {
                        outputMessageNum--;
                    }
                    shouldUpdate();
                }
            });
        }
    }

    @Override
    protected void updateRelationshipEnds(RamRectangleComponent viewFrom, RamRectangleComponent viewTo,
            Position fromEndPosition, Position toEndPosition) {
        // now we set the positions.

        // process from end
        LinkableView<?> classViewFrom = (LinkableView<?>) viewFrom;
        LinkableView<?> classViewTo = (LinkableView<?>) viewTo;

        // if previous and current positions are different
        // also if fromEnd.getPosition() is null
        if (fromEndPosition != fromEnd.getPosition()) {
            classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
            classViewFrom.updateLayout();
        } else {
            // if position is the same reset the positions
            // for all ends on the same edge of this view.
            classViewFrom.setCorrectPosition(fromEnd);
        }

        // process to end
        if (toEndPosition != toEnd.getPosition()) {
            classViewTo.moveRelationshipEnd(toEnd, toEndPosition);
            classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
            classViewTo.updateLayout();
        } else {
            classViewTo.setCorrectPosition(toEnd);
        }
        
    }
    
    /**
     * Creates a view for the multiplicityView.
     *
     */
    private void createMultiplicityView() {
        multiplicityView = new MultiplicityTextView(actor, EmPackage.Literals.ACTOR__MESSAGES);

        ITextViewHandler handler = EmHandlerFactory.INSTANCE.getActorCommunicationMultiplicityHandler();
        multiplicityView.registerTapProcessor(handler);

        multiplicityView.setFont(Fonts.getSmallFontByColor(drawColor));
        multiplicityView.setBufferSize(Cardinal.SOUTH, 0);
        addChild(multiplicityView);
    }
    
    private void moveMultiplicityView() {
        float x = fromEnd.getLocation().getX();
        float y = fromEnd.getLocation().getY();

        switch (fromEnd.getPosition()) {
            case BOTTOM:
                multiplicityView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x -= TEXT_OFFSET_FROM_ASSOCIATION;
                y += TEXT_OFFSET_FROM_CLASSIFIER;
                break;
            case TOP:
                multiplicityView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x -= TEXT_OFFSET_FROM_ASSOCIATION;
                y -= TEXT_OFFSET_FROM_CLASSIFIER;
                break;
            case LEFT:
                multiplicityView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x -= TEXT_OFFSET_FROM_CLASSIFIER;
                y -= TEXT_OFFSET_FROM_ASSOCIATION;
                break;
            case RIGHT:
                multiplicityView.setAnchor(PositionAnchor.LOWER_LEFT);
                x += TEXT_OFFSET_FROM_CLASSIFIER;
                y -= TEXT_OFFSET_FROM_ASSOCIATION;
                break;
        }
        multiplicityView.setPositionRelativeToParent(new Vector3D(x, y));
    }

    protected void initializeMessages() {
        for (Message message : actor.getMessages()) {
            addMessageView(message);          
        }
    }

    private void addMessageView(Message message) {
        if (message.getMessageDirection() == MessageDirection.INPUT) {
            inputMessageNum++;
        } else {
            outputMessageNum++;
        }

        MessageView messageView = new MessageView(message);
        
        messageView.setBufferSize(Cardinal.NORTH, 0.0f);
        messageView.setBufferSize(Cardinal.SOUTH, 0.0f);
        messageView.setBufferSize(Cardinal.WEST, 0.0f);
        messageView.setBufferSize(Cardinal.EAST, 0.0f);
        
        addChild(messageView);
        
        messageView.setHandler(EmHandlerFactory.INSTANCE.getMessageViewHandler());

        messageLocations.put(message, messageView);

    }
    
//    /**
//     * Adds listeners to the textview parameter.
//     *
//     * @param textView The field where the listeners occurred
//     * @param handler The text view handler to add
//     * @param enabledTapAndHold If the tapAndHold event is enabled for this textview
//     */
//    private void registerTextViewProcessors(TextView textView, ITextViewHandler handler, boolean enabledTapAndHold) {
//
//        textView.registerTapProcessor(handler);
//
//        if (enabledTapAndHold) {
//            textView.registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(),
//                    GUIConstants.TAP_AND_HOLD_DURATION));
//            textView.addGestureListener(TapAndHoldProcessor.class,
//                    new TapAndHoldVisualizer(RamApp.getApplication(), this));
//            textView.addGestureListener(TapAndHoldProcessor.class, handler);
//        }
//    }
    
    /**
     * This function is used to draw a navigability arrow head along with the aggregation/composition shape. It happens
     * when the reference type is aggregation or composition and navigability is negative.
     *
     * @param x the x position
     * @param y the y position
     * @param drawColor the color
     * @param position the current position
     * @return a new polygon that is properly positioned
     */
    private MTPolygon getShiftedRotatedArrowPolygon(float x, float y, MTColor drawColor, Position position) {
        MTPolygon polygon = null;

        Vector3D rotationPoint = new Vector3D(x, y);
        polygon = new ArrowPolygon(x, y, drawColor);
        // rotate depending on position
        switch (position) {
            case BOTTOM:
                polygon.rotateZ(rotationPoint, ROTATION);
                break;
            case TOP:
                polygon.rotateZ(rotationPoint, -ROTATION);
                break;

            case LEFT:
                polygon.rotateZ(rotationPoint, 2 * ROTATION);
                break;

            case RIGHT:
                break;
        }
 
        return polygon;
    }
    
}