package ca.mcgill.sel.environmentmodel.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.components.visibleComponents.shapes.MTEllipse;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutHorizontallyCentered;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.layouts.Layout;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.LayoutElement;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IActorViewHandler;
import ca.mcgill.sel.environmentmodel.EmPackage;

public class ActorView extends LinkableView<IActorViewHandler> {
    /**
     * The length of the actor arms. It has public visibility because the ActorUseCaseAssociationView
     * needs it to calculate until where the lines should be displayed. 
     */
    public static final float ACTOR_ARM_LENGTH = 50.0f;

    private static final float MINIMUM_WIDTH = 75f;
    private static final float ACTOR_BODY_LENGTH = 30;

    private static final int ACTOR_HEAD_RADIUS = 15;
    private static final float ACTOR_LEG_LENGTH = 25f;
    private static final float ACTOR_STROKE_WEIGHT = 3f;    

    /**
     * The length of the actor stick man figure. 
     */
    public static final float ACTOR_CONTAINER_LENGTH = 
            2.0f * ACTOR_HEAD_RADIUS + ACTOR_BODY_LENGTH + ACTOR_LEG_LENGTH;

    private RamRectangleComponent actorContainer;
    private MTEllipse actorHead;
    private RamLineComponent actorBody;
    private RamLineComponent actorArms;
    private RamLineComponent actorLeftLeg;
    private RamLineComponent actorRightLeg;
    private MultiplicityTextView multiplicityView;
    private RamRectangleComponent typeNameContainer;

    private TextView nameField;
    private TextView typeField;
    private RamTextComponent colon;

    protected ActorView(CommunicationDiagramView communicationDiagramView, Actor actor, LayoutElement layoutElement) {
        super(communicationDiagramView, actor, layoutElement);
        setMinimumWidth(MINIMUM_WIDTH);
        setNoStroke(true);
        setNoFill(true);
        setLayout(new VerticalLayout(Layout.HorizontalAlignment.CENTER));
        setBuffers(0.0f);

        // translate the class based on the meta-model
        if (layoutElement != null) {
            setLayoutElement(layoutElement);
        }        

        addMultiplicityField();

        buildTypeNameContainer();

        RamRectangleComponent r = new RamRectangleComponent();
        r.setLayout(new HorizontalLayoutHorizontallyCentered());
        r.setNoStroke(true);
        r.setNoFill(true);
        r.setAutoMaximizes(true);

        actorContainer = new RamRectangleComponent();
        actorContainer.setMinimumHeight(ACTOR_CONTAINER_LENGTH);
        actorContainer.setMinimumWidth(ACTOR_ARM_LENGTH);
        actorContainer.setAutoMaximizes(true);
        actorContainer.setNoFill(true);
        buildActor();

        r.addChild(actorContainer);

        addChild(r);

        addChild(typeNameContainer);
        if (getActor().getActorType().getName() == null) {
            typeField.showKeyboard();

        }
    }

    public Actor getActor() {
        return (Actor) this.represented;
    }

    @Override
    public void destroy() {       
        destroyRelationships();
        this.destroyAllChildren();
        super.destroy();
    }

    @Override
    public void setStrokeColor(MTColor color) {
        if (actorContainer != null) {
            actorHead.setStrokeColor(color);
            actorBody.setStrokeColor(color);
            actorArms.setStrokeColor(color);
            actorLeftLeg.setStrokeColor(color);
            actorRightLeg.setStrokeColor(color);
        }        
    }

    @Override
    public MTColor getStrokeColor() {
        if (actorBody != null) {
            return actorBody.getStrokeColor();
        } else {
            return super.getStrokeColor();
        }
    }

    @Override
    public void setWidthLocal(float width) {
        super.setWidthLocal(width);
    }

    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        Actor actor = this.getActor();
        if (notification.getNotifier() == actor) {
            if (notification.getFeature() == EmPackage.Literals.ACTOR__MESSAGES) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        if (actor.getMessages().size() == 1) {
                            getCommunicationDiagramView().addActorMessagesView(actor);                            
                        }
                    case Notification.REMOVE:
                        if (actor.getMessages().size() == 0) {
                            getCommunicationDiagramView().removeActorMessagesView(actor);
                        }                   
                }
            }

        }      
    }     

    private void addMultiplicityField() {
        // Add the multiplicity field to base view
        multiplicityView = new MultiplicityTextView(getActor());
        multiplicityView.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        multiplicityView.setAlignment(Alignment.CENTER_ALIGN);
        multiplicityView.setBufferSize(Cardinal.WEST, ACTOR_ARM_LENGTH);
        multiplicityView.setBufferSize(Cardinal.NORTH, 0.0f);
        multiplicityView.setBufferSize(Cardinal.SOUTH, 0.0f);
        multiplicityView.setNewlineDisabled(true);
        addChild(multiplicityView);

        ITextViewHandler handler = EmHandlerFactory.INSTANCE.getActorMultiplicityViewHandler();
        multiplicityView.registerTapProcessor(handler);
    }

    private void buildActor() {        
        // Head
        // We use the name field to determine the width of the view because it is always going to be the widest
        // child control in this view
        actorHead = new MTEllipse(
                RamApp.getApplication(), 
                new Vector3D(ACTOR_ARM_LENGTH / 2, ACTOR_HEAD_RADIUS + ACTOR_STROKE_WEIGHT), 
                ACTOR_HEAD_RADIUS, 
                ACTOR_HEAD_RADIUS);
        actorHead.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        actorHead.setStrokeWeight(ACTOR_STROKE_WEIGHT);
        actorContainer.addChild(actorHead);        

        // Body
        Vertex bodyTop = new Vertex(
                actorHead.getCenterPointGlobal().getX(), actorHead.getCenterPointGlobal().getY() + ACTOR_HEAD_RADIUS);
        Vertex bodyBottom = new Vertex(bodyTop.getX(), bodyTop.getY() + ACTOR_BODY_LENGTH);
        actorBody = new RamLineComponent(bodyTop, bodyBottom);
        actorBody.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        actorContainer.addChild(actorBody);        

        // Arms
        Vertex centerPoint = new Vertex(
                bodyTop.getX(), bodyTop.getY() + ((bodyBottom.getY() - bodyTop.getY()) / 2));
        Vertex armLeft = new Vertex(centerPoint.getX() - ACTOR_ARM_LENGTH / 2, centerPoint.getY());
        Vertex armRight = new Vertex(centerPoint.getX() + ACTOR_ARM_LENGTH / 2, centerPoint.getY());
        actorArms = new RamLineComponent(armLeft, armRight);
        actorContainer.addChild(actorArms);        

        // Legs
        float leftLegBottomX = bodyBottom.getX() - ((float) Math.sqrt(ACTOR_LEG_LENGTH * ACTOR_LEG_LENGTH / 2));
        float leftLegBottomY = bodyBottom.getY() + ((float) Math.sqrt(ACTOR_LEG_LENGTH * ACTOR_LEG_LENGTH / 2));
        float rightLegBottomX = bodyBottom.getX() + ((float) Math.sqrt(ACTOR_LEG_LENGTH * ACTOR_LEG_LENGTH / 2));
        float rightLegBottomY = bodyBottom.getY() + ((float) Math.sqrt(ACTOR_LEG_LENGTH * ACTOR_LEG_LENGTH / 2));

        actorLeftLeg = new RamLineComponent(bodyBottom, new Vertex(leftLegBottomX, leftLegBottomY));
        actorContainer.addChild(actorLeftLeg);
        actorRightLeg = new RamLineComponent(bodyBottom, new Vertex(rightLegBottomX, rightLegBottomY));
        actorContainer.addChild(actorRightLeg);
    }


    public void buildTypeNameContainer() {
        typeNameContainer = new RamRectangleComponent(); 

        nameField = new TextView(getActor(), EmPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setPlaceholderText(Strings.PH_ENTER_ACTOR_NAME);
        nameField.setBufferSize(Cardinal.SOUTH, 0);
        nameField.setBufferSize(Cardinal.WEST, 0);
        nameField.setBufferSize(Cardinal.EAST, 0);
        nameField.setUniqueName(true);
        nameField.setHandler(EmHandlerFactory.INSTANCE.getTextViewHandler());

        colon = new RamTextComponent(":", false);
        colon.setBufferSize(Cardinal.SOUTH, 0);
        colon.setBufferSize(Cardinal.WEST, 2.0f);
        colon.setBufferSize(Cardinal.EAST, 2.0f);

        typeField = new TextView(getActor(), EmPackage.Literals.ACTOR__ACTOR_TYPE);
        typeField.setBufferSize(Cardinal.SOUTH, 0);
        typeField.setBufferSize(Cardinal.WEST, 0);
        typeField.setBufferSize(Cardinal.EAST, 0);
        typeNameContainer.addChild(typeField);
        typeField.setHandler(EmHandlerFactory.INSTANCE.getTypeNameHandler());

        typeNameContainer.setLayout(new HorizontalLayoutVerticallyCentered(0));
        typeNameContainer.setAutoMaximizes(false);
        typeNameContainer.setBufferSize(Cardinal.EAST, 0);
        typeNameContainer.setBufferSize(Cardinal.WEST, 0);
    }

    public void showName() {
        if (!nameShowed()) {
            typeNameContainer.addChild(0, nameField);
            typeNameContainer.addChild(1, colon);
            if (getActor().getName() == null) {
                nameField.showKeyboard();
            }
        } else {
            typeNameContainer.removeChild(0);
            typeNameContainer.removeChild(0);
        }
    }

    public boolean nameShowed() {
        if (typeNameContainer.getChildCount() == 1) {
            return false;
        } 
        return true;
    }

}
