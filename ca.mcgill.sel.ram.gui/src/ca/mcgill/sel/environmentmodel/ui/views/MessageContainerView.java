package ca.mcgill.sel.environmentmodel.ui.views;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.LayoutElement;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.MessageDirection;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IBaseViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ContainerComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.perspective.controller.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;


/**
 * This view acts as a super class for all views that represent a sub-class of {@link Classifier}. It contains the basic
 * structure of a class and supports messages and relationships. Sub-classes may override the build methods in order
 * to change the visual appearance.
 *
 * @param <H> the handler interface that this component uses
 * @author qiutan
 */
public class MessageContainerView extends ContainerComponent<IGestureEventListener>{

//    /**
//     * The view container for messages.
//     */
//    protected RamRectangleComponent messageContainer;
//
//    /**
//     * The map of messages to their views.
//     */
//    protected Map<Message, MessageView> messages;
//    
//    /**
//     * The direction of the contained messages.
//     */
//    protected MessageDirection direction;
//
//
//    /**
//     * Creates a new {@link MessageContainerView} for a given classifier and layout element.
//     *
//     * @param structuralDiagramView the {@link ClassDiagramView} that is the parent of this view
//     * @param classifier the {@link Classifier} this view represents
//     * @param layoutElement the {@link LayoutElement} that contains the layout information for this classifier
//     * @param editableMessages true, if messages can be edited, false otherwise
//     */
//    protected MessageContainerView(CommunicationDiagramView structuralDiagramView, Actor actor, MessageDirection direction,
//            LayoutElement layoutElement) {
//        super(structuralDiagramView, actor, layoutElement);
//
//        this.direction = direction;
//        setEnabled(true);
//
//        // build components
//        build(classifier);
//
//        // now init based on the class
//        initializeClass(classifier);
//        updateStyle();
//
//    }
//    
//    /**
//     * Creates a new {@link MessageContainerView} for a given classifier and layout element. The messages are editable.
//     *
//     * @param structuralDiagramView the {@link ClassDiagramView} that is the parent of this view
//     * @param classifier the {@link Classifier} this view represents
//     * @param layoutElement the {@link LayoutElement} that contains the layout information for this classifier
//     */
//    protected MessageContainerView(ClassDiagramView structuralDiagramView, Classifier classifier,
//            LayoutElement layoutElement) {
//        this(structuralDiagramView, classifier, layoutElement, true);
//    }
//
//    /**
//     * Adds a new message view for the given message to this view at the given index. The index is an index inside
//     * the {@link #messagesContainer}.
//     *
//     * @param index the index inside the messages container where this message should be added to
//     * @param message the message to add
//     */
//    protected void addMessageField(int index, Message message) {
//        // Ensure that message field is added at the end of the messages container and not at an
//        // index out of bound , there might be more messages in the class than in the
//        // messages container.
//        int cleanedIndex = Math.min(index, messageContainer.getChildCount());
//        
//        MessageView messageView = new MessageView(message, editableMessages);
//        messages.put(message, messageView);
//
//        messageView.setHandler(EmHandlerFactory.INSTANCE.getMessageViewHandler());       
//    }
//    
//    /**
//     * Builds this view for the given classifier. Builds the name, attributes and messages.
//     *
//     * @param classifier the classifier for this view
//     */
//    protected void build(Actor actor, MessageDirection direction) {
//        setMinimumWidth(MINIMUM_WIDTH);
//
//        buildNameField(classifier);
//        if (currentPerspective != null) {
//            if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateAttribute(currentPerspective, 
//                    currentLanguageRole)) {
//                buildAttributesContainer();
//            }
//            
//            if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateMessage(currentPerspective, 
//                    currentLanguageRole)) {
//                buildMessagesContainer();
//            } else {
//                //in the case of domain model, hide attributesContainer until if it's empty
//                if (classifier.getAttributes().size() == 0 && attributesContainer != null) {
//                    removeChild(attributesContainer);
//                }
//            }
//        }
//    }
//
// 
//
//    /**
//     * Builds the empty messages container.
//     */
//    protected void buildMessagesContainer() {
//        messageContainer = new RamRectangleComponent(new VerticalLayout());
//        addChild(messageContainer);
//
//        messages = new HashMap<Message, MessageView>();
//        messageContainer = new RamRectangleComponent(new VerticalLayout());
//        messageContainer.setNoStroke(true);
//        messageContainer.setMinimumHeight(ICON_SIZE);
//        messageContainer.setBufferSize(Cardinal.SOUTH, BUFFER_CONTAINER_BOTTOM);
//        messageContainer.addChild(messageContainer);
//        
//       
//    }
//
////    @Override
////    public void destroy() {
////        // destroy relationships
////        for (RelationshipView<?, ?> view : getAllRelationshipViews()) {
////            view.destroy();
////        }
////
////        relationshipEndByPosition.clear();
////
////        // destroy rest
////        super.destroy();
////    }
//
////    /**
////     * Returns the classifier represented by this view.
////     *
////     * @return the {@link Classifier} represented by this view
////     */
////    @Override
////    public Actor getActor() {
////        return (Actor) represented;
////    }
//
//    /**
//     * Returns the component containing all {@link MessageView}s.
//     *
//     * @return the {@link RamRectangleComponent} containing all {@link MessageView}s
//     */
//    public RamRectangleComponent getMessagesContainer() {
//        return messageContainer;
//    }
//
//    /**
//     * Returns the {@link ClassDiagramView} which contains this view.
//     *
//     * @return the {@link ClassDiagramView} (parent of this view)
//     */
//    @Override
//    public CommunicationDiagramView getCommunicationDiagramView() {
//        return communicationDiagramView;
//    }
//
//    /**
//     * Adds message views to the view for each message of the given classifier.
//     *
//     * @param classifier the classifier for which to add message views for
//     */
//    protected void initializeMessages(Classifier classifier) {
//        if (this.messagesContainer != null) {
//            for (int i = 0; i < classifier.getMessages().size(); i++) {
//                Message message = classifier.getMessages().get(i);
//                addMessageField(i, message);
//            }
//        }
//    }
//    
//    @Override
//    public void notifyChanged(Notification notification) {
//        super.notifyChanged(notification);
//        Classifier classifier = (Classifier) represented;
//        if (notification.getNotifier() == classifier) {
//            if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__OPERATIONS) {
//                switch (notification.getEventType()) {
//                    case Notification.REMOVE:
//                        Message message = (Message) notification.getOldValue();
//                        removeMessage(message);
//                        break;
//                    case Notification.ADD:
//                        message = (Message) notification.getNewValue();
//                        addMessageField(notification.getPosition(), message);
//                        break;
//                    case Notification.MOVE:
//                        message = (Message) notification.getNewValue();
//                        setMessagePosition(message, notification.getPosition());
//                        break;    
//                }
//            } else if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__SUPER_TYPES) {
//                switch (notification.getEventType()) {
//                    case Notification.REMOVE:
//                        Classifier superType = (Classifier) notification.getOldValue();
//                        classDiagramView.removeInheritanceView(classifier, superType);
//                        break;
//                    case Notification.ADD:
//                        superType = (Classifier) notification.getNewValue();
//                        classDiagramView.addInheritanceView(classifier, superType);
//                        break;
//                } 
//            } else if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__ABSTRACT) {
//                boolean italic = notification.getNewBooleanValue();
//                setNameItalic(italic);
//            } else if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__ATTRIBUTES) {
//                switch (notification.getEventType()) {
//                    case Notification.MOVE:
//                        Attribute attribute = (Attribute) notification.getNewValue();
//                        setAttributePosition(attribute, notification.getPosition());
//                        break;
//                }
//            }
//        }
//    }
//   
//    /**
//     * Set the class name in italic.
//     *
//     * @param italic true if you want to set the class name in italic. false otherwise.
//     */
//    private void setNameItalic(boolean italic) {
//        if (COREModelUtil.isReference(represented)) {
//            if (italic) {
//                nameField.setFont(Fonts.FONT_CLASS_NAME_REFERENCE_ITALIC);                
//            } else {
//                nameField.setFont(Fonts.FONT_CLASS_NAME_REFERENCE);
//            }
//        } else {
//            if (italic) {
//                nameField.setFont(Fonts.FONT_CLASS_NAME_ITALIC);                
//            } else {
//                nameField.setFont(Fonts.FONT_CLASS_NAME);
//            }
//        }
//    }
//
//    /**
//     * Removes the view of the given message from this view.
//     *
//     * @param message the message to remove
//     */
//    protected void removeMessage(final Message message) {
//        if (messages.containsKey(message)) {
//            RamApp.getApplication().invokeLater(new Runnable() {
//
//                @Override
//                public void run() {
//                    MessageView messageView = messages.remove(message);
//
//                    messagesContainer.removeChild(messageView);
//                    messageView.destroy();
//                }
//            });
//        }
//    }
//    
//    public void setMessagePosition(Message message, int index) {
//        RamApp.getApplication().invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                MessageView messageView = messages.get(message);
//
//                messagesContainer.removeChild(messageView);
//                messagesContainer.addChild(index, messageView);
//            }
//        });
//    }
//    
//    public void setAttributePosition(Attribute attribute, int index) {
//        RamApp.getApplication().invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                AttributeView attributeView = attributes.get(attribute);
//
//                attributesContainer.removeChild(attributeView);
//                attributesContainer.addChild(index, attributeView);
//            }
//        });
//    }
//
//    @Override
//    public void removeRelationshipEnd(CDEnd<? extends NamedElement, ? extends RamRectangleComponent> end) {
//        super.removeRelationshipEnd(end);
//        updateSpacerSize();
//    }
//
//    @Override
//    public void scale(float x, float y, float z, Vector3D scalingPoint, TransformSpace transformSpace) {
//        super.scale(x, y, z, scalingPoint, transformSpace);
//    }
//
//    /**
//     * Determines how many messages are visible in the message container.
//     *
//     * @return the number of visible opersations
//     */
//    protected int getNumberOfVisibleMessages() {
//        int result = 0;
//        for (Message o : messages.keySet()) {
//            if (shouldShowMessage(o)) {
//                result++;
//            }
//        }
//        return result;
//    }
//
//    /**
//     * Calculates the new size of the spacer and sets the size in order to ensure that the association
//     * end role names and multiplicities do not overlap.
//     */
//    void updateSpacerSize() {
//        List<CDEnd<?, ?>> leftList = relationshipEndByPosition.get(Position.LEFT);
//        List<CDEnd<?, ?>> rightList = relationshipEndByPosition.get(Position.RIGHT);            
//    
//        int leftSlots = 0;
//        if (leftList.size() > 1) {
//            leftSlots = calculateNumberOfSlots(leftList, Position.LEFT);
//        }
//        int rightSlots = 0;
//        if (rightList.size() > 1) {
//            rightSlots = calculateNumberOfSlots(rightList, Position.RIGHT);
//        }
//    
//        int verticalSlotsNeeded = Math.max(leftSlots, rightSlots);
//        boolean sizeChanged = false;
//                                
//        if (verticalSlotsNeeded != previousVerticalSlotsNeeded 
//                && classifierviewSpacer != null && attributesContainer != null) {
//            previousVerticalSlotsNeeded = verticalSlotsNeeded;
//            float baseHeight = nameContainer.getHeight() + attributesContainer.getHeight()
//                + attributesContainer.getBufferSize(Cardinal.SOUTH)
//                + attributesContainer.getBufferSize(Cardinal.NORTH)
//                + messagesContainer.getHeight();
//            float desiredHeight = verticalSlotsNeeded * (Fonts.FONTSIZE_ASSOCIATION_TEXT
//                    + AssociationView.TEXT_OFFSET_FROM_ASSOCIATION / 2);
//            float newHeight = 0;
//            
//            if (verticalSlotsNeeded > 0) {
//                newHeight = desiredHeight - baseHeight;
//            }
//            
//            classifierviewSpacer.setMinimumHeight(newHeight);
//            sizeChanged = true;
//        }
//        
//        List<CDEnd<?, ?>> topList = relationshipEndByPosition.get(Position.TOP);
//        List<CDEnd<?, ?>> bottomList = relationshipEndByPosition.get(Position.BOTTOM);            
//    
//        int topSlots = 0;
//        if (topList.size() > 1) {
//            topSlots = calculateNumberOfSlots(topList, Position.TOP);
//        }
//        
//        int bottomSlots = 0;
//        if (bottomList.size() > 1) {
//            bottomSlots = calculateNumberOfSlots(bottomList, Position.BOTTOM);
//        }
//    
//        int horizontalSlotsNeeded = Math.max(topSlots, bottomSlots);
//        
//        if (horizontalSlotsNeeded != previousHorizontalSlotsNeeded && classifierviewSpacer != null) {
//            previousHorizontalSlotsNeeded = horizontalSlotsNeeded;
//            float newWidth = horizontalSlotsNeeded * (Fonts.FONTSIZE_ASSOCIATION_TEXT
//                    + AssociationView.TEXT_OFFSET_FROM_ASSOCIATION / 2);
//            classifierviewSpacer.setMinimumWidth(newWidth);
//            sizeChanged = true;
//        }
//        
//        if (sizeChanged && classifierviewSpacer != null) {
//            classifierviewSpacer.updateParent();
//        }
//    }
//    
//    @Override
//    public void setSizeLocal(float width, float height) {
//        super.setSizeLocal(width, height);
//        updateRelationships();
//    }
//
//    @Override
//    public void translate(Vector3D dirVect) {
//        super.translate(dirVect);
//        updateRelationships();
//    }
//
//    /**
//     * Updates the style of this view depending on whether the classifier is a reference or not.
//     */
//    @Override
//    protected void updateStyle() {
//        Classifier classifier = (Classifier) represented;
//        if (COREModelUtil.isReference(classifier)) {
//            setFillColor(Colors.CLASS_VIEW_REFERENCE_FILL_COLOR);
//            setStrokeColor(Colors.CLASS_VIEW_REFERENCE_STROKE_COLOR);
//            visibilityField.setFont(Fonts.REFERENCE_FONT);
//        } else {
//            setFillColor(Colors.CLASS_VIEW_DEFAULT_FILL_COLOR);
//            setStrokeColor(Colors.CLASS_VIEW_DEFAULT_STROKE_COLOR);
//            visibilityField.setFont(Fonts.DEFAULT_FONT);
//        }
//        
//        setNameItalic(classifier.isAbstract());
//    }

}
