package ca.mcgill.sel.environmentmodel.ui.views;

import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;

import ca.mcgill.sel.environmentmodel.EmFactory;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.LayoutElement;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageDetailViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.views.AbstractView;


public class MessageDetailView extends AbstractView<IMessageDetailViewHandler> {

    private static final float STARTING_X = 50.0f;
    private static final float STARTING_Y = 100.0f;
    
    private Message message;
    private CommunicationDiagramView diagramView;
    
    private MessageInformationView view;
            
    public MessageDetailView(Message message, CommunicationDiagramView diagramView, float width, float height) {
        super(width, height);
        
        this.message = message;
        this.diagramView = diagramView;
                
        buildAndLayout(width, height);
    }
    
    @Override
    protected void registerGestureListeners(IGestureEventListener listener) {
        super.registerGestureListeners(listener);

        addGestureListener(TapProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), getParent()));
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));
    }    
    
    @Override
    public void destroy() {
        view.destroy();
        
        // do rest
        super.destroy();
    }
    
    public Message getMessage() {
        return this.message;
    }
    
    private void buildAndLayout(float width, float height) {
        LayoutElement layoutElement = EmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(STARTING_X);
        layoutElement.setY(STARTING_Y);
        
        view = new MessageInformationView(diagramView, message, layoutElement);
        addChild(view);
        view.setHandler(EmHandlerFactory.INSTANCE.getMessageInformationViewHandler());
    }    
}
