package ca.mcgill.sel.environmentmodel.ui.views;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.LayoutElement;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.MessageType;
import ca.mcgill.sel.environmentmodel.Parameter;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageInformationViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener.ActionEvent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;


public class MessageInformationView extends BaseView<IMessageInformationViewHandler> {

    public static final String ACTION_PARAMETER_ADD = "messagetype.parameter.add";
    
    private static final int LABEL_PADDING = 30;
    
    private RamRectangleComponent summaryContainer;
    private RamTextComponent nameLabel;     
    
    private RamRectangleComponent parameterContainer;
    private RamTextComponent parameterLabel;
    private RamButton addParameterButton;
    
    private Message message;
    private Map<Parameter, ParameterView> parameterToViewMap;    
    
    protected MessageInformationView(CommunicationDiagramView diagramView, Message message,
            LayoutElement layoutElement) {
        super(diagramView, message.getMessageType(), layoutElement);
       
        this.message = message;
        setNoFill(false);
        setNoStroke(false);
        setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        
        parameterToViewMap = new HashMap<Parameter, ParameterView>();
        
        if (layoutElement != null) {
            setLayoutElement(layoutElement);
        }
        
        build();
    }
    
    private void build() {    
        MessageType messageType = message.getMessageType();
        
        summaryContainer = new RamRectangleComponent(new VerticalLayout());
        summaryContainer.setNoStroke(false);
        summaryContainer.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        
        // Name
        nameContainer = new RamRectangleComponent(new HorizontalLayout());
        nameContainer.setNoFill(true);
        nameContainer.setNoStroke(true);
        
        nameLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
        nameLabel.setText(Strings.LABEL_USE_CASE_NAME);
        nameLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
        nameContainer.addChild(nameLabel);
        
        nameField = new TextView(message, EmPackage.Literals.MESSAGE__MESSAGE_TYPE);
        nameField.setPlaceholderText(Strings.PH_ENTER_MESSAGE);
        nameContainer.addChild(nameField);
        summaryContainer.addChild(nameContainer);      
        
        // Handlers
        ((TextView) nameField).setHandler(EmHandlerFactory.INSTANCE.getTypeNameHandler());          

        parameterContainer = new RamRectangleComponent(new HorizontalLayout());
        parameterContainer.setNoFill(true);
        parameterContainer.setNoStroke(true);

        parameterLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
        parameterLabel.setText(Strings.LABEL_PARAMETER);
        parameterLabel.setBufferSize(Cardinal.EAST, LABEL_PADDING);
        parameterContainer.addChild(parameterLabel);
        parameterContainer.setLayout(new VerticalLayout());

        RamImageComponent addImage = new RamImageComponent(Icons.ICON_ADD, Colors.ICON_STRUCT_DEFAULT_COLOR,
                ICON_SIZE, ICON_SIZE);
        ActionListener actionListener = (ActionListener) EmHandlerFactory.INSTANCE.getMessageInformationViewHandler();
        addParameterButton = new RamButton(addImage) {
            @Override
            public void executeAction() {
                if (this.isEnabled()) {
                    ActionEvent event = new ActionEvent(messageType, this.getActionCommand());
                    actionListener.actionPerformed(event);     
                }
            }
        };

        addParameterButton.setActionCommand(ACTION_PARAMETER_ADD);
        addParameterButton.addActionListener((ActionListener) EmHandlerFactory.INSTANCE.getMessageInformationViewHandler());
       
        parameterContainer.addChild(addParameterButton); 

        
        List<Parameter> parameters = messageType.getParameters();
        for (Parameter parameter : parameters) {
            addParameter(parameter);
        }            
        
        summaryContainer.addChild(parameterContainer);
 
        addChild(summaryContainer);
    
    }
    
    public void addParameter(Parameter parameter) {
        ParameterView parameterView = new ParameterView(parameter);
        parameterToViewMap.put(parameter, parameterView);
        parameterContainer.addChild(parameterView);
        parameterView.setHandler(EmHandlerFactory.INSTANCE.getParameterViewHandler());
        if (parameter.getType().getName() == null) {
            parameterView.getTypeField().showKeyboard();
//        } else {
//            parameterView.getTypeField()
        }
    }
    
    public void removeParameter(Parameter parameter) {
        if (parameterToViewMap.containsKey(parameter)) {
            RamApp.getApplication().invokeLater(new Runnable() {
                @Override
                public void run() {
                    ParameterView parameterView = parameterToViewMap.remove(parameter);
                    parameterContainer.removeChild(parameterView);
                    parameterView.destroy();
                }
            });
        }
    }
    
    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        
        if (notification.getNotifier() == represented) {
            if (notification.getFeature() == EmPackage.Literals.MESSAGE_TYPE__PARAMETERS) {
                switch (notification.getEventType()) {
                    case Notification.REMOVE:
                        Parameter parameter = (Parameter) notification.getOldValue();
                        removeParameter(parameter);
                        break;
                    case Notification.ADD:
                        parameter = (Parameter) notification.getNewValue();
                        addParameter(parameter);
                        break;
                }
            }
        }
    }
    
    @Override
    public void destroy() {
        // Destroy children
        nameLabel.destroy();
        
        parameterContainer.destroy();
        
        summaryContainer.destroy();
       
        super.destroy();
    }
    
}
