package ca.mcgill.sel.environmentmodel.ui.views;

import org.mt4j.util.font.IFont;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Parameter;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IParameterViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.IHandled;


/**
 * A {@link ParameterView} represents a {@link Parameter} and is contained inside an {@link MessageView}.
 * 
 * @author mschoettle
 * @author yhattab
 */
public class ParameterView extends RamRectangleComponent implements IHandled<IParameterViewHandler> {

    private static final float SPACER_WIDTH = 30.0f;
    
    private TextView nameField;
    private TextView typeField;

    private Parameter parameter;

    private boolean isMutable;

    private GraphicalUpdater graphicalUpdater;
    private COREArtefact artefact;

    private IParameterViewHandler handler;

    /**
     * Creates a new {@link ParameterView} for the given {@link Parameter}.
     * 
     * @param messageView the parent {@link MessageView}
     * @param parameter the {@link Parameter} to be represented
     * @param mutable is this parameter view mutable
     */
    public ParameterView(Parameter parameter, boolean mutable) {
        this.parameter = parameter;
        setBuffers(0);

        isMutable = mutable;

        typeField = new TextView(parameter, EmPackage.Literals.PARAMETER__TYPE);
        typeField.setBufferSize(Cardinal.SOUTH, 2);
        typeField.setBufferSize(Cardinal.WEST, 2);
        typeField.setBufferSize(Cardinal.EAST, 2);
        typeField.setPlaceholderText("Enter Param Type");
        addChild(typeField);
        
        addSpacer(getHeight(), SPACER_WIDTH);

        nameField = new TextView(parameter, EmPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setBufferSize(Cardinal.SOUTH, 2);
        nameField.setBufferSize(Cardinal.EAST, 2);
        nameField.setBufferSize(Cardinal.WEST, 2);
        nameField.setUniqueName(true);
        nameField.setPlaceholderText("Enter Param Name");
        addChild(nameField);

        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONT_SIZE / 8));

        if (isMutable) {
            typeField.setHandler(EmHandlerFactory.INSTANCE.getTypeNameHandler());
            nameField.setHandler(EmHandlerFactory.INSTANCE.getTextViewHandler());
        }

        EnvironmentModel em = EMFModelUtil.getRootContainerOfType(parameter, EmPackage.Literals.ENVIRONMENT_MODEL);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(em);
        graphicalUpdater.addGUListener(parameter, this);
        
//        EMFEditUtil.addListenerFor(parameter, this);
//        artefact = COREArtefactUtil.getReferencingExternalArtefact(parameter);
//        EMFEditUtil.addListenerFor(artefact, this);
    }

    /**
     * Constructor. Makes a default mutable parameter.
     * 
     * @param parameter Parameter to add the the messageView
     */
    public ParameterView(Parameter parameter) {
        this(parameter, true);
    }

    @Override
    public void destroy() {
        super.destroy();

        graphicalUpdater.removeGUListener(parameter, this);
//        EMFEditUtil.removeListenerFor(parameter, this);
//        
//        if (artefact != null) {
//            EMFEditUtil.removeListenerFor(artefact, this);
//        }
    }
    
    /**
     * Returns the {@link Parameter} that is represented by this view.
     * 
     * @return the represented {@link Parameter}
     */
    public Parameter getParameter() {
        return parameter;
    }

    @Override
    public IParameterViewHandler getHandler() {
        return this.handler;
    }

    @Override
    public void setHandler(IParameterViewHandler handler) {
        this.handler = handler;
    }

    /**
     * Returns the type component.
     * 
     * @return the type component
     */
    public TextView getTypeField() {
        return typeField;
    }
    
    /**
     * Returns the name component.
     * 
     * @return the name component
     */
    public TextView getNameField() {
        return nameField;
    }

    /**
     * Updates the style of this view.
     * 
     * @param font the font to use for text components of this view
     */
    protected void updateStyle(IFont font) {
        nameField.setFont(font);
        typeField.setFont(font);
    }
}
