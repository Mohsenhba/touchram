package ca.mcgill.sel.environmentmodel.ui.views.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;

public interface IMessageDetailViewHandler extends IAbstractViewHandler, ITapListener, ITapAndHoldListener {

}
