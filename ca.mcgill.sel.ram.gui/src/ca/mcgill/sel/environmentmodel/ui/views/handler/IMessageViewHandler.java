package ca.mcgill.sel.environmentmodel.ui.views.handler;

import org.mt4j.input.inputProcessors.IGestureEventListener;

import ca.mcgill.sel.environmentmodel.ui.views.MessageView;

/**
 * This interface is implemented by something that can handle events for a {@link MessageView}.
 * 
 * @author qiutan
 */
public interface IMessageViewHandler extends IGestureEventListener {

    /**
     * Handles creating a new {@link ca.mcgill.sel.ram.Parameter} at the given model index and visual index.
     * 
     * @param messageView
     *            the affected {@link MessageView}
     */
    void createParameter(MessageView messageView);

    /**
     * Handles the removal of an {@link ca.mcgill.sel.ram.Message}.
     * 
     * @param messageView
     *            the affected {@link MessageView}
     */
    void removeMessage(MessageView messageView);


}
