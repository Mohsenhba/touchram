package ca.mcgill.sel.environmentmodel.ui.views.handler;

import ca.mcgill.sel.environmentmodel.ui.views.SystemBoxView;

//import ca.mcgill.sel.environmentmodel.ui.views.SystemBoxView;
//import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
//import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
//import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;

public interface ISystemBoxViewHandler extends IBaseViewHandler {
    /**
     * Create a new time-triggered event(and view).
     * 
     * @param view The SystemBoxView where this event should be added.
     */
    void createTimeTriggeredEvent(SystemBoxView view);
}

//public interface ISystemBoxViewHandler extends IBaseViewHandler, IAbstractViewHandler, 
//    ITapListener, ITapAndHoldListener {
//

//}
