package ca.mcgill.sel.environmentmodel.ui.views.handler;

import org.mt4j.input.inputProcessors.IGestureEventListener;

import ca.mcgill.sel.environmentmodel.ui.views.TimeTriggeredEventView;


/**
 * The implementation of this interface can handle events for a {@link EnumLiteralView}}.
 * 
 * @author Franz
 */
public interface ITimeTriggeredEventViewHandler extends IGestureEventListener {

    void removeEvent(TimeTriggeredEventView eventView);
}
