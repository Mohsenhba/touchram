package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import java.util.EnumSet;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.NamedElement;
import ca.mcgill.sel.environmentmodel.Note;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelController;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.ActorView;
import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.environmentmodel.ui.views.CommunicationDiagramView;
import ca.mcgill.sel.environmentmodel.ui.views.NoteView;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ICommunicationDiagramViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;

public class CommunicationDiagramViewHandler extends AbstractViewHandler 
        implements ICommunicationDiagramViewHandler {

    private enum CreateFeature {
        CREATE_ACTOR,
        CREATE_NOTE
    }
    
    private enum CreateRelationship {
        CREATE_ACTOR_ACTOR_COMMUNICATION,
        CREATE_ANNOTATION
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            final CommunicationDiagramView target = (CommunicationDiagramView) tapAndHoldEvent.getTarget();
            final Vector3D position = tapAndHoldEvent.getLocationOnScreen();
            
            OptionSelectorView<CreateFeature> selector = 
                        new OptionSelectorView<CreateFeature>(CreateFeature.values());
            
            RamApp.getActiveScene().addComponent(selector, tapAndHoldEvent.getLocationOnScreen());
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            selector.registerListener(new AbstractDefaultRamSelectorListener<CreateFeature>() {

                @Override
                public void elementSelected(RamSelectorComponent<CreateFeature> selector, CreateFeature element) {
                    switch (element) {
                        case CREATE_ACTOR: 
                            createNewActor(target, position);
                            break;
                        case CREATE_NOTE:
                            createNewNote(target, position);
                            break;
                    }
                }
            });
        }
        return true;
    }

    @Override
    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
            UnistrokeEvent event) {
        CommunicationDiagramView diagramView = (CommunicationDiagramView) target;

        switch (event.getId()) {
            case MTGestureEvent.GESTURE_ENDED:
                Vector3D startGesturePoint = event.getCursor().getStartPosition();
                Vector3D endGesturePoint = event.getCursor().getPosition();
                BaseView<?> startView = null;
                BaseView<?> endView = null;

                for (BaseView<?> view : diagramView.getBaseViews()) {
                    if (MathUtils.pointIsInRectangle(startGesturePoint, view, GUIConstants.MARGIN_ELEMENT_DETECTION)) {
                        startView = view;
                    }
                    if (view.containsPointGlobal(endGesturePoint)) {
                        endView = view;
                    }

                    if (startView != null && endView != null) {
                        if (startView instanceof ActorView && endView instanceof ActorView) {
                            handleActorActorCommunicationCreation(diagramView, startView, endView, endGesturePoint);
                            return;
                        } else if ((startView instanceof NoteView && !(endView instanceof NoteView)) 
                                || (endView instanceof NoteView && !(startView instanceof NoteView))) {
                            handleAnnotationCreation(diagramView, startView, endView, endGesturePoint);
                            return;
                        }
                    }
                }
                break;
        }
        
        // TODO: shapes
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isTapped()) {

            CommunicationDiagramView target = (CommunicationDiagramView) tapEvent.getTarget();
            target.deselect();
        }

        return true;
    }

    
    /**
     * Handles the creation of a actor in the diagram.
     *
     * @param view the view representing the current diagram
     * @param position the position where the note should be located
     */
    @SuppressWarnings("static-method")
    private void createNewActor(final CommunicationDiagramView view, final Vector3D position) {
        final EnvironmentModel em = view.getEnvironmentModel();

        //TO-DO
//        em.eAdapters().add(new EContentAdapter() {
//            
//            private Actor actor;
//            
//            @Override
//            public void notifyChanged(Notification notification) {
//                if (notification.getFeature() == EmPackage.Literals.ENVIRONMENT_MODEL__ACTORS) {
//                    if (notification.getEventType() == Notification.ADD) {
//                        actor = (Actor) notification.getNewValue();                 
//                    }
//                } else if (notification.getFeature() == EmPackage.Literals.CONTAINER_MAP__VALUE) {
//                    if (notification.getEventType() == Notification.ADD) {
//                        view.getActorView(actor).showKeyboard();
//                        view.getActorView(actor).clearNameField();
//                        em.eAdapters().remove(this);
//                    }
//                }
//            }
//        });
       
//        Set<String> actorNames = new HashSet<String>();
//        for (Actor a : view.getEnvironmentModel().getActors()) {
//            actorNames.add(a.getName());
//        }        
//        String actorName = StringUtil.createUniqueName(Strings.DEFAULT_ACTOR_NAME, actorNames);
        
//        Set<String> actorTypeNames = new HashSet<String>();
//        for (ActorType t : view.getEnvironmentModel().getActorTypes()) {
//            actorTypeNames.add(t.getName());
//        }        
//        String actorTypeName = StringUtil.createUniqueName(Strings.DEFAULT_TYPE_NAME, actorTypeNames);

        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().createActor(
                em, null, null, position.getX(), position.getY());
    }
 
    /**
     * Handles the creation of a note in the class diagram.
     *
     * @param view the view representing the current class diagram
     * @param position the position where the note should be located
     */
    @SuppressWarnings("static-method")
    private void createNewNote(final CommunicationDiagramView view, final Vector3D position) {
        final EnvironmentModel em = view.getEnvironmentModel();

        em.eAdapters().add(new EContentAdapter() {

            private Note note;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == EmPackage.Literals.ENVIRONMENT_MODEL__NOTES) {
                    if (notification.getEventType() == Notification.ADD) {
                        note = (Note) notification.getNewValue();
                    }
                } else if (notification.getFeature() == EmPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        view.getNoteView(note).showKeyboard();
                        em.eAdapters().remove(this);
                    }
                }

            }
        });

        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController()
            .createNote(em, position.getX(), position.getY());
    }
    
    /**
     * Handles creation of relationships for an actor-actor communication.
     * 
     * @param communicationDiagramView the {@link CommunicationDiagramView} this gesture was performed in
     * @param startView the {@link BaseView} from which the gesture was performed
     * @param endView the {@link BaseView} to which the gesture was performed
     * @param endGesturePoint the position gesture finished at
     */
    private static void handleActorActorCommunicationCreation(CommunicationDiagramView communicationDiagramView,
            BaseView<?> startView, BaseView<?> endView,
            Vector3D endGesturePoint) {
        EnvironmentModel em = communicationDiagramView.getEnvironmentModel();
        EnumSet<CreateRelationship> availableOptions = EnumSet.allOf(CreateRelationship.class);

        EObject fromEntity = startView.getRepresented();
        EObject toEntity = endView.getRepresented();   

        availableOptions.remove(CreateRelationship.CREATE_ANNOTATION); 


        OptionSelectorView<CreateRelationship> selector = 
                new OptionSelectorView<CreateRelationship>(availableOptions.toArray(new CreateRelationship[] { }));

        RamApp.getActiveScene().addComponent(selector, endGesturePoint);

        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
        selector.registerListener(new AbstractDefaultRamSelectorListener<CreateRelationship>() {

            @Override
            public void elementSelected(RamSelectorComponent<CreateRelationship> selector,
                    CreateRelationship element) {                                 
                EnvironmentModelController controller = EnvironmentModelControllerFactory
                        .INSTANCE.getEnvironmentModelController();                    
                switch (element) {
                    case CREATE_ACTOR_ACTOR_COMMUNICATION:
                        controller.createActorActorCommunication(em, (Actor) fromEntity, (Actor) toEntity, 
                                null);
                        break;                                       
                    }
            }
        });
    }
    
    /**
     * Handles creation of an annotation.
     * 
     * @param communicationDiagramView the {@link CommunicationDiagramView} this gesture was performed in
     * @param startView the {@link BaseView} from which the gesture was performed
     * @param endView the {@link BaseView} to which the gesture was performed
     * @param endGesturePoint the position gesture finished at
     */
    private static void handleAnnotationCreation(CommunicationDiagramView communicationDiagramView,
            BaseView<?> startView, BaseView<?> endView,
            Vector3D endGesturePoint) {
        EnvironmentModel em = communicationDiagramView.getEnvironmentModel();
        EnumSet<CreateRelationship> availableOptions = EnumSet.allOf(CreateRelationship.class);

        EObject fromEntity = startView.getRepresented();
        EObject toEntity = endView.getRepresented();  

        availableOptions.remove(CreateRelationship.CREATE_ACTOR_ACTOR_COMMUNICATION); 

        OptionSelectorView<CreateRelationship> selector = 
                new OptionSelectorView<CreateRelationship>(availableOptions.toArray(new CreateRelationship[] { }));

        RamApp.getActiveScene().addComponent(selector, endGesturePoint);

        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
        selector.registerListener(new AbstractDefaultRamSelectorListener<CreateRelationship>() {

            @Override
            public void elementSelected(RamSelectorComponent<CreateRelationship> selector,
                    CreateRelationship element) {                                 
                EnvironmentModelController controller = EnvironmentModelControllerFactory
                        .INSTANCE.getEnvironmentModelController();                                    
                switch (element) {
                    case CREATE_ANNOTATION:
                        if (fromEntity instanceof Note) {
                            controller.createAnnotation(em, (Note) fromEntity, (NamedElement) toEntity);
                        } else {
                            controller.createAnnotation(em, (Note) toEntity, (NamedElement) fromEntity);
                        }                         
                        break;
                    }
            }
        });

    }
    
    @Override
    public boolean handleTapAndHold(CommunicationDiagramView communicationDiagramView, BaseView<?> target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean handleDoubleTap(CommunicationDiagramView structuralDiagramView, BaseView<?> target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void dragAllSelected(CommunicationDiagramView ccdv, Vector3D directionVector) {
        for (BaseView<?> baseView : ccdv.getSelectedElements()) {
            // create a copy of the translation vector, because translateGlobal modifies it
            baseView.translateGlobal(new Vector3D(directionVector));
        }        
    }

}
