package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EmFactory;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.MessageType;
import ca.mcgill.sel.environmentmodel.Parameter;
import ca.mcgill.sel.environmentmodel.ParameterType;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelController;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.environmentmodel.ui.views.MessageDetailView;
import ca.mcgill.sel.environmentmodel.ui.views.MessageInformationView;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageInformationViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener.ActionEvent;
import ca.mcgill.sel.ram.ui.views.SelectorView;

public class MessageInformationViewHandler extends BaseViewHandler 
    implements IMessageInformationViewHandler, ActionListener {

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        // TODO Auto-generated method stub        
    }

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        final MessageInformationView target = (MessageInformationView) dragEvent.getTarget();
        final MessageDetailView detailView = target.getParentOfType(MessageDetailView.class);
        dragEvent.setTarget(detailView);
        return detailView.getHandler().processDragEvent(dragEvent);
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        final MessageInformationView target = (MessageInformationView) tapAndHoldEvent.getTarget();
        final MessageDetailView detailView = target.getParentOfType(MessageDetailView.class);
        tapAndHoldEvent.setTarget(detailView);
        return detailView.getHandler().processTapAndHoldEvent(tapAndHoldEvent);
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        final MessageInformationView target = (MessageInformationView) tapEvent.getTarget();
        final MessageDetailView detailView = target.getParentOfType(MessageDetailView.class);
        tapEvent.setTarget(detailView);
        return detailView.getHandler().processTapEvent(tapEvent);
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getActionCommand() == MessageInformationView.ACTION_PARAMETER_ADD){
            MessageType messageType = (MessageType) event.getTarget();
            createParameter(messageType);
        }
    }
    
    public void createParameter(MessageType messageType) {
        EnvironmentModel em = (EnvironmentModel) messageType.eContainer();
        EnvironmentModelController controller = EnvironmentModelControllerFactory.INSTANCE
                .getEnvironmentModelController();
        
        if (em.getParametertype().size() > 0) {
            controller.createParameter(messageType, messageType.getParameters().size(), null, 
                    em.getParametertype().get(0));
        } else {
            ParameterType parameterType = controller.createParameterType(em, em.getParametertype().size(), null);
            controller.createParameter(messageType, messageType.getParameters().size(), null, parameterType);
        }
    
    }
}
