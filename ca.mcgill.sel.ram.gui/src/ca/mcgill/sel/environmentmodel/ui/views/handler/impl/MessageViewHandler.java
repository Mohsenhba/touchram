package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.Parameter;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelController;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.utils.EnvironmentModelUtils;
import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.environmentmodel.ui.views.MessageView;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener.ActionEvent;
import ca.mcgill.sel.ram.ui.perspective.controller.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.SelectorView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;


/**
 * The default handler for an {@link MessageView}.
 * 
 * @author qiutan
 */
//public class MessageViewHandler extends BaseViewHandler implements IMessageViewHandler, ILinkedMenuListener {
public class MessageViewHandler extends TextViewHandler implements IMessageViewHandler, ILinkedMenuListener {

    private static final String ACTION_OPERATION_REMOVE = "view.message.remove";
    private static final String ACTION_TYPE_SET = "view.message.type.set";
    private static final String ACTION_PARAMETER_ADD = "view.parameter.add";
    private static final String ACTION_SHOW_PARAMETERS = "view.message.parameters";
    private static final String SUBMENU_CLASS_MORE = "sub.class.more";

    
    @Override
    public void createParameter(final MessageView messageView) {
        EnvironmentModelUtils.createParameterEndMessageType(messageView);
    }


    @Override
    public void removeMessage(MessageView messageView) {
        EnvironmentModelControllerFactory.INSTANCE.getActorController().removeMessage(messageView.getMessage());
    }
    
    private void setMessageType(MessageView messageView, Vector3D location) {
        TextView target = messageView.getNameField();
        RamSelectorComponent<Object> selector = new SelectorView(target.getData(), target.getFeature());
        
        RamApp.getActiveScene().addComponent(selector, location);

        selector.registerListener(new AbstractDefaultRamSelectorListener<Object>() {
            @Override
            public void elementSelected(RamSelectorComponent<Object> selector, Object element) {
                setValue(target.getData(), target.getFeature(), element);
                messageView.rebuild();
            }
        });
        selector.addPlusButton(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                EnvironmentModelController controller = EnvironmentModelControllerFactory.INSTANCE
                        .getEnvironmentModelController();

                Message message = messageView.getMessage();
                
                EnvironmentModel em = (EnvironmentModel) (message.getMessageType().eContainer());
                setValue(target.getData(), target.getFeature(), controller.createMessageType(em, null));

                selector.destroy();
                
                messageView.rebuild();
                messageView.getNameField().showKeyboard();
                
            }
            
        });
        
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            MessageView messageView = (MessageView) linkedMenu.getLinkedView();           
            
            if (ACTION_OPERATION_REMOVE.equals(actionCommand)) {
                removeMessage(messageView);
            } else if (ACTION_TYPE_SET.equals(actionCommand)) {             
                Vector3D location = linkedMenu.getPosition(TransformSpace.GLOBAL);
                setMessageType(messageView, location);

            } else if (ACTION_PARAMETER_ADD.equals(actionCommand)) {
                createParameter(messageView);
            } else if (ACTION_SHOW_PARAMETERS.equals(actionCommand)) {
                messageView.showParameters();
                updateButtons(linkedMenu);
//            } else if (ACTION_OPERATION_SHIFT_UP.equals(actionCommand)) {
//                shiftMessageUp(messageView);
//            } else if (ACTION_OPERATION_SHIFT_DOWN.equals(actionCommand)) {
//                shiftMessageDown(messageView);
            }
        }
    }    

//    public void shiftMessageUp(MessageView messageView) {
//        EList<Message> messages = ((Classifier) messageView.getMessage().eContainer()).getMessages();
//        int index = 0;
//        
//        for (int i = 0; i < messages.size(); i++) {
//            if (messages.get(i).equals(messageView.getMessage())) {
//                index = i;
//            }
//        }
//        
//        if (index > 0) {
////            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().setMessagePosition(messageView
////                    .getMessage(), index - 1);
//            ControllerFactory.INSTANCE.getClassController().setMessagePosition(messageView
//                    .getMessage(), index - 1);
//        }
//    }
//    
//    public void shiftMessageDown(MessageView messageView) {
//        EList<Message> messages = ((Classifier) messageView.getMessage().eContainer()).getMessages();
//        int index = 0;
//        
//        for (int i = 0; i < messages.size(); i++) {
//            if (messages.get(i).equals(messageView.getMessage())) {
//                index = i;
//            }
//        }
//        
//        if (index < messages.size() - 1) {
////            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().setMessagePosition(messageView
////                    .getMessage(), index + 1);
//            ControllerFactory.INSTANCE.getClassController().setMessagePosition(messageView
//                    .getMessage(), index + 1);
//        }
//    }
 
    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((MessageView) rectangle).getMessage();
    }

    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {        
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_OPERATION_REMOVE, this, true);
        
        menu.addSubMenu(1, SUBMENU_CLASS_MORE);   
        menu.addAction(Strings.MENU_STATIC, Strings.MENU_NO_STATIC, Icons.ICON_MENU_STATIC,
                Icons.ICON_MENU_NOT_STATIC,
                ACTION_SHOW_PARAMETERS, this, SUBMENU_CLASS_MORE, true, true);
        
        menu.addAction(Strings.MENU_ATTRIBUTE_SETTER, Icons.ICON_MENU_SWITCH, ACTION_TYPE_SET, this, true);
        
        MessageView messageView = (MessageView) menu.getLinkedView();
        if (messageView.isMutable()) {

            menu.addAction(Strings.MENU_PARAMETER_ADD, Icons.ICON_MENU_ADD_PARAMETER, ACTION_PARAMETER_ADD, this, true);

//            // add the message shift up and down buttons if there is more than 1 message
//            if (((Classifier) messageView.getMessage().eContainer()).getMessages().size() > 1) {
//                menu.addAction(Strings.MENU_OPERATION_SHIFT_UP, 
//                        Icons.ICON_MENU_ELEMENT_SHIFT_UP, ACTION_OPERATION_SHIFT_UP, this, true);
//                menu.addAction(Strings.MENU_OPERATION_SHIFT_DOWN, 
//                        Icons.ICON_MENU_ELEMENT_SHIFT_DOWN, ACTION_OPERATION_SHIFT_DOWN, this, true);
//            }
//            
                  
//
//            menu.addAction(Strings.MENU_ABSTRACT, Strings.MENU_NOT_ABSTRACT, Icons.ICON_MENU_ABSTRACT,
//                    Icons.ICON_MENU_NOT_ABSTRACT, ACTION_OPERATION_ABSTRACT, this, SUBMENU_CLASS_MORE, true,
//                    false);
            menu.setLinkInCorners(false);
//            updateButtons(menu);
        }

    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        MessageView messageView = (MessageView) rectangle;
        List<EObject> ret = new ArrayList<EObject>();
        ret.add(messageView.getMessage());
        ret.add(COREArtefactUtil.getReferencingExternalArtefact(messageView.getMessage()));
        return ret;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        
    }


    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }

    /**
     * Updates buttons inside the menu.
     * 
     * @param menu - the menu which contains buttons.
     */
    private static void updateButtons(RamLinkedMenu menu) {
        MessageView messageView = (MessageView) menu.getLinkedView();

        menu.toggleAction(messageView.parametersShowed(), ACTION_SHOW_PARAMETERS);

    }

//    @Override
//    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
//        MessageView view = (MessageView) link;
//        return view.getNameField();
//    }


}
