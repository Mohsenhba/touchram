package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

//import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
//import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
//import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
//import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
//import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
//import org.mt4j.util.math.Vector3D;
//
//import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
//import ca.mcgill.sel.environmentmodel.ui.views.CommunicationDiagramView;
//import ca.mcgill.sel.environmentmodel.ui.views.SystemBoxView;
//import ca.mcgill.sel.environmentmodel.ui.views.handler.ISystemBoxViewHandler;
//import ca.mcgill.sel.ram.ui.events.WheelEvent;
//import ca.mcgill.sel.ram.ui.views.AbstractView;
//import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;
////import ca.mcgill.sel.usecases.ui.views.SystemBoxView;
////import ca.mcgill.sel.usecases.ui.views.CommunicationDiagramView;
////import ca.mcgill.sel.usecases.ui.views.handler.ISystemBoxViewHandler;
//
///**
// * This handler's job is just to forward events to the diagram view (its parent).
// * @author rlanguay
// *
// */
//public class SystemBoxViewHandler extends AbstractViewHandler 
//    implements ISystemBoxViewHandler {
//
//    @Override
//    public boolean processTapEvent(TapEvent tapEvent) {
//        final SystemBoxView target = (SystemBoxView) tapEvent.getTarget();
//        final CommunicationDiagramView diagramView = target.getParentOfType(CommunicationDiagramView.class);
//        tapEvent.setTarget(diagramView);
//        return diagramView.getHandler().processTapEvent(tapEvent);
//    }
//
//    @Override
//    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
//        final SystemBoxView target = (SystemBoxView) tapAndHoldEvent.getTarget();
//        final CommunicationDiagramView diagramView = target.getParentOfType(CommunicationDiagramView.class);
//        tapAndHoldEvent.setTarget(diagramView);
//        return diagramView.getHandler().processTapAndHoldEvent(tapAndHoldEvent);
//    }
//
//    @Override
//    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
//            UnistrokeEvent event) {
//        final CommunicationDiagramView diagramView = target.getParentOfType(CommunicationDiagramView.class);
//        ((CommunicationDiagramViewHandler) diagramView.getHandler())
//            .handleUnistrokeGesture(diagramView, gesture, startPosition, event);
//    }
//    
//    @Override
//    public boolean processUnistrokeEvent(UnistrokeEvent uniStrokeEvent) {
//        SystemBoxView target = (SystemBoxView) uniStrokeEvent.getTarget();
//        final CommunicationDiagramView diagramView = target.getParentOfType(CommunicationDiagramView.class);
//        uniStrokeEvent.setTarget(diagramView);
//        return diagramView.getHandler().processUnistrokeEvent(uniStrokeEvent);
//    }
//    
//    @Override
//    protected boolean processWheelEvent(WheelEvent wheelEvent) {
//        if (wheelEvent.getTarget() instanceof SystemBoxView) {
//            SystemBoxView target = (SystemBoxView) wheelEvent.getTarget();
//            final CommunicationDiagramView diagramView = target.getParentOfType(CommunicationDiagramView.class);
//            wheelEvent.setTarget(diagramView);
//            return diagramView.getHandler().processGestureEvent(wheelEvent);
//        }
//        
//        return super.processWheelEvent(wheelEvent);
//    }
//    
//    @Override
//    public boolean processDragEvent(DragEvent dragEvent) {
//        SystemBoxView target = (SystemBoxView) dragEvent.getTarget();
//        final CommunicationDiagramView diagramView = target.getParentOfType(CommunicationDiagramView.class);
//        dragEvent.setTarget(diagramView);
//        return diagramView.getHandler().processDragEvent(dragEvent);
//    }
//
//    @Override
//    public void removeRepresented(BaseView<?> baseView) {
//        // TODO Auto-generated method stub
//      
//    }
//
//}

import java.util.HashSet;
import java.util.Set;

import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.SystemBox;
import ca.mcgill.sel.environmentmodel.TimeTriggeredEvent;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.environmentmodel.ui.views.CommunicationDiagramView;
import ca.mcgill.sel.environmentmodel.ui.views.SystemBoxView;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ISystemBoxViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamKeyboard;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.listeners.DefaultRamKeyboardListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.ram.util.MetamodelRegex;


/**
 * The default handler for a {@link SystemBoxView}.
 * 
 * @author Franz
 * @author yhattab
 */
public class SystemBoxViewHandler extends BaseViewHandler implements ISystemBoxViewHandler, ILinkedMenuListener {

    private static final String ACTION_TIME_TRIGGERED_EVENT_ADD = "view.systemBox.TimeTriggeredEvent.add";
   

    @Override
    public void createTimeTriggeredEvent(final SystemBoxView systemBoxView) {
//        // Get the index where we should add the literal
        final int index = ((EnvironmentModel) systemBoxView.getRepresented()).getTimeTriggeredEvents().size();

        // create new row
        final RamTextComponent textRow = new RamTextComponent();
        textRow.setPlaceholderText(Strings.PH_ENTER_TIME_TRIGGERED_EVENT);

        // Get the container holding all the event
        final RamRectangleComponent eventContainer = systemBoxView.getTimeTriggeredEventsContainer();

        // Add the new text row to it
        eventContainer.addChild(textRow);

        // Create keyboard and add listener
        RamKeyboard keyboard = new RamKeyboard();
        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                // If cancelled remove the row and enable the edit buttons
                eventContainer.removeChild(textRow);
            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    // Create the literal in the model
                    createTimeTriggeredEvent((EnvironmentModel) systemBoxView.getRepresented(), index,
                            textRow.getText());
                    
                    // Remove text row and enable the edit buttons
                    eventContainer.removeChild(textRow);
                } catch (final IllegalArgumentException e) {
                    return false;
                }
                return true;
            }
        });

        textRow.showKeyboard(keyboard);

    }

//    /**
//     * Creates a SystemBox Literal by calling the SystemBoxController.
//     * 
//     * @param owner RSystemBox that should contain this literal
//     * @param index Index where we should insert it
//     * @param systemBoxLiteralName Name given to this new literal
//     * @throws IllegalArgumentException If it is a invalid systemBox literal.
//     */
    private void createTimeTriggeredEvent(EnvironmentModel em, int index, String eventName) {
        
//        if (!eventName.matches(MetamodelRegex.)) {
//            throw new IllegalArgumentException("The string " + eventName
//                    + " is not valid syntax for enum literals");
//        }
        
        isEventUniqueIn(em, eventName);
        
        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController()
            .createTimeTriggeredEvent(em, index, eventName);
    }

    /**
     * Checks if time-triggered event is unique.
     * 
     * @param owner Owner of the event.
     * @param eventName name of the event.
     * @throws IllegalArgumentException
     */
    @SuppressWarnings("static-method")
    private void isEventUniqueIn(EnvironmentModel owner, String eventName) {
        for (TimeTriggeredEvent event : owner.getTimeTriggeredEvents()) {
            if (event.getName().toLowerCase().equals(eventName.toLowerCase())) {
                throw new IllegalArgumentException("The string " + eventName
                        + " already exists in" + owner.getName());
            }
        }

    }
//
//    /**
//     * Checks if it has a duplicate literal.
//     * 
//     * @param event All event.
//     */
//    @SuppressWarnings("static-method")
//    private void hasDuplicateLiterals(String[] event) {
//        Set<String> literalSet = new HashSet<String>();
//        for (String literal : event) {
//            literalSet.add(literal.toLowerCase().trim());
//        }
//        if (literalSet.size() != event.length) {
//            throw new IllegalArgumentException("The string contains literal duplicates.");
//        }
//    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            SystemBoxView systemBoxView = (SystemBoxView) linkedMenu.getLinkedView();       

            if (ACTION_TIME_TRIGGERED_EVENT_ADD.equals(actionCommand)) {
                createTimeTriggeredEvent(systemBoxView);
            }
        }
        super.actionPerformed(event);
    }

    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        menu.addAction(Strings.MENU_TIME_TRIGGEREED_EVENT_ADD, Icons.ICON_MENU_ADD_LITERAL,
                ACTION_TIME_TRIGGERED_EVENT_ADD, this, SUBMENU_ADD, true);
        menu.removeAction(Strings.MENU_DELETE);
    }

    @Override
    public void removeRepresented(BaseView<?> baseView) {
       
    }

}
