package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.environmentmodel.TimeTriggeredEvent;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.environmentmodel.ui.views.TimeTriggeredEventView;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ITimeTriggeredEventViewHandler;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener.ActionEvent;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;


/**
 * The default handler for a {@link TimeTriggeredEventView}.
 * 
 * @author Franz
 * @author yhattab
 */
public class TimeTriggeredEventViewHandler extends BaseHandler 
    implements ITimeTriggeredEventViewHandler, ILinkedMenuListener {

    private static final String ACTION_TIME_TRIGGERED_EVENT_REMOVE = "view.TimeTriggeredEvent.remove";

    @Override
    public void removeEvent(TimeTriggeredEventView eventView) {
        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController()
            .removeTimeTriggeredEvent(eventView.getEvent());;
        
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            TimeTriggeredEventView eventView = (TimeTriggeredEventView) linkedMenu.getLinkedView();

            if (ACTION_TIME_TRIGGERED_EVENT_REMOVE.equals(actionCommand)) {
                removeEvent(eventView);
            }
        }
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((TimeTriggeredEventView) rectangle).getEvent();
    }

    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_TIME_TRIGGERED_EVENT_REMOVE, this, true);

        menu.setLinkInCorners(false);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        return null;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {

    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }
}
