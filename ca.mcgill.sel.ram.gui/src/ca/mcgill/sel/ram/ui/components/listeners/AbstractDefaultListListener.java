package ca.mcgill.sel.ram.ui.components.listeners;

import ca.mcgill.sel.ram.ui.components.RamListComponent;

/**
 * Default listener for objects wishing to listen to {@link RamListComponent}s.
 * The default behaviour for double-clicking and tap-and-holding on afterElements of the list is to perform nothing.
 * Sub-classes need at least to implement the {@link #elementSelected(RamListComponent, Object)} method.
 *
 * @author mschoettle
 *
 * @param <T> the type of the afterElements being selected.
 */
public abstract class AbstractDefaultListListener<T> implements RamListListener<T> {

    @Override
    public void elementDoubleClicked(RamListComponent<T> list, T element) {
        
    }

    @Override
    public void elementHeld(RamListComponent<T> list, T element) {
        
    }

}
