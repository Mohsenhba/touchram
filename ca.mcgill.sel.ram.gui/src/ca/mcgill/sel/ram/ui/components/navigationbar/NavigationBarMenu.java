package ca.mcgill.sel.ram.ui.components.navigationbar;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.NavigationMapping;
import ca.mcgill.sel.core.navigation.IntraLanguageMappingTree.ILMNode;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Filter;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;

/**
 * A menu containing a list of {@link NavigationBarMenuElement} and showing elements which can be tapped. It represents
 * a menu linked to a {@link NavigationBarSection} which allows to navigate to next sections not shown.
 * 
 * @author g.Nicolas
 *
 */
public class NavigationBarMenu extends RamRoundedRectangleComponent {

    private List<NavigationBarMenuElement<?>> elements;

    /**
     * Namer which allows to initialize the {@link NavigationBarMenu} with {@link NavigationBarMenuElement}.
     * 
     * @author g.Nicolas
     *
     * @param <T> - type of element related to the {@link NavigationBarSection} related to the menu.
     */
    public interface NavigatonBarNamer<T> {

        /**
         * Initialize the {@link NavigationBarMenu} with {@link NavigationBarMenuElement}.
         * 
         * @param menu - the menu to initialize related to {@link NavigationBarSection}
         * @param element - element related to {@link NavigationBarSection}
         */
        void initMenu(NavigationBarMenu menu, T element);

    }
    
    /**
     * Namer used to generically move through scenes within navbar. Should also be able to eecognize reuse.
     * @author ian
     *
     * @param <T> type of element within the {@link NavigationBarSection}
     */
    public interface NavigationBarNamerGeneric<T> extends NavigatonBarNamer<T>{
        void setReuse(boolean reuse);
    }
    
    /**
     * Namer allowing to initialize two different menus from a {@link NavigationBarMenuElement}.
     * 
     * @author andrea
     *
     * @param <T> - type of element the section will treat.
     */
    public interface NavigatonBarNamerRM<T> {
        
        /**
         * Initialize normal menu of a Section.
         * 
         * @param menu menus to be initialized.
         * @param element element related to specific section to whom the menus will be attached.
         */
        void initMenu(NavigationBarMenu menu, T element);
        
        /**
         * Initialize fan Out menu of a section.
         * 
         * @param menu to be initialized.
         * @param element related to specific fan out section search.
         */
        void initFanOutMenu(NavigationBarMenu menu, T element);
        
    }
    
    /**
     * Namer which will allow for the updating of menu elements when called from listeners.
     * @author ian
     *
     * @param <T> the type of element that will be present within the menu
     */
    public interface NavigationBarNamerFeatureSwitch<T> extends NavigatonBarNamer<T> {
        
        void initMenu(NavigationBarMenu menu, T element);
        
        void setFeature(COREFeature feature);
        
    }

    /**
     * Constructs a menu and created an empty {@link NavigationBarMenuElement} list.
     */
    NavigationBarMenu() {
        super(10);
        elements = new LinkedList<NavigationBarMenuElement<?>>();

        setLayout(new VerticalLayout());
        setNoStroke(false);
        setNoFill(false);
        setFillColor(Colors.NAVIGATION_BAR_MENU_COLOR);
    }
    
    public <T> void addMenuElementGeneric(String label, ILMNode node, NavigatonBarNamer<T> namer, 
            RamListListener<T> listener, NavigationMapping mapping) {
        NavigationBarMenuElement<T> elem = new NavigationBarMenuElement<T>(this, label, node, namer, listener,
                mapping, 1);
        int position = sortElements(label);
        elements.add(position, elem);
        addChild(position, elem);
        
    }

    /**
     * Adds a {@link NavigationBarMenuElement} in the menu. It's a general method to centralize the addMenuElement
     * behavior.
     * 
     * @param label - label of the {@link NavigationBarMenuElement}
     * @param data - {@link EObject} to construct a {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent} list.
     *            See
     *            {@link NavigationBarMenuElement}.
     * @param feature - {@link EStructuralFeature} to construct a
     *            {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent} list. See
     *            {@link NavigationBarMenuElement}.
     * @param type - {@link EClassifier} to construct a {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     *            list. See
     *            {@link NavigationBarMenuElement}.
     * @param list - list for a {@link NavigationBarMenuElement}
     * @param namer - namer to initialize a sub-menu when clicking an element of the {@link NavigationBarMenuElement}
     *            list
     * @param listener - listener when clicking an element of the {@link NavigationBarMenuElement} list
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    // CHECKSTYLE:IGNORE ParameterNumberCheck: general method which centralize the addMenuElement behavior.
    private <T> void addMenuElement(String label, EObject data, EStructuralFeature feature, EClassifier type,
            List<T> list, NavigatonBarNamer<T> namer, RamListListener<T> listener, Filter<T> filter) {
        NavigationBarMenuElement<T> elem =
                new NavigationBarMenuElement<T>(this, label, data, feature, type, list, namer, listener, filter);
        elements.add(elem);
        addChild(elem);
        
    }
    
    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, with a label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamListComponent}.
     * 
     * @param label - label heading the {@link NavigationBarMenuElement} list.
     * @param list - list of {T} elements for the {@link NavigationBarMenuElement}
     * @param listener - listener called when clicking an element of the {@link NavigationBarMenuElement} list
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(String label, List<T> list, RamListListener<T> listener, Filter<T> filter) {
        this.addMenuElement(label, null, null, null, list, null, listener, filter);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, with a label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamListComponent} which
     * contains sub-menus.
     * 
     * @param label - label heading the {@link NavigationBarMenuElement} list.
     * @param list - list of {T} elements for the {@link NavigationBarMenuElement}
     * @param namer - {@link NavigatonBarNamer} to initialize sub-menu when taping on an element of the list.
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(String label, List<T> list, NavigatonBarNamer<T> namer) {
        this.addMenuElement(label, null, null, null, list, namer, null, null);
    }
    /**
     * Returns true if the menu contains at least one element.
     * 
     * @return true if the menu contains at least one element. False otherwise.
     */
    public boolean containsElements() {
       
        return this.elements.size() > 0;
    }
    
    public boolean checkForDuplicateNode(ILMNode node, NavigationMapping ilm) {
        for (NavigationBarMenuElement<?> element : elements) {
            if (element.checkForDuplicateElement(node, ilm)) {
                return true;
            }
        }
        return false;
    }
    
    private int sortElements(String newElementName) {
        List<String> namesToSort = new ArrayList<String>();
        if (elements.size() == 0) {
            return 0;
        }
        for (NavigationBarMenuElement elem : elements) {
            namesToSort.add(elem.getNameElem().toUpperCase());
        }
        
        namesToSort.add(newElementName.toUpperCase());
        
        java.util.Collections.sort(namesToSort);
        
        int position = 0;
        
        for (String name : namesToSort) {
            if (name.equals(newElementName.toUpperCase())) {
                break;
            }
            position++;
        }
        return position;

        
    }
    
}
