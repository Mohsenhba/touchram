package ca.mcgill.sel.ram.ui.components.navigationbar;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.components.TransformSpace;
import org.mt4j.sceneManagement.Iscene;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.impl.ClassDiagramImpl;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.COREFeatureImpl;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.impl.NamedElementImpl;
import ca.mcgill.sel.ram.impl.OperationImpl;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSpacerComponent;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigatonBarNamer;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutHorizontallyCentered;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.impl.UseCaseModelImpl;
import processing.core.PImage;

/**
 * A section in the {@link NavigationBar}. It represents an eObject in the path of the {@link NavigationBar}. When it's
 * shown in the {@link NavigationBar} it means that the related eObject is linked to the previous and next sections.
 * It can contain an icon and a {@link NavigationBarMenu} which allows to navigate to next sections not shown.
 * When a menu is defined, an arrow will be added on the right of the section in order to show/hide it.
 *
 * @author Andrea
 */
public class NavigationBarSection extends RamRectangleComponent {


    private RamButton expandButton;
    private RamButton filteredSwitchUpButton;
    private RamButton label;
    private RamRectangleComponent headerContainer;
    private RamRectangleComponent labelContainer;
    private RamRectangleComponent topLabelContainer;
    private RamRectangleComponent bottomLabelContainer;
    private Object typeOfSection;
    private EObject sectionObject;
    private boolean menuShown;
    private GraphicalUpdater graphicalUpdater;
    private RamImageComponent reuseSectionElement;

    /**
     * Constructs a section with or without icon or menu in function of their value.
     * 
     * @param icon - icon in the section. Null to not add an icon.
     * @param label - label in the section.
     * @param namer - namer which constructs the menu. Null to not add a menu.
     * @param eObject - related eObject of the section.
     * @param <T> - type of the related eObject.
     */
    <T> NavigationBarSection(PImage icon, String label, final NavigatonBarNamer<T> namer, final T eObject) {
        super(new VerticalLayout());

        this.menuShown = false;
        
        if (eObject instanceof COREImpactNode) {
            this.label = new RamButton((COREImpactNode) eObject);
        } else {
            this.label = new RamButton(label);
        }
        this.label.setAutoMaximizes(false);
        this.label.setAutoMinimizes(false);

        setPickable(false);
        this.typeOfSection = eObject.getClass();
        this.sectionObject = (EObject) eObject;
        headerContainer = new RamRectangleComponent();
        headerContainer.setPickable(false);
        headerContainer.setLayout(new VerticalLayout(-9.0f));
        topLabelContainer = new RamRectangleComponent();
        topLabelContainer.setPickable(false);
        topLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        labelContainer = new RamRectangleComponent();
        labelContainer.setPickable(false);
        labelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        bottomLabelContainer = new RamRectangleComponent();
        bottomLabelContainer.setPickable(false);
        bottomLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        addChild(headerContainer);

        if (eObject.getClass().equals(COREFeatureImpl.class)) {
            this.label = new RamButton("<" + label + ">");
            createFeatureSection(namer, eObject);
        } else {
            createGenericSection(namer, eObject);
        }
    }
    
    <T> NavigationBarSection(final NavigatonBarNamer<T> namer, EObject reuseObject) { 
        super(new VerticalLayout());
        this.typeOfSection = reuseObject.getClass();
        this.sectionObject = reuseObject;
        
        reuseSectionElement = new RamImageComponent(Icons.ICON_REUSE_COLLAPSE, 
                MTColor.WHITE);
        reuseSectionElement.setMinimumSize(50, 50);
        reuseSectionElement.setMaximumSize(50, 50);
        reuseSectionElement.setBufferSize(Cardinal.WEST, 10);
        reuseSectionElement.setBufferSize(Cardinal.EAST, 10);
        
        headerContainer = new RamRectangleComponent();
        headerContainer.setPickable(false);
        headerContainer.setLayout(new VerticalLayout(-9.0f));
        
        headerContainer.addChild(reuseSectionElement);
        
        bottomLabelContainer = new RamRectangleComponent();
        bottomLabelContainer.setPickable(false);
        bottomLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());
        
        
        addExpandableButton(namer, null);
        headerContainer.addChild(bottomLabelContainer);
        
        addChild(headerContainer);
        
    }
    
    
    /**
     * This should be the generic version of creating a section for the navbar. It should automatically return to the 
     * scene of the original model if the navbar section is pressed.
     * 
     * @param namer for initializing the menu
     * @param modelObject The actual Object of the section
     * @param <T> is the TYPE of the object
     */
    private <T> void createGenericSection(final NavigatonBarNamer<T> namer, final T modelObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);
        headerContainer.addChild(topLabelContainer);
        
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));
        
        EObject object = (EObject) modelObject;
        
        EStructuralFeature feature = null;
        if (modelObject.getClass().equals(ClassDiagramImpl.class)) {
            feature = CdmPackage.Literals.NAMED_ELEMENT__NAME;
        } else if (modelObject.getClass().equals(UseCaseModelImpl.class)) {
            feature = UcPackage.Literals.NAMED_ELEMENT__NAME;
        } else if (modelObject instanceof NamedElementImpl) {
            feature = RamPackage.Literals.NAMED_ELEMENT__NAME;
        } else if (modelObject instanceof NamedElement) {
            feature = UcPackage.Literals.NAMED_ELEMENT__NAME;
        } else {
            feature = CorePackage.Literals.CORE_NAMED_ELEMENT__NAME;
        }
        
        TextView objName = new TextView(object, feature);
        
        objName.setFont(Fonts.DEFAULT_FONT_MENU);
        objName.setMaximumHeight(38.0f);
        objName.registerTapProcessor(HandlerFactory.INSTANCE.getTextViewHandler());
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(object);
        if (graphicalUpdater != null) {
            graphicalUpdater.addGUListener(object, objName);
        }
        

        this.label.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                EObject eobject = sectionObject;
                if (eobject == null) {
                    return;
                }
                Iscene sectionScene = SceneCreationAndChangeFactory.getFactory().getSceneFromModel(eobject);
                if (sectionScene == null) {
                    return;
                }
                boolean isSaveNeeded = EMFEditUtil.getCommandStack(eobject).isSaveNeeded();
                if (isSaveNeeded) {
                    final List<RamAbstractScene<?>> unsavedScenes = new ArrayList<RamAbstractScene<?>>();
                    unsavedScenes.add((RamAbstractScene<?>) sectionScene);
                    RamApp.getApplication().invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            RamApp.getApplication().confirmExit(unsavedScenes);
                        }
                    });
                }
                if (sectionObject instanceof COREConcern) {
                    getParentOfType(NavigationBar.class).popSection(typeOfSection, (EObject) modelObject);
                } else {
                    getParentOfType(NavigationBar.class).popSection(sectionObject);
                }
                SceneCreationAndChangeFactory.getFactory().changeSceneAndUpdate(sectionScene);
            }
        });
        if (!(modelObject instanceof COREConcern) && !(modelObject instanceof COREImpactModel)) {
            this.label.removeAllChildren();
            this.label.addChild(objName);
        }
        
        labelContainer.addChild(label);
        headerContainer.addChild(labelContainer);
        addExpandableButton(namer, modelObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a section in the navigation bar specifically for a Feature element.
     * 
     * @param namer for initializing the menu
     * @param eObject The actual Object of the section
     * @param <T> is the TYPE of the object, which is a COREFeatureImpl
     */
    private <T> void createFeatureSection(final NavigatonBarNamer<T> namer, final T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);
        headerContainer.addChild(topLabelContainer);

        Vector3D p = this.label.getPosition(TransformSpace.GLOBAL);
        p.y += 30;
        this.label.setPositionGlobal(p);
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));
        
        labelContainer.addChild(this.label);
        
        Vector3D pos = labelContainer.getPosition(TransformSpace.GLOBAL);
        pos.y += 20;
        labelContainer.setPositionGlobal(pos);
        headerContainer.addChild(labelContainer);

        addExpandableButton(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates the menu, call the Namer to initialize it and call the menu manager in {@link NavigationBar} to position
     * it.
     * 
     * @param namer - namer which initialize the menu.
     * @param eObject - related eObject of the section.
     * @param <T> - type of the related eObject.
     */
    private <T> void initMenu(NavigatonBarNamer<T> namer, T eObject) {
        NavigationBarMenu menu = new NavigationBarMenu();
        namer.initMenu(menu, eObject);
        Vector3D menuPosition = expandButton.getPosition(TransformSpace.GLOBAL);
        menuPosition.x += expandButton.getWidthXY(TransformSpace.GLOBAL);
        getParentOfType(NavigationBar.class).openMenu(this, menu, menuPosition);
    }

    /**
     * Updates the expand button icon in function of the given boolean. When the parameter is true the icon will be the
     * expand one. Otherwise it will be the collapse one. It also updated the class state to menu shown.
     * 
     * @param expand - true if you want the expand icon, false for the collapse icon.
     */
    public void updateExpandButton(boolean expand) {
        if (typeOfSection.equals(OperationImpl.class)) {
            PImage expandIcon = expand ? Icons.ICON_FILTERED_SWITCH_EXPANDED : Icons.ICON_FILTERED_SWITCH_COLLAPSE;
            expandButton.getIconImage().setTexture(expandIcon);
            menuShown = expand;
        } else {
            PImage expandIcon = expand ? Icons.ICON_NAVIGATION_EXPAND : Icons.ICON_NAVIGATION_COLLAPSE;
            expandButton.getIconImage().setTexture(expandIcon);
            menuShown = expand;
        }
    }

    /**
     * Updates the filteredSwitchUpButton whenever from a different section and expand button is clicked/tapped so that
     * the UP arrow returns to where it has to be and the menu closes.
     * 
     * @param expand toggle the second menu of a RM section
     */
    public void updateUpArrowSwitch(boolean expand) {
        PImage expandIcon = expand ? Icons.ICON_FILTERED_SWITCH_EXPANDED : Icons.ICON_FILTERED_SWITCH_COLLAPSE;
        
        if (filteredSwitchUpButton != null) {
            filteredSwitchUpButton.setNoStroke(true);
            filteredSwitchUpButton.getIconImage().setTexture(expandIcon);
        }
        
        menuShown = expand;
    }

    /**
     * Add an expandable button in the header.
     * 
     * @param namer - namer which constructs the menu. Null to not add a menu.
     * @param eObject - related eObject of the section.
     * @param <T> - type of the related eObject.
     */
    public <T> void addExpandableButton(final NavigatonBarNamer<T> namer, final T eObject) {
        if (this.expandButton == null) {
            RamImageComponent expandImage = new RamImageComponent(Icons.ICON_NAVIGATION_COLLAPSE, MTColor.WHITE);
            expandImage.setSizeLocal(32, 32);
            this.expandButton = new RamButton(expandImage);
            this.expandButton.setAutoMaximizes(false);
            this.expandButton.setAutoMinimizes(false);

            // EXPAND BUTTON WILL ALWAYS BE IN THE MIDDLE POINT OF THE SECTION OF THE MENU
            if (labelContainer != null) {
                expandButton.setPositionRelativeToOther(labelContainer, labelContainer.getCenterPointGlobal());
            } else {
                expandButton.setPositionRelativeToOther(reuseSectionElement, reuseSectionElement.
                        getCenterPointGlobal());
            }
           

            if (this.filteredSwitchUpButton != null) {
                filteredSwitchUpButton
                        .setPositionRelativeToOther(labelContainer, labelContainer.getCenterPointGlobal());
            }

            if (namer != null) {
                this.expandButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent event) {
                        if (!menuShown) {
                            initMenu(namer, eObject);
                        } else {
                            getParentOfType(NavigationBar.class).closeMenu();
                        }

                    }

                });
            }

            bottomLabelContainer.addChild(this.expandButton);
        }
    }

    /**
     * Removes the expand button if it exists and if there is no menu to show.
     * 
     * @return needed for removing the within notation fullstop
     */
    public boolean removeUnusedExpandButton() {
        boolean value;
        
        if (this.expandButton != null && !this.expandButton.hasListeners()) {
            headerContainer.removeChild(this.expandButton);
            
            if (filteredSwitchUpButton != null) {
                headerContainer.removeChild(this.filteredSwitchUpButton);
            }
            this.expandButton.destroy();
            this.expandButton = null;
            if (filteredSwitchUpButton != null) {
                this.filteredSwitchUpButton.destroy();
                this.filteredSwitchUpButton = null;
            }
            value = true;
        } else if (this.filteredSwitchUpButton != null) {
            if (!this.filteredSwitchUpButton.hasListeners()) {
                value = true;
            }
            value = false;
        } else {
            value = false;
        }
        return value;
    }

    /**
     * Method needed for retrieving the type of section inside the NavigationBar class.
     * 
     * @return type of section
     */
    public Object retrieveTypeOfSection() {
        return typeOfSection;
    }
    
    /**
     * Get the base object related to the section.
     * @return the object which is associated with the section.
     */
    public EObject getSectionEObject() {
        return sectionObject;
    }
    /**
     * Method to toggle the removal of arrow from a section that does not have any successor.
     */
    public void hideArrows() {
        bottomLabelContainer.removeAllChildren();
        RamSpacerComponent fakeArrow = new RamSpacerComponent(0.0f, 32.0f);
        bottomLabelContainer.addChild(fakeArrow);
    }

}
