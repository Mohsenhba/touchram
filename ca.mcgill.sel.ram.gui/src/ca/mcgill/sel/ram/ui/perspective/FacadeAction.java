package ca.mcgill.sel.ram.ui.perspective;

import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.ram.ui.perspective.controller.PerspectiveException;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;

/**
 * This class contains operations which act as facade to call actual language
 * actions in a perspective.
 * 
 * @author Hyacinth Ali
 *
 */
public final class FacadeAction {

	/**
	 * Facade action which creates an attribute, as well as other model elements
	 * which need to be mapped with an attribute element.
	 * 
	 * @param metaclass - metaclass of the element to create.
	 * @param mappingType - language element mapping of the metaclass.
	 * @param perspective
	 * @param roleName - the role name of the model to contain the new element.
	 * @param otherOwner - the container of the element to be mapped with the
	 * new element.
	 * @param name
	 * @param objectType
	 * @return the new element.
	 * @throws PerspectiveException
	 */
	public static EObject createAttributeAndOthers(EObject metaclass, CORELanguageElementMapping mappingType,
			COREPerspective perspective, String roleName, EObject otherOwner, String name, ObjectType objectType)
			throws PerspectiveException {

		EObject newElement = null;

		if (metaclass.equals(CdmPackage.eINSTANCE.getAttribute())) {
			newElement = ClassDiagramFacade.createAttribute(perspective, otherOwner, roleName, name, objectType);
		}

		return newElement;
	}

	/**
	 * Facade action which creates a class, as well as other model elements
	 * which need to be mapped with a class element. This covers both directly
	 * and indirectly corresponding elements with a class element.
	 * 
	 * @param perspective - the perspectibve of the model element.
	 * @param metaclass - metaclass of the element to create.
	 * @param otherOwner - the container of a model element which is to be
	 * mapped with the new element.
	 * @param roleName - role name of the model to contain the new element.
	 * @param name
	 * @param dataType
	 * @param x
	 * @param y
	 * @return the new created model element.
	 */
	public static EObject createClassAndOthers(COREPerspective perspective, EObject metaclass, EObject otherOwner,
			String roleName, String name, boolean dataType, float x, float y) {

		EObject newElement = null;

		if (metaclass.equals(UcPackage.eINSTANCE.getActor())) {
			newElement = UseCaseFacade.createActor(perspective, roleName, otherOwner, name, x, y);
		} else if (metaclass.equals(CdmPackage.eINSTANCE.getClass_())) {
			newElement = ClassDiagramFacade.createClass(perspective, otherOwner, roleName, name, dataType, x, y);
		}

		return newElement;
	}

	/**
	 * Facade action which creates a class, as well as other model elements
	 * which need to be mapped with a class element. This covers both directly
	 * and indirectly corresponding elements with an actor element.
	 * 
	 * @param perspective
	 * @param metaclass - metaclass of the element to create.
	 * @param otherOwner - the container of a model element which is to be
	 * mapped with the new element.
	 * @param roleName - role name of the model to contain the new element.
	 * @param name - name of the element to create.
	 * @param x
	 * @param y
	 * @return the newly created corresponding model element.
	 */
	public static EObject createUseCaseActorAndOthers(COREPerspective perspective, EObject metaclass,
			EObject otherOwner, String roleName, String name, float x, float y) {

		EObject newElement = null;
		// map parameters to create class and then call the language action.
		// List<Object> parameters = new ArrayList<Object>();
		if (metaclass.equals(UcPackage.eINSTANCE.getActor())) {
			newElement = UseCaseFacade.createActor(perspective, roleName, otherOwner, name, x, y);
		} else if (metaclass.equals(CdmPackage.eINSTANCE.getClass_())) {
			// the user can be asked to provide the data type value
			newElement = ClassDiagramFacade.createClass(perspective, otherOwner, roleName, name, false, x, y);
		}

		return newElement;
	}

	/**
	 * A facade action to delete model element.
	 * 
	 * @param element - the element to delete
	 */
	public static void deleteModelElement(EObject element) {

		if (element instanceof Classifier) {
			ControllerFactory.INSTANCE.getClassDiagramController().removeClassifier((Classifier) element);
		} else if (element instanceof Actor) {
			UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController().removeActor((Actor) element);
		} else if (element instanceof Attribute) {
			ControllerFactory.INSTANCE.getClassController().removeAttribute((Attribute) element);
		}

	}

	/**
	 * This is a helper method which retrieves the corresponding container of an
	 * element to create.
	 * 
	 * @param perspective
	 * @param currentOwner
	 * @param otherRole - role name of the corresponding model in the
	 * perspective.
	 * @return the container of the element to create.
	 */
	public static EObject getOwner(COREPerspective perspective, EObject currentOwner, String otherRole) {
		EObject ownerOther = null;

		List<COREModelElementMapping> ownerMappings = COREPerspectiveUtil.INSTANCE.getMappings(currentOwner);
		outerloop: for (COREModelElementMapping mapping : ownerMappings) {
			ownerOther = COREPerspectiveUtil.INSTANCE.getOtherElement(mapping, currentOwner);
			CORELanguageElementMapping mappingType = COREPerspectiveUtil.INSTANCE.getMappingType(perspective, mapping);
			for (MappingEnd mappingEnd : mappingType.getMappingEnds()) {
				if (mappingEnd.getRoleName().equals(otherRole)) {
					ownerOther = COREPerspectiveUtil.INSTANCE.getOtherElement(mapping, currentOwner);
					break outerloop;
				}
			}
		}

		return ownerOther;
	}
	
	/**
	 * This is a helper method which retrieves the corresponding container of an
	 * element to create.
	 * @param perspective
	 * @param scene -  the scene of the models
	 * @param currentOwner
	 * @param otherRole
	 * @return the container of the element to create.
	 */
	public static EObject getOwner(COREPerspective perspective, COREScene scene, EObject currentOwner, String otherRole) {
		EObject ownerOther = null;

		List<COREModelElementMapping> ownerMappings = COREPerspectiveUtil.INSTANCE.getMappings(currentOwner, scene);
		outerloop: for (COREModelElementMapping mapping : ownerMappings) {
			ownerOther = COREPerspectiveUtil.INSTANCE.getOtherElement(mapping, currentOwner);
			CORELanguageElementMapping mappingType = COREPerspectiveUtil.INSTANCE.getMappingType(perspective, mapping);
			for (MappingEnd mappingEnd : mappingType.getMappingEnds()) {
				if (mappingEnd.getRoleName().equals(otherRole)) {
					ownerOther = COREPerspectiveUtil.INSTANCE.getOtherElement(mapping, currentOwner);
					break outerloop;
				}
			}
		}

		return ownerOther;
	}

	public static EObject createOtherElement(COREPerspective perspective, EObject metaclass, String toCreateRoleName,
			HashMap<Object, Object> parameters) {
		EObject newElement = null;

		if (metaclass.equals(UcPackage.eINSTANCE.getActor())) {
			newElement = UseCaseFacade.createActor(perspective, toCreateRoleName, parameters);
		} else if (metaclass.equals(CdmPackage.eINSTANCE.getClass_())) {
			newElement = ClassDiagramFacade.createClass(perspective, toCreateRoleName, parameters);
		}

		return newElement;
	}

}
