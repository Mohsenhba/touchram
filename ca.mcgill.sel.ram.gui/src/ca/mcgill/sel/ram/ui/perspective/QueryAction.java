package ca.mcgill.sel.ram.ui.perspective;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;

public class QueryAction {

	public static final QueryAction INSTANCE = new QueryAction();

	private boolean createOtherElement = true;
	private boolean userOption = false;
	private int numberOfMapping;
	private boolean isCreateMapping;

	QueryAction() {

	}

	public int getNumberOfMapping() {
		return numberOfMapping;
	}

	public void setNumberOfMapping(int numberOfMapping) {
		this.numberOfMapping = numberOfMapping;
	}

	public void setCreateMapping(boolean isCreateMapping) {
		this.isCreateMapping = isCreateMapping;
	}

	/**
	 * Check either to create other element. This should after asking for user's
	 * opinion.
	 * 
	 * @return the createOtherElement
	 */
	public boolean isCreateOtherElement() {
		return createOtherElement;
	}

	/**
	 * Sets the value of either creating other model element based on the input
	 * of the user.
	 * 
	 * @param createOtherElement the createOtherElement to set
	 */
	public void setCreateOtherElement(boolean createOtherElement) {
		this.createOtherElement = createOtherElement;
	}

	/**
	 * @return the userOption
	 */
	public boolean isUserOption() {
		return userOption;
	}

	/**
	 * @param userOption the userOption to set
	 */
	public void setUserOption(boolean userOption) {
		this.userOption = userOption;
	}

	public boolean isCreateMapping() {
		// TODO: TouchCORE, currently, does not support interactive modelling.
		return isCreateMapping;
	}

	/**
	 * Finds a corresponding element by using identifiable properties, e.g., a
	 * corresponding class with the same name.
	 *
	 * @param element of interest
	 * @param roleName
	 * @return correspondingElement
	 */
	@SuppressWarnings("static-method")
	private EObject findCorrespondingClassByName(EObject element, String roleName) {
		EObject correspondingElement = null;
		COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(element);
		if (artefact != null) {
			COREScene scene = artefact.getScene();
			List<COREArtefact> artefacts = scene.getArtefacts().get(roleName);
			if (artefacts != null) {
				for (COREArtefact a : artefacts) {
					if (a instanceof COREExternalArtefact) {
						COREExternalArtefact extArtefact = (COREExternalArtefact) a;
						EObject rootModelElement = extArtefact.getRootModelElement();
						if (element instanceof Class) {
							if (rootModelElement instanceof ClassDiagram) {
								ClassDiagram model = (ClassDiagram) rootModelElement;
								for (Classifier c : model.getClasses()) {
									if (c instanceof Class && c.getName().equals(((Class) element).getName())) {
										correspondingElement = c;
									}
								}
							}
						}
					}
				}
			}
		}
		return correspondingElement;
	}

//	public EObject findCorrespondingElementByName(EObject metaclass, EObject currentElement, List<EObject> elements) {
//		EObject correspondingElement = null;
//		String currentName = null;
//		if (currentElement instanceof Class) {
//			Class currentClazz = (Class) currentElement;
//			currentName = currentClazz.getName();
//		}
//		for (EObject element : elements) {
//			if (element instanceof Class) {
//				Class clazz = (Class) element;
//				String name = clazz.getName();
//				if (name.equals(currentName) && !element.equals(currentElement)) {
//					correspondingElement = element;
//				}
//			}
//		}
//		return correspondingElement;
//	}

	/**
	 * Finds a corresponding element by using identifiable properties, e.g., a
	 * corresponding class with the same name.
	 * 
	 * @param scene - the scene of the model
	 * @param element - the known primary element
	 * @param roleName - language role of the corresponding model, i.e., the
	 * model that may contain the corresponding element.
	 * 
	 * @return correspondingElement
	 */
	public EObject findCorrespondingElementByName(COREScene scene, EObject element, String roleName) {
		EObject correspondingElement = null;

		if (scene != null) {
			List<COREArtefact> artefacts = scene.getArtefacts().get(roleName);
			if (artefacts != null) {
				outer_loop: for (COREArtefact a : artefacts) {
					if (a instanceof COREExternalArtefact) {
						COREExternalArtefact extArtefact = (COREExternalArtefact) a;
						EObject rootModelElement = extArtefact.getRootModelElement();
						// get all contents of the root model element
						EList<EObject> rootContents = rootModelElement.eContents();
						Iterator<EObject> contents = rootContents.iterator();
						while (contents.hasNext()) {
							EObject rootContent = contents.next();
							// assumption, only instances of EClass are
							// supported
							if (!(rootContent.eClass() instanceof EClass)) {
								continue;
							}
							EClass contentEClass = rootContent.eClass();
							for (EAttribute attr : contentEClass.getEAllAttributes()) {
								// CHALLENGE: EMF can only, generically, return
								// the values of direct attributes.
								// Hence, the values of inherited attributes
								// can't be accessed by using reflection
								// This can be solved if the language metamodel
								// has a common metaclass (NamedElement).
								if (attr.getName().equals("name")) {
									String rootContentName = (String) rootContent
											.eGet(CdmPackage.eINSTANCE.getNamedElement_Name());
									String elementName = (String) element
											.eGet(CdmPackage.eINSTANCE.getNamedElement_Name());
									if (rootContentName.equals(elementName)) {
										correspondingElement = rootContent;
										break outer_loop;
									}
								}
							}
						}
					}
				}
			}
		}

		return correspondingElement;
	}

	/**
	 * This method queries the existing of the perspective to find a corresponding element, i.e.,
	 * an element which have the required feature and the value to be mapped with the current element.
	 * @param scene - the scene which groups models for the perspective.
	 * @param mappingType
	 * @param currentLanguageElement - the language element of the current model element.
	 * @param currentModelElement
	 * @param currentRoleName
	 * @param otherRoleName - the role name of the model that may contain the expected element.
	 * @return
	 */
	public EObject findCorrespondingElement(COREScene scene, CORELanguageElementMapping mappingType,
			EObject currentLanguageElement, EObject currentModelElement, String currentRoleName, String otherRoleName) {
		
		EObject correspondingElement = null;
		
		List<COREArtefact> artefacts = scene.getArtefacts().get(otherRoleName);
		outer_loop:for (COREArtefact a : artefacts) {
			if (a instanceof COREExternalArtefact) {
				COREExternalArtefact extArtefact = (COREExternalArtefact) a;
				EObject otherModel = extArtefact.getRootModelElement();
				if (otherModel == null) {
					return null;
				}
				EObject toLanguageElement = COREPerspectiveUtil.INSTANCE
						.getOtherLanguageElements(mappingType, currentLanguageElement, currentRoleName).get(0);

				// get nested mapping types
				List<CORELanguageElementMapping> nestedMappings = mappingType.getNestedMappings();

				// iterate nested mapping types to get the type with mathMaker (true)
				EObject fromNestedLanguageElement = null;
				EObject toNestedLanguageElement = null;
				for (CORELanguageElementMapping nestedType : nestedMappings) {
					if (nestedType.isMatchMaker()) {
						for (MappingEnd end : nestedType.getMappingEnds()) {
							if (end.getRoleName().equals(currentRoleName)) {
								fromNestedLanguageElement = end.getLanguageElement().getLanguageElement();
							} else if (end.getRoleName().equals(otherRoleName)) {
								toNestedLanguageElement = end.getLanguageElement().getLanguageElement();
							}
						}
						// There can only be one matching nested mapping in a mapping type.
					}
				}
				// For now, all the language elements in perspectives are instances of
				// EClass
				// all nested language elements are instances of EStructuralFeature.

				// get the from feature value
				Object fromFeatureValue = currentModelElement.eGet((EStructuralFeature) fromNestedLanguageElement, true);
				Collection<EObject> toModelElements = EcoreUtil.getObjectsByType(otherModel.eContents(), (EClass) toLanguageElement);
				for (EObject toModelElement : toModelElements) {
					try {
						Object toFeatureValue = toModelElement.eGet((EStructuralFeature) toNestedLanguageElement, true);
						if (fromFeatureValue.equals(toFeatureValue)) {
							correspondingElement = toModelElement;
							break outer_loop;
						}
					} catch (IllegalArgumentException exception) {
						// The language element doesn't have the targeted feature, ignoe
					}

				}
			}
			
		}
		
		return correspondingElement;
	}
	
	/**
	 * This method queries the existing of the perspective to find a corresponding element, i.e.,
	 * an element which have the required feature and the value to be mapped with the current element.
	 * @param scene - the scene which groups models for the perspective.
	 * @param mappingType
	 * @param currentLanguageElement - the language element of the current model element.
	 * @param currentModelElement
	 * @param currentRoleName
	 * @param otherRoleName - the role name of the model that may contain the expected element.
	 * @return
	 */
	public List<EObject> findCorrespondingElements(COREScene scene, CORELanguageElementMapping mappingType,
			EObject currentLanguageElement, EObject currentModelElement, String currentRoleName, String otherRoleName) {
		
		List<EObject> correspondingElements = new ArrayList<>();
		
		List<COREArtefact> artefacts = scene.getArtefacts().get(otherRoleName);
		for (COREArtefact a : artefacts) {
			if (a instanceof COREExternalArtefact) {
				COREExternalArtefact extArtefact = (COREExternalArtefact) a;
				EObject otherModel = extArtefact.getRootModelElement();
				EObject toLanguageElement = COREPerspectiveUtil.INSTANCE
						.getOtherLanguageElements(mappingType, currentLanguageElement, currentRoleName).get(0);

				// get nested mapping types
				List<CORELanguageElementMapping> nestedMappings = mappingType.getNestedMappings();

				// iterate nested mapping types to get the type with mathMaker (true)
				EObject fromNestedLanguageElement = null;
				EObject toNestedLanguageElement = null;
				for (CORELanguageElementMapping nestedType : nestedMappings) {
					if (nestedType.isMatchMaker()) {
						for (MappingEnd end : nestedType.getMappingEnds()) {
							if (end.getRoleName().equals(currentRoleName)) {
								fromNestedLanguageElement = end.getLanguageElement().getLanguageElement();
							} else if (end.getRoleName().equals(otherRoleName)) {
								toNestedLanguageElement = end.getLanguageElement().getLanguageElement();
							}
						}
						// There can only be one matching nested mapping in a mapping type.
					}
				}
				// For now, all the language elements in perspectives are instances of
				// EClass
				// all nested language elements are instances of EStructuralFeature.

				// get the from feature value
				Object fromFeatureValue = currentModelElement.eGet((EStructuralFeature) fromNestedLanguageElement, true);
				Collection<EObject> toModelElements = EcoreUtil.getObjectsByType(otherModel.eContents(), (EClass) toLanguageElement);
				for (EObject toModelElement : toModelElements) {
					try {
						Object toFeatureValue = toModelElement.eGet((EStructuralFeature) toNestedLanguageElement, true);
						if (fromFeatureValue.equals(toFeatureValue)) {
							correspondingElements.add(toModelElement);
						}
					} catch (IllegalArgumentException exception) {
						// The language element doesn't have the targeted feature, ignoe
					}

				}
			}
			
		}
		
		return correspondingElements;
	}

	public int askNumberOfMappings() {
		// TODO currently, TouchCORE doesn't support interactive modeling.
		return numberOfMapping;
	}

	public int askNumberOfMappingsAtLeastOne() {
		// TODO currently, TouchCORE doesn't support interactive modeling.
		return numberOfMapping;
	}

}
