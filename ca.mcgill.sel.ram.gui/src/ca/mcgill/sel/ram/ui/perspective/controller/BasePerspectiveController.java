package ca.mcgill.sel.ram.ui.perspective.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.FeatureController;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.MappingDetail;
import ca.mcgill.sel.core.perspective.MappingType;
import ca.mcgill.sel.core.perspective.TemplateType;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.LanguageConstant;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.utils.BasicActionsUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.util.RAMModelUtil;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.util.UcModelUtil;

/**
 * This class handles common perspective actions which include user create mapping and model elements updates.
 * 
 * @author Hyacinth Ali
 *
 */
public final class BasePerspectiveController {

    /**
     * The action to add a new attribute.
     */
    public static final String CREATE_MAPPING = "view.perspective.create.mapping";

    /**
     * The singleton instance of this {@link BasePerspectiveController}.
     */
    // public static final PerspectiveInternalAction INSTANCE = new PerspectiveInternalAction();

    public BasePerspectiveController() {

    }

    /**
     * Handles the request to create a new design model and associate it to a feature.
     * 
     * @param concern the concern to add it to
     * @param feature the feature being realized by the design model
     * @param perspective the of the language {@link COREExternalLanguage}
     * @param roleName the name of the role the model plays in the perspective that should be created
     * @return exterternalModel
     */
    public static EObject createNewDesignModel(COREConcern concern, COREScene scene, COREFeature feature,
            COREPerspective perspective, String roleName) {

        final EObject externalModel;
        
        CORELanguage lang = perspective.getLanguages().get(roleName);
        switch (lang.getName()) {

            case LanguageConstant.RAM_LANGUAGE_NAME:
                externalModel = (Aspect) RAMModelUtil.createAspect(feature.getName(), concern);
                break;
            case LanguageConstant.CLASS_DIAGRAM_LANGUAGE_NAME:
                externalModel = CdmModelUtil.createClassDiagram(feature.getName(), concern);
                break;

            case LanguageConstant.UC_LANGUAGE_NAME:
                externalModel = UcModelUtil.createUseCaseDiagram(feature.getName());
                break;

            default:
                externalModel = null;
        }

        FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
        controller.addModelAndAssociateWithScene(concern, scene, feature, perspective, roleName, externalModel,
                feature.getName());
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(externalModel);

        try {
            COREPerspectiveUtil.INSTANCE.saveModel(concern, roleName, artefact);
        } catch (IOException e) {
            RamApp.getActiveScene()
                    .displayPopup(Strings.POPUP_ERROR_FILE_SAVE + e.getMessage(), PopupType.ERROR);
        }

        return externalModel;

    }

    /**
     * The method creates a mapping between two model elements. This action is often initiated by a user from the 
     * view.
     * 
     * @author Hyacinth Ali
     * @param perspective - the perspective which the type of the mapping to be created.
     * @param currentRole - the role name of the currently displayed model.
     * @param fromModelElement - from model element to be mapped.
     * @param selectorLocation - the location of the from element in the view.
     */
    public static void createMapping(COREPerspective perspective, String currentRole, EObject fromModelElement,
            Vector3D selectorLocation) throws PerspectiveException {

        List<EObject> mappedDiagrams = new ArrayList<EObject>();
        // for now, only elements that are contained in the root element are supported.
        EObject fromOwner = fromModelElement.eContainer();
        List<COREModelElementMapping> fromOnwerMappings = COREPerspectiveUtil.INSTANCE.getMappings(fromOwner);
        for (COREModelElementMapping mapping : fromOnwerMappings) {
            List<EObject> others = COREPerspectiveUtil.INSTANCE.getOtherElements(mapping, fromOwner);
            mappedDiagrams.addAll(others);
        }

        RamSelectorComponent<EObject> selector;
        selector = new RamSelectorComponent<EObject>(new ArrayList<EObject>(mappedDiagrams));

        RamAbstractScene<?> scene = RamApp.getActiveScene();

        scene.addComponent(selector, selectorLocation);

        selector.registerListener(new AbstractDefaultRamSelectorListener<EObject>() {

            @Override
            public void elementSelected(RamSelectorComponent<EObject> selector, EObject selectedModel) {
                modelElementsMapping(perspective, currentRole, fromModelElement, selectedModel, selectorLocation);
                selector.destroy();
            }
        });
    }
    
    /**
     * Establishes mapping between two model elements.
     * @param perspective
     * @param currentRole
     * @param fromModelElement
     * @param selectedModel
     * @param selectorLocation
     */
    private static void modelElementsMapping(COREPerspective perspective, String currentRole, EObject fromModelElement,
            EObject selectedModel, Vector3D selectorLocation) throws PerspectiveException {
        
        // the role name of the to model element.
        // list to avoid "Local variable toRoleName defined in an enclosing scope must be final or effectively final"
        List<String> toRoleNames = new ArrayList<String>();
        
        // get the language name of the selected model.
        String languageName = COREArtefactUtil.getReferencingExternalArtefact(selectedModel).getLanguageName();
        EMap<String, CORELanguage> languages = perspective.getLanguages();
        for (String roleName : languages.keySet()) {
            CORELanguage language = languages.get(roleName);
            if (language instanceof COREExternalLanguage) {
                COREExternalLanguage extLanguage = (COREExternalLanguage) language;
                if (extLanguage.getName().equals(languageName) && !roleName.equals(currentRole)) {
                    toRoleNames.add(roleName);
                    break;
                }
            }
        }

        // get all the mapping types of the from language element, i.e., the metaclass of the from model element.
        List<CORELanguageElementMapping> mappingTypes = COREPerspectiveUtil.INSTANCE.getMappingTypes(perspective,
                fromModelElement.eClass(), currentRole);

        List<EObject> toElements = new ArrayList<EObject>();
        if (selectedModel instanceof ClassDiagram) {
            ClassDiagram cd = (ClassDiagram) selectedModel;
            toElements.addAll(cd.getClasses());
        } else if (selectedModel instanceof UseCaseModel) {
            UseCaseModel ucd = (UseCaseModel) selectedModel;
            toElements.addAll(ucd.getActors());
        }
        
        RamSelectorComponent<EObject> selector;
        selector = new RamSelectorComponent<EObject>(new ArrayList<EObject>(toElements));

        RamAbstractScene<?> scene = RamApp.getActiveScene();

        scene.addComponent(selector, selectorLocation);

        selector.registerListener(new AbstractDefaultRamSelectorListener<EObject>() {

            @Override
            public void elementSelected(RamSelectorComponent<EObject> selector, EObject selectedElement) {
                Map<String, EObject> elements = new HashMap<String, EObject>();
                elements.put(currentRole, fromModelElement);
                elements.put(toRoleNames.get(0), selectedElement);
                
                CORELanguageElementMapping toUseType = getToUseMappingType(mappingTypes, fromModelElement, 
                        selectedElement);
                
                if (toUseType != null) {
                    try {
                        createModelElementMapping(perspective, elements, toUseType);
                    } catch (PerspectiveException e) {
                        RamApp.getActiveScene().displayPopup(e.getMessage());
                    }
                    
                }
                
                selector.destroy();
            }
        });
    }
    
    /**
     * Returns the mapping type {@link CORELanguageElementMapping} to be used in a user initiated model element mapping.
     * @param mappingTypes - existing mapping of the first language element
     * @param fromModelElement
     * @param selectedElement
     * @return the mapping type to be used.
     */
    public static CORELanguageElementMapping getToUseMappingType(List<CORELanguageElementMapping> mappingTypes, 
            EObject fromModelElement, EObject selectedElement) {
        
        CORELanguageElementMapping toUseType = null;
        EObject fromMetaclass = fromModelElement.eClass();
        EObject toMetaclass = selectedElement.eClass();
        
        for (CORELanguageElementMapping type : mappingTypes) {
            // getting binary mapping language elements
            EObject firstMappedLanguageElement = type.getMappingEnds().get(0).getLanguageElement()
                    .getLanguageElement();
            EObject secondMappedLanguageElement = type.getMappingEnds().get(1).getLanguageElement()
                    .getLanguageElement();
            if (firstMappedLanguageElement.equals(fromMetaclass) && secondMappedLanguageElement
                    .equals(toMetaclass)) {  
                toUseType = type;
                break;
            }
            
        }
        return toUseType;
    }

     /**
     * Creates a model mapping between two instances, potentially from different languages.
     * This can be applied to n-ary mappings.
     * 
     * @param perspective
     * @param elements - model elements to be mapped, each mapped to its role name.
     * @param type
     */
    public static void createModelElementMapping(COREPerspective perspective,
            Map<String, EObject> elements, CORELanguageElementMapping type) throws PerspectiveException {
        
        COREScene scene =
                COREArtefactUtil.getReferencingExternalArtefact(elements.values().iterator().next()).getScene();
        MappingDetail mappingDetail = TemplateType.INSTANCE.getMappingDetails(elements, type);
        if (!mappingDetail.isMap()) {
            throw new PerspectiveException(mappingDetail.getMessage());
        } else {
            if (isMapped(type, elements)) {
                throw new PerspectiveException("The two model elements are already mapped.");
            }
        }

        MappingType mappingType = mappingDetail.getType();
        EObject firstModelElement = mappingDetail.getFirstElement();
        EObject secondModelElement = mappingDetail.getSecondElement();

        List<COREModelElementMapping> firstMappings = COREPerspectiveUtil.INSTANCE.getMappings(type,
                firstModelElement);
        List<COREModelElementMapping> secondMappings = COREPerspectiveUtil.INSTANCE.getMappings(type,
                secondModelElement);

        if (mappingType == MappingType.COMPULSORY_OPTIONAL) {
            // 1, 11 (1_inverse): Compulsory Optional (from 1 -- 0..1 to) Template
            if (COREPerspectiveUtil.INSTANCE.getMappings(type, firstModelElement).size() == 1) {
                throw new PerspectiveException(Strings.POP_CANNOT_DELETE_MAPPED);
            } else if (secondMappings.size() == 1) {
                // the mapping type doesn't support more than one mapping.
                COREModelElementMapping mapping = secondMappings.get(0);
                BasePerspectiveController.removeMapping(mapping);
                map(elements, type, scene);

            } else {
                map(elements, type, scene);
            }
        } else if (mappingType == MappingType.COMPULSORY_OPTIONAL_MULTIPLE) {
            // 3, 12 (3_inverse): Compulsory Optional-Multiple (from 1 -- 0..* to) Template
            if (secondMappings.size() == 1) {
                COREModelElementMapping mapping = secondMappings.get(0);
                BasePerspectiveController.removeMapping(mapping);
                map(elements, type, scene);
            } else {
                map(elements, type, scene);
            }
        } else if (mappingType == MappingType.COMPULSORY_COMPULSORY_MULTIPLE) {
            // TODO; Buggy, please update
            // This may not be used here since each mapping needs to be created when the element is created.
            // 4, 13 (4_inverse): Compulsory Compulsory-Multiple (from 1 -- 1..* to) Template
            if (secondMappings.size() == 1) {
                EObject mappedSecondElement = COREPerspectiveUtil.INSTANCE.getOtherElement(secondMappings.get(0),
                        secondModelElement);
                List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(type,
                        mappedSecondElement);
                if (mappings.size() == 1) {
                    throw new PerspectiveException(Strings.POP_CANNOT_DELETE_MAPPED);
                }
            } else if (secondMappings.size() == 1) {
                COREModelElementMapping mapping = secondMappings.get(0);
                BasePerspectiveController.removeMapping(mapping);
                map(elements, type, scene);
            } else {
                map(elements, type, scene);
            }
        } else if (mappingType == MappingType.OPTIONAL_OPTIONAL) {
            // 5. Optional Optional (from 0..1 -- 0..1 to) Template
            if (firstMappings.size() == 1 && secondMappings.size() == 0) {
                COREModelElementMapping mapping = firstMappings.get(0);
                BasePerspectiveController.removeMapping(mapping);
                map(elements, type, scene);
            } else if ((firstMappings.size() == 0 && secondMappings.size() == 1)
                    || (firstMappings.size() == 1 && secondMappings.size() == 1)) {
                COREModelElementMapping mapping = secondMappings.get(0);
                BasePerspectiveController.removeMapping(mapping);
                map(elements, type, scene);
            } else {
                map(elements, type, scene);
            }
        } else if (mappingType == MappingType.COMPULSORY_MULTIPLE_OPTIONAL) {
            // 6, 14 (6_inverse): Compulsory-Multiple Optional (from 1..* -- 0..1 to) Template
            if (firstMappings.size() == 1) {
                // get the corresponding mapped element.
                EObject mappedFirstElement = COREPerspectiveUtil.INSTANCE.getOtherElement(firstMappings.get(0),
                        firstModelElement);
                List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(type,
                        mappedFirstElement);
                if (mappings.size() == 1) {
                    throw new PerspectiveException(Strings.POP_CANNOT_DELETE_MAPPED);
                }
            } else if (firstMappings.size() == 1) {
                COREModelElementMapping mapping = firstMappings.get(0);
                BasePerspectiveController.removeMapping(mapping);
                map(elements, type, scene);
            } else {
                map(elements, type, scene);
            }
        } else if (mappingType == MappingType.OPTIONAL_MULTIPLE_OPTIONAL) {
            // 7, 15 (7_inverse): Optional-Multiple Optional (from 0..* -- 0..1 to) Template
            if (firstMappings.size() == 1) {
                COREModelElementMapping mapping = firstMappings.get(0);
                BasePerspectiveController.removeMapping(mapping);
                map(elements, type, scene);
            } else {
                map(elements, type, scene);
            }
        } else {
            // 8, 9, 10, 16 (10_inverse), Just Map
            map(elements, type, scene);
        }

    }

    /**
     * Creates model element mapping {@link COREModelElementMapping} between two or more model elements.
     * @param elements - a map of model elements with their respective role names.
     * @param mappingType 
     * @param scene
     * @return the model element mapping
     */
    private static COREModelElementMapping map(Map<String, EObject> elements, CORELanguageElementMapping mappingType,
            COREScene scene) {
        COREModelElementMapping mapping = CoreFactory.eINSTANCE.createCOREModelElementMapping();
        scene.getElementMappings().add(mapping);

        for (Map.Entry<String, EObject> roleElement : elements.entrySet()) {
            mapping.getModelElements().add(roleElement.getValue());
        }
        mapping.setLEMid(mappingType.getIdentifier());
        
        RamApp.getActiveScene().displayPopup("Successfully mapped!");
        
        return mapping;
    }

    /**
     * Checks if two model elements are already mapped.
     * @param type - language element mapping {@link CORELanguageElementMapping} of the mapping, 
     * {@link COREModelElementMapping}
     * 
     * @param elements
     * @return
     */
    private static boolean isMapped(CORELanguageElementMapping type, Map<String, EObject> elements) {

        boolean isMapped = false;
        EObject firstModelElement = null;
        EObject secondModelElement = null;

        int counter = 1;
        for (Map.Entry<String, EObject> roleElement : elements.entrySet()) {
            // Only binary mapping is covered
            if (counter == 1) {
                firstModelElement = roleElement.getValue();
            } else {
                secondModelElement = roleElement.getValue();
            }
            counter++;
        }

        // get mapping for the first model element.
        List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(type, firstModelElement);

        for (COREModelElementMapping mapping : mappings) {
            if (secondModelElement.equals(COREPerspectiveUtil.INSTANCE.getOtherElement(mapping, firstModelElement))) {
                isMapped = true;
            }
        }
        return isMapped;

    }

    public static void removeMapping(COREModelElementMapping mapping) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(mapping);
        CompoundCommand compoundCommand = new CompoundCommand();

        // Create remove Command.
        compoundCommand.append(RemoveCommand.create(editingDomain, mapping));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Executes the given command on the given editing domain.
     *
     * @param editingDomain the editing domain the command should be executed on
     * @param command the command to be executed
     */
    protected static void doExecute(EditingDomain editingDomain, Command command) {
        if (!command.canExecute()) {
            throw new RuntimeException("Command " + command + " not executable.");
        }
        editingDomain.getCommandStack().execute(command);
    }
    

    /**
     * Saves a concern.
     * @param scene - the scene of the current perspective
     */
	public static void saveModel(COREScene scene) {
		
     	COREConcern concern = (COREConcern) EcoreUtil.getRootContainer(scene);
        
        // save the concern
        BasicActionsUtils.saveConcern((COREConcern) concern);
        
	}
	

	/**
	 * This is a generic perspective update method. It calls the recursive method which handles the consistency between
	 * the primary model element and the mapped model elements.
	 * @param perspective 
	 * @param currentRole - the role name of the model in question
	 * @param element - the element being updated
	 * @param feature - the feature (e.g., name, type, visibility) which is being updated.
	 * @param value - the new value for the feature of the element.
	 */
	public void updateElement(COREPerspective perspective, String currentRole, EObject element,
			EStructuralFeature feature, Object value) {
		
		// list of updated attributes.
		List<EObject> updatedElements = new ArrayList<EObject>();
		updateElements(perspective, currentRole, updatedElements, element, feature, value);
	}

	private void updateElements(COREPerspective perspective, String currentRole, List<EObject> updatedElements,
			EObject modelElement, EStructuralFeature feature, Object value) {
		
		// call respective language action (generically) to update the element.
		EMFEditUtil.getPropertyDescriptor(modelElement, feature).setPropertyValue(modelElement, value);
		
		// add the updated model element
		updatedElements.add(modelElement);

		// get the updated core language element, i.e., the representation of
		// the metaclass of the updated model element in the perspective
		CORELanguageElement updatedCoreLanguageElement = COREPerspectiveUtil.INSTANCE
				.getCoreLanguageElement(perspective, modelElement.eClass());

		// check if the metaclass of the updated element has a feature, i.e.,
		// nested language element(s). Otherwise, return.
		if (updatedCoreLanguageElement == null || updatedCoreLanguageElement.getNestedElements().size() == 0) {
			return;
		}
		
		// the updated nested language element (i.e., feature) of the model element.
		CORELanguageElement updatedNestedElement = COREPerspectiveUtil.INSTANCE
				.getNestedLanguageElement(updatedCoreLanguageElement, feature);

		// iterate over all the model mappings of the updated model element
		for (COREModelElementMapping mapping : COREPerspectiveUtil.INSTANCE.getMappings(modelElement)) {

			// get the mappingType, i.e., language element mapping, of the model element mapping.
			CORELanguageElementMapping mappingType = COREPerspectiveUtil.INSTANCE.getMappingType(perspective, mapping);

			// get the mapping end of the updated nested element
			MappingEnd nestedMappingEnd = COREPerspectiveUtil.INSTANCE.getNestedMappingEnd(mappingType, currentRole,
					updatedNestedElement);

			// null indicates that this nested element is not mapped.
			if (nestedMappingEnd != null) {
				
				List<MappingEnd> otherMappingEnds = COREPerspectiveUtil.INSTANCE.getOtherMappingEnds(nestedMappingEnd);

				// this aims to support ternary mapping
				List<EObject> others = COREPerspectiveUtil.INSTANCE.getOtherElements(mapping, modelElement);
				for (EObject otherElement : others) {
					EObject otherMetaclass = otherElement.eClass();

					CORELanguageElement otherCoreLE = COREPerspectiveUtil.INSTANCE.getCoreLanguageElement(perspective,
							otherMetaclass);
					for (MappingEnd mappingEnd : otherMappingEnds) {
						CORELanguageElement toBeUpdatedElement = mappingEnd.getLanguageElement();
						if (otherCoreLE.getNestedElements().contains(toBeUpdatedElement)) {
							if (!updatedElements.contains(otherElement)) {
								EStructuralFeature otherFeature = (EStructuralFeature) mappingEnd.getLanguageElement()
										.getLanguageElement();
								String roleName = mappingEnd.getRoleName();
								// perspective recursive call
								updateElements(perspective, roleName, updatedElements, otherElement, otherFeature,
										value);
							}
						}
					}
				}
			}

		}
	}
	
	/**
	 * This is a generic perspective update method. It calls the recursive method which handles the consistency between
	 * the primary model element and the mapped model elements.
	 * @param perspective 
	 * @param currentRole - the role name of the model in question
	 * @param currentElement - the element being updated
	 * @param currentFeature - the feature (e.g., name, type, visibility) which is being updated.
	 * @param value - the new value for the feature of the element.
	 */
	public void updateElement(COREPerspective perspective, COREScene scene, String currentRole, EObject currentElement,
			EStructuralFeature currentFeature, Object value) {
		
		// list of updated attributes.
		List<EObject> updatedElements = new ArrayList<EObject>();
		// call the language action to update the element.
		EMFEditUtil.getPropertyDescriptor(currentElement, currentFeature).setPropertyValue(currentElement, value);
		updateElements(perspective, scene, currentRole, updatedElements, currentElement, currentFeature, value);
	}

	private void updateElements(COREPerspective perspective, COREScene scene, String currentRole, List<EObject> updatedElements,
			EObject currentElement, EStructuralFeature currentFeature, Object value) {
		
		// add the updated model element
		updatedElements.add(currentElement);
		
		for (COREModelElementMapping mapping : COREPerspectiveUtil.INSTANCE.getMappings(scene, currentElement)) {
			EObject otherElement = COREPerspectiveUtil.INSTANCE.getOtherElement(mapping, currentElement);
			Map<String, EObject> otherDetails = COREPerspectiveUtil.INSTANCE.getNestedOtherDetails(perspective, currentRole, mapping, currentFeature);
			if (otherDetails != null) {
				for (Map.Entry<String, EObject> detailsEntry : otherDetails.entrySet()) {
					String otherRoleName = detailsEntry.getKey();
					EStructuralFeature otherFeature = (EStructuralFeature) detailsEntry.getValue();
					
					if (!updatedElements.contains(otherElement)) {
						// call respective language action (generically) to update the element.
						EMFEditUtil.getPropertyDescriptor(otherElement, otherFeature).setPropertyValue(otherElement, value);
						updateElements(perspective, scene, otherRoleName, updatedElements, otherElement, otherFeature,
								value);
					}
					
				}
			}
		}
	}
	
	

}
