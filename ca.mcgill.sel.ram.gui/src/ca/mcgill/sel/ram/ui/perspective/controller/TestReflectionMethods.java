package ca.mcgill.sel.ram.ui.perspective.controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

public class TestReflectionMethods {

	int name;
	float x;
	float y;

	public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public void testReflection(List<Object> values) {
		for (Object v : values) {
			System.out.println(v);
		}
	}
	
	public void testReflection2(HashMap<Object, Object> values) throws NoSuchMethodException, SecurityException {
		int count = 1;
		for (Map.Entry<Object, Object> value : values.entrySet()) {
			if (count == 1) {
				Method method2 = TestReflectionMethods.class.getMethod("setY", float.class);
				
				try {
					method2.invoke(this, value.getKey());
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			count++;
	          System.out.println("Key: "+value.getKey() + " & Value: " + value.getValue());
	        }
	}

}
