package ca.mcgill.sel.ram.ui.scenes;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.ram.Annotation;
import ca.mcgill.sel.ram.ArtifactSignature;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Import;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.impl.RamFactoryImpl;
import ca.mcgill.sel.restif.DynamicFragment;

/**
 * Generic interface for annotation based RESTifying of java code. Offers mainly
 * two functionalities: A method to add the maven/signatures+imports required
 * for the framework specific imports (depends on the implementation of this
 * interface) and a method to translate the actual RIF_RAM mappings into fpcdm
 * annotations for the selected framework. The latter attaches the annotations
 * inserted to the fpcdm to the previously added imports.
 * 
 * @author Maximilian Schiedermeier
 *
 */
public abstract class AbstractFunctionalityPoorAnnotateUtil {

    /**
     * String used to produce the quotation mark symbol.
     */
    protected static final String QUOTATION_MARK_SYMBOL = "\"";
    
    /**
     * Prepares structurals required for REST-technology specific annotations.
     * If a JDK external framework is used, the build-tool artifact details must
     * be declared here and be added to the fpcdm structural view. Also all
     * imports required for REST specific annotations must be declared here,
     * optionally linked to the previous artifact(s), and likewise be added to
     * the fpcdm structural view.
     * 
     * @param fpcdm
     */
    public abstract void addAnnotationSupport(COREExternalArtefact fpcdm);

    /**
     * Translates RIF-RAM mapping information into REST-technology specific
     * annotations and adds them to a fpcdm. (This includes class-level
     * annotations for controllers)
     * 
     * @param fpcdm
     * @param rifRamConcern
     */
    public abstract void addAnnotations(COREExternalArtefact fpcdm, COREConcern rifRamConcern);

    /**
     * Helper method to locate the reference to a build tool artifact that has
     * previously been added to a fpcdms structural view.
     * 
     * @param name as string that is used for identification. The name field is
     * assumed a unique key.
     * @param fpcdm as the ram aspect housing the structural view to be searched
     * for the artifact signature.
     * @return the artifact, if found / null if not found.
     */
    public ArtifactSignature identifyArtifactByName(COREExternalArtefact fpcdm, String name) {

        // Iterate over all declared dependencies (signature artefacts)
        Aspect fpcdmAspect = (AspectImpl) fpcdm.getRootModelElement();
        for (ArtifactSignature dependency : fpcdmAspect.getStructuralView().getDependencies()) {
            // return the first match by name
            if (dependency.getName().equals(name)) {
                return dependency;
            }
        }

        // no matching signature found
        return null;
    }

    /**
     * Helper method to locate the reference to an import that has previously
     * been added to a fpcdms structural view.
     * 
     * @param name as string that is used for identification. The name field is
     * assumed a unique key.
     * @param fpcdm as the ram aspect housing the structural view to be searched
     * for the import.
     * @return the import, if found / null if not found.
     */
    public static Import identifyImportByName(COREExternalArtefact fpcdm, String name) {

        // Iterate over all declared dependencies (signature artefacts)
        Aspect fpcdmAspect = (AspectImpl) fpcdm.getRootModelElement();
        for (Import importStub : fpcdmAspect.getStructuralView().getImportstubs()) {
            // return the first match by name
            if (importStub.getName().equals(name)) {
                return importStub;
            }
        }

        // no matching import stub found
        return null;
    }

    /**
     * 
     * @param dependency as an optional build tool dependency. If not null the
     * import will be mark the provided signature as dependency. The signature
     * must be declared as dependency of the identical structural view.
     * @param stub
     * @param name
     * @param view
     * @return
     */
    public Import addImport(ArtifactSignature dependency, String stub, String name, StructuralView view) {

        // Create import with name and stub:
        Import importStub = RamFactoryImpl.init().createImport();
        importStub.setName(name);
        importStub.setStub(stub);

        // Not all imports depend on a JDK-external artifact - check if one was
        // provided:
        if (dependency != null) {
            importStub.setExternalartifact(dependency);
        }

        // Add import to fpcdm model
        view.getImportstubs().add(importStub);
        return importStub;
    }
    
    /**
     * Extracts the name from the input dynamic fragment and formats it by removing the curly brackets and adding
     * quotation marks.
     * 
     * @param dynamicFragment input dynamic fragment
     * @return formatted name
     */
    public static String extractedFormattedNameFromDynamicFragment(DynamicFragment dynamicFragment) {
        String name = dynamicFragment.getPlaceholder();
        
        return QUOTATION_MARK_SYMBOL + name.substring(2, name.length() - 1) + QUOTATION_MARK_SYMBOL;
    }
    
    /**
     * Checks if there are any parameters in a given operation without any mappings and returns the first one that does
     * not. If there are no parameters without mappings, return null.
     * 
     * @param controllerOperation input operation
     * @return first parameter in the operation that does not have a mapping, returns null if there are not any
     */
    public static Parameter getUnmappedParameterFromOperation(Operation controllerOperation) {
        for (Parameter parameter : controllerOperation.getParameters()) {
            if (parameter.getAnnotation().size() == 0) {
                return parameter;
            }
        }

        return null;
    }

    /**
     * Searches a provided fpcdm structural view for a dedicated fpcdm service
     * launcher, by name.
     * 
     * @param structuralView
     * @return a reference to the matching classifier. Throws an exception if no
     * class matched.
     */
    abstract public Classifier extractLauncherClass(StructuralView structuralView);
}
