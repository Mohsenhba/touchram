package ca.mcgill.sel.ram.ui.scenes;

import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.RArray;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.impl.RamFactoryImpl;
import ca.mcgill.sel.ram.util.StructuralViewUtil;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.ram.ArtifactSignature;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Import;

/**
 * Utility class for all operations related to creating and filling a
 * FPCDM-Artifact with content. Content covers EObject structure (including
 * annotations) as well as mappings to the extended FrCdm Aspect. Note: This
 * class only takes care of structural element, not behavioural models. For SDs,
 * see "AbstractTrafoDelegationUtils" and framework-specific implementations.
 * 
 * @author Maximilian Schiedermeier
 *
 */
public abstract class AbstractFunctionalityPoorCdmFillUtil {

	/**
	 * Launcher name getter must be specified by framework specific extensions
	 * to this class.
	 * 
	 * @return
	 */
	protected abstract Object getLauncherName();

	/**
	 * Adds a generic REST Framework specific launcher class
	 * "RestServiceLauncher" with an annotated main method that calls:
	 * "SpringApplication.run(RestSerivceLauncher.class, args);" This implicitly
	 * also adds REST framework implementation class and all dependencies (e.g.
	 * String[], Class<?> wrapped as parameters, run operation, return type).
	 * The run methods of the Spring Boot Application class is called by
	 * launcher and the corresponding return type (interface) as implementation
	 * classes to the fpcdm. This is required to generate to required import
	 * statement in the custom launcher. Additional maven artifacts are not
	 * required as the jars are identical to the ones implicitly pulled via
	 * annotations.
	 * 
	 * @param fpcdmArtefact as the artefact that requires the spring boot
	 * launcher
	 * @return
	 */
	public abstract Classifier addRestApplicationLauncherClass(COREExternalArtefact fpcdmArtefact);

	/**
	 * Add information used to generate reflective maven signature information.
	 * 
	 * @param fpcdmArtefact
	 */
	public static void addMavenSignature(COREExternalArtefact fpcdmArtefact, Classifier launcher, ArtifactSignature frcdmSignature) {

		
		// 1) Build new signature: Reflective signature is strongly based on the original frcdm signature
		// Name stays the same, groupid is changed.
		ArtifactSignature ownSignature = RamFactoryImpl.init().createArtifactSignature();
		ownSignature.setGroupId("ca.mcgill.sel.restified");
		ownSignature.setArtifactId(frcdmSignature.getName());
		ownSignature.setVersion("1.0.0");

		// 3) Add signature to fpcdm's structural view
		Aspect fpcdmAspect = (AspectImpl) fpcdmArtefact.getRootModelElement();
		fpcdmAspect.getStructuralView().setSignature(ownSignature);

		// Set launcher class
        // commented out since we are using generating the launcher class as a static class with Acceleo now
		//fpcdmAspect.getStructuralView().setLauncherclass(launcher);
	}

	/**
	 * Ensures a copy of the provided classifier exists in the provided aspect.
	 * If it does not already exist, a new empty class is added, carrying the
	 * same name as the originally provided and mapped to the original. If the
	 * classifier is an implementation class the requires imports and maven
	 * signatures are cloned, too.
	 * 
	 * @param originalClassifier as the classifier which must be mapped to by
	 * the fpcdm aspect.
	 * @param fpcdmArtefact as a fpcdm CoreExternalArtefact that contains the
	 * fpcdm aspect (as rootmodelelement) and mappings to the extended frcdm (as
	 * coreModelExtension)
	 * 
	 * @return the corresponding fpcdm classifier. Can be either the one that
	 * was already there, or the one that has just been added.
	 */
	public static Classifier ensureClassifierExists(Classifier originalClassifier, COREExternalArtefact fpcdmArtefact) {

		// Classifier exists and is mapped => no action required.
		if (isMappedClassifier(originalClassifier, fpcdmArtefact)
				& isValidClassifier(originalClassifier, fpcdmArtefact)) {
			System.out.println("Classifier already cloned and mapped - no action required.");
			return resolveFpClassifierByName(originalClassifier.getName(), (Aspect) fpcdmArtefact.getRootModelElement(),
					true);
		}
		// Classifier does no exist and is not mapped => Add a copy of the
		// classifier and map it
		if (!isMappedClassifier(originalClassifier, fpcdmArtefact)
				& !isValidClassifier(originalClassifier, fpcdmArtefact)) {
			return addAndMapClassifierToArtefact(originalClassifier, fpcdmArtefact);
		}

		// Any other combination => The model is corrupt.
		throw new TransformerException("Provided model is inconsistent. Found a classifier that is not coherent"
				+ " in mapping and fpcdm existence status.");
	}

	/**
	 * Helper method to create a shallow clone of an element, regardless of it's
	 * type. Only "classifier / impl-class", "operation", "parameter" are
	 * supported inputs.
	 * 
	 * @param original as the classifier, @Override operation or parameter to
	 * clone
	 * @param suffix optional suffix to append to the name of the cloned object.
	 * If unsure, set to empty string. Settings this string also is interpreted
	 * as the wish to transform a cloned implementation class into a classifier.
	 * (can only be applied if the original is a classifier)
	 * 
	 * @return the cloned copy (only name field set)
	 */
	private static EObject cloneElementAndName(EObject original, String suffix) {

		// Use a RAM factory to create an empty copy.
		// Only set the name, ignore all other properties of original
		RamFactory factory = RamFactoryImpl.init();
		if (original instanceof ca.mcgill.sel.ram.Class
				|| (original instanceof ImplementationClass && !suffix.isBlank())) {
			Classifier originalClassifier = (Classifier) original;
			Classifier clone = factory.createClass();
			clone.setName(originalClassifier.getName() + suffix);
			return clone;
		} else if (original instanceof ImplementationClass && suffix.isBlank()) {
			ImplementationClass originalImplementationClass = (ImplementationClass) original;
			ImplementationClass clone = factory.createImplementationClass();
			clone.setName(originalImplementationClass.getName() + suffix);
			clone.setInstanceClassName(originalImplementationClass.getInstanceClassName());
			return clone;
		} else if (original instanceof Operation && suffix.isBlank()) {
			Operation originalOperation = (Operation) original;
			Operation clone = factory.createOperation();
			clone.setName(originalOperation.getName());
			clone.setStatic(originalOperation.isStatic());
			return clone;
		} else if (original instanceof Parameter && suffix.isBlank()) {
			Parameter originalClassifier = (Parameter) original;
			Parameter clone = factory.createParameter();
			clone.setName(originalClassifier.getName());
			return clone;
		} else {
			// Input was not a classifier, operation or parameter
			throw new TransformerException("Cloning of " + original
					+ "is not supported. Is not a classifier, operation or parameter or uses an invalid comination with suffix parameter.");
		}
	}

	/**
	 * Adds a previously inexistent classifier to a fpcdm. Also adds a mapping
	 * to the original classifier.
	 * 
	 * @param originalClassifier as the classifier to be cloned.
	 * @param fpcdmArtefact as the artefact holding the aspect and
	 * mapping-bundle to be modified.
	 * 
	 * @return classifier that has been added and mapped
	 */
	private static Classifier addAndMapClassifierToArtefact(Classifier originalClassifier,
			COREExternalArtefact fpcdmArtefact) {

		// create an empty clone of the provided classifier
		Classifier classifierCopy = (Classifier) cloneElementAndName(originalClassifier, "");

		// add the empty clone to the fpcdm aspect
		Aspect fpcdm = (Aspect) fpcdmArtefact.getRootModelElement();
		fpcdm.getStructuralView().getClasses().add(classifierCopy);

		// add a mapping from the original classifier to the new classifier
		// extended model maps to extending model, mapping is contained in
		// extending model.
		COREModelExtension mappings = fpcdmArtefact.getModelExtensions().get(0);
		addClassifierMapping(classifierCopy, originalClassifier, mappings);

		// in case the added classifier was an implementation class, ensure its
		// imports are covered as well
		if (originalClassifier instanceof ImplementationClass) {
			cloneImplementationClassImports((ImplementationClass) originalClassifier,
					(ImplementationClass) classifierCopy, fpcdm.getStructuralView());
		}

		return classifierCopy;
	}

	/**
	 * Cloned implementation classes need to be backened by the corresponding
	 * maven signature. calling this method on a previously replicated
	 * implementation class (to the FPCDM) ensures potential importstubs and
	 * maven signatures are correctly added to the FPCDM structural view and
	 * correctly referenced by the cloned implementation class.
	 * 
	 * @param originalImplementationClass as an implementation class that has
	 * already been cloned to the FPCDM.
	 * @param clonedImplementationClass as the clone of the original
	 * implementation class, located in the FPCDM.
	 * @param fpcdmStructuralView as the structural view of the FPCDM. Eligible
	 * signature stubs and maven signatures are contained here and referenced by
	 * the classifier copy.
	 */
	// TODO: Make sure it does not RE-PRINT warning for all artifacts.
	private static void cloneImplementationClassImports(ImplementationClass originalImplementationClass,
			ImplementationClass clonedImplementationClass, StructuralView fpcdmStructuralView) {

		// In case the original impl. class has an import, make sure it is
		// correctly integrated into the FPCDM
		// A clone of the import and the associated signature must be added to
		// the FPCDM (if not already exists) and the classifier copy must
		// reference the cloned import.
		if (originalImplementationClass.getImport() != null) {
			Import importClone = ensureImportCloneIsInFpcdmStructuralView(originalImplementationClass.getImport(),
					fpcdmStructuralView);
			clonedImplementationClass.setImport(importClone);

			// Avert user to convert used JAR into maven artefact
			ArtifactSignature artefact = originalImplementationClass.getImport().getExternalartifact();
			StringBuilder mavenCommandBuilder = new StringBuilder("mvn install:install-file -Dfile=");
			mavenCommandBuilder.append("YOUR_BUSINESS_LOGIC.jar");
			mavenCommandBuilder.append(" -DgroupId=");
			mavenCommandBuilder.append(artefact.getGroupId());
			mavenCommandBuilder.append(" -DartifactId=");
			mavenCommandBuilder.append(artefact.getArtifactId());
			mavenCommandBuilder.append(" -Dversion=");
			mavenCommandBuilder.append(artefact.getVersion());
			mavenCommandBuilder.append(" -Dpackaging=jar -DcreateChecksum=true");
			System.out.println(AsteriskWarningBuilder.generateFormattedWarning(
					"You receive this message because a dependency to an invoked implementation classes is presumably not auto-resolvable by the selected build system. Unless the corresponding JAR file is manually injected to your local artifact repository this will result in compile and runtime errors. Use below command to convert the used JAR into an classpath-compatible artifact:",
					mavenCommandBuilder.toString(), false));
		}
	}

	/**
	 * Verifies a semantical equivalent of a provided import is present in a
	 * provided structural view. If not ensures the import is added. This
	 * includes running the identical verification for the signature referenced
	 * by the import. Returns a reference to the identified or cloned matching
	 * import equivalent.
	 * 
	 * @param originalImport
	 * @param targetStructuralView
	 * @return the identified or cloned matching import equivalent.
	 */
	private static Import ensureImportCloneIsInFpcdmStructuralView(Import originalImport,
			StructuralView targetStructuralView) {

		// Compary the provided import to all imports contained in structural
		// view
		for (Import existingImport : targetStructuralView.getImportstubs()) {
			// import stubs are semantically equivalent if all fields match
			boolean sameStub = existingImport.getStub().equals(existingImport.getStub());
			boolean sameName_ = existingImport.getName().equals(existingImport.getName());
			if (sameName_ && sameStub) {
				// No need to clone. A semantically equivalent import already
				// exists.
				return existingImport;
			}
		}

		// If this code is reached, none of the existing imports matched. The
		// import must be cloned, alongside with it's signature.
		ArtifactSignature importSignature = ensureSignatureCloneIsInFpcdmStructuralView(
				originalImport.getExternalartifact(), targetStructuralView);
		Import importClone = RamFactoryImpl.init().createImport();
		importClone.setName(originalImport.getName());
		importClone.setStub(originalImport.getStub());
		importClone.setExternalartifact(importSignature);
		targetStructuralView.getImportstubs().add(importClone);
		return importClone;
	}

	/**
	 * Verifies a semantical equivalent of a provided signature is present in a
	 * provided structural view. If not ensures the signature equivalent is
	 * added. Returns a reference to the identified or cloned matching siganture
	 * equivalent.
	 * 
	 * @param externalartifact as the original signature to be identified or
	 * imitated.
	 * @param targetStructuralView as the structural view to analyze / modify.
	 * @return the identified or cloned matching signature equivalent.
	 */
	private static ArtifactSignature ensureSignatureCloneIsInFpcdmStructuralView(ArtifactSignature externalartifact,
			StructuralView targetStructuralView) {

		// Compare the provided signature to all signatures contained in
		// structural view
		for (ArtifactSignature existingSignature : targetStructuralView.getDependencies()) {
			// signatures are semantically equivalent if all fields match
			boolean sameArtifact = existingSignature.getArtifactId().equals(externalartifact.getArtifactId());
			boolean sameGroupId = existingSignature.getGroupId().equals(externalartifact.getGroupId());
			boolean sameName = existingSignature.getName().equals(externalartifact.getName());
			if (sameArtifact && sameGroupId && sameName) {
				// No need to clone. A semantically equivalent signature already
				// exists.
				return existingSignature;
			}
		}

		// If this code is reached, none of the existing signatures matched. The
		// signature must be cloned alongside all its string fields.
		ArtifactSignature signatureClone = RamFactoryImpl.init().createArtifactSignature();
		signatureClone.setName(externalartifact.getName());
		signatureClone.setGroupId(externalartifact.getGroupId());
		signatureClone.setArtifactId(externalartifact.getArtifactId());
		signatureClone.setVersion(externalartifact.getVersion());
		targetStructuralView.getDependencies().add(signatureClone);
		return signatureClone;
	}

	/**
	 * Helper method to determine if a semantically equivalent classifier
	 * mapping is already present.
	 * 
	 * @param fpcdmClassifier
	 * @param frcdmClassifier
	 * @param coreModelExtension
	 */
	private static void addClassifierMapping(Classifier fpcdmClassifier, Classifier frcdmClassifier,
			COREModelExtension coreModelExtension) {

		// reject mapping, if an equivalent mapping already exists.
		if (lookupClassifierMapping(frcdmClassifier, fpcdmClassifier, coreModelExtension) != null) {
			throw new TransformerException(
					"Classifier mapping rejected, because an equivalent " + "mapping is already present.");
		}

		// Create a new mapping
		COREMapping<Classifier> classifierMapping = RamFactoryImpl.init().createClassifierMapping();

		// source is the BL/FRCDM (extended model)
		classifierMapping.setFrom(frcdmClassifier);

		// target is the FPCDM (extending model)
		classifierMapping.setTo(fpcdmClassifier);

		// Add it at top level
		coreModelExtension.getCompositions().add(classifierMapping);
	}

	/**
	 * Helper method to look up an existing classifier mapping.
	 * 
	 * @param fpcdmClassifier
	 * @param frcdmClassifier
	 * @param coreModelExtension
	 * @return the matching classifier mapping or null, if none of the checked
	 * mappings matches.
	 */
	public static COREMapping<Classifier> lookupClassifierMapping(Classifier frcdmClassifier,
			Classifier fpcdmClassifier, COREModelExtension coreModelExtension) {
		// Top level mappings must be all classifier mappings. Cast and iterate
		// over them
		for (COREModelElementComposition<?> composition : coreModelExtension.getCompositions()) {
			COREMapping<Classifier> classifierMapping = (COREMapping<Classifier>) composition;

			// Return if a mapping matches by source and target
			if (classifierMapping.getFrom().equals(frcdmClassifier)
					&& classifierMapping.getTo().equals(fpcdmClassifier)) {
				return classifierMapping;
			}
		}

		// None of the registered mappings matches.
		return null;
	}

	/**
	 * Checks if a provided classifier name is already contained in an aspect.
	 * 
	 * @param originalClassifier as classifier to search for
	 * @param fpcdmArtefact as the fpcdmArtefact to analyze
	 * @return a boolean, indicating if the resolving was successful
	 */
	public static boolean isValidClassifier(Classifier originalClassifier, COREExternalArtefact fpcdmArtefact) {

		return resolveFpClassifierByName(originalClassifier.getName(), (Aspect) fpcdmArtefact.getRootModelElement(),
				true) != null;
	}

	/**
	 * Iterates over all classifiers found in a fpcdmArtefact's aspect's
	 * structural-view. Returns the only classifier that matches by name (names
	 * are unique) or null if there is no match.
	 * 
	 * @param name as the classifier name to search for.
	 * @param aspect as the fpcdm main aspect.
	 * @param nullAccepted to indicate whether an exception should be raised if
	 * this method could not resolve the provided string to a classifier.
	 * @return the matching fpcdm classifier or null if accepted.
	 */
	public static Classifier resolveFpClassifierByName(String name, Aspect aspect, boolean nullAccepted) {
		// obtain a list of all fpcdm classifiers

		EList<Classifier> allFpcdmClassifiers = aspect.getStructuralView().getClasses();

		// search for a classifier with the provided name
		for (Classifier classifier : allFpcdmClassifiers) {
			if (classifier.getName().equals(name)) {
				return classifier;
			}
		}

		// return null if no classifier matched
		if (nullAccepted) {
			return null;
		}
		throw new TransformerException(
				"The provided string: " + name + " could not be resolved to a FPCDM classifier.");
	}

	/**
	 * Iterates over all classifiers found in a fpcdmArtefact's aspect's
	 * structural-view. Returns the only classifier that matches by name (names
	 * are unique) or null if there is no match.
	 * 
	 * @param className as the classifier name to search for.
	 * @param operationName as the operation name to search for.
	 * @param fpcdm as the fpcdm main aspect artefact.
	 * @return the matching fpcdm classifier
	 */
	public static Operation resolveFpOperationByName(String className, String operationName,
			COREExternalArtefact fpcdm) {

		// resolve launcher class (search the "getClass" operation)
		// Note: getClass is NOT an operation of the launcher, but an operation
		// of the "object" implementation class.
		// Although the launcher offers indirectly this method, as it extends
		// object.
		Classifier objectImplementationClass = AbstractFunctionalityPoorCdmFillUtil.resolveFpClassifierByName(className,
				(Aspect) fpcdm.getRootModelElement(), false);

		for (Operation operation : objectImplementationClass.getOperations()) {
			if (operation.getName().equals(operationName)) {
				return operation;
			}
		}
		throw new TransformerException(
				"The operation you are looking for does not exist: " + className + "." + operationName);
	}

	/**
	 * Verifies if COREExternalArtefact contains a CoreModelExtension with a
	 * mapping to a provided classifier instance.
	 * 
	 * @param originalClassifier as the classifier to be checked
	 * @param fpcdmArtefact as the artefact containing the mappings to be
	 * searched
	 * 
	 * @return a flag indicating whether there already is a classifier mapping
	 * in the coreExternalArtefact for the provided classifier.
	 */
	public static boolean isMappedClassifier(Classifier originalClassifier, COREExternalArtefact fpcdmArtefact) {

		// extract the coreModelExtension that eventually stores the mappings
		// between fpcdm and frcdm.
		COREModelExtension classifierMappings = fpcdmArtefact.getModelExtensions().get(0);

		// iterate over all mappings on top level (can be cast, only classifier
		// allowed here) and verify if a source ("from" field) matches the
		// provided original classifier. (The extended / BL model is the
		// "from".)
		for (COREModelElementComposition<?> classifierMapping : classifierMappings.getCompositions()) {
			if (((COREMapping<Classifier>) classifierMapping).getFrom().equals(originalClassifier)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Clones a provided frcdm operation into a fpcdm model. If requested also
	 * adds mappings between original and cloned operation. If the operation has
	 * parameters, those are cloned, added and mapped individually, too.
	 * 
	 * @param originalOperation as the operation that must be cloned.
	 * @param fpcdmClassifier as the (already existing) target classifier in the
	 * fpcdm, that will house the operation clone.
	 * @param originalClassifier as the classifier the fpcdmClassifier is mapped
	 * to.
	 * @param fpCdmArtefact as the artefact housing (fpcdm) aspect and mappings
	 * to the original aspect (frcdm). Can be set to null of next flag is false
	 * @param mappingRequested as a flag to indicate whether the newly created
	 * operation must be mapped to the original operation, too.
	 * 
	 * @return the newly created operation clone.
	 */
	public static Operation cloneOperation(Operation originalOperation, Classifier fpcdmClassifier,
			Classifier originalClassifier, COREExternalArtefact fpCdmArtefact, boolean mappingRequested) {

		// clone the operation (without parameters)
		Operation fpcdmOperation = (Operation) cloneElementAndName(originalOperation, "");

		// add the empty clone to the parent fpcdm classifer
		fpcdmClassifier.getOperations().add(fpcdmOperation);

		// if requested: add a mapping from the original operation (BL/FRCDM)
		// to the new one (FPCDM)
		COREMapping<Operation> operationMapping = null;
		if (mappingRequested) {

			// The operation mapping is added as child of the corresponding
			// classifier mapping.
			COREModelExtension mappings = fpCdmArtefact.getModelExtensions().get(0);
			COREMapping<Classifier> classifierMapping = lookupClassifierMapping(originalClassifier, fpcdmClassifier,
					mappings);

			if (classifierMapping == null) {
				throw new Error("Unable to resolve classifier mapping that should have been previously inserted for"
						+ " the parent of operation: " + originalOperation.getName());
			}

			// Add a mapping from the original to the cloned operation to the
			// list of operation mappings in the cloned classifier's mapping.
			operationMapping = RamFactoryImpl.init().createOperationMapping();
			operationMapping.setFrom(originalOperation);
			operationMapping.setTo(fpcdmOperation);
			classifierMapping.getMappings().add(operationMapping);
		}

		// In addition, clone all parameters, map them.
		for (Parameter originalParameter : originalOperation.getParameters()) {

			// Make sure the parameter type is valid in the fpcdm context.
			Type originalParameterType = originalParameter.getType();
			if (originalParameterType == null) {
				System.out.println("Figure out why this is null. Type of getBookDetails(arg0)");
			}
			Type fpcdmParameterType = ensureTypeExistsAndIsMapped(originalParameterType, fpCdmArtefact);
			cloneParameter(originalParameter, fpcdmParameterType, fpcdmOperation, fpCdmArtefact, mappingRequested,
					operationMapping);
		}

		// In addition ensure existence/mapping of operation result parameter
		// (even if void)
		Type fpcdmOperationReturnType = ensureTypeExistsAndIsMapped(originalOperation.getReturnType(), fpCdmArtefact);
		fpcdmOperation.setReturnType(fpcdmOperationReturnType);

		return fpcdmOperation;
	}

	/**
	 * Helper method to ensure existence of additional types (can be either
	 * classifiers or primitives, including generics). Searches a provided RAM
	 * model for an equivalent to the provided type. If not found the code
	 * ensures the provided type is cloned into the RAM model and correctly
	 * mapped (for potential weaving). The type equivalent (either already
	 * existed, or newly created) is then returned.
	 * 
	 * @param type as a classifier or primitive to search an equivalent for.
	 * @param fpCdmArtefact as a distinct RAM model for which existence of the
	 * type equivalent is searched / ensured..
	 * @return the type equivalent of the provided type, which is guaranteed to
	 * be correctly integrated and mapped in the provided artefact after code
	 * execution.
	 */
	private static Type ensureTypeExistsAndIsMapped(Type type, COREExternalArtefact fpCdmArtefact) {

		// Step 1: create a reliable search string (is required in step 2, to
		// search the FPCDM for a type equivalent).
		// Name construction depends on kind the type is. Can be either of:
		// - "ObjectType": (Collection, Implementation Class etc...)
		// - "Primitive": "void", "String", "int", ... (Note: TouchCORE
		// considers strings primitives)
		String typeString;

		// See metamodel for if clause: Non-primitives are only the types that
		// subclass "objecttype", but not "primitivetype":
		if (type instanceof ObjectType && !(type instanceof PrimitiveType)) {

			// We found a type that is an actual class: In this case we need the
			// fully classified
			// name, including package signature and generics (should they
			// exist)
			ObjectType objectType = (ObjectType) type;
			typeString = StructuralViewUtil.getClassNameWithGenerics(objectType);
		} else {
			// We found a primitive. We just use the base name to detect the
			// FPCDM equivalent.
			typeString = type.getName();
		}

		// Step 2: Search FPCDM for a type equivalent (use search string)
		// Returns null if it does not yet exist.
		Type resolvedType = StructuralViewUtil.getTypeByName(typeString,
				((Aspect) fpCdmArtefact.getRootModelElement()).getStructuralView());

		// If the type equivalent already exists we can stop here.
		if (resolvedType != null)
			return resolvedType;

		// Step 3: (Only if null) The provided type has yet no equivalent in the
		// FPCDM. We have to create it (and map it to the original type).
		// Since primitives are guaranteed to exist in all RAM models (including
		// the FPCDM), we know the target type is an object type that is
		// not a primitive type.
		// According to metamodel it can thus only be a classifier. We therefore
		// clone the classifier and map it.
		Classifier missingClassifier = (Classifier) type;
		return addAndMapClassifierToArtefact(missingClassifier, fpCdmArtefact);
	}

	/**
	 * Clones a provided frcdm parameter into a fpcdm operation and also adds
	 * mappings between original and cloned parameter.
	 * 
	 * @param originalParameter as the parameter that must be cloned.
	 * @param fpcdmOperation as the (already existing) target operation in the
	 * fpcdm, that will house the parameter clone.
	 * @param fpCdmArtefact as the artefact housing (fpcdm) aspect and mappings
	 * to the original aspect (frcdm)
	 * @param mappingRequested as a flag to indicate whether the newly created
	 * parameter must be mapped to the original parameter, too.
	 * @param operationMapping as the operation mapping the parameter mapping
	 * shall be added to, if a mapping was requested. Can be null otherwise.
	 * 
	 * @return the newly created parameter clone.
	 */
	public static Parameter cloneParameter(Parameter originalParameter, Type copiedType, Operation fpcdmOperation,
			COREExternalArtefact fpCdmArtefact, boolean mappingRequested, COREMapping<Operation> operationMapping) {

		// clone the parameter
		Parameter fpcdmParameter = (Parameter) cloneElementAndName(originalParameter, "");
		fpcdmParameter.setType(copiedType);

		// add the parameter clone to the parent fpcdm operation
		fpcdmOperation.getParameters().add(fpcdmParameter);

		// if mapping requested: add a mapping from the original parameter to
		// the new frcdm parameter, as sub-mapping to the parent operation
		// mapping
		if (mappingRequested) {

			COREMapping<Parameter> parameterMapping = RamFactoryImpl.init().createParameterMapping();
			parameterMapping.setFrom(originalParameter);
			parameterMapping.setTo(fpcdmParameter);
			operationMapping.getMappings().add(parameterMapping);
		}

		return fpcdmParameter;
	}

	/**
	 * Duplicates all classifiers found in a fpcdm artefact's aspect. The copied
	 * classifiers are suffixed with "Controller".
	 * 
	 * @param fpcdmArtefact
	 */
	public void addControllers(COREExternalArtefact fpcdmArtefact, Set<Classifier> originalTargetClassfiers) {

		// Obtain list of all classifiers in fpcdm artefact
		Aspect fpcdm = (Aspect) fpcdmArtefact.getRootModelElement();

		
		// Extract reference to fpcdm classlist, so we can inject the controllers we are about to create.
		Collection<Classifier> classlist = fpcdm.getStructuralView().getClasses();


		// Create a DEEP copy of every classifier (adding the "Controller"
		// suffix to every classifier name, clone all operations and parameters,
		// ignore mappings).
		// However, change type of called classifiers from implementation
		// classes to actual classes (controllers must be generated.)
		for (Classifier fpcdmClassifier : originalTargetClassfiers) {

			// Create the new controller classifier with correct suffix
			Classifier controller = (Classifier) cloneElementAndName(fpcdmClassifier, "Controller");

			// Add all operations (and parameters) of original classifier
			for (Operation fpcdmOperation : fpcdmClassifier.getOperations()) {
				// create deep copy of operation, add it (unmapped) to new
				// controller classifier
				Operation controllerOperation = cloneOperation(fpcdmOperation, controller, null, fpcdmArtefact, false);
				controller.getOperations().add(controllerOperation);
			}

			// stage deep copy of controller as "add-to-fpcdm".
			classlist.add(controller);
		}
	}

	/**
	 * Iterates over a list of provided classifiers (all original frcdm
	 * classifiers, that contain at least one RIF-RAM mapped operation) and
	 * clones (and maps) their "getInstance" methods into the corresponding
	 * fpcdm classifiers.
	 * 
	 * @param fpcdmArtefact
	 * @param originalTargetClassifiers
	 */
	public void addAndMapClassifierSingletonAccessMethods(COREExternalArtefact fpcdmArtefact,
			Set<Classifier> originalTargetClassifiers) {

		// Iterate over all frcdm classifiers with at least one RIF-RAM mapped
		// operation.
		for (Classifier originalClassifier : originalTargetClassifiers) {

			// locate the corresponding fpcdm classifier
			Classifier clonedClassifier = resolveFpClassifierByName(originalClassifier.getName(),
					(Aspect) fpcdmArtefact.getRootModelElement(), false);

			// clone (and map) the "getInstance" method into the corresponding
			// fpcdm classifier
			cloneOperation(locateSingeltonAccess(originalClassifier), clonedClassifier, originalClassifier,
					fpcdmArtefact, true);
		}
	}

	/**
	 * Iterates over all operations of a provided classifier and returns the
	 * unique getInstance method (no parameters). If no such operation exists, a
	 * trafo-excpetion is thrown.
	 * 
	 * @param classifier as the classifier to be searched for a singleton access
	 * operation.
	 * @return the operation, in case it exists.
	 */
	@SuppressWarnings("static-method")
	private Operation locateSingeltonAccess(Classifier classifier) {

		for (Operation operation : classifier.getOperations()) {
			if (operation.getName().toLowerCase().equals("getinstance")) {
				return operation;
			}
		}
		throw new TransformerException("Can not locate singleton access method of provided classifier. "
				+ "There is no \"getInstance\" method");
	}

	/**
	 * Helper method to add RArray-String- (type) to the fpcdm RAM model.
	 * Required by call to Spring and Launchers "main" method.
	 * 
	 * @param aspect as the RAM aspect the string array shall be added to. to.
	 * @return the newly added RArray-String-.
	 */
	protected RArray addStringArrayType(Aspect aspect) {

		// Obtain reference to default / auto-generated String type.
		// This is done by iterating over all default types and searching for a
		// type with matching name.
		ObjectType stringType = (ObjectType) extractPrimitiveType("String", aspect);

		RArray stringArray = RamFactoryImpl.init().createRArray();
		stringArray.setType(stringType);
		stringArray.setName("StringArray");
		aspect.getStructuralView().getTypes().add(stringArray);
		return stringArray;
	}

	/**
	 * Helper method to extract an auto generated (existing) primitive type.
	 * Throws a runtime exception if the requested type does not exists in the
	 * provided aspect.
	 * 
	 * @param identifier as a keyword that identifies the primitive type to
	 * search for.
	 * @param aspect as the RAM aspect to be searched for the target primitive
	 * type.
	 * @return the requested type object.
	 */
	protected Type extractPrimitiveType(String identifier, Aspect aspect) {
		Type targetType = null;
		for (Type type : aspect.getStructuralView().getTypes()) {
			if (type.getName().toLowerCase().equals(identifier.toLowerCase())) {
				targetType = type;
			}
		}
		if (targetType == null) {
			throw new RuntimeException(
					"The provided fpcdm RAM model does not contain the required Type: " + identifier);
		}
		return targetType;
	}

	/**
	 * Helper method to re-extract a previously added RArray<String>.
	 * 
	 * @return RArray<String> that must have been previously added to the fpcdm.
	 */
	protected RArray extractStringArray(Aspect fpcdmAspect) {
		// Obtain reference to previously added RArray<String>
		// This is done by iterating over all registered types and searching for
		// an array type with payload of matching name.
		for (Type type : fpcdmAspect.getStructuralView().getTypes()) {
			// Here we are only interested in types that are arrays and have a
			// payload of type "String"
			String typeName = type.getName().toLowerCase();
			boolean containsArray = typeName.contains("string[]");
			if (containsArray) {
				RArray typeArray = (RArray) type;
				if (typeArray.getType().getName().toLowerCase().equals("string")) {
					return typeArray;
				}
			}
		}

		throw new RuntimeException(
				"Impossible to extract RArray<Sting>. The provided RAM model does not contain this type.");
	}

	/**
	 * Resolves a non-controller operation to the controller counterpart's proxy
	 * operation.
	 * 
	 * @param operation ram operation of a non controller.
	 * @param fpcdm as the FPCDM RAM model to search for a controller operation
	 * counterpart
	 * @return cloned controller counterpart operation of the input, if it
	 * exists. Throws transformer exception otherwise.
	 */
	public static Operation resolveToControllerOperation(Operation operation, COREExternalArtefact fpcdm) {

		// Step 1: resolve received operation to the parent classifier:
		Classifier operationClassifier = (Classifier) operation.eContainer();
		String nonControllerName = operationClassifier.getName();

		// Step 2: resolve original (non controller) classifier name to the
		// controller counterpart
		String controllerName = nonControllerName + "Controller";
		Classifier controllerClassifier = resolveFpClassifierByName(controllerName,
				(Aspect) fpcdm.getRootModelElement(), false);

		// Step 3: resolve to controller operation with matching name.
		for (Operation controllerOperation : controllerClassifier.getOperations()) {
			if (controllerOperation.getName().equals(operation.getName())) {
				return controllerOperation;
			}
		}

		// Non of the visited operations matched, something is wrong:
		throw new TransformerException("Failed to resolve operation to a matching controller operation.");
	}


    /**
     * Resolves a non-controller operation to the controller counterpart's proxy
     * parameter.
     * 
     * @param parameter
     *            ram parameter of a non controller.
     * @param fpcdm
     *            as the FPCDM RAM model to search for a controller operation
     *            counterpart
     * @return cloned controller counterpart parameter of the input, if it
     *         exists. Throws transformer exception otherwise.
     */
    public static Parameter resolveToControllerParameter(Parameter parameter, COREExternalArtefact fpcdm) {
        
        // Step 1: resolve received operation to the parent operation and classifier:
        Operation operationClassifier = (Operation) parameter.eContainer();
        Classifier parameterClassifier = (Classifier) parameter.eContainer().eContainer();
        String nonControllerName = parameterClassifier.getName();
        
        // Step 2: resolve original (non controller) classifier name to the
        // controller counterpart
        String controllerName = nonControllerName + "Controller";
        Classifier controllerClassifier = resolveFpClassifierByName(
                controllerName, (Aspect) fpcdm.getRootModelElement(), false);

        // Step 3: resolve to controller operation and parameter with matching name.
        for (Operation controllerOperation : controllerClassifier.getOperations()) {
            if (controllerOperation.getName().equals(operationClassifier.getName())) {
                for (Parameter controllerParameter : controllerOperation.getParameters()) {
                    if (controllerParameter.getName().equals(parameter.getName())) {
                        return controllerParameter;
                    }
                }
            }
        }

        // Non of the visited operations matched, something is wrong:
        throw new TransformerException(
                "Failed to resolve operation to a matching controller operation.");
    }
}
