package ca.mcgill.sel.ram.ui.scenes;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class to build pretty warning messages, decorated / framed by
 * asterisks.
 * 
 * @author Maximilian Schiedermeier
 *
 */
public class AsteriskWarningBuilder {

    // Top line of warning message. If shorten than any of the arguments
    // provided to "prontFormattedWarning", this constant also determines the
    // size of the printed card.
    private final static String WARNING_HEADER = "TRANSFORMATOR WARNING";

    // Amount of characters per payload line of block comment before a
    // line-break is automatically inserted.
    private final static int BLOCK_COMMENT_LINEBREAK = 64;

    // margin to apply to the effectively used space (amount of characters to
    // both sides of text between asterisk and used space)
    private final static int MARGIN = 2;

    /**
     * Main utility method of this class. Provide a block-comment and optional
     * command string. This method generates a nicely formated warning message
     * introduced by a header message and enclosed by asterisk borders. If the
     * block-comment exceeds the default line width, line breaks are
     * automatically inserted.
     * 
     * @param blockComment
     *            as a block comment to be neatly displayed in the generated
     *            warning.
     * @param optionalCommand
     *            as an optional command that will be printed below (without
     *            leading / trailing asterisk for convenient copy paste) and
     *            will not be block formatted.
     * @param adaptToCommandWidth
     *            to enlarge the block message in case the provided command
     *            exceeds the default message width.
     */
    protected static String generateFormattedWarning(String blockComment,
            String optionalCommand, boolean adaptToCommandWidth) {

        // Determine effective available space per line (only leading and
        // trailing asterisk excluded)

        int effectiveAvailableSpace = BLOCK_COMMENT_LINEBREAK;
        if (adaptToCommandWidth) {
            effectiveAvailableSpace = getLineLength(optionalCommand);
        }

        // Build the warning message sequentially.
        // Start with a framed centered header message
        StringBuilder warningMessageBuilder = new StringBuilder("");
        warningMessageBuilder
                .append("\n" + getAsteriskLine(effectiveAvailableSpace));
        warningMessageBuilder.append(
                getMessageLine(effectiveAvailableSpace, WARNING_HEADER, true));
        warningMessageBuilder.append(getAsteriskLine(effectiveAvailableSpace));

        // print the actual block message:
        for (String line : splitString(blockComment,
                effectiveAvailableSpace - 2)) {
            warningMessageBuilder.append(
                    getMessageLine(effectiveAvailableSpace, line, false));
        }
        warningMessageBuilder.append(optionalCommand).append("\n");
        warningMessageBuilder.append(getAsteriskLine(effectiveAvailableSpace));

        // return the constructed warning message string
        return warningMessageBuilder.toString();
    }

    /**
     * Total amount of characters taken by effective line width and both side
     * margins (asterisks excluded).
     * 
     * @return amount of characters required between leading and trailing
     *         asterisk.
     */
    private static int getLineLength(String optionalCommand) {

        // line width may increase if command exceeds default width.
        if (optionalCommand != null && !optionalCommand.isBlank()
                && optionalCommand.length() >= BLOCK_COMMENT_LINEBREAK) {

            // use block command to determine required space;
            return MARGIN + optionalCommand.length();
        } else {
            return MARGIN + BLOCK_COMMENT_LINEBREAK;
        }
    }

    /**
     * Helper method to build a full line of asterisks, taking up the exat width
     * of the overall warning message.
     * 
     * @param optionalCommand
     *            as optional user line that might lead to a dynamic increase of
     *            message width.
     * @return line filled with asterisks.
     */
    private static String getAsteriskLine(int avaiableSpace) {

        // leading asterisk
        StringBuilder asteriskLineBuilder = new StringBuilder("*");

        // as many asterisk as required to fill the effective line length
        for (int i = 0; i < avaiableSpace; i++) {
            asteriskLineBuilder.append('*');
        }
        // trailing asterisk
        asteriskLineBuilder.append('*').append('\n');
        return asteriskLineBuilder.toString();
    }

    /**
     * Encloses a provided string (must fit into available space) centered,
     * enclosed by asterisks.
     * 
     * @param availableSpace
     *            as the space available between enclosing asterisks.
     * @return formatted line with leading and trailing asterisk and centered
     *         message.
     */
    private static String getMessageLine(int availableSpace, String lineContent,
            boolean centered) {
        if (WARNING_HEADER.length() > availableSpace) {
            throw new RuntimeException(
                    "Impossible to generate nicely formatted warning message. Header does not fit.");
        }
        StringBuilder headerBuilder = new StringBuilder("");
        headerBuilder.append("*");

        // Compute how many spaces are required before and after warning message
        // string. By default the message is left aligned.
        int leadingSpaces = 1;
        if (centered) {
            leadingSpaces = (availableSpace - lineContent.length()) / 2;
        }
        int trailingSpaces = availableSpace - lineContent.length()
                - leadingSpaces;

        // Add leading spaces
        for (int i = 0; i < leadingSpaces; i++) {
            headerBuilder.append(' ');
        }

        // Add actual warning header message
        headerBuilder.append(lineContent);

        // Add trailing spaces
        for (int j = 0; j < trailingSpaces; j++) {
            headerBuilder.append(' ');
        }
        headerBuilder.append('*').append('\n');
        return headerBuilder.toString();
    }

    /**
     * Reformat singel long line into block lines, split only at whitepsaces.
     * Source:
     * http://www.davismol.net/2015/02/03/java-how-to-split-a-string-into-fixed-length-rows-without-breaking-the-words/
     * 
     * @param msg
     *            as the unfromatted input.
     * @param lineSize
     *            as the maximum allowed length per line.
     * @return list of lines that all respect max line size and no words
     *         truncated.
     */
    public static List<String> splitString(String msg, int lineSize) {
        List<String> res = new ArrayList<String>();

        Pattern p = Pattern.compile("\\b.{1," + (lineSize - 1) + "}\\b\\W?");
        Matcher m = p.matcher(msg);

        while (m.find()) {
            res.add(m.group());
        }
        return res;
    }

}
