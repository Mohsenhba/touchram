package ca.mcgill.sel.ram.ui.scenes;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.globalProcessors.CursorTracer;
import org.mt4j.sceneManagement.AbstractScene;
import org.mt4j.sceneManagement.ISceneChangeListener;
import org.mt4j.sceneManagement.Iscene;
import org.mt4j.sceneManagement.SceneChangeEvent;
import org.mt4j.sceneManagement.transition.ITransition;
import org.mt4j.util.MT4jSettings;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.ui.scenes.DisplayClassDiagramScene;
import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IClassDiagramViewHandler;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.impl.COREConcernImpl;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.menu.RamMenu;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.DomainUsecaseSplitViewWithMappings;
import ca.mcgill.sel.ram.ui.views.GenericSplitView;
import ca.mcgill.sel.ram.ui.views.GenericSplitViewWithMappings;
import ca.mcgill.sel.ram.ui.views.RifRamSplitViewWithMappings;
import ca.mcgill.sel.ram.ui.views.handler.DomainUseCaseSplitViewWithMappingsHandler;
import ca.mcgill.sel.ram.ui.views.handler.GenericSplitViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.RifRamSplitViewWithMappingsHandler;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.ram.ui.views.structural.handler.IStructuralViewHandler;
import ca.mcgill.sel.restif.ui.scenes.DisplayRestTreeScene;
import ca.mcgill.sel.restif.ui.views.RestTreeView;
import ca.mcgill.sel.restif.ui.views.handler.IRestTreeViewHandler;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseModelScene;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseDiagramViewHandler;

/**
 * RamAbstractScene is the top class which define the basic structure for
 * sub-classes with the menu and a main layer.
 *
 * @author g.Nicolas
 * @param <T> Generic type for the handler of the scene
 */
public abstract class RamAbstractScene<T extends IRamAbstractSceneHandler> extends AbstractScene
        implements ISceneChangeListener, ActionListener {

    // Actions for the default menu
    private static final String ACTION_SAVE = "action.save";
    private static final String ACTION_UNDO = "action.undo";
    private static final String ACTION_REDO = "action.redo";

    // HACK: Action to trigger the RIF/RAM trafo. I just need some button
    // anywhere so I placed it here - will be removed once the trafo algorithm
    // works and we have figures out where it belongs.
    private static final String ACTION_TRAFO = "action.trafo";
    
    // Action for entering split view
    private static final String ACTION_ENTER_SPLIT_VIEW = "action.enter.split.view";

    // Action for generic split view menu
    private static final String ACTION_SWITCH_SPLIT_ORIENTATION = "action.switch.split.orientation";
    
    // Action for toggling enable touch split button
    private static final String ACTION_TOGGLE_ENABLE_TOUCH_SPLIT = "action.toggle.enable.touch.split";

    /**
     * Handler for the scene.
     */
    protected T handler;
    /**
     * Layer in which all components will be added for the scene.
     */
    protected MTComponent containerLayer;
    /**
     * Menu.
     */
    protected RamMenu menu;
    /**
     * Navigation Bar Singleton.
     */
    protected NavigationBar navbar;
    
    /**
     * The current view that is on the screen in the given scene.
     */
    protected MTComponent currentView;
    
    /**
     * A hashmap which should link a given view to the base eobject of that view.
     */
    protected Map<MTComponent, EObject> viewToObject;
    
    /**
     * The main object that is the basis of the scene. For a class diagram scene it is the class diagram etc.
     */
    protected EObject sceneRootModelObject;
    
    /**
     * The artefact associated with this scene.
     */
    protected COREArtefact artefact;  
    
    private final RamApp application;
    private Iscene previousScene;
    private Iscene previousNextScene;
    private boolean defaultActions;
    private CommandStackListener commandStackListener;
    private MTComponent topLayer;
    private MTComponent navLayer;
    private List<RamRectangleComponent> tempComponents;
    
    /**
     * Constructs a RamAbstractScene with the default actions in the menu.
     * 
     * @param application - The current {@link RamApp}
     * @param name - the name of the scene
     */
    public RamAbstractScene(final RamApp application, final String name) {
        this(application, name, null, true);
    }
    
    /**
     * Constructs a RamAbstractScene with the default actions in the menu and a specific artefact.
     * 
     * @param application - The current {@link RamApp}
     * @param name - the name of the scene
     * @param artefact - the artefact
     */
    public RamAbstractScene(final RamApp application, final String name, COREExternalArtefact artefact) {
        this(application, name, artefact, true);
    }
    
    /**
     * Constructs a RamAbstractScene with a flag for default actions.
     * 
     * @param application - The current {@link RamApp}
     * @param name - the name of the scene
     * @param addDefaultActions - true if you want default actions in the menu
     */
    public RamAbstractScene(final RamApp application, final String name, boolean addDefaultActions) {
        this(application, name, null, addDefaultActions);
    }

    /**
     * Constructs a RamAbstractScene.
     *
     * @param application - The current {@link RamApp}
     * @param name - the name of the scene
     * @param artefact - the artefact
     * @param addDefaultActions - true if you want default actions in the menu
     */
    public RamAbstractScene(final RamApp application, final String name, COREExternalArtefact artefact, 
            boolean addDefaultActions) {
        super(application, name);

        this.application = application;
        this.artefact = artefact;
        this.defaultActions = addDefaultActions;
        this.tempComponents = new ArrayList<RamRectangleComponent>();
        this.viewToObject = new HashMap<MTComponent, EObject>();
        // create scene
        setClearColor(Colors.BACKGROUND_COLOR);
        registerGlobalInputProcessor(new CursorTracer(application, this));
        application.addSceneChangeListener(this);
        
        initDisplay();
    }

    /**
     * Initialize the scene creating the menu, positioning it, adding default
     * actions if needed and layers initialization.
     */
    private void initDisplay() {
        this.menu = new RamMenu();
        this.menu.setMenuInTopRight(getWidth());
        if (this.defaultActions) {
            addDefaultActionsInMenu();
        }
        this.navbar = NavigationBar.getInstance();

        initMenu();

        // menus and the items that we want to put on top of the structural view
        // are added in this layer
        topLayer = new MTComponent(application, "top layer group");
        navLayer = new MTComponent(application, "nav layer group");
        // views are added into this layer
        containerLayer = new MTComponent(application, "container layer");

        topLayer.addChild(this.containerLayer);
        topLayer.addChild(this.menu);
        navLayer.addChild(this.navbar);
        
        menu.addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), topLayer));
        getCanvas().addChild(topLayer);
        
        getCanvas().addChild(navLayer);
        
    }

    /**
     * Initialize the menu.
     */
    protected abstract void initMenu();

    /**
     * Add the default actions in the menu and initialize them.
     */
    protected void addDefaultActionsInMenu() {
        this.menu.addAction(Strings.MENU_SAVE, Icons.ICON_MENU_SAVE, ACTION_SAVE, this, true);
        this.menu.addAction(Strings.MENU_UNDO, Icons.ICON_MENU_UNDO, ACTION_UNDO, this, true);
        this.menu.addAction(Strings.MENU_REDO, Icons.ICON_MENU_REDO, ACTION_REDO, this, true);

        // HACK - extra button for my trafo
        this.menu.addAction(Strings.MENU_TRAFO, Icons.ICON_MENU_TRAFO, ACTION_TRAFO, this, true);

        // extra button for toggle split view
        if (this instanceof DisplayAspectScene || this instanceof DisplayRestTreeScene 
                || this instanceof DisplayUseCaseModelScene || this instanceof DisplayClassDiagramScene) {
            this.menu.addAction(Strings.MENU_ENTER_SPLIT_VIEW, Icons.ICON_MENU_ENTER_SPLIT_VIEW, 
                    ACTION_ENTER_SPLIT_VIEW, this, true);
        }
        
        if (getElementToSave() != null && commandStackListener == null) {
            setCommandStackListener(getElementToSave());
        }
    }

    /**
     * Function called to add a component to the topLayer.
     * 
     * @param rectangle - the rectangle to add
     * @param location - where to put the selector
     */
    public void addComponent(RamRectangleComponent rectangle, Vector3D location) {
        topLayer.addChild(rectangle);
        // Position the selector centered on the menu
        location.x -= rectangle.getWidth() / 2;
        location.y -= rectangle.getHeight() / 2;

        if ((location.getX() + rectangle.getWidth()) > getWidth()) {
            location.x -= (location.getX() + rectangle.getWidth()) - getWidth();
        } else if (location.getX() < 0) {
            location.x -= location.x;
        }
        if ((location.getY() + rectangle.getHeight()) > getHeight()) {
            location.y -= (location.getY() + rectangle.getHeight()) - getHeight();
        } else if (location.getY() < 0) {
            location.y -= location.y;
        }
        rectangle.translate(location.getX(), location.getY());
        this.tempComponents.add(rectangle);
    }

    /**
     * Initialize a listener for the commandStack.
     * 
     * @param elementToSave - The element that will be modified.
     */
    protected final void setCommandStackListener(EObject elementToSave) {
        if (elementToSave == null || commandStackListener != null) {
            return;
        }
        // Add listener to command stack for updating of Save/Redo/Undo buttons.
        final BasicCommandStack commandStack = EMFEditUtil.getCommandStack(elementToSave);
        updateDefaultActionsStatus(commandStack);
        commandStackListener = new CommandStackListener() {
            @Override
            public void commandStackChanged(EventObject event) {
                // disable/enable buttons depending on whether undo/redo is
                // possible
                updateDefaultActionsStatus(commandStack);
            }
        };
        commandStack.addCommandStackListener(commandStackListener);
    }
    
    /**
     * Update default actions state in the RamMenu in function of the command
     * stack.
     * 
     * @param commandStack - the command stack
     */
    private void updateDefaultActionsStatus(BasicCommandStack commandStack) {
        RamButton rb = menu.getAction(ACTION_SAVE);
        if (rb != null) {
            rb.setEnabled(commandStack.isSaveNeeded());
        }
        rb = menu.getAction(ACTION_UNDO);
        if (rb != null) {
            rb.setEnabled(commandStack.canUndo());
        }
        rb = menu.getAction(ACTION_REDO);
        if (rb != null) {
            rb.setEnabled(commandStack.canRedo());
        }
    }

    /**
     * Method used to retrieve the element to save in the current scene.
     *
     * @return The EObject we want to save for this scene.
     */
    protected abstract EObject getElementToSave();

    @Override
    public boolean destroy() {
        if (commandStackListener != null) {
            EMFEditUtil.getCommandStack(getElementToSave()).removeCommandStackListener(commandStackListener);
        }
        
        application.removeSceneChangeListener(this);
        return super.destroy();
    }

    /**
     * Display the given {@link RamPopup}.
     *
     * @param popup - The {@link RamPopup} to display.
     */
    public void displayPopup(final RamPopup popup) {
        popup.translate((getWidth() - popup.getWidth()) / 2, (getHeight() - popup.getHeight()) / 2);
        getCanvas().addChild(popup);
    }

    /**
     * Display a {@link RamPopup}.
     *
     * @param popup - The text to display in the popup.
     */
    public void displayPopup(final String popup) {
        displayPopup(new RamPopup(popup, true));
    }

    /**
     * Display a {@link RamPopup}.
     *
     * @param popup - The text to display in the popup.
     * @param popupType - The type of popup.
     */
    public void displayPopup(final String popup, PopupType popupType) {
        displayPopup(new RamPopup(popup, true, popupType));
    }

    /**
     * Get the RamMenu.
     *
     * @return the RamMenu
     */
    public RamMenu getMenu() {
        return menu;
    }
    
    /**
     * Get the {@link RamApp}.
     *
     * @return the RamApp
     */
    public RamApp getApplication() {
        return application;
    }

    /**
     * Get the height of the app.
     * 
     * @return the height.
     */
    public float getHeight() {
        return application.getHeight();
    }

    /**
     * Get the width of the app.
     * 
     * @return the width.
     */
    public float getWidth() {
        return application.getWidth();
    }
    
    public COREArtefact getArtefact() {
        return artefact;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (handler != null) {
            String actionCommand = event.getActionCommand();
            if (ACTION_SAVE.equals(actionCommand)) {
                handler.save(getElementToSave());
            } else if (ACTION_UNDO.equals(actionCommand)) {
                handler.undo(getElementToSave());
            } else if (ACTION_REDO.equals(actionCommand)) {
                handler.redo(getElementToSave());
            } else if (ACTION_SWITCH_SPLIT_ORIENTATION.equals(actionCommand)) {
                // get the generic split view from the scene
                GenericSplitView<?, ?, ?> genericSplitView = (GenericSplitView<?, ?, ?>) RamApp.getActiveScene()
                        .getContainerLayer().getChildByIndex(0).getChildByIndex(0);

                // switch the split orientation of the generic split view
                ((GenericSplitViewHandler<?, ?, ?>) genericSplitView.getHandler()).switchSplitOrientation();
            } else if (ACTION_TOGGLE_ENABLE_TOUCH_SPLIT.equals(actionCommand)) {
                // get the generic split view from the scene
                GenericSplitViewWithMappings<?, ?, ?> genericSPlitViewWithMappings = (GenericSplitViewWithMappings
                        <?, ?, ?>) RamApp.getActiveScene().getContainerLayer().getChildByIndex(0).getChildByIndex(0);
            
                // display appropriate message based on touchEnabled for split view
                if (genericSPlitViewWithMappings.isTouchEnabled()) {
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_SPLIT_TOUCH_DISABLED);
                } else {
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_SPLIT_TOUCH_ENABLED);
                }
                
                // set touchEnabled to the opposite of what it was on split view
                genericSPlitViewWithMappings.setTouchEnabled(!genericSPlitViewWithMappings.isTouchEnabled());
            } else if (ACTION_TRAFO.equals(actionCommand)) {
                
                // HACK (until we have figured out where the trafo button
                // belongs): handling the TRAFO click event.
                System.out.println("RamAbstractScene: Registered a click on the transformer button.");

                // Retrieve the Model that is currently loaded (when the Trafo
                // button is clicked.)
                COREConcernImpl inputModel = (COREConcernImpl) NavigationBar.getInstance().getCurrentConcern();
                
                // Find the selected Restify implementation from the user's concern reuses.
                RestifyImplementation selectedRestifyImplementation = TrafoUtils
                        .getSelectedRestifyImplementation(inputModel);
                
                // if there is no valid restify implementation, display error message and return
                if (selectedRestifyImplementation == RestifyImplementation.NONE) {
                    RamApp.getActiveScene().displayPopup("Error. There is no valid selected Restify implementation." 
                            + "Please reuse the Restify concern, and select a valid implementation.");
                    
                    return;
                }

                // Create and place a new FP-CDM in the present concern. The
                // newly generated CDM will contain a CoreModelExtension with
                // weaver-interpretable mappings onto the existing FR-CDM
                // (pre-existing RAM)
                COREExternalArtefact fpcdm = TrafoUtils.transform(inputModel, selectedRestifyImplementation);
                
                // Fiddle the two RAM models together (fpcdms extension links to source model, therefore only one argument is required.
                TrafoUtils.weaveAndGenerate(fpcdm, selectedRestifyImplementation);
                
            } else if (ACTION_ENTER_SPLIT_VIEW.equals(actionCommand)) {
                /** 
                 * The following class is taken from {@link FeatureEditModeHandler} 
                 * handleOpenModelSingleScene(COREScene s, COREPerspective selectedPerspective, 
                 * Vector3D selectorLocation) method.
                 * 
                 * It is modified to display other artefacts of the current COREScene with a 
                 * {@link RamSelectorComponent} to enter the split view with, if there are no
                 * other artefacts, then display an error message.
                 */
                
                final class ModelSelectorNamer implements Namer<COREArtefact> {
                    @Override
                    public RamRectangleComponent getDisplayComponent(COREArtefact element) {
                        String name = element.getName();
                        // look for the role name of this artefact in the scene
                        COREScene scene = element.getScene();
                        for (String role : scene.getArtefacts().keySet()) {
                            if (scene.getArtefacts().get(role).contains(element)) {
                                name = name + " (" + role + ")";
                                break;
                            }
                        }            
                        RamTextComponent textComponent = new RamTextComponent(name);
                        textComponent.setNoFill(false);
                        textComponent.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
                        textComponent.setAutoMaximizes(true);
                        textComponent.setNoStroke(false);
                        textComponent.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
                        return textComponent;
                    }

                    @Override
                    public String getSortingName(COREArtefact element) {
                        return element.getName();
                    }

                    @Override
                    public String getSearchingName(COREArtefact element) {
                        return element.getName();
                    }
                }
                
                
                RamSelectorComponent<COREArtefact> selector;
                selector = new RamSelectorComponent<COREArtefact>(new ModelSelectorNamer());

                // get current scene from navigation bar
                COREScene currentScene = NavigationBar.getInstance().getCurrentScene();
                List<COREArtefact> artefacts = new ArrayList<>();
                
                // sets the current model role name.
                for (String artefactMapKey : currentScene.getArtefacts().keySet()) {
                    for (COREArtefact coreArtefact : currentScene.getArtefacts().get(artefactMapKey)) {
                        if (coreArtefact != NavigationBar.getInstance().getCurrentArtefact()) {
                            artefacts.add(coreArtefact);
                        }
                    }
                }
                
                EList<COREModelElementMapping> mappings = currentScene.getElementMappings();
                
                selector.setElements(artefacts);

                if (artefacts.size() > 0) {
                    this.addComponent(selector, new Vector3D(this.getWidth() / 2, this.getHeight() / 2));
                    
                    RamAbstractScene<T> firstScene = this;
                    
                    selector.registerListener(new AbstractDefaultRamSelectorListener<COREArtefact>() {
                        
                        @Override
                        public void elementSelected(RamSelectorComponent<COREArtefact> selector, 
                                COREArtefact element) {
                            
                            // load the second model from the element selected
                            EObject secondModel = ((COREExternalArtefact) element).getRootModelElement();
                            // get the scene from the model
                            RamAbstractScene<IDisplaySceneHandler> secondScene = RamApp.getApplication()
                                    .getExistingOrCreateModelScene(secondModel);
                            
                            // get the view from the model
                            AbstractView<? extends IAbstractViewHandler> secondView = RamApp.getApplication()
                                    .createViewFromModel(secondModel);
                            
                            // check which split view to create based on first and second scenes' types
                            if (firstScene instanceof DisplayAspectScene) {
                                if (secondView instanceof RestTreeView) {
                                    // create generic split view
                                    RifRamSplitViewWithMappings<IDisplaySceneHandler, IRestTreeViewHandler, 
                                        IStructuralViewHandler> rifRamSplitView =
                                        new RifRamSplitViewWithMappings<>((DisplayAspectScene) firstScene,
                                            (RestTreeView) secondView, false, mappings);
                                    
                                    // create handler
                                    RifRamSplitViewWithMappingsHandler<IDisplaySceneHandler, IRestTreeViewHandler, 
                                        IStructuralViewHandler> handler = new RifRamSplitViewWithMappingsHandler<>
                                        (rifRamSplitView);
                                    
                                    // set handler
                                    rifRamSplitView.setHandler(handler);
                                    
                                    // switch to split view
                                    ((DisplayAspectScene) firstScene).switchToView(rifRamSplitView);
                                    
                                    // remove add split view button
                                    RamApp.getActiveScene().getMenu().removeAction(ACTION_ENTER_SPLIT_VIEW);
                                }
                            } else if (firstScene instanceof DisplayRestTreeScene) {
                                if (secondView instanceof StructuralDiagramView) {
                                    // create generic split view
                                    RifRamSplitViewWithMappings<IDisplaySceneHandler, IStructuralViewHandler, 
                                        IRestTreeViewHandler> rifRamSplitView = new RifRamSplitViewWithMappings<>(
                                            (DisplayRestTreeScene) firstScene, (StructuralDiagramView) secondView, 
                                            false, mappings);
                                    
                                    // create handler
                                    RifRamSplitViewWithMappingsHandler<IDisplaySceneHandler, IStructuralViewHandler, 
                                        IRestTreeViewHandler> handler = new RifRamSplitViewWithMappingsHandler<>(
                                                rifRamSplitView);
                                
                                    // set handler
                                    rifRamSplitView.setHandler(handler);
                                    
                                    // switch to split view
                                    ((DisplayRestTreeScene) firstScene).switchToView(rifRamSplitView);
                                    
                                    // remove add split view button
                                    RamApp.getActiveScene().getMenu().removeAction(ACTION_ENTER_SPLIT_VIEW);
                                }
                            } else if (firstScene instanceof DisplayUseCaseModelScene) {                                
                                if (secondView instanceof ClassDiagramView) {
                                    // create generic split view
                                    DomainUsecaseSplitViewWithMappings<IDisplaySceneHandler, IClassDiagramViewHandler, 
                                        IUseCaseDiagramViewHandler> domainUsecaseSplitView = 
                                        new DomainUsecaseSplitViewWithMappings<>((DisplayUseCaseModelScene) firstScene, 
                                            (ClassDiagramView) secondView, false, mappings);
                                    
                                    // create handler
                                    DomainUseCaseSplitViewWithMappingsHandler<IDisplaySceneHandler, 
                                        IClassDiagramViewHandler, IUseCaseDiagramViewHandler> handler = new 
                                            DomainUseCaseSplitViewWithMappingsHandler<>(domainUsecaseSplitView);
                                    
                                    // set handler
                                    domainUsecaseSplitView.setHandler(handler);
                                    
                                    // switch to split view
                                    ((DisplayUseCaseModelScene) firstScene).switchToView(domainUsecaseSplitView, false);
                                    
                                    // remove add split view button
                                    RamApp.getActiveScene().getMenu().removeAction(ACTION_ENTER_SPLIT_VIEW);
                                }
                            } else if (firstScene instanceof DisplayClassDiagramScene) {
                                if (secondView instanceof UseCaseDiagramView) {
                                    // create generic split view
                                    DomainUsecaseSplitViewWithMappings<IDisplaySceneHandler, 
                                        IUseCaseDiagramViewHandler, IClassDiagramViewHandler> domainUsecaseSplitView = 
                                        new DomainUsecaseSplitViewWithMappings<>((DisplayClassDiagramScene) firstScene, 
                                            (UseCaseDiagramView) secondView, false, mappings);
                                
                                    // create handler
                                    DomainUseCaseSplitViewWithMappingsHandler<IDisplaySceneHandler, 
                                        IUseCaseDiagramViewHandler, IClassDiagramViewHandler> handler = new 
                                        DomainUseCaseSplitViewWithMappingsHandler<>(domainUsecaseSplitView);
                                    
                                    // set handler
                                    domainUsecaseSplitView.setHandler(handler);
                                    
                                    // switch to split view
                                    ((DisplayClassDiagramScene) firstScene).switchToView(domainUsecaseSplitView);
                                    
                                    // remove add split view button
                                    RamApp.getActiveScene().getMenu().removeAction(ACTION_ENTER_SPLIT_VIEW);
                                }
                            }
                            selector.destroy();
                        }
                    });
                } else {
                    // display cannot enter split view if there are no other artefacts in the current COREScene
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_CANNOT_ENTER_SPLIT_VIEW);
                }
            }
        }
    }

    /**
     * Get the container layer.
     * 
     * @return the container layer
     */
    public MTComponent getContainerLayer() {
        return containerLayer;
    }

    /**
     * Get the RamAbstractScene handler.
     * 
     * @return the scene handler
     */
    public T getHandler() {
        return handler;
    }

    /**
     * Set the RamAbstractScene handler.
     * 
     * @param handler - the RamAbstractScene handler
     */
    public void setHandler(T handler) {
        this.handler = handler;
    }

    @Override
    public void setTransition(final ITransition transition) {
        if (MT4jSettings.getInstance().isOpenGlMode()) {
            super.setTransition(transition);
        }
        // P3D rendering doesn't seem to support transitions
    }

    /**
     * Ask this scene whether it is acceptable to exit the application.
     * Sub-classes can override this method in order to implement certain checks
     * that make sure that is is acceptable for that particular scene to exit.
     * For example, this can include requesting the user to confirm.
     * 
     * @return true, if this scene can be quit, false otherwise
     */
    public boolean shouldExit() {
        return true;
    }

    /**
     * Returns the previous scene. The previous scene is the scene previously
     * switched from to this one.
     * 
     * @return the previous scene
     */
    public Iscene getPreviousScene() {
        return previousScene;
    }
    
    public MTComponent getCurrentView() {
        return currentView;
    }
    public void setCurrentView(MTComponent view) {
        currentView = view;
    }
    
    public Map<MTComponent, EObject> getViewToObject() {
        return viewToObject;
    }
    
    public EObject getSceneRootModelObject() {
        return sceneRootModelObject;
    }

    @Override
    public void processSceneChangeEvent(SceneChangeEvent sc) {
        // Also store the previous next scene (the scene switched to from this
        // one),
        // because when coming back from the previous scene, it should not be
        // overwritten with the last scene (switched from), but the actual
        // previous scene should be kept.
        if (sc.getNewScene() == this && previousNextScene != sc.getLastScene()) {
            previousScene = sc.getLastScene();
        } else if (sc.getLastScene() == this) {
            previousNextScene = sc.getNewScene();
        }
    }

    /**
     * Removes all temporary components on the top Layer. It can be selectors,
     * menus, etc...
     */
    protected void clearTemporaryComponents() {
        for (RamRectangleComponent rect : this.tempComponents) {
            rect.destroy();
        }
    }

}
