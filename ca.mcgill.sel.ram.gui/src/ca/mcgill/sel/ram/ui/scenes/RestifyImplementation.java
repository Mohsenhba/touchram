package ca.mcgill.sel.ram.ui.scenes;

/**
 * Enum to represent currently supported Restify implementations.
 * 
 * @author Bowen
 */
public enum RestifyImplementation {
    /**
     * Spring Boot implementation.
     */
    SPRING_BOOT,
    /**
     * Jersey implementation.
     */
    JERSEY,
    /**
     * JBoss RESTEasy implementation.
     */
    JBOSS_RESTEASY,
    /**
     * Apache CXF implementation.
     */
    APACHE_CXF,
    /**
     * No implementation, this is used when the user has not yet selected a Restify implementation in the concern reuse.
     */
    NONE
}
