package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import org.eclipse.emf.common.command.CompoundCommand;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.controller.AssociationController;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.utils.Strings;

public class AssociationConcernSelectSceneHandler extends ConcernSelectSceneHandler {
    
    private Aspect aspect;
    private AssociationEnd associationEnd;
    private CompoundCommand command;
    
    public AssociationConcernSelectSceneHandler(Aspect aspect, AssociationEnd associationEnd, CompoundCommand command) {
        this.aspect = aspect;
        this.associationEnd = associationEnd;
        this.command = command;
    }
    @Override
    public void reuse(DisplayConcernSelectScene scene) {
        scene.displayPopup(Strings.POPUP_REUSING);
        AssociationController associationController = ControllerFactory.INSTANCE
                .getAssociationController();

        // Restore the hidden features.
        command.undo();

        associationController.changeFeatureSelection(aspect, associationEnd,
                SelectionsSingleton.getInstance().getSelectedConfiguration());

        switchToPreviousSceneWithoutUndo(scene);
        SelectionsSingleton.getInstance().clearAll();
    }

    @Override
    public void switchToPreviousScene(DisplayConcernSelectScene scene) {
        super.switchToPreviousScene(scene);

        // Restore the hidden features.
        command.undo();
    }

    public void switchToPreviousSceneWithoutUndo(DisplayConcernSelectScene scene) {
        super.switchToPreviousScene(scene);
    }
}
