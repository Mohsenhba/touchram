package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.sceneManagement.transition.BlendTransition;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.language.weaver.COREWeaver;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene.DisplayMode;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature.FeatureSelectionStatus;

/**
 * Handler that handles events occurring in the feature select scene when reusing a concern.
 *
 * @author Nishanth
 * @author mschoettle
 * @author cbensoussan
 * @author oalam
 */
public class ConcernSelectSceneHandler implements IConcernSelectSceneHandler {

    /**
     * Duration for the slide transition.
     */
    private static final int TRANSITION_DURATION = 700;

    @Override
    public void switchToPreviousScene(DisplayConcernSelectScene scene) {
        SelectionsSingleton.getInstance().clearAll();
        //TODO: arthur -> this makes it bug with the current merge, temporary situation
        //deleteReuseFolder(scene);
        scene.unLoadAllResources();
        scene.setTransition(new BlendTransition(RamApp.getApplication(), TRANSITION_DURATION));
        SceneCreationAndChangeFactory.getFactory().changeSceneAndUpdate(scene.getPreviousScene());
        scene.getApplication().destroySceneAfterTransition(scene);
    }

    @Override
    public void reuse(DisplayConcernSelectScene scene) {
        try {
            if (scene.hasClashes() || !scene.getCanReuse()) {
                return;
            }

            scene.displayPopup(Strings.POPUP_REUSING);
            
            COREConcern currentConcern = NavigationBar.getInstance().getCurrentConcern();
            
            COREExternalArtefact reusingArtefact = scene.getReusingArtefact();

            COREConcern reusedConcern = scene.getConcern();
            
            COREConfiguration configuration = SelectionsSingleton.getInstance().getSelectedConfiguration();
            
            COREReuse associatedReuse = RamApp.getActiveConcernSelectScene().getAssociatedReuse();
            
            // if the currently displayed reused concern has an associated core reuse, we simply update the 
            // configuration of all model reuses associated with the core reuse
            if (associatedReuse != null) {
                COREControllerFactory.INSTANCE.getReuseController().updateCOREConfiguration(currentConcern, 
                        associatedReuse, configuration);
                                
                SelectionsSingleton.getInstance().clearAll();
                switchToPreviousScene(scene);
                
                return;
            }
                        
            Map<COREReuse, COREConfiguration> reuseDefaultConfiguration = 
                    SelectionsSingleton.getInstance().getReuseDefaultConfiguration();
            
            Map<COREReuse, Map<COREFeature, FeatureSelectionStatus>> reuseUserConfiguration = 
                    SelectionsSingleton.getInstance().getReuseUserConfiguration();
            
            // For every reuse that is within the selection, with it's default merged config
            for (Entry<COREReuse, COREConfiguration> entry : reuseDefaultConfiguration.entrySet()) {
                COREReuse reuse = entry.getKey();
                COREConfiguration extendingConfig = entry.getValue();
                
                // if the user has made a selection on that reuse
                if (reuseUserConfiguration.containsKey(reuse)) {
                    // get the user selections
                    Map<COREFeature, FeatureSelectionStatus> selections = reuseUserConfiguration.get(reuse);
                    
                    // apply that selection
                    for (Map.Entry<COREFeature, FeatureSelectionStatus> entrySelect : selections.entrySet()) {
                        COREFeature feature = entrySelect.getKey();
                        FeatureSelectionStatus status = entrySelect.getValue();
                        
                        if (status == FeatureSelectionStatus.SELECTED) {
                            extendingConfig.getReexposed().remove(feature);
                            extendingConfig.getSelected().add(feature);
                        } else if (status == FeatureSelectionStatus.NOT_SELECTED) {
                            extendingConfig.getReexposed().remove(feature);
                        }
                        
                    }
                    
                    // Add the configuration that has been modified by the user to the extending configuration list
                    configuration.getExtendingConfigurations().add(extendingConfig);
                    
                }
            }
            
            // if reusingArtefact is null, then the current scene is DisplayConcernEditScene, from here, we create
            // a new core reuse without the RAM model - Bowen
            if (reusingArtefact == null) {                
                COREControllerFactory.INSTANCE.getReuseController().createNewReuseAndModelReuseInDummyReuseArtefact(
                        currentConcern, reusedConcern, configuration);
            } else {
                COREConcern reusingConcern = reusingArtefact.getCoreConcern();

                COREExternalArtefact wovenArtefact = 
                        COREWeaver.getInstance().weaveReuse(reusingArtefact, reusedConcern, configuration);
    
                if (scene.getExtendingReuse() != null) {
                    COREControllerFactory.INSTANCE.getReuseController().createModelReuse(reusingArtefact, 
                            wovenArtefact, configuration, scene.getExtendingReuse());
                } else {
                    COREControllerFactory.INSTANCE.getReuseController().createNewReuseAndModelReuse(reusingArtefact, 
                            reusingConcern, reusedConcern, wovenArtefact, configuration);
                }
            }

            SelectionsSingleton.getInstance().clearAll();
            switchToPreviousScene(scene);

            // CHECKSTYLE:IGNORE IllegalCatch: Many exceptions can occur and we don't want to crash the application.
        } catch (Exception e) {
            scene.displayPopup(Strings.POPUP_ERROR_REUSE, PopupType.ERROR);
            e.printStackTrace();
        }
    }

    /**
     * Creates the model reuse from the reused concern and the given woven model.
     * 
     * @param reusingArtefact The reusing aspect.
     * @param wovenArtefact The woven aspect.
     * @param concern The reused concern.
     */
    public void createModelReuse(COREExternalArtefact reusingArtefact, COREExternalArtefact wovenArtefact, 
            COREConcern concern) {
        COREConfiguration configuration = SelectionsSingleton.getInstance().getSelectedConfiguration();
        COREConcern reusingConcern = reusingArtefact.getCoreConcern();
        
        COREControllerFactory.INSTANCE.getReuseController()
                .createNewReuseAndModelReuse(reusingArtefact, reusingConcern, concern, wovenArtefact, configuration);
    }

    @Override
    public void clear(DisplayConcernSelectScene scene) {
        // clear selections
        SelectionsSingleton.getInstance().resetSelection();
        scene.drawFeatureDiagram(true);
    }

    @Override
    public void switchMode(DisplayConcernSelectScene scene) {
        if (scene.getCurrentMode().equals(DisplayMode.NEXT)) {
            scene.setCurrentMode(DisplayMode.FULL);
        } else if (scene.getCurrentMode().equals(DisplayMode.FULL)) {
            scene.setCurrentMode(DisplayMode.NEXT);
        }
        scene.updateModeButtons();
        scene.drawFeatureDiagram(true);
    }

    @Override
    public void save(EObject element) {
        // unused
    }

    @Override
    public void undo(EObject element) {
        // unused
    }

    @Override
    public void redo(EObject element) {
        // unused
    }
    
    public boolean isInputConcernReuseAlreadyReused(COREConcern inputConcernReuse) {        
        COREConcern currentConcern = NavigationBar.getInstance().getCurrentConcern();
        for (COREReuse reuse : currentConcern.getReuses()) {
            COREConcern reusedConcern = reuse.getReusedConcern();
            
            if (inputConcernReuse.equals(reusedConcern)) {
                return true;
            }
        }
        return false;
    }
}
