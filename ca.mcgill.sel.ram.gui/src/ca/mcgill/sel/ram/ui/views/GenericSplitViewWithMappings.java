package ca.mcgill.sel.ram.ui.views;

import java.util.HashMap;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.LanguageElementMappingProvider;

/**
 * This is a generic view used for composition and individual view editing by seeing higher and lower level views 
 * at the same time. This class contains additional functionality for displaying and editing mappings and it extends
 * {@link GenericSplitView}. This is an abstract class and need to be extended to be used.
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IAbstractViewHandler} for the outer level view
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 */
public abstract class GenericSplitViewWithMappings<S extends IRamAbstractSceneHandler, T extends IAbstractViewHandler, 
    U extends IAbstractViewHandler> extends GenericSplitView<S, T, U> implements INotifyChangedListener {

    // hashmap to store mappings between the inner and outer views and the lines attached to them
    private HashMap<COREModelElementMapping, RamLineComponent> mappingToLinesMap;
    
    // boolean to store whether gestures within individual views are enabled or not
    private boolean touchEnabled;
    
    // helper class to store the next LEMid for a new mapping
    private LanguageElementMappingProvider languageElementMappingProvider;
    
    public GenericSplitViewWithMappings(RamAbstractScene<S> displaySceneInnerLevel,
            AbstractView<T> outerLevelView, boolean isHorizontal, EList<COREModelElementMapping> mappings) {
        super(displaySceneInnerLevel, outerLevelView, isHorizontal);

        // add a button to enable/disable gestures within individual views
        RamApp.getActiveScene().getMenu().addAction(Strings.MENU_TOGGLE_ENABLE_TOUCH_SPLIT, 
                Icons.ICON_MENU_TOGGLE_ENABLE_TOUCH_SPLIT, "action.toggle.enable.touch.split",
                RamApp.getActiveScene(), true);
        
        this.mappingToLinesMap = new HashMap<>();
        this.touchEnabled = false;
        this.languageElementMappingProvider = new LanguageElementMappingProvider(mappings);
        
        // display lines between mappings
        displayMappings(mappings);
        
        // add listener for the current COREScene
        EMFEditUtil.addListenerFor(NavigationBar.getInstance().getCurrentScene(), this);
    }
    
    /**
     * Displays lines between mappings.
     * 
     * @param mappings the current mappings between the two models
     */
    public void displayMappings(EList<COREModelElementMapping> mappings) {
        if (mappings != null) {
            // loop through every mapping
            for (COREModelElementMapping mapping : mappings) {
                EList<EObject> mappingElements = mapping.getModelElements();
                
                RamRectangleComponent firstMappedElement = null;
                RamRectangleComponent secondMappedElement = null;
                
                // a mapping always consists of two mapped elements
                if (mappingElements.size() == 2) {
                    firstMappedElement = getViewOf(mappingElements.get(0));
                    secondMappedElement = getViewOf(mappingElements.get(1));
                }
                
                // draw lines between firstMappedElement and secondMappedElement if they both exist
                if (firstMappedElement != null && secondMappedElement != null) {
                    // get x and y values from the mapped elements (modified for visual purposes)
                    Vector3D firstMappedElementXY = getModifiedXYFromView(firstMappedElement);
                    Vector3D secondMappedElementXY = getModifiedXYFromView(secondMappedElement);
                    
                    boolean firstMappedElementVisible;
                    boolean secondMappedElementVisible;
                                
                    // check which view the firstMappedElement is in
                    boolean firstElementWithinInnerView = checkElementWithinInnerView(firstMappedElement);
                    
                    // check if the first and second mapped element are visible in their respective views
                    if (firstElementWithinInnerView) {
                        firstMappedElementVisible = checkViewIsVisible(firstMappedElementXY, true);
                        secondMappedElementVisible = checkViewIsVisible(secondMappedElementXY, false);
                    } else {
                        firstMappedElementVisible = checkViewIsVisible(firstMappedElementXY, false);
                        secondMappedElementVisible = checkViewIsVisible(secondMappedElementXY, true);
                    }
                    
                    RamLineComponent mappingLine;
                    
                    // create a mapping line based on the visibility of the first and second mapped elements
                    // if they are both visible, create a line between them
                    // if only one mapped element is visible, draw a short stippled line from it
                    // if there are no mapped elements visible, do not draw anything
                    if (firstMappedElementVisible) {
                        if (secondMappedElementVisible) {                        
                            mappingLine = new RamLineComponent(firstMappedElementXY.getX(),
                                    firstMappedElementXY.getY(), secondMappedElementXY.getX(),
                                    secondMappedElementXY.getY());
                        } else {
                            mappingLine = createStippledLine(firstMappedElementXY, firstMappedElement);
                        }
                    } else {
                        if (secondMappedElementVisible) {
                            mappingLine = createStippledLine(secondMappedElementXY, secondMappedElement);
                        } else {                        
                            mappingLine = new RamLineComponent(0, 0, 0, 0);
                        }
                    }
                    
                    // set the color of the line based on the type of mapping
                    // this is used to differentiate from different mapping types
                    setLineColorBasedOnMapping(mappingLine, firstMappedElement, secondMappedElement);
                    
                    // add the mapping and mapping line to the hash map
                    mappingToLinesMap.put(mapping, mappingLine);
                    // add the mapping line to the container layer to display it
                    containerLayer.addChild(mappingLine);
                }
            }
        }
    }
    
    /**
     * Checks if a view is visible or not in the current window. This method checks which side of the split view 
     * it is, and whether the split line is horizontal, then compares the position of the view to the appropriate 
     * boundaries.
     * 
     * @param positionOfView the x and y positions of the view being checked
     * @param withinInnerView a boolean to indicate whether or not the view is within inner view or not
     */
    public boolean checkViewIsVisible(Vector3D positionOfView, boolean withinInnerView) {
        if (!isHorizontal) {
            if (withinInnerView) {
                if (positionOfView.getX() >= 0 && positionOfView.getX() <= splitLine.getCenterPointGlobal().getX()) {
                    if (positionOfView.getY() >= 0 && positionOfView.getY() <= RamApp.getApplication().getHeight()) {
                        return true;
                    }
                }
            } else {
                if (positionOfView.getX() >= splitLine.getCenterPointGlobal().getX() 
                        && positionOfView.getX() <= RamApp.getApplication().getWidth()) {
                    if (positionOfView.getY() >= 0 && positionOfView.getY() <= RamApp.getApplication().getHeight()) {
                        return true;
                    }
                }
            }
        } else {
            if (withinInnerView) {
                if (positionOfView.getX() >= 0 && positionOfView.getX() <= RamApp.getApplication().getWidth()) {
                    if (positionOfView.getY() >= 0 
                            && positionOfView.getY() <= splitLine.getCenterPointGlobal().getY()) {
                        return true;
                    }
                }
            } else {
                if (positionOfView.getX() >= 0 && positionOfView.getX() <= RamApp.getApplication().getWidth()) {
                    if (positionOfView.getY() >= splitLine.getCenterPointGlobal().getY() 
                            && positionOfView.getY() <= RamApp.getApplication().getHeight()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getFeature() == CorePackage.Literals.CORE_SCENE__ELEMENT_MAPPINGS) {
            COREModelElementMapping mapping = null;
            
            switch (notification.getEventType()) {
                case Notification.ADD:                    
                    mapping = (COREModelElementMapping) notification.getNewValue();
                    
                    if (mapping.getModelElements().size() == 2) {
                        RamRectangleComponent viewAtStartPosition = getViewOf(mapping.getModelElements().get(0));
                        RamRectangleComponent viewAtEndPosition = getViewOf(mapping.getModelElements().get(1));
                        
                        Vector3D firstMappedElementXY = getModifiedXYFromView(viewAtStartPosition);
                        Vector3D secondMappedElementXY = getModifiedXYFromView(viewAtEndPosition);
    
                        // create a mapping line and display it by adding it to container layer
                        RamLineComponent mappingLine = new RamLineComponent(firstMappedElementXY.getX(), 
                                firstMappedElementXY.getY(), secondMappedElementXY.getX(), 
                                secondMappedElementXY.getY());
                        setLineColorBasedOnMapping(mappingLine, viewAtStartPosition, viewAtEndPosition);
                        
                        mappingToLinesMap.put(mapping, mappingLine);
                        containerLayer.addChild(mappingLine);
                    }
                    
                    break;
                case Notification.REMOVE:                    
                    mapping = (COREModelElementMapping) notification.getOldValue();
                    
                    RamLineComponent lineToRemove = mappingToLinesMap.remove(mapping);
                
                    containerLayer.removeChild(lineToRemove);

                    break;
            }
        }
    }
    
    /**
     * Get the associated mapping from the line in the mapping to lines hash map if it exists.
     * 
     * @param line the {@link RamLineComponent}
     * 
     * @return the associated {@link COREModelElementMapping}
     */
    public COREModelElementMapping getMappingFrom(RamLineComponent line) {
        for (COREModelElementMapping mapping : mappingToLinesMap.keySet()) {
            if (mappingToLinesMap.get(mapping).equals(line)) {
                return mapping;
            }
        }
        return null;
    }
    
    /**
     * Get the mapping to lines hash map.
     * 
     * @return the {@link HashMap}
     */
    public HashMap<COREModelElementMapping, RamLineComponent> getMappingToLinesMap() {
        return mappingToLinesMap;
    }

    /**
     * Get the touch enabled boolean variable.
     * 
     * @return touchEnabled
     */
    public boolean isTouchEnabled() {
        return touchEnabled;
    }

    /**
     * Sets the touch enabled boolean variable.
     * 
     * @param touchEnabled the new boolean value
     */
    public void setTouchEnabled(boolean touchEnabled) {
        this.touchEnabled = touchEnabled;
    }
    
    /**
     * Get the language element mapping provider.
     * 
     * @return languageElementMappingProvider the {@link LanguageElementMappingProvider}
     */
    public LanguageElementMappingProvider getLanguageElementMappingProvider() {
        return languageElementMappingProvider;
    }
    
    @Override
    public void destroy() {
        // remove listeners
        EMFEditUtil.removeListenerFor(NavigationBar.getInstance().getCurrentScene(), this);
        
        super.destroy();
    }
    
    /**
     * Abstract method to get the associated view of the model element. Implementing 
     * this method requires one to check the instance of the model element and passing the model
     * element to the actual scene to find the associated view.
     * 
     * @param element the {@link EObject} model element
     * 
     * @return the associated {@link RamRectangleComponent}
     */
    public abstract RamRectangleComponent getViewOf(EObject element);
    
    /**
     * Abstract method to get a modified x and y coordinate in the form of a Vector3D from the given view.
     * This method is used to find the x and y coordinates of a view for the mapping line. Implementing this 
     * method requires one to check the instance of the view, and modify the x and y accordingly.
     * 
     * @param view the {@link RamRectangleComponent}
     * 
     * @return the modified {@link Vector3D}
     */
    public abstract Vector3D getModifiedXYFromView(RamRectangleComponent view);
    
    /**
     * Abstract method to set the color of the given mapping line depending on the types of the first and second 
     * mapped elements. Implementing this method requires one to check the instance of the mapped elements and 
     * determine the color accordingly.
     * 
     * @param mappingLine the associated {@link RamLineComponent}
     * @param firstMappedElement the first {@link RamRectangleComponent} associated with the line
     * @param secondMappedElement the second {@link RamRectangleComponent} associated with the line
     */
    public abstract void setLineColorBasedOnMapping(RamLineComponent mappingLine,
            RamRectangleComponent firstMappedElement, RamRectangleComponent secondMappedElement);
    
    /**
     * Abstract method to check if a given mapped element is within the inner view. Implementing this method requires
     * one to check the instance of the inner view and the instance of the given mapped element and whether or not they
     * are correlated.
     * 
     * @param mappedElement the given {@link RamRectangleComponent}
     * 
     * @return whether or not the element is within the inner view
     */
    protected abstract boolean checkElementWithinInnerView(RamRectangleComponent mappedElement);
    
    /**
     * Abstract method to create a stippled line given the position of the view and the actual view. Implementing this 
     * method allows one to create a customizable stippled line based on the type of the mapped element at the location 
     * of the given vector.

     * @param mappedElementXY the given {@link Vector3D} coordinates
     * @param mappedElement the given {@link RamRectangleComponent} view
     * 
     * @return the created stippled {@link RamLineComponent}
     */
    protected abstract RamLineComponent createStippledLine(Vector3D mappedElementXY, 
            RamRectangleComponent mappedElement);
}
