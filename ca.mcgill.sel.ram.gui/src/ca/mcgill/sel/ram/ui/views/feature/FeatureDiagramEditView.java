package ca.mcgill.sel.ram.ui.views.feature;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.Direction;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.views.RelationshipView.LineStyle;
import ca.mcgill.sel.ram.ui.views.feature.handler.IFeatureHandler;
import ca.mcgill.sel.ram.ui.views.feature.handler.impl.FeatureDiagramEditHandler;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * View used to represent a feature model. It can be edited
 * 
 * @author Nishanth
 */
public class FeatureDiagramEditView extends FeatureDiagramView<FeatureDiagramEditHandler>
        implements UINotifyChangedListener {

    private DisplayConcernEditScene scene;

    /**
     * Create a feature diagram representation view.
     * 
     * @param width - The width of the view
     * @param height - The height of the view
     * @param scene - The {@link DisplayConcernEditScene} containing the view
     */
    public FeatureDiagramEditView(float width, float height, DisplayConcernEditScene scene) {
        super(width, height, scene.getConcern().getFeatureModel());
        this.scene = scene;
        EMFEditUtil.addListenerFor(featureModel, this);
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));

        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(RamApp.getApplication());
        up.addTemplate(UnistrokeGesture.X, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.X, Direction.COUNTERCLOCKWISE);

        registerInputProcessor(up);
    }

    /*
     * ----------------------------- BEHAVIOR ---------------------------
     */
    @Override
    public void handleNotification(Notification notification) {
        /* Features in the model */
        if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE_MODEL__FEATURES) {
            // redraw diagram if a feature has been added, removed or moved
            switch (notification.getEventType()) {
                case Notification.ADD:
                    COREFeature feature = (COREFeature) notification.getNewValue();
                    addNewFeature(feature);
                    updateFeaturesDisplay(true);
                    break;
                case Notification.REMOVE:
                    removeFeature((COREFeature) notification.getOldValue());
                    updateFeaturesDisplay(true);
                    break;
            }
        } else if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE__REALIZED_BY) {
            /* Realization for the feature */
            FeatureView feature = features.get((COREFeature) notification.getNotifier());
            if (feature == null) {
                return;
            }
            switch (notification.getEventType()) {
                case Notification.ADD:
                case Notification.REMOVE:
                    if (expandFeature(feature.getParentFeatureView())) {
                        updateFeaturesDisplay(true);
                    }
                    feature.updateFeatureColor();
                    break;
            }
        } else if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE__PARENT_RELATIONSHIP
                && notification.getEventType() == Notification.SET && !notification.isTouch()) {
            updateRelationShipView(notification);
            /* Constraints */
        } else if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE__EXCLUDES
                || notification.getFeature() == CorePackage.Literals.CORE_FEATURE__REQUIRES) {
            // Update the container in the scene
            scene.notifyChanged(notification);

        } else if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE__CHILDREN) {
            switch (notification.getEventType()) {
                case Notification.ADD:
                    // Only useful in the case we already have the feature in the diagram, but change its parent
                    COREFeature feature = (COREFeature) notification.getNewValue();
                    boolean toRedraw = false;
                    if (features.containsKey(feature)) {
                        addNewFeature(feature);
                        toRedraw = true;
                    }
                    // Expand collapsed features if necessary
                    if (expandFeature(features.get(feature.getParent()))) {
                        toRedraw = true;
                    }
                    // Update the view
                    if (toRedraw) {
                        updateFeaturesDisplay(true);
                    }
                    break;
                case Notification.REMOVE:
                    // If
                    COREFeature feat = (COREFeature) notification.getNotifier();
                    FeatureView f = features.get(feat);
                    if (feat.getChildren().isEmpty()) {
                        f.setChildrenRelationship(null);
                    }
                    // Expand collapsed features if necessary
                    if (expandFeature(f)) {
                        updateFeaturesDisplay(true);
                    }
                    break;
                case Notification.MOVE:
                    COREFeature ff = (COREFeature) notification.getNewValue();
                    moveFeature((COREFeature) notification.getNewValue(), (Integer) notification.getOldValue());
                    // Expand if necessary
                    expandFeature(features.get(ff.getParent()));

                    updateFeaturesDisplay(true);
                    break;
            }
        } else if (notification.getFeature() == CorePackage.Literals.CORE_NAMED_ELEMENT__NAME) {
            FeatureView notifier = null;
            // Expand feature if necessary
            if (notification.getNotifier() instanceof COREFeature) {
                notifier = features.get((COREFeature) notification.getNotifier());
            }
            if (notifier != null && notifier.getParentFeatureView() != null) {
                expandFeature(notifier.getParentFeatureView());
            }
            // place feature correctly again (would be better to update position/size of the view during edit?)
            RamApp.getApplication().invokeLater(new Runnable() {
                @Override
                public void run() {
                    updateFeaturesDisplay(true);
                }
            });
        }
    }

    /**
     * Update the display for the relationships if they change.
     * 
     * @param notification - The received notification
     */
    private void updateRelationShipView(Notification notification) {
        /* Realization for the feature */
        COREFeatureRelationshipType oldV = (COREFeatureRelationshipType) notification.getOldValue();
        COREFeatureRelationshipType newV = (COREFeatureRelationshipType) notification.getNewValue();

        FeatureView view = features.get(notification.getNotifier());
        if (view == null) {
            return;
        }
        FeatureView parentV = view.getParentFeatureView();

        // Expand if necessary
        boolean expanded = expandFeature(parentV);
        boolean toRedraw = false;

        if (newV == COREFeatureRelationshipType.MANDATORY || newV == COREFeatureRelationshipType.OPTIONAL) {
            view.setParentRelationship(newV);
            // Just switch colors
            if (oldV == COREFeatureRelationshipType.MANDATORY || oldV == COREFeatureRelationshipType.OPTIONAL) {
                view.updateRelationshipColor();
            } else {
                // Update the display
                for (FeatureView child : parentV.getChildrenFeatureViews()) {
                    if (!child.equals(view) && !child.isReuse()
                            && child.getParentRelationship() != parentV.getChildrenRelationship()) {
                        parentV.setChildrenRelationship(null);
                    }
                }
                toRedraw = true;
            }
        } else if (newV == COREFeatureRelationshipType.OR || newV == COREFeatureRelationshipType.XOR) {
            view.setParentRelationship(newV);
            if (oldV == COREFeatureRelationshipType.XOR || oldV == COREFeatureRelationshipType.OR) {
                // Update display color
                for (FeatureView child : parentV.getChildrenFeatureViews()) {
                    if (!child.equals(view) && !child.isReuse() && parentV.getChildrenRelationship() != newV
                            && child.getParentRelationship() != parentV.getChildrenRelationship()) {
                        parentV.setChildrenRelationship(newV);
                        parentV.updateRelationshipColor();
                    }
                }
            } else {
                for (FeatureView child : parentV.getChildrenFeatureViews()) {
                    if (!child.equals(view) && !child.isReuse() && child.getParentRelationship() == newV) {
                        // Parent is no longer MANDATORY/OPTIONAL, update the display
                        parentV.setChildrenRelationship(newV);
                    }
                }
                toRedraw = true;
            }
        }
        // Update the view
        if (toRedraw) {
            updateFeaturesDisplay(expanded);
        } else if (expanded) {
            updateFeaturesDisplay(true);
        }
    }

    /**
     * Expands the feature and its parent if necessary. Does not refresh the display.
     * 
     * @param featureView - The {@link FeatureView} we want to make visible
     * @return true if we had to expand at least one {@link FeatureView}
     */
    protected final boolean expandFeature(FeatureView featureView) {
        Set<FeatureView> collapsed = getCollapsedFeatures(featureView);
        collapsedFeatures.removeAll(collapsed);
        
        return !collapsed.isEmpty();
    }

    /**
     * Change position of a feature under its parent.
     * 
     * @param feature - The moved {@link COREFeature}.
     * @param oldPosition - The previous position of the feature.
     */
    private void moveFeature(COREFeature feature, Integer oldPosition) {
        FeatureView view = features.get(feature);
        FeatureView parent = view.getParentFeatureView();
        int newPosition = parent.getFeature().getChildren().indexOf(feature);
        int move = newPosition - oldPosition;
        // We have to do this because if we have reuses they are located at the start children feature views list
        int position = parent.getChildrenFeatureViews().indexOf(view) + move;
        parent.getChildrenFeatureViews().remove(view);
        parent.getChildrenFeatureViews().add(position, view);
    }

    /**
     * Remove completely a {@link COREFeature} from the diagram.
     * Remove link from the parent {@link FeatureView} and destroys the view.
     * 
     * @param feature - The feature to remove.
     */
    private void removeFeature(COREFeature feature) {
        FeatureView view = features.remove(feature);

        if (view == null || view.getChildrenFeatureViews().size() > 0) {
            return;
        }

        // Remove from the parent
        FeatureView parent = view.getParentFeatureView();
        if (parent != null) {
            parent.getChildrenFeatureViews().remove(view);
        }
        destroyFeatureView(view);
    }

    @Override
    public void updateFeaturesDisplay(boolean repopulate) {
        super.updateFeaturesDisplay(repopulate);
        scene.updateExpandButton();
    }

    @Override
    protected void destroyFeatureView(FeatureView view) {
        EMFEditUtil.removeListenerFor(view.getFeature(), this);
        super.destroyFeatureView(view);
    }

    @Override
    public void destroy() {
        // Remove model listener
        EMFEditUtil.removeListenerFor(featureModel, this);
        super.destroy();
    }

    @Override
    protected void registerListeners(FeatureView pass) {
        if (!pass.isReuse() && !pass.isParentReuse()) {
            EMFEditUtil.removeListenerFor(pass.getFeature(), this);
            EMFEditUtil.addListenerFor(pass.getFeature(), this);
        }

        IFeatureHandler handler = HandlerFactory.INSTANCE.getFeatureEditModeHandler();
        pass.setListenersEditMode(handler);
        pass.addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(),
                containerLayer));
    }

    @Override
    protected DisplayOptions shouldDisplay(FeatureView child) {
        DisplayOptions option = super.shouldDisplay(child);

        if (option == DisplayOptions.COLLAPSE) {
            return DisplayOptions.SHOW;
        }
        return option;
    }

    /*
     * ------------------- COLLAPSE -------------------
     */
    /**
     * Expand all currently visible features.
     */
    public void expandAllFeatures() {
        Set<FeatureView> collapsed = getAllCollapsed(true);
        for (FeatureView colFeature : collapsed) {
            collapsedFeatures.remove(colFeature);
            colFeature.setLineStipple(LineStyle.SOLID.getStylePattern());
        }
        if (collapsed.size() > 0) {
            updateFeaturesDisplay(true);
        }
    }

    /**
     * Switch the collapsed status for a {@link FeatureView}.
     * 
     * @param feature - The {@link FeatureView} to collapse
     */
    public void switchCollapse(FeatureView feature) {
        if (collapsedFeatures.contains(feature) || !canCollapse(feature)) {
            collapsedFeatures.remove(feature);
        } else {
            collapsedFeatures.add(feature);
        }
        updateFeaturesDisplay(true);
    }

    @Override
    protected boolean canCollapse(FeatureView featureView) {
        // If we don't show reuses only display children COREFeatures.
        COREFeature feature = featureView.getFeature();
        return features.containsKey(feature) && feature.getParent() != null && !feature.getChildren().isEmpty();
    }

    @Override
    protected Set<FeatureView> getAllCollapsed(boolean onlyVisible) {
        Set<FeatureView> collapsed = new HashSet<FeatureView>();
        // Add normal features
        if (!onlyVisible) {
            for (FeatureView feature : features.values()) {
                collapsed.addAll(getCollapsedFeatures(feature, onlyVisible));
            }
        }

        return collapsed;
    }

    /**
     * Returns the scene that contains the DiagramView.
     * 
     * @return the scene 
     */
    public DisplayConcernEditScene getScene() {
        return scene;
    }
}
