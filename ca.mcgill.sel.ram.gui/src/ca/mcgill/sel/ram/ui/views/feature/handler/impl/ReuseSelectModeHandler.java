package ca.mcgill.sel.ram.ui.views.feature.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.scenes.AbstractConcernScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.views.feature.FeatureView;
import ca.mcgill.sel.ram.ui.views.feature.handler.IFeatureHandler;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature.FeatureSelectionStatus;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;

/**
 * Handler class used to manage action on feature in the ReuseDiagramView in select mode.
 * @author arthurls
 *
 */
public class ReuseSelectModeHandler extends BaseHandler implements IFeatureHandler {

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            final DisplayConcernSelectScene scene = RamApp.getActiveConcernSelectScene();
            
            FeatureView featureView = (FeatureView) tapAndHoldEvent.getTarget();
            COREFeature feature = featureView.getFeature();
            COREReuse reuse = scene.getCurrentSelectedReuse();
            
            // Maps the reuse (if not done before) to a userConfiguration
            SelectionsSingleton.getInstance().mapReuseToUserConfiguration(reuse);
            
            if (featureView.getFeatureSelectionStatus() == FeatureSelectionStatus.RE_EXPOSED) {
                SelectionsSingleton.getInstance()
                    .addSelectionToUserConfiguration(reuse, feature, FeatureSelectionStatus.NOT_SELECTED);
                featureView.setSelectionStatus(FeatureSelectionStatus.NOT_SELECTED);
            } else {
                SelectionsSingleton.getInstance()
                    .addSelectionToUserConfiguration(reuse, feature, FeatureSelectionStatus.RE_EXPOSED);
                featureView.setSelectionStatus(FeatureSelectionStatus.RE_EXPOSED);
            }
            scene.getReuseDiagramView().updateFeatureColors();
            
        }
        return true;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isTapped()) {
            final DisplayConcernSelectScene scene = RamApp.getActiveConcernSelectScene();
            
            FeatureView featureView = (FeatureView) tapEvent.getTarget();
            COREFeature feature = featureView.getFeature();
            COREReuse reuse = scene.getCurrentSelectedReuse();
            
            // Maps the reuse (if not done before) to a userConfiguration
            SelectionsSingleton.getInstance().mapReuseToUserConfiguration(reuse);
            
            // if it's reexposed, set the feature selection status to SELECTED
            if (featureView.getFeatureSelectionStatus() == FeatureSelectionStatus.RE_EXPOSED
                    || featureView.getFeatureSelectionStatus() == FeatureSelectionStatus.NOT_SELECTED) {
                SelectionsSingleton.getInstance()
                    .addSelectionToUserConfiguration(reuse, feature, FeatureSelectionStatus.SELECTED);
                
                featureView.setSelectionStatus(FeatureSelectionStatus.SELECTED);

            } else if (featureView.getFeatureSelectionStatus() == FeatureSelectionStatus.SELECTED
                    || featureView.getFeatureSelectionStatus() == FeatureSelectionStatus.AUTO_SELECTED) {
                SelectionsSingleton.getInstance()
                    .addSelectionToUserConfiguration(reuse, feature, FeatureSelectionStatus.NOT_SELECTED);
                
                featureView.setSelectionStatus(FeatureSelectionStatus.NOT_SELECTED);
            }
            
            scene.getReuseDiagramView().updateFeatureColors();
        }
        
        return true;
    }

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setPositionUpdate(FeatureView featureIcon, AbstractConcernScene<?, ?> scene) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void renameFeature(FeatureView view) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteFeature(DisplayConcernEditScene scene, FeatureView view) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void hideFeature(DisplayConcernEditScene scene, FeatureView view) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void addChild(DisplayConcernEditScene scene, COREFeature feature, int position,
            COREFeatureRelationshipType optional) {
        // TODO Auto-generated method stub
        
    }

}
