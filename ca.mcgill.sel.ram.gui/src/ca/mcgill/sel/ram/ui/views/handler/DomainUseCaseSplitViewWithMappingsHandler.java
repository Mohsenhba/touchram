package ca.mcgill.sel.ram.ui.views.handler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;
import org.mt4j.components.PickResult;
import org.mt4j.components.PickResult.PickEntry;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.classdiagram.ui.views.ClassView;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.GenericSplitViewWithMappings;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ui.views.ActorView;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;

/**
 * This is an implementation of {@link GenericSplitViewWithMappingsHandler} that handles a Domain and Use Case model 
 * in the split view.
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IAbstractViewHandler} for the outer level view
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 */
public class DomainUseCaseSplitViewWithMappingsHandler<S extends IRamAbstractSceneHandler, T extends 
    IAbstractViewHandler, U extends IAbstractViewHandler> extends GenericSplitViewWithMappingsHandler<S, T, U> {

    public DomainUseCaseSplitViewWithMappingsHandler(
            GenericSplitViewWithMappings<S, T, U> genericSplitViewWithMappings) {
        super(genericSplitViewWithMappings);
    }

    /**
     * Get a view from a given point. Only allowed model elements for mappings are {@link Actor}, 
     * {@link Class}.
     * 
     * @param position the given {@link Vector3D} point
     * 
     * @return the associated {@link MTComponent}
     */
    @Override
    protected MTComponent getViewFromPosition(Vector3D position) {
        boolean isInInnerLevel = isInInnerLevelView(position);

        // get all possible views from the position
        PickResult pickResults = genericSplitView.getContainerLayer().pick(
                position.getX(), position.getY(), false);

        for (final PickEntry pick : pickResults.getPickList()) {
            final MTComponent pickComponent = pick.hitObj;

            if (isInInnerLevel) {
                if (genericSplitView.getInnerLevelView() instanceof UseCaseDiagramView) {
                    if (pickComponent instanceof ActorView) {
                        return pickComponent;
                    }
                } else if (genericSplitView.getInnerLevelView() instanceof ClassDiagramView) {
                    if (pickComponent instanceof ClassView) {
                        return pickComponent;
                    }
                }
            } else {
                if (genericSplitView.getOuterLevelView() instanceof UseCaseDiagramView) {
                    if (pickComponent instanceof ActorView) {
                        return pickComponent;
                    }
                } else if (genericSplitView.getOuterLevelView() instanceof ClassDiagramView) {
                    if (pickComponent instanceof ClassView) {
                        return pickComponent;
                    }
                }
            }
            
        }
        return null;
    }

    /**
     * Get the model element from a given view. Only allowed model elements for mappings are {@link Actor}, 
     * {@link Class}.
     * 
     * @param view the given {@link MTComponent}
     * 
     * @return the associated {@link EOBject} model element
     */
    @Override
    protected EObject getModelElementFromView(MTComponent view) {
        if (view instanceof ActorView) {
            return ((ActorView) view).getActor();
        } else if (view instanceof ClassView) {
            return ((ClassView) view).getClassifier();
        }
        
        return null;
    }

    /**
     * Check whether a view is the outmost view.
     * 
     * @param pickComponent the given {@link MTComponent}
     * 
     * @return whether or not the given view is the outmost view
     */
    @Override
    protected boolean isOutmostView(MTComponent pickComponent) {
        if (pickComponent instanceof ClassDiagramView || pickComponent instanceof UseCaseDiagramView) {
            return true;
        }
        
        return false;
    }

    @Override
    protected boolean mappingIsValid(EObject firstModelElement, EObject secondModelElement) {
        if (firstModelElement instanceof Classifier && secondModelElement instanceof Actor 
                || firstModelElement instanceof Actor && secondModelElement instanceof Classifier) {
            return true;
        }
        RamApp.getActiveScene().displayPopup("Error. Can only create mappings between Class and Actor.");
        return false;
    }

    @Override
    protected List<COREModelElementMapping> findAdditionalMappingsToRemove(
            HashSet<COREModelElementMapping> mappingsToRemove, List<COREModelElementMapping> allModelElementMappings) {
        return new ArrayList<COREModelElementMapping>();
    }

}
