package ca.mcgill.sel.ram.ui.views.handler;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;
import org.mt4j.components.PickResult;
import org.mt4j.components.PickResult.PickEntry;
import org.mt4j.input.inputData.AbstractCursorInputEvt;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.scaleProcessor.ScaleEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.WheelEvent;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.GenericSplitViewWithMappings;
    
/**
 * This is the handler for {@link GenericSplitViewWithMappings} and extends {@link GenericSplitViewHandler}.
 * This handler adds on functionality to add and remove mapping lines with unistroke gestures.
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IAbstractViewHandler} for the outer level view
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 */
public abstract class GenericSplitViewWithMappingsHandler<S extends IRamAbstractSceneHandler, 
    T extends IAbstractViewHandler, U extends IAbstractViewHandler> extends GenericSplitViewHandler<S, T, U> {

    public GenericSplitViewWithMappingsHandler(GenericSplitViewWithMappings<S, T, U> genericSplitViewWithMappings) {
        super(genericSplitViewWithMappings);
    }

    /**
     * Handles the unistroke event in the generic split view by letting the inner or outer level views
     * handle the unistroke event if the start and end positions are in the same view.
     * 
     * This is the a modified method from {@link GenericSplitViewHandler} to only pass on gestures to inner or outer 
     * level views when the boolean touchEnabled is true. It also contains functionality to add and remove mappings
     * with unistroke gestures.
     * 
     * @param unistrokeEvent - The received {@link UnistrokeEvent}.
     */
    private void handleUnistrokeGesture(UnistrokeEvent unistrokeEvent) {
        Vector3D startPosition = unistrokeEvent.getCursor().getStartPosition();
        Vector3D endPosition = unistrokeEvent.getCursor().getPosition();

        MTComponent viewAtStartPosition = getViewFromPosition(startPosition);
        MTComponent viewAtEndPosition = getViewFromPosition(endPosition);

        // if unistroke event starts and ends at inner and outer level views or vice versa, check if a mapping 
        // can be created, if it can, add it to COREScene and visually
        if (isInInnerLevelView(startPosition) != isInInnerLevelView(endPosition)
                && viewAtStartPosition != null && viewAtEndPosition != null) {
            
            EObject firstModelElement = getModelElementFromView(viewAtStartPosition);
            EObject secondModelElement = getModelElementFromView(viewAtEndPosition);
            
            if (mappingIsValid(firstModelElement, secondModelElement)) {
                if (firstModelElement != null && secondModelElement != null) {
                    createMapping(firstModelElement, secondModelElement,
                            viewAtStartPosition, viewAtEndPosition);
                }
            }
        } else {
            // get an array of points from unistroke gesture
            InputCursor inputCursor = unistrokeEvent.getCursor();
            Collection<AbstractCursorInputEvt> evts = inputCursor.getEvents();
            AbstractCursorInputEvt[] eventArray = new AbstractCursorInputEvt[0];
            
            eventArray = evts.toArray(eventArray);

            // check if there are any intersecting mapping lines with the array of points from unistroke gesture
            HashSet<RamLineComponent> linesToRemove = checkIntersectedMappings(eventArray, 
                    ((GenericSplitViewWithMappings<S, T, U>) genericSplitView).getMappingToLinesMap().values());
            
            HashSet<COREModelElementMapping> mappingsToRemove = new HashSet<>();
            
            // loop through each line in linesToRemove and add the mapping to mappings to remove if it exists 
            for (RamLineComponent lineToRemove : linesToRemove) {
                COREModelElementMapping mapping = ((GenericSplitViewWithMappings<S, T, U>) genericSplitView)
                        .getMappingFrom(lineToRemove);
                
                if (mapping != null) {
                    mappingsToRemove.add(mapping);
                }
            }
            
            // find additional mappings to remove to enforce validation rules and add them to mappings to remove
            List<COREModelElementMapping> additionalMappingsToRemove = findAdditionalMappingsToRemove(mappingsToRemove, 
                    NavigationBar.getInstance().getCurrentScene().getElementMappings());
            mappingsToRemove.addAll(additionalMappingsToRemove);
            
            // remove each mapping in mappings to remove
            for (COREModelElementMapping mapping : mappingsToRemove) {
                removeMapping(mapping);
            }
            
            // pass the event to one of the components at the pick point
            // iff start position and end position are in the same view
            if (((GenericSplitViewWithMappings<S, T, U>) genericSplitView).isTouchEnabled()) {
                if (isInInnerLevelView(startPosition) == isInInnerLevelView(endPosition)) {
                    PickResult pickResults = genericSplitView.getContainerLayer().pick(
                            endPosition.getX(), endPosition.getY(), false);
    
                    for (final PickEntry pick : pickResults.getPickList()) {
                        final MTComponent pickComponent = pick.hitObj;
                        unistrokeEvent.setTarget(pickComponent);
                        if (pickComponent instanceof IHandled) {
                            // see if the component wants to handle the event
                            IHandled<?> handledComponent = (IHandled<?>) pickComponent;
    
                            if (handledComponent.getHandler() != null && handledComponent
                                    .getHandler().processGestureEvent(unistrokeEvent)) {
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Create a mapping from the given model elements, call the controller to add it to the COREScene, and 
     * add it visually to the split view.
     * 
     * @param firstModelElement the given {@link EObject}
     * @param secondModelElement the given {@link EObject}
     * @param viewAtStartPosition the given {@link MTComponent} associated with firstModelElement
     * @param viewAtEndPosition the given {@link MTComponent} associated with secondModelElement
     */
    private void createMapping(EObject firstModelElement, EObject secondModelElement,
            MTComponent viewAtStartPosition, MTComponent viewAtEndPosition) {
        // call controller to create mapping in COREScene
        COREControllerFactory.INSTANCE.getPerspectiveController().createMapping(firstModelElement, secondModelElement, 
                ((GenericSplitViewWithMappings<S, T, U>) genericSplitView).getLanguageElementMappingProvider()
                .getNextLEMid(), NavigationBar.getInstance().getCurrentScene());         
    }

    /**
     * Remove a given mapping and the line associated with it. This method calls the controller to use an EMF
     * command to remove the mapping from the COREScene and removes the line from the container layer.
     * 
     * @param mapping the {@link COREModelElementMapping} to remove
     */
    private void removeMapping(COREModelElementMapping mapping) {
        COREControllerFactory.INSTANCE.getPerspectiveController().removeMapping(mapping, 
                NavigationBar.getInstance().getCurrentScene());    
    }

    /**
     * This method checks if there are any intersections between an abstract cursor input event array
     * all mapping lines. This method creates a line segment for every two input point and checks for
     * intersections with every mapping line. This method is used for removing mappings with unistroke
     * gestures, the eventArray is the coordinates of points on the unistroke gesture, while mappingLines
     * is an array of {@link RamLineComponent}.
     * 
     * @param eventArray the given array of {@link AbstractCursorInputEvt}, represents points in an unistroke 
     * gesture
     * @param mappingLines the given array of  {@link RamLineComponent}
     * 
     * @return all {@link RamLineComponent} that intersect with the event array
     */
    private static HashSet<RamLineComponent> checkIntersectedMappings(AbstractCursorInputEvt[] eventArray, 
            Collection<RamLineComponent> mappingLines) {
        HashSet<RamLineComponent> linesToRemove = new HashSet<>();
        
        Vector3D prevPoint = new Vector3D(eventArray[0].getX(), eventArray[0].getY());
        
        // instead of points try line segments of eventArray~
        for (int i = 1; i < eventArray.length; i++) {
            RamLineComponent unistrokeLine = new RamLineComponent(prevPoint.getX(), prevPoint.getY(), 
                    eventArray[i].getX(), eventArray[i].getY());
            
            for (RamLineComponent mappingLine : mappingLines) {
                if (checkIntersectionOf(unistrokeLine, mappingLine)) {
                    linesToRemove.add(mappingLine);
                }
            }
            
            prevPoint = new Vector3D(eventArray[i].getX(), eventArray[i].getY());
        }
        
        return linesToRemove;
    }
    
    /**
     * This method checks if two lines are intersecting each other.
     * 
     * @param lineOne the given {@link RamLineComponent}
     * @param lineTwo the given {@link RamLineComponent}
     * 
     * @return whether or not the two lines intersect
     */
    private static boolean checkIntersectionOf(RamLineComponent lineOne, RamLineComponent lineTwo) {
        // find vertices of lineOne
        Vector3D[] verticesOne = lineOne.getVerticesLocal();
        
        // find m, b if line is not vertical, find x if line is vertical
        double m1 = 0;
        double b1 = 0;
        double x1 = 0;
        boolean verticalOne = false;
        
        if (verticesOne[1].getX() == verticesOne[0].getX()) {
            x1 = verticesOne[1].getX();
            verticalOne = true;
        } else {
            m1 = (verticesOne[1].getY() - verticesOne[0].getY()) / (verticesOne[1].getX() - verticesOne[0].getX());
            b1 = verticesOne[1].getY() - m1 * verticesOne[1].getX();
        }
        
        // find vertices of lineTwo
        Vector3D[] verticesTwo = lineTwo.getVerticesLocal();
        
        // find m, b if line is not vertical, find x if line is vertical
        double m2 = 0;
        double b2 = 0;
        double x2 = 0;
        boolean verticalTwo = false;
        
        if (verticesTwo[1].getX() == verticesTwo[0].getX()) {
            x2 = verticesTwo[1].getX();
            verticalTwo = true;
        } else {
            m2 = (verticesTwo[1].getY() - verticesTwo[0].getY()) / (verticesTwo[1].getX() - verticesTwo[0].getX());
            b2 = verticesTwo[1].getY() - m2 * verticesTwo[1].getX();
        }
        
        double intersectX;
        double intersectY;
        boolean withinDomain;
        boolean withinRange;
        
        // find x and y of intersection point, then check if it is within domain and range of both lines
        if (!verticalOne && !verticalTwo) {
            intersectX = (-1 * b1 + b2) / (m1 - m2);
            intersectY = m1 * intersectX + b1;
            
            withinDomain = (intersectX >= verticesOne[0].getX() && intersectX <= verticesOne[1].getX() 
                    || intersectX >= verticesOne[1].getX() && intersectX <= verticesOne[0].getX())
                    && (intersectX >= verticesTwo[0].getX() && intersectX <= verticesTwo[1].getX() 
                    || intersectX >= verticesTwo[1].getX() && intersectX <= verticesTwo[0].getX());
            
            withinRange = (intersectY >= verticesOne[0].getY() && intersectY <= verticesOne[1].getY() 
                    || intersectY >= verticesOne[1].getY() && intersectY <= verticesOne[0].getY())
                    && (intersectY >= verticesTwo[0].getY() && intersectY <= verticesTwo[1].getY() 
                    || intersectY >= verticesTwo[1].getY() && intersectY <= verticesTwo[0].getY());
            
            if (withinDomain) {
                if (withinRange) {
                    return true;
                }
            }
            return false;
        } else if (verticalOne && !verticalTwo) {
            intersectX = x1;
            intersectY = m2 * intersectX + b2;
            
            withinDomain = intersectX >= verticesTwo[0].getX() && intersectX <= verticesTwo[1].getX() 
                    || intersectX >= verticesTwo[1].getX() && intersectX <= verticesTwo[0].getX();
            
            withinRange = (intersectY >= verticesOne[0].getY() && intersectY <= verticesOne[1].getY() 
                    || intersectY >= verticesOne[1].getY() && intersectY <= verticesOne[0].getY())
                    && (intersectY >= verticesTwo[0].getY() && intersectY <= verticesTwo[1].getY() 
                    || intersectY >= verticesTwo[1].getY() && intersectY <= verticesTwo[0].getY());
          
            if (withinDomain) {
                if (withinRange) {
                    return true;
                }
            }
            return false;
        } else if (!verticalOne && verticalTwo) {
            intersectX = x2;
            intersectY = m1 * intersectX + b1;
            
            withinDomain = intersectX >= verticesOne[0].getX() && intersectX <= verticesOne[1].getX() 
                    || intersectX >= verticesOne[1].getX() && intersectX <= verticesOne[0].getX();
            
            withinRange = (intersectY >= verticesOne[0].getY() && intersectY <= verticesOne[1].getY() 
                    || intersectY >= verticesOne[1].getY() && intersectY <= verticesOne[0].getY())
                    && (intersectY >= verticesTwo[0].getY() && intersectY <= verticesTwo[1].getY() 
                    || intersectY >= verticesTwo[1].getY() && intersectY <= verticesTwo[0].getY());

            if (withinDomain) {
                if (withinRange) {
                    return true;
                }
            }
            return false;
        } else {
            // if both lines vertical, compare their x values
            return x1 == x2;
        }
    }

    /**
     * Handles the visualization of the unistroke event in the generic split view and passes on the gesture
     * to handleUnistrokeGesture(UnistrokeEvent unistrokeEvent) if the gesture ended.
     * 
     * @param unistrokeEvent the received {@link UnistrokeEvent}
     * 
     * @return always true because when this method is called it will be handled in the generic split view
     */
    private boolean handleUniStrokeEvent(UnistrokeEvent unistrokeEvent) {

        switch (unistrokeEvent.getId()) {
            case MTGestureEvent.GESTURE_STARTED:
                genericSplitView.getUnistrokeLayer().addChild(unistrokeEvent.getVisualization());
                break;
            case MTGestureEvent.GESTURE_UPDATED:
                break;
            case MTGestureEvent.GESTURE_ENDED:
                // handle the unistroke gesture appropriately
                Vector3D startPosition = unistrokeEvent.getCursor().getStartPosition();
                Vector3D endPosition = unistrokeEvent.getCursor().getPosition();

                // only handle gesture if start and end positions are different
                // this is a workaround to when tap gestures are also received as unistroke
                if (startPosition.getX() != endPosition.getX() || startPosition.getY() != endPosition.getY()) {
                    handleUnistrokeGesture(unistrokeEvent);
                }

                genericSplitView.cleanUnistrokeLayer();
                break;
            case MTGestureEvent.GESTURE_CANCELED:
                genericSplitView.cleanUnistrokeLayer();
                break;
        }

        return true;
    }

    /**
     * Gestures from the overlay of the generic split view are processed here.
     * This method first checks if the generic split view can handle the gestures itself,
     * if not, it will pass it to one of the underlying views and process it there.
     * 
     * This is the a modified method from {@link GenericSplitViewHandler} to reformat mapping lines
     * after gestures.
     * 
     * @param gestureEvent - the gesture event passed from the generic split view
     * 
     * @return whether or not the gesture was processed here
     */
    @Override
    public boolean processGestureEvent(MTGestureEvent gestureEvent) {
        boolean dragProcessedBySplitView = false;
        boolean unistrokeProcessedBySplitView = false;
        
        if (gestureEvent instanceof DragEvent) {
            dragProcessedBySplitView = handleDragEvent((DragEvent) gestureEvent);
        } else if (gestureEvent instanceof UnistrokeEvent) {
            unistrokeProcessedBySplitView = handleUniStrokeEvent((UnistrokeEvent) gestureEvent);
        }

        if (!dragProcessedBySplitView && !unistrokeProcessedBySplitView) {
            Vector3D pickPoint = null;
            boolean stopProcessing = false;

            if (gestureEvent instanceof TapEvent) {
                // prevent a tap event from being processed when a tap and hold event was processed before
                if (tapAndHoldPerformed) {
                    tapAndHoldPerformed = false;
                    stopProcessing = true;
                }

                pickPoint = ((TapEvent) gestureEvent).getLocationOnScreen();
            } else if (gestureEvent instanceof DragEvent) {
                pickPoint = ((DragEvent) gestureEvent).getFrom();
            } else if (gestureEvent instanceof TapAndHoldEvent) {
                TapAndHoldEvent tapAndHoldEvent = (TapAndHoldEvent) gestureEvent;
                // used for workaround to prevent tap event from being executed at the same time
                switch (tapAndHoldEvent.getId()) {
                    case MTGestureEvent.GESTURE_ENDED:
                        if (tapAndHoldEvent.isHoldComplete()) {
                            tapAndHoldPerformed = true;
                        }
                        break;
                }

                pickPoint = tapAndHoldEvent.getLocationOnScreen();
            } else if (gestureEvent instanceof ScaleEvent) {
                pickPoint = ((ScaleEvent) gestureEvent).getScalingPoint();
            } else if (gestureEvent instanceof WheelEvent) {
                pickPoint = ((WheelEvent) gestureEvent).getLocationOnScreen();
            } else if (gestureEvent instanceof UnistrokeEvent) {
                pickPoint = ((UnistrokeEvent) gestureEvent).getCursor().getStartPosition();
                if (pickPoint == null) {
                    stopProcessing = true;
                }
            } else {
                stopProcessing = true;
            }
            if (!stopProcessing) {
                PickResult pickResults = genericSplitView.getContainerLayer().pick(
                        pickPoint.getX(), pickPoint.getY(), false);

                for (final PickEntry pick : pickResults.getPickList()) {
                    final MTComponent pickComponent = pick.hitObj;
                    
                    if (!((GenericSplitViewWithMappings<S, T, U>) genericSplitView).isTouchEnabled()) {
                        if (pickComponent != genericSplitView.getOverlay()) {
                            if (isOutmostView(pickComponent)) {
                                
                                // check gestures
                                if (gestureEvent instanceof DragEvent || gestureEvent instanceof ScaleEvent 
                                        || gestureEvent 
                                        instanceof WheelEvent) {                                          
                                    gestureEvent.setTarget(pickComponent);
                                    if (pickComponent instanceof IHandled) {
                                        // see if the component wants to handle the event
                                        IHandled<?> handledComponent = (IHandled<?>) pickComponent;
            
                                        if (handledComponent.getHandler() != null
                                                && handledComponent.getHandler().processGestureEvent(gestureEvent)) {
                                            // reformat all mapping lines after gesture has been processed
                                            reformatMappingLines(((GenericSplitViewWithMappings<S, T, U>) 
                                                    genericSplitView).getMappingToLinesMap());
                                            
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    } else { 
                        if (pickComponent != genericSplitView.getOverlay()) {
                            gestureEvent.setTarget(pickComponent);
                            if (pickComponent instanceof IHandled) {
                                // see if the component wants to handle the event
                                IHandled<?> handledComponent = (IHandled<?>) pickComponent;
    
                                if (handledComponent.getHandler() != null
                                        && handledComponent.getHandler().processGestureEvent(gestureEvent)) {
                                    // reformat all mapping lines after gesture has been processed
                                    reformatMappingLines(((GenericSplitViewWithMappings<S, T, U>) 
                                            genericSplitView).getMappingToLinesMap());
                                    
                                    return false;
                                }
                            } else if (pickComponent instanceof RamButton) {
    
                                if (pickComponent.processGestureEvent(gestureEvent)) {
                                    // reformat all mapping lines after gesture has been processed
                                    reformatMappingLines(((GenericSplitViewWithMappings<S, T, U>) 
                                            genericSplitView).getMappingToLinesMap());
                                    
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }

        // reformat all mapping lines
        reformatMappingLines(((GenericSplitViewWithMappings<S, T, U>) genericSplitView).getMappingToLinesMap());

        return true;
    }
    
    /**
     * Redraw all mapping lines with new positions. This method is called after any gesture or change split 
     * orientation button to ensure the mapping lines still correlate to their associated views.
     * 
     * @param mappingToLinesMap the new mapping to lines hash map
     */
    private void reformatMappingLines(HashMap<COREModelElementMapping, RamLineComponent> mappingToLinesMap) {
        EList<COREModelElementMapping> mappings = new BasicEList<COREModelElementMapping>();
        
        Set<COREModelElementMapping> oldMappings = new HashSet<>();
        
        for (COREModelElementMapping mapping : mappingToLinesMap.keySet()) {
            oldMappings.add(mapping);
        }
        
        for (COREModelElementMapping mapping : oldMappings) {
            EList<EObject> mappingElements = mapping.getModelElements();
            
            RamRectangleComponent firstMappedElement = null;
            RamRectangleComponent secondMappedElement = null;
            
            // if the size is two, the is a correct mapping, get views from the mapping
            if (mapping.getModelElements().size() == 2) {
                firstMappedElement = ((GenericSplitViewWithMappings<S, T, U>) genericSplitView)
                        .getViewOf(mappingElements.get(0));
                secondMappedElement = ((GenericSplitViewWithMappings<S, T, U>) genericSplitView)
                        .getViewOf(mappingElements.get(1));
                
                // if either mapped element is null, remove it from the COREScene
                if (firstMappedElement == null || secondMappedElement == null) {
                    //TODO: remove this when core controller removes everything associated to object when removed,
                    // thus this line is not need
                    removeMapping(mapping);
                } else {
                    // add mapping to mappings and remove the current mapping line from the container layer
                    mappings.add(mapping);
                    genericSplitView.getContainerLayer().removeChild(mappingToLinesMap.get(mapping));
                }
            }
        }
        // call the display mapping method in generic split view to redraw all of the mappings
        ((GenericSplitViewWithMappings<S, T, U>) genericSplitView).displayMappings(mappings);
    }

    /**
     * Switches the split orientation from vertical to horizontal or vice versa.
     * Also resets every split line position by removing it then adding a new one.
     */
    @Override
    public void switchSplitOrientation() {
        super.switchSplitOrientation();

        reformatMappingLines(((GenericSplitViewWithMappings<S, T, U>) genericSplitView).getMappingToLinesMap());
    }
    
    /**
     * Abstract method to get a view from a given point. This is used for the creation of a mapping line. 
     * Implementing this method requires one to pick from the container layer with the given position and return 
     * the appropriate view based on the type of the inner / outer level view (based on position of point and 
     * split line). One can use this method to exclude certain types that do not want to be mapped.
     * 
     * @param position the given {@link Vector3D} point
     * 
     * @return the associated {@link MTComponent}
     */
    protected abstract MTComponent getViewFromPosition(Vector3D position);
    
    /**
     * Abstract method to get the model element from a given view. Implementing this method requires one to check the 
     * instance of the given view and cast it to get the underlying model element.
     * 
     * @param view the given {@link MTComponent}
     * 
     * @return the associated {@link EOBject} model element
     */
    protected abstract EObject getModelElementFromView(MTComponent view);
    
    /**
     * Abstract method to check whether a view is the outmost view. Implementing this method requires one to check the 
     * instance of given view and compare the types to the types of the outmost views of the inner and outer scenes.
     * 
     * @param pickComponent the given {@link MTComponent}
     * 
     * @return whether or not the given view is the outmost view
     */
    protected abstract boolean isOutmostView(MTComponent pickComponent);
    
    /**
     * Abstract method to check if the input 2 elements are valid as a mapping. Implementing this method requires one to
     * check the types of the elements and perhaps enforce some rules about the accepted mappings. If the mapping is not
     * valid, display an error message to the user.
     * 
     * @param firstModelElement first model in mapping
     * @param secondModelElement second model in mapping
     * @return whether or not the mapping is valid or not
     */
    protected abstract boolean mappingIsValid(EObject firstModelElement, EObject secondModelElement);
    
    /**
     * Abstract method to check if there are any additional mappings that are required to be removed to enforce
     * validation rules defined in the method mappingIsValid.
     * 
     * @param mappingsToRemove currently removed mappings
     * @param allModelElementMappings all model element mappings
     * @return additional mappings that are required to be removed
     */
    protected abstract List<COREModelElementMapping> findAdditionalMappingsToRemove(HashSet<COREModelElementMapping> 
        mappingsToRemove, List<COREModelElementMapping> allModelElementMappings);
}
