package ca.mcgill.sel.ram.ui.views.handler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;
import org.mt4j.components.PickResult;
import org.mt4j.components.PickResult.PickEntry;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.GenericSplitViewWithMappings;
import ca.mcgill.sel.ram.ui.views.structural.OperationView;
import ca.mcgill.sel.ram.ui.views.structural.ParameterView;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.ui.components.ResourceRamButton;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;

/**
 * This is an implementation of {@link GenericSplitViewWithMappingsHandler} that handles a Ram and Rif model in the 
 * split view.
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IAbstractViewHandler} for the outer level view
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 */
public class RifRamSplitViewWithMappingsHandler<S extends IRamAbstractSceneHandler, T extends IAbstractViewHandler,
    U extends IAbstractViewHandler> extends GenericSplitViewWithMappingsHandler<S, T, U> {

    public RifRamSplitViewWithMappingsHandler(GenericSplitViewWithMappings<S, T, U> genericSplitViewWithMappings) {
        super(genericSplitViewWithMappings);
    }

    /**
     * Get a view from a given point. Only allowed model elements for mappings are {@link Operation}, 
     * {@link Parameter}, {@link AcessMethod}, and {@link PathFragment}.
     * 
     * @param position the given {@link Vector3D} point
     * 
     * @return the associated {@link MTComponent}
     */
    @Override
    protected MTComponent getViewFromPosition(Vector3D position) {
        boolean isInInnerLevel = isInInnerLevelView(position);

        // get all possible views from the position
        PickResult pickResults = genericSplitView.getContainerLayer().pick(
                position.getX(), position.getY(), false);

        for (final PickEntry pick : pickResults.getPickList()) {
            final MTComponent pickComponent = pick.hitObj;

            if (isInInnerLevel) {
                // RAM model
                if (genericSplitView.getInnerLevelView() instanceof StructuralDiagramView) {
                    if (pickComponent instanceof ParameterView || pickComponent instanceof OperationView) {
                        return pickComponent;
                    }
                    // RestIF model
                } else if (genericSplitView.getInnerLevelView() instanceof RestTreeView) {
                    if (pickComponent instanceof ResourceRamButton || pickComponent instanceof PathFragmentView) {
                        return pickComponent;
                    }
                }

            } else {
                // RAM model
                if (genericSplitView.getOuterLevelView() instanceof StructuralDiagramView) {
                    if (pickComponent instanceof ParameterView || pickComponent instanceof OperationView) {
                        return pickComponent;
                    }
                    // RestIF model
                } else if (genericSplitView.getOuterLevelView() instanceof RestTreeView) {
                    if (pickComponent instanceof ResourceRamButton || pickComponent instanceof PathFragmentView) {
                        return pickComponent;
                    }
                }

            }
        }

        return null;
    }

    /**
     * Get the model element from a given view. Only allowed model elements for mappings are {@link Operation}, 
     * {@link Parameter}, {@link AcessMethod}, and {@link PathFragment}.
     * 
     * @param view the given {@link MTComponent}
     * 
     * @return the associated {@link EOBject} model element
     */
    @Override
    protected EObject getModelElementFromView(MTComponent view) {
        if (view instanceof ParameterView) {
            return ((ParameterView) view).getParameter();
        } else if (view instanceof OperationView) {
            return ((OperationView) view).getOperation();
        } else if (view instanceof ResourceRamButton) {
            return ((ResourceRamButton) view).getAccessMethod();
        } else if (view instanceof PathFragmentView) {
            return ((PathFragmentView) view).getPathFragment();
        }
        
        return null;
    }

    /**
     * Check whether a view is the outmost view.
     * 
     * @param pickComponent the given {@link MTComponent}
     * 
     * @return whether or not the given view is the outmost view
     */
    @Override
    protected boolean isOutmostView(MTComponent pickComponent) {
        if (pickComponent instanceof RestTreeView || pickComponent instanceof StructuralDiagramView) {
            return true;
        }

        return false;
    }

    @Override
    protected boolean mappingIsValid(EObject firstModelElement, EObject secondModelElement) {
        EList<COREModelElementMapping> mappings = NavigationBar.getInstance().getCurrentScene().getElementMappings();
        
        if (firstModelElement instanceof DynamicFragment && secondModelElement instanceof Parameter 
                || firstModelElement instanceof Parameter && secondModelElement instanceof DynamicFragment) {
            
            // set parameter and dynamic fragment of input mapping
            Parameter parameter = null;
            DynamicFragment dynamicFragment = null;
            
            if (firstModelElement instanceof Parameter) {
                parameter = (Parameter) firstModelElement;
                dynamicFragment = (DynamicFragment) secondModelElement;
            } else {
                parameter = (Parameter) secondModelElement;
                dynamicFragment = (DynamicFragment) firstModelElement;
            }
            
            // get the operation of the input parameter
            Operation operationOfParameter = (Operation) parameter.eContainer();
            
            // get the operation-access-method mapping associated with the operation, and find the associated
            // accessmethod's path fragment
            PathFragment pathFragmentOfOperationAccessMethodMapping = null;
            for (COREModelElementMapping mapping : mappings) {
                // extract model elements of mapping
                EObject mappingObjectOne = mapping.getModelElements().get(0);
                EObject mappingObjectTwo = mapping.getModelElements().get(1);
                
                // enforce only 1 mapping per parameter
                if (mappingObjectOne instanceof Parameter || mappingObjectTwo instanceof Parameter) {
                    Parameter mappedParameter = getParameterFromParameterDynamicFragmentMapping(mapping);   
                    if (mappedParameter.equals(parameter)) {
                        RamApp.getActiveScene().displayPopup(
                                "Error. There can only be one mapping for a Parameter.");
                        
                        return false;
                    }
                }
                
                if (mappingObjectOne instanceof Operation || mappingObjectTwo instanceof Operation) {
                    Operation mappedOperation = getOperationFromOperationAccessMethodMapping(mapping);
                    if (mappedOperation.equals(operationOfParameter)) {
                        AccessMethod mappedAccessMethod = getAccessMethodFromOperationAccessMethodMapping(mapping);
                        
                        Resource resource = (Resource) mappedAccessMethod.eContainer();
                        pathFragmentOfOperationAccessMethodMapping = resource.getEndpoint();
                    }
                }
            }
            
            // if the associated path fragment exists
            if (pathFragmentOfOperationAccessMethodMapping != null) {
                // if the associated path fragment is a descendant or equal to the input dynamic fragment, the mapping 
                // is valid, otherwise display error message and return false
                if (isPathFragOneDescendantOrEqualToPathFragTwo(
                        pathFragmentOfOperationAccessMethodMapping, dynamicFragment)) {
                    return true;
                } else {
                    RamApp.getActiveScene().displayPopup(
                            "Error. The DynamicFragment of the Parameter-DynamicFragment mapping must be on the path of"
                            + " the mapped Operation.");
                    
                    return false;
                }
            } else {
                // if the associated path fragment does not exist, display error meessage and return false
                RamApp.getActiveScene().displayPopup(
                        "Error. Cannot create mappings between Parameter and DynamicFragment if the Operation is" 
                                + " not mapped.");
                
                return false;
            }

        } else if (firstModelElement instanceof AccessMethod && secondModelElement instanceof Operation 
                || firstModelElement instanceof Operation && secondModelElement instanceof AccessMethod) {
            
            // set parameter and dynamic fragment of input mapping
            AccessMethod accessMethod = null;
            Operation operation = null;
            
            if (firstModelElement instanceof AccessMethod) {
                accessMethod = (AccessMethod) firstModelElement;
                operation = (Operation) secondModelElement;
            } else {
                accessMethod = (AccessMethod) secondModelElement;
                operation = (Operation) firstModelElement;
            }
            
            for (COREModelElementMapping mapping : mappings) {
                // extract model elements of mapping
                EObject mappingObjectOne = mapping.getModelElements().get(0);
                EObject mappingObjectTwo = mapping.getModelElements().get(1);
                
                // enforce 1 mapping per access method / operation
                if (mappingObjectOne instanceof AccessMethod || mappingObjectTwo instanceof AccessMethod) {
                    AccessMethod mappedAccessMethod = getAccessMethodFromOperationAccessMethodMapping(mapping);
                    
                    if (mappedAccessMethod.equals(accessMethod)) {
                        RamApp.getActiveScene().displayPopup(
                                "Error. There can only be one mapping for an AccessMethod.");
                        
                        return false;
                    }

                    Operation mappedOperation = getOperationFromOperationAccessMethodMapping(mapping);
                    
                    if (mappedOperation.equals(operation)) {
                        RamApp.getActiveScene().displayPopup(
                                "Error. There can only be one mapping for an Operation.");
                        
                        return false;
                    }
                }
            }
            
            return true;
        }
        RamApp.getActiveScene().displayPopup(
                "Error. Can only create mappings between DynamicFragment and Parameter or AccessMethod and Operation.");
        return false;
    }
    
    @Override
    protected List<COREModelElementMapping> findAdditionalMappingsToRemove(
            HashSet<COREModelElementMapping> mappingsToRemove, List<COREModelElementMapping> allModelElementMappings) {
        List<COREModelElementMapping> additionalMappingsToRemove = new ArrayList<>();
        
        for (COREModelElementMapping removedMapping : mappingsToRemove) {
            if (removedMapping.getModelElements().get(0) instanceof Operation || removedMapping.getModelElements()
                    .get(1) instanceof Operation) {
                Operation parentOperation = getOperationFromOperationAccessMethodMapping(removedMapping);
                
                additionalMappingsToRemove.addAll(findParameterDynamicFragmentMappingsOf(parentOperation, 
                        allModelElementMappings));
            }
        }
        
        return additionalMappingsToRemove;
    }
    
    /**
     * Finds and returns Parameter-DynamicFragment mappings from the input operation's parameters.
     * 
     * @param parentOperation input operation
     * @param allModelElementMappings all mappings in the current core scene
     * @return all Parameter-DynamicFragment mappings that contain a parameter that is in the input operation
     */
    private static Collection<? extends COREModelElementMapping> findParameterDynamicFragmentMappingsOf(
            Operation parentOperation, List<COREModelElementMapping> allModelElementMappings) {
        List<COREModelElementMapping> parameterDynamicFragmentMappingsToRemove = new ArrayList<>();

        for (Parameter parameterOfOperation : parentOperation.getParameters()) {
            for (COREModelElementMapping mapping : allModelElementMappings) {
                if (mapping.getModelElements().get(0) instanceof Parameter || mapping.getModelElements().get(1) 
                        instanceof Parameter) {
                    Parameter mappedParameter = getParameterFromParameterDynamicFragmentMapping(mapping);
                    
                    if (parameterOfOperation.equals(mappedParameter)) {
                        parameterDynamicFragmentMappingsToRemove.add(mapping);
                    }
                }
            }
        }
        
        return parameterDynamicFragmentMappingsToRemove;
    }

    /**
     * Helper method to check if the input path fragment one is a descendant or equal to input path fragment two.
     * 
     * @param pathFragmentOne input path fragment one
     * @param pathFragmentTwo input path fragment two
     * @return whether or not the input path fragment one is a descendant or equal to input path fragment two
     */
    private static boolean isPathFragOneDescendantOrEqualToPathFragTwo(PathFragment pathFragmentOne, PathFragment 
            pathFragmentTwo) {
        EList<PathFragment> currentNodes = new BasicEList<PathFragment>();
        currentNodes.add(pathFragmentTwo);
        
        while (currentNodes.size() > 0) {
            EList<PathFragment> children = new BasicEList<PathFragment>();
            
            for (PathFragment node : currentNodes) {
                if (node.equals(pathFragmentOne)) {
                    return true;
                }
                
                children.addAll(node.getChild());
            }
            
            currentNodes = children;
        }
        
        return false;
    }
    
    /**
     * Helper method to get a Parameter from a Parameter-DynamicFragment mapping.
     * 
     * @param mapping Parameter-DynamicFragment mapping
     * @return the Parameter object
     */
    private static Parameter getParameterFromParameterDynamicFragmentMapping(COREModelElementMapping mapping) {
        // extract model elements of mapping
        EObject mappingObjectOne = mapping.getModelElements().get(0);
        EObject mappingObjectTwo = mapping.getModelElements().get(1);
        
        if (mappingObjectOne instanceof Parameter) {
            return (Parameter) mappingObjectOne;
        } else {
            return (Parameter) mappingObjectTwo;
        }
    }
    
    /**
     * Helper method to get a Operation from a Operation-AccessMethod mapping.
     * 
     * @param mapping Operation-AccessMethod mapping
     * @return the Operation object
     */
    private static Operation getOperationFromOperationAccessMethodMapping(COREModelElementMapping mapping) {
        // extract model elements of mapping
        EObject mappingObjectOne = mapping.getModelElements().get(0);
        EObject mappingObjectTwo = mapping.getModelElements().get(1);
        
        if (mappingObjectOne instanceof Operation) {
            return (Operation) mappingObjectOne;
        } else {
            return (Operation) mappingObjectTwo;
        }
    }
    
    /**
     * Helper method to get an AccessMethod from a Operation-AccessMethod mapping.
     * 
     * @param mapping Operation-AccessMethod mapping
     * @return the AccessMethod object
     */
    private static AccessMethod getAccessMethodFromOperationAccessMethodMapping(COREModelElementMapping mapping) {
        // extract model elements of mapping
        EObject mappingObjectOne = mapping.getModelElements().get(0);
        EObject mappingObjectTwo = mapping.getModelElements().get(1);
        
        if (mappingObjectOne instanceof AccessMethod) {
            return (AccessMethod) mappingObjectOne;
        } else {
            return (AccessMethod) mappingObjectTwo;
        }
    }

}
