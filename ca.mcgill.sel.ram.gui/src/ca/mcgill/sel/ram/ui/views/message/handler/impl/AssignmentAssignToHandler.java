package ca.mcgill.sel.ram.ui.views.message.handler.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.FragmentsController;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;

/**
 * The handler for handling everything related to "assignTo" of an assignment statement. 
 * It supports providing a choice of selections of possible values,
 *  but also to create a temporary property (including renaming it after it is created). 
 *  Furthermore, a previously chosen value can be changed later. 
 *  This handler is not stateless, it needs to keep references to specific model objects it handles.
 *
 * @author mschoettle
 */
public class AssignmentAssignToHandler extends AssignToHandler {

    @Override
    public void handleTemporaryCreate(TextView target, Vector3D position) {
        final AssignmentStatement assignmentStatement = (AssignmentStatement) target.getData();
        
        // Build choice of values.
        final List<ObjectType> choiceOfValues = new ArrayList<>();
        choiceOfValues.addAll(RAMInterfaceUtil.getAvailableClasses(assignmentStatement));
        choiceOfValues.addAll(RAMInterfaceUtil.getAvailablePrimitiveTypes(assignmentStatement));
        
        RamSelectorComponent<ObjectType> typeSelector = new RamSelectorComponent<>(choiceOfValues);
        // Use order of list.
        typeSelector.setElementsOrder(new Comparator<ObjectType>() {
            
            @Override
            public int compare(ObjectType o1, ObjectType o2) {
                return Integer.compare(choiceOfValues.indexOf(o1), choiceOfValues.indexOf(o2));
            }
        });
        typeSelector.registerListener(new AbstractDefaultRamSelectorListener<ObjectType>() {

            @Override
            public void elementSelected(RamSelectorComponent<ObjectType> selector, ObjectType element) {
                FragmentsController controller = ControllerFactory.INSTANCE.getFragmentsController();
                controller.createTemporaryAssignment(assignmentStatement, Strings.DEFAULT_PROPERTY_NAME, element);
                
                closeSelector(selector);
            }
        });
        
        RamApp.getActiveScene().addComponent(typeSelector, position);
    }

    @Override
    public void handleElementSelected(EObject owner, StructuralFeature structuralFeature) {
        AssignmentStatement assignmentStatement = (AssignmentStatement) owner;
        FragmentsController controller = ControllerFactory.INSTANCE.getFragmentsController();

        controller.changeAssignmentAssignTo(assignmentStatement, structuralFeature);
    }

}
