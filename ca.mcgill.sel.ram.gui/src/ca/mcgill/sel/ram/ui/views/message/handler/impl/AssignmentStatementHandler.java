package ca.mcgill.sel.ram.ui.views.message.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.FragmentsController;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.events.listeners.IDragListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView.Iconified;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.message.AssignmentStatementView;

/**
 * The default handler for a view representing an {@link ca.mcgill.sel.ram.AssignmentStatement}.
 * 
 * @author mschoettle
 */
public class AssignmentStatementHandler extends BaseHandler implements IDragListener, ITapAndHoldListener {
    
    private IDragListener dragHandler;
    
    /**
     * The options to display for an execution statement.
     */
    private enum FragmentOptions implements Iconified {
        DELETE(new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR));
        
        private RamImageComponent icon;
        
        /**
         * Creates a new option literal with the given icon.
         * 
         * @param icon the icon to use for this option
         */
        FragmentOptions(RamImageComponent icon) {
            this.icon = icon;
        }
        
        @Override
        public RamImageComponent getIcon() {
            return icon;
        }
    }
    
    /**
     * Creates AssignmentStatementHandler with a drag listener.
     * Drag events are delegated to be handled by the drag listener.
     * 
     * @param handler default drag listener
     */
    public AssignmentStatementHandler(IDragListener handler) {
        this.dragHandler = handler;
        
    }
    
    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            AssignmentStatementView target = (AssignmentStatementView) tapAndHoldEvent.getTarget();
            final InteractionFragment statement = target.getAssignmentStatement();
            
            OptionSelectorView<FragmentOptions> selector =
                    new OptionSelectorView<FragmentOptions>(FragmentOptions.values());
            
            RamApp.getActiveScene().addComponent(selector, tapAndHoldEvent.getLocationOnScreen());
            
            selector.registerListener(new AbstractDefaultRamSelectorListener<FragmentOptions>() {
                @Override
                public void elementSelected(RamSelectorComponent<FragmentOptions> selector, FragmentOptions element) {
                    FragmentsController controller = ControllerFactory.INSTANCE.getFragmentsController();
                    
                    switch (element) {
                        case DELETE:
                            controller.removeInteractionFragment(statement);
                            break;
                    }
                }
            });
            
        }
        
        return true;
    }
    
    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        return dragHandler.processDragEvent(dragEvent);        
    }
    
}
