package ca.mcgill.sel.ram.ui.views.message.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.Gate;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageEnd;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterValue;
import ca.mcgill.sel.ram.ParameterValueMapping;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.ui.views.message.LifelineView;
import ca.mcgill.sel.ram.util.MessageViewUtil;

/**
 * Class that represents a composition of interactions.<br>
 * Internally, a {@link CompositeInteraction} is a tree of {@link Interaction}. Interactions are stored
 * in units called <i>Nodes</i>. Each node can have zero to many children. A child corresponds to an
 * expanded message.
 * 
 * @author emmanuelngch
 *
 */
public class CompositeInteraction {

    /**
     * Unit that stores an interaction a message and a list of child nodes.<br>
     * When a message is expanded, a new node is created and appended to the right parent,
     * i.e. the node that stores the interaction that contains the message. The set of nodes
     * is thus a tree that grows as messages are expanded.
     * 
     * @author emmanuelngch
     *
     */
    private class Node {

        private Interaction interaction;
        private Message message;
        private List<Node> children = new ArrayList<>();
        private Node parent;

    }

    private Node root;
    private List<Message> expandedMessages;

    /**
     * Constructor.
     * 
     * @param initial The initial interaction.
     */
    public CompositeInteraction(Interaction initial) {
        this.root = new Node();
        this.expandedMessages = new ArrayList<>();

        root.interaction = initial;
        root.message = null;
        root.parent = null;
    }

    /**
     * Expands a message.
     * 
     * @param message The expanded message
     * @return The message specification or <code>null</code> if the expansion was unsuccessful
     */
    public Interaction expandMessage(Message message) {
        if (!isExpanded(message)) {
            Interaction specification = MessageViewUtil.findSpecification(message.getSignature());
            Node parent = findNodeWithInteraction(root, message.getInteraction());

            if (parent != null) {
                Node node = new Node();

                node.interaction = specification;
                node.message = message;
                node.parent = parent;

                parent.children.add(node);
                expandedMessages.add(message);

                return specification;
            }
        }

        return null;
    }

    /**
     * Collapses a message.
     * 
     * @param message The message to collapse
     * @return the removed interaction
     */
    public Interaction collapseMessage(Message message) {
        if (isExpanded(message)) {
            Node node = findNodeWithMessage(root, message);
            removeNode(node);
            return node.interaction;
        }

        return null;
    }

    /**
     * Removes a node.
     * 
     * @param node the node to remove
     */
    private void removeNode(Node node) {
        for (Node child : new ArrayList<Node>(node.children)) {
            removeNode(child);
        }

        if (node.parent != null) {
            node.parent.children.remove(node);
            expandedMessages.remove(node.message);
        }
    }

    /**
     * Returns the ordered list of interaction fragments.
     * 
     * @return The ordered list of fragments
     */
    public List<InteractionFragment> getFragments() {
        return updateFragments(root, false);
    }

    /**
     * Returns the list of all interaction fragments (not necessarily ordered).
     * The returned list also includes the fragments located inside the combined fragments.
     * 
     * @return The ordered list of fragments
     */
    public List<InteractionFragment> getAllFragments() {
        return updateFragments(root, true);
    }

    /**
     * Returns the list of messages.
     * 
     * @return The list of messages
     */
    public List<Message> getMessages() {
        return updateMessages(root);
    }
    
    /**
     * Returns the list of all interaction messages (not necessarily ordered).
     * The returned list also includes the messages located inside the combined fragments.
     * 
     * @return The ordered list of messages
     */
    public List<Message> getAllMessages() {
        List<Message> result = new ArrayList<>();
        List<InteractionFragment> allFragments = getAllFragments();

        for (InteractionFragment fragment : allFragments) {
            if (fragment instanceof MessageOccurrenceSpecification) {
                Message message = ((MessageOccurrenceSpecification) fragment).getMessage();
                if (result.isEmpty() || result.get(result.size() - 1) != message) {
                    result.add(message);
                }
            }
        }

        return result;
    }

    /**
     * Returns the list of life lines.
     * 
     * @return The list of life lines
     */
    public List<Lifeline> getLifelines() {
        return updateLifelines(root);
    }

    /**
     * Returns the list of interactions.
     * 
     * @return The list of interactions
     */
    public List<Interaction> getInteractions() {
        return updateInteractions(root);
    }

    /**
     * Returns the map between parameters and their actual parameter value mapping.
     * 
     * @return the map
     */
    public Map<Parameter, ParameterValueMapping> getParameterMappings() {
        return updateParameterMappings(root, new HashMap<>());
    }

    /**
     * Returns the list of fragments, starting from the given node.
     * 
     * @param node the starting node
     * @param includeCombinedFragmentsContents indicates whether the combined fragments
     *            contents must be added to the final list
     * @return the list of fragments
     */
    private List<InteractionFragment> updateFragments(Node node, boolean includeCombinedFragmentsContents) {
        List<InteractionFragment> fragments = new ArrayList<>();

        for (InteractionFragment fragment : node.interaction.getFragments()) {
            if (canAddFragment(fragment, node)) {
                fragments.add(fragment);

                // Add the combined fragments contents
                if (includeCombinedFragmentsContents && fragment instanceof CombinedFragment) {
                    for (InteractionOperand operand : ((CombinedFragment) fragment).getOperands()) {
                        fragments.addAll(getVisualizedFragments(operand, true));
                    }
                }

                if (fragment instanceof MessageOccurrenceSpecification) {
                    Message message = ((MessageOccurrenceSpecification) fragment).getMessage();
                    for (Node child : node.children) {
                        if (child.message == message && message.getReceiveEvent() == fragment) {
                            fragments.addAll(updateFragments(child, includeCombinedFragmentsContents));
                            break;
                        }
                    }
                }
            }
        }

        return fragments;
    }

    /**
     * Returns the list of messages, starting from the given node.
     * 
     * @param node the starting node
     * @return the list of messages
     */
    private List<Message> updateMessages(Node node) {
        List<Message> messages = new ArrayList<>();

        for (Message message : node.interaction.getMessages()) {
            if (canAddMessage(message, node)) {
                messages.add(message);
            }
        }

        for (Node child : node.children) {
            messages.addAll(updateMessages(child));
        }

        return messages;
    }

    /**
     * Returns the list of life lines, starting from the given node.
     * 
     * @param node the starting node
     * @return the list of life lines
     */
    private List<Lifeline> updateLifelines(Node node) {
        List<Lifeline> lifelines = new ArrayList<>();

        for (Lifeline lifeline : node.interaction.getLifelines()) {
            if (canAddLifeline(lifeline, node)) {
                lifelines.add(lifeline);
            }
        }

        for (Node child : node.children) {
            lifelines.addAll(updateLifelines(child));
        }

        return lifelines;
    }

    /**
     * Returns the list of interactions, starting from the given node.
     * 
     * @param node the starting node
     * @return the list of interactions
     */
    private List<Interaction> updateInteractions(Node node) {
        List<Interaction> interactions = new ArrayList<>();

        interactions.add(node.interaction);

        for (Node child : node.children) {
            interactions.addAll(updateInteractions(child));
        }

        return interactions;
    }

    /**
     * Returns the map between parameters and their actual parameter value mapping,
     * starting from the given node.
     * 
     * @param node the starting node
     * @param previousResult the result from the parent node
     * @return the map
     */
    private Map<Parameter, ParameterValueMapping> updateParameterMappings(Node node,
            Map<Parameter, ParameterValueMapping> previousResult) {
        Map<Parameter, ParameterValueMapping> result = new HashMap<>();
    
        if (node.message != null) {
            for (ParameterValueMapping argument : node.message.getArguments()) {
                Parameter parameter = argument.getParameter();
                ValueSpecification value = argument.getValue();
                ParameterValueMapping actual = argument;
    
                if (value instanceof ParameterValue) {
                    Parameter references = ((ParameterValue) value).getParameter();
                    if (previousResult.containsKey(references)) {
                        actual = previousResult.get(references);
                    }
                }
    
                result.put(parameter, actual);
            }
        }
    
        for (Node child : node.children) {
            result.putAll(updateParameterMappings(child, result));
        }
    
        return result;
    }

    /**
     * Indicates whether a message can be added to the message list.
     * 
     * @param message the message
     * @param node the node
     * @return <code>true</code> if the message can be added, <code>false</code> otherwise
     */
    private boolean canAddMessage(Message message, Node node) {
        if (message.getSendEvent() instanceof Gate && node != root) {
            return false;
        }

        return true;
    }

    /**
     * Indicates whether a fragment can be added to the fragment list.
     * 
     * @param fragment the fragment
     * @param node the node
     * @return <code>true</code> if the fragment can be added, <code>false</code> otherwise
     */
    private boolean canAddFragment(InteractionFragment fragment, Node node) {
        if (fragment instanceof MessageOccurrenceSpecification
                && !canAddMessage(((MessageOccurrenceSpecification) fragment).getMessage(), node)) {
            return false;
        }

        return true;
    }

    /**
     * Indicates whether a life line can be added to the life line list.
     * 
     * @param lifeline the life line
     * @param node the node
     * @return <code>true</code> if the life line can be added, <code>false</code> otherwise
     */
    private boolean canAddLifeline(Lifeline lifeline, Node node) {
        if ("target".equals(lifeline.getRepresents().getName()) && node != root) {
            return false;
        }

        return true;
    }

    /**
     * Returns the root interaction.
     * 
     * @return The root interaction
     */
    public Interaction getInitialInteraction() {
        return root.interaction;
    }

    /**
     * Finds the {@link Node} that contains the given interaction.
     * 
     * @param node The starting node (in general the root node)
     * @param interaction The interaction
     * @return The node if it exists, <code>false</code> otherwise
     */
    private Node findNodeWithInteraction(Node node, Interaction interaction) {
        if (node.interaction == interaction) {
            return node;
        }

        for (Node child : node.children) {
            Node result = findNodeWithInteraction(child, interaction);
            if (result != null) {
                return result;
            }
        }

        return null;
    }

    /**
     * Finds the node that has the given message as its node.message.
     * 
     * @param node the node
     * @param message the message
     * @return The node if it exists, <code>false</code> otherwise
     */
    private Node findNodeWithMessage(Node node, Message message) {
        if (node.message == message) {
            return node;
        }

        for (Node child : node.children) {
            Node result = findNodeWithMessage(child, message);
            if (result != null) {
                return result;
            }
        }

        return null;
    }

    /**
     * Finds the node that contains the given fragment container.
     * 
     * @param node the starting node
     * @param container the container
     * @return the node if it exists, or <code>null</code> otherwise
     */
    private Node findNodeWithContainer(Node node, FragmentContainer container) {
        if (node.interaction == container || EMFModelUtil.getRootContainerOfType(container,
                RamPackage.Literals.INTERACTION) == node.interaction) {
            return node;
        }

        for (Node child : node.children) {
            Node foundNode = findNodeWithContainer(child, container);
            if (foundNode != null) {
                return foundNode;
            }
        }

        return null;
    }

    /**
     * Returns the start fragment of an interaction, i.e. the fragment after which the expanded
     * messages must be placed.
     * 
     * @param interaction The interaction
     * @return The start fragment if it exists, <code>null</code> otherwise
     */
    public InteractionFragment getStartFragment(Interaction interaction) {
        Node node = findNodeWithInteraction(root, interaction);

        if (node != null && node.message != null) {
            MessageEnd end = node.message.getReceiveEvent();
            if (end instanceof InteractionFragment) {
                return (InteractionFragment) end;
            }
        }

        return null;
    }

    /**
     * Returns the expanded message specified by the given interaction.
     * 
     * @param interaction the interaction
     * @return the message or <code>null</code> if it does not exist
     */
    public Message getStartMessage(Interaction interaction) {
        Node node = findNodeWithInteraction(root, interaction);
        return node != null ? node.message : null;
    }

    /**
     * Returns the life line that calls an interaction, i.e the life line that contains the expanded message
     * specified by the given interaction.
     * 
     * @param interaction The interaction
     * @return The calling lifeline or <code>null</code> if it does not exist
     */
    public Lifeline getCallingLifeline(Interaction interaction) {
        Node node = findNodeWithInteraction(root, interaction);

        if (node != null) {
            MessageEnd end = node.message.getSendEvent();
            if (end instanceof InteractionFragment) {
                return ((InteractionFragment) end).getCovered().get(0);
            }
        }

        return null;
    }

    /**
     * Returns whether the given message is currently expanded or not.
     * 
     * @param message The message
     * @return <code>true</code> if the message is expanded, <code>false</code> otherwise
     */
    public boolean isExpanded(Message message) {
        return expandedMessages.contains(message);
    }

    /**
     * Indicates whether this composite interaction contains the given interaction.
     * 
     * @param interaction The interaction
     * @return <code>true</code> if the this contains the interaction, <code>false</code> otherwise
     */
    public boolean containsInteraction(Object interaction) {
        return getInteractions().contains(interaction);
    }

    /**
     * Indicates whether the given message can be expanded.
     * 
     * @param message The message
     * @return <code>true</code> if it can be expanded, <code>false</code> otherwise
     */
    public boolean canExpandAndCollapse(Message message) {
        if (expandedMessages.contains(message)) {
            // If the message is already expanded, always allow expand/collapse
            return true;
        } else {
            Operation signature = message.getSignature();
            Aspect aspect = EMFModelUtil.getRootContainerOfType(signature, RamPackage.Literals.ASPECT);
            
            return signature != null
                    && aspect != null
                    && !message.isSelfMessage()
                    && MessageViewUtil.findSpecification(signature) != root.interaction
                    && !isRecursiveCall(message)
                    && (signature != null
                            && !(signature.eContainer() instanceof ImplementationClass));
        }
    }

    /**
     * Returns the list of fragments that are visualized for a given fragment container.
     * Does NOT include the contents from the combined fragments.
     * 
     * @param container the container
     * @return the list of fragments
     */
    public List<InteractionFragment> getVisualizedFragments(FragmentContainer container) {
        return getVisualizedFragments(container, false);
    }

    /**
     * Returns the list of fragments that are visualized for a given fragment container.
     * 
     * @param container the container
     * @param includeCombinedFragmentsContents indicates whether the contents of the combined fragments must be added
     * @return the list of fragments
     */
    public List<InteractionFragment> getVisualizedFragments(FragmentContainer container,
            boolean includeCombinedFragmentsContents) {
        List<InteractionFragment> fragments = new ArrayList<>();
        Node node = findNodeWithContainer(root, container);

        if (node != null) {
            for (InteractionFragment fragment : container.getFragments()) {
                if (canAddFragment(fragment, node)) {
                    fragments.add(fragment);

                    // Add the combined fragments contents
                    if (includeCombinedFragmentsContents && fragment instanceof CombinedFragment) {
                        CombinedFragment combinedFragment = (CombinedFragment) fragment;
                        for (InteractionOperand operand : combinedFragment.getOperands()) {
                            fragments.addAll(getVisualizedFragments(operand, includeCombinedFragmentsContents));
                        }
                    }

                    if (fragment instanceof MessageOccurrenceSpecification) {
                        Message message = ((MessageOccurrenceSpecification) fragment).getMessage();
                        for (Node child : node.children) {
                            if (child.message == message && message.getReceiveEvent() == fragment) {
                                fragments.addAll(updateFragments(child, includeCombinedFragmentsContents));
                                break;
                            }
                        }
                    }
                }
            }
        }

        return fragments;
    }

    /**
     * Returns the ordered list of fragments from the given container visualized on the given life lines.
     * The returned list includes the fragments from the expanded messages.
     * 
     * @param container the container
     * @param lifelines the life lines
     * @param includeOperandContents if <code>true</code>, also includes the operand contents
     * @return the ordered list of fragment
     */
    public List<InteractionFragment> getVisualizedFragments(FragmentContainer container, Set<Lifeline> lifelines,
            boolean includeOperandContents) {
        List<InteractionFragment> result = new ArrayList<>();

        for (InteractionFragment fragment : getVisualizedFragments(container, includeOperandContents)) {
            List<Lifeline> covered = null;

            /*
             * If the fragment is a combined fragment get the actual list of covered fragments.
             * For instance, if a life line part of an expanded message within a combined fragment,
             * then the combined fragment will be added to the list of visualized fragments on this
             * life line.
             */
            if (fragment instanceof CombinedFragment) {
                covered = getCoveredLifelines((CombinedFragment) fragment);
            } else {
                covered = fragment.getCovered();
            }

            for (Lifeline lifeline : covered) {
                if (lifelines.contains(lifeline)) {
                    result.add(fragment);
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Returns the list of visualized fragments contained in the message's specification.
     * The fragments from the expanded messages located below the given message in the hierarchy are NOT included
     * (no recursion).
     * 
     * @param message the message
     * @return the list of fragments. The list is empty if the message is not expanded.
     */
    public List<InteractionFragment> getVisualizedFragments(Message message) {
        List<InteractionFragment> result = new ArrayList<>();

        if (isExpanded(message)) {
            Node node = findNodeWithMessage(root, message);
            for (InteractionFragment fragment : node.interaction.getFragments()) {
                if (canAddFragment(fragment, node)) {
                    result.add(fragment);
                }
            }
        }

        return result;
    }

    /**
     * Returns the list of all the lifelines covered by the fragment (including target and parameter life lines).
     * This list includes the lifelines from the expanded messages. Two life lines from the returned list might
     * be represented by the same {@link LifelineView} in the message view view.
     * 
     * @param fragment the fragment
     * @return the list of lifelines
     */
    public List<Lifeline> getCoveredLifelines(InteractionFragment fragment) {
        List<Lifeline> result = new ArrayList<>(fragment.getCovered());

        if (fragment instanceof CombinedFragment) {
            CombinedFragment combinedFragment = (CombinedFragment) fragment;

            for (InteractionOperand operand : combinedFragment.getOperands()) {
                for (InteractionFragment visualized : getVisualizedFragments(operand, true)) {
                    for (Lifeline lifeline : visualized.getCovered()) {
                        if (!result.contains(lifeline)) {
                            result.add(lifeline);
                        }
                    }
                }
            }

        }

        return result;
    }

    /**
     * Returns all the fragments visualized in the given combined fragment.
     * 
     * @param combinedFragment the combined fragment
     * @return the set of fragments
     */
    public Set<InteractionFragment> getVisualizedFragments(CombinedFragment combinedFragment) {
        Set<InteractionFragment> result = new HashSet<>();

        for (InteractionOperand operand : combinedFragment.getOperands()) {
            result.addAll(getVisualizedFragments(operand, true));
        }

        return result;
    }

    /**
     * Indicates whether a message is a recursive call (i.e. a call to itself).
     * 
     * @param message the message
     * @return <code>true</code> if it is a recursive call, <code>false</code> otherwise
     */
    public boolean isRecursiveCall(Message message) {
        Node messageNode = findNodeWithInteraction(root, message.getInteraction());

        if (messageNode != null) {
            Node currentNode = messageNode;
            while (currentNode != null && currentNode.message != null) {
                if (currentNode.message.getSignature() == message.getSignature()) {
                    return true;
                }
                currentNode = currentNode.parent;
            }
        }

        return false;
    }

    /**
     * Returns the actual root container of the given fragment.
     * 
     * @param fragment the fragment
     * @return the root container
     */
    public FragmentContainer getActualRootContainer(InteractionFragment fragment) {
        FragmentContainer container = fragment.getContainer();

        if (container instanceof InteractionOperand) {
            return container;
        } else {
            Interaction interaction = EMFModelUtil.getRootContainerOfType(fragment, RamPackage.Literals.INTERACTION);
            Node node = findNodeWithInteraction(root, interaction);

            while (node != null && node.message != null) {
                EObject startFragmentContainer = node.message.getReceiveEvent().eContainer();

                if (startFragmentContainer instanceof InteractionOperand) {
                    return (InteractionOperand) startFragmentContainer;
                }

                node = node.parent;
            }

            return root.interaction;
        }
    }

    /**
     * Returns the list of expanded message in the order in which they were expanded.
     * 
     * @return the list
     */
    public List<Message> getExpandedMessages() {
        return new ArrayList<>(expandedMessages);
    }

}
