package ca.mcgill.sel.ram.ui.views.structural.handler;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.ui.views.handler.ICompositionViewHandler;

/**
 * This interface can be implemented by an handler which handles the operations of a composition view such as
 * classifier mapping or enum mapping adding/removing.
 * 
 * @author eyildirim
 */
public interface IRamCompositionViewHandler extends ICompositionViewHandler {
    
    /**
     * This function is used to add a classifier mapping view.
     * 
     * @param composition the composition
     */
    void addClassifierMapping(COREModelComposition composition);
    
    /**
     * This function is used to add an enum mapping view.
     * 
     * @param composition the composition
     */
    void addEnumMapping(COREModelComposition composition);    
}
