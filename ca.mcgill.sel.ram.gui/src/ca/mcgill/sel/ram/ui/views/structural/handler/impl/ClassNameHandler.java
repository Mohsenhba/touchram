package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.ValidatingTextViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.IClassNameHandler;
import ca.mcgill.sel.ram.util.MetamodelRegex;

/**
 * The default handler for a {@link TextView} representing the name of a {@link ca.mcgill.sel.ram.Class}. 
 * The name gets validated in order to only allow valid names.
 *
 * @author mschoettle
 * @author oalam
 */
public class ClassNameHandler extends ValidatingTextViewHandler implements IClassNameHandler {

    /**
     * Creates a new instance of {@link ClassNameHandler}.
     */
    public ClassNameHandler() {
        super(MetamodelRegex.REGEX_LONG_CLASS_NAME);
    }

    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        String text = textView.getText();

        if (!text.isEmpty()) {
            if (text.contains(".")) {
                String[] names = text.split("\\.");
                names[names.length-1] = StringUtil.toUpperCaseFirst(names[names.length-1]);
                textView.setText(StringUtil.toUpperCaseFirst(text));
                StringBuilder s = new StringBuilder();
                s.append(names[0]);
                for (int i = 1; i < names.length; i++) {
                    s.append(".");
                    s.append(names[i]);
                }
                textView.setText(s.toString());
            } else {
                textView.setText(StringUtil.toUpperCaseFirst(text));
            }
        }

        return super.shouldDismissKeyboard(textView);
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        return false;
    }

}
