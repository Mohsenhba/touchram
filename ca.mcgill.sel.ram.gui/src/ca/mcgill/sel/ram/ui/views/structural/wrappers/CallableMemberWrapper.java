package ca.mcgill.sel.ram.ui.views.structural.wrappers;

import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.List;

import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.util.StructuralViewUtil;

/**
 * An abstract callable member wrapper for Java callable members. A callable
 * member can be, for example, a method or a constructor.
 * 
 * @author Franz Garcia
 */
public abstract class CallableMemberWrapper {

    /**
     * The default parameter name prefix.
     */
    public static final String DEFAULT_PARAM_NAME = "arg";

    /**
     * The owner of the callable member within the model.
     */
    protected ImplementationClass owner;

    /**
     * The modifiers of the callable member.
     */
    protected int modifier;

    /**
     * The name of the callable member.
     */
    protected String name;

    /**
     * The list of parameter types. Comment by Max: this is bad naming. It does
     * not actually store the parameters, only the parameter types. The
     * parameter names are unfortunately not taken into account in this
     * implementation.
     */
    protected List<Parameter> parameters;

    /**
     * Creates a new callable member wrapper.
     * 
     * @param owner the {@link ImplementationClass} in the model this member
     * belongs to
     * @param modifier the modifiers
     * @param name the name
     * @param parameters the list of parameter types
     */
    public CallableMemberWrapper(ImplementationClass owner, int modifier, String name, List<Parameter> parameters) {
        this.owner = owner;
        this.modifier = modifier;
        this.name = name;
        this.parameters = parameters;
    }

    /**
     * Getter for the owner of this constructor.
     * 
     * @return Implementation class owner of this method.
     */
    public ImplementationClass getOwner() {
        return owner;
    }

    /**
     * Getter for visibility.
     * 
     * @return String representing the visibility.
     */
    public String getVisibility() {
        String v = "public";
        if (Modifier.isProtected(this.modifier)) {
            v = "protected";
        } else if (Modifier.isPrivate(this.modifier)) {
            v = "private";
        }
        return v;
    }

    /**
     * Getter for the name of the method.
     * 
     * @return name of the method
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for the parameters. Also display parameter names if the provided
     * artifact contains them (only the case if jar was build with compiler
     * instructions to include debug information).
     * 
     * @return String with all the parameters
     * @author Maxmilian Schiedermeier
     */
    public String getParametersAsString() {

        // Initialize counter required for parameter name enumeration (fallback
        // naming, if no qualified parameter names are available)
        int defaultArgNameCounter = 0;

        // Iterate over all method parameters and construct effective parameter
        // string on the fly using a stringbuilder.
        StringBuilder allParametersSB = new StringBuilder();
        for (java.lang.reflect.Parameter parameter : parameters) {

            // Append parameter type and separating whitespace
            allParametersSB.append(StructuralViewUtil.getNameOfType(parameter.getParameterizedType(), owner));
            allParametersSB.append(" ");

            // Append effective parameter name
            allParametersSB.append(getEffectiveParamName(parameter, defaultArgNameCounter++));

            // Append parameter separator ", "
            allParametersSB.append(", ");
        }

        // Remove last trailing ", " (serves only as parameter separator, the
        // last parameter does not need a tariling separator).
        if (allParametersSB.length() > 0) {
            allParametersSB.setLength(allParametersSB.length() - 2);
        }
        
        // Return the full strign covering all parameters (types and effective
        // names)
        return allParametersSB.toString();
    }

    /**
     * Helper method to determine the effective name of a parameter. Depending
     * on the compile mode of the provided JAR artifact, parameter names may be
     * present or not. This method tries to extract a qualified parameter name
     * and falls back to a default naming convention of no qualified name is
     * present.
     * 
     * @param param as the parameter to determine an effective name for
     * @param index as the index of the parameter in the encompassing operation
     * @return the effective parameter name string. Either a qualified name,
     * like e.g. "foo", or as fallback the indexed name "argX" where X marks the
     * parameter index.
     * @author Maximilian Schiedermeier
     */
    public static String getEffectiveParamName(Parameter param, int index) {

        if (param.isNamePresent()) {
            // Looks good, the provided JAR was build with debug flags and
            // we can conveniently extract default parameter names using
            // reflection
            return param.getName();
        } else {
            return DEFAULT_PARAM_NAME + index;
        }
    }

    /**
     * Returns the list of parameters of this callable member as RAM parameters.
     * 
     * @return the list of parameters
     */
    public List<Parameter> getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        return getStringRepresentation();
    }

    /**
     * Returns the string representation of this callable member.
     * 
     * @return the string reperesentation
     */
    protected abstract String getStringRepresentation();

}
