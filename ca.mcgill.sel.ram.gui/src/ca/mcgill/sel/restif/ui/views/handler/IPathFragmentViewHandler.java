package ca.mcgill.sel.restif.ui.views.handler;

import ca.mcgill.sel.restif.ui.views.PathFragmentView;

/**
 * The handler of the {@link PathFragmentView}.
 * 
 * @author Bowen
 */
public interface IPathFragmentViewHandler extends IBaseViewHandler {
    
    /**
     * Handles the switch of a {@link PathFragment} from {@link StaticFragment} to {@link DynamicFragment} 
     * and vice versa.
     * 
     * @param pathFragmentView - the view representing the {@link PathFragment} to be switched
     */
    void switchPathFragment(PathFragmentView pathFragmentView);
}
