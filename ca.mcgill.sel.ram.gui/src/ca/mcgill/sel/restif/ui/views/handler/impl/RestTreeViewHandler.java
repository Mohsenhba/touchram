package ca.mcgill.sel.restif.ui.views.handler.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.GenericSplitView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.language.controller.ControllerFactory;
import ca.mcgill.sel.restif.ui.scenes.DisplayRestTreeScene;
import ca.mcgill.sel.restif.ui.views.BaseView;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;
import ca.mcgill.sel.restif.ui.views.handler.IRestTreeViewHandler;

/**
 * The handler of the {@link RestTreeView}.
 * 
 * @author Bowen
 */
public class RestTreeViewHandler extends AbstractViewHandler implements IRestTreeViewHandler {

    private enum CreatePathFragment {
        CREATE_STATIC_FRAGMENT,
        CREATE_DYNAMIC_FRAGMENT
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {        
        if (tapEvent.isTapped()) {

            RestTreeView target = (RestTreeView) tapEvent.getTarget();
            target.deselect();
        }

        return true;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {        
        return false;
    }

    @Override
    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
            UnistrokeEvent event) {
        RestTreeView view;
        
        // added for when current view is in generic split view to get appropriate scene
        if (RamApp.getActiveScene().getCurrentView() instanceof GenericSplitView) {
            GenericSplitView<?, ?, ?> genericSplitView = (GenericSplitView<?, ?, ?>) RamApp
                    .getActiveScene().getCurrentView();
            
            if (genericSplitView.getDisplaySceneInnerLevel() instanceof DisplayRestTreeScene) {
                view = (RestTreeView) genericSplitView.getInnerLevelView();
            } else {
                view = (RestTreeView) genericSplitView.getOuterLevelView();
            }
        } else {
            view = ((DisplayRestTreeScene) RamApp.getActiveScene()).getRestTreeView();
        }
                
        if (view != null) {
            PathFragmentView startPathFragmentView = view.liesAround(
                    event.getCursor().getStartPosition());
            Vector3D childPosition = event.getCursor().getPosition();

            if (startPathFragmentView != null) {
                OptionSelectorView<CreatePathFragment> selector =
                        new OptionSelectorView<CreatePathFragment>(CreatePathFragment.class.getEnumConstants());

                RamApp.getActiveScene().addComponent(selector, childPosition);
                
                // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                selector.registerListener(new AbstractDefaultRamSelectorListener<CreatePathFragment>() {
                    @Override
                    public void elementSelected(RamSelectorComponent<CreatePathFragment> selector,
                            CreatePathFragment element) {
                        switch (element) {
                            case CREATE_STATIC_FRAGMENT:
                                createStaticFragment((RestTreeView) target, startPathFragmentView.getPathFragment());
                                break;
                            case CREATE_DYNAMIC_FRAGMENT:
                                createDynamicFragment((RestTreeView) target, startPathFragmentView.getPathFragment());
                                break;
                        }
                    }
                });
            }
        }
    }

    /**
     * This method calls the controller to execute an EMF command to add a {@link StaticFragment}.
     * 
     * @param view - the {@link RestTreeView}
     * @param parent - the parent of the created {@link StaticFragment}
     */
    public void createStaticFragment(final RestTreeView view, PathFragment parent) {
        final RestIF restIF = (RestIF) view.getRestIF();

        restIF.eAdapters().add(new EContentAdapter() {
            private PathFragment pathFragment;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == RestifPackage.Literals.PATH_FRAGMENT__CHILD) {
                    if (notification.getEventType() == Notification.ADD) {
                        pathFragment = (PathFragment) notification.getNewValue();
                        
                        view.getPathFragmentViewOf(pathFragment).showKeyboard();
                        view.getPathFragmentViewOf(pathFragment).clearNameField();
                        restIF.eAdapters().remove(this);
                    }
                }
            }
        });

        String childName = "/child";
        ControllerFactory.INSTANCE.getRestTreeController().addChildStaticFragment(
                view.getRestIF(), parent, childName);
    }

    /**
     * This method calls the controller to execute an EMF command to add a {@link DynamicFragment}.
     * 
     * @param view - the {@link RestTreeView}
     * @param parent - the parent of the created {@link DynamicFragment}
     */
    public void createDynamicFragment(final RestTreeView view, PathFragment parent) {
        final RestIF restIF = (RestIF) view.getRestIF();

        restIF.eAdapters().add(new EContentAdapter() {
            private PathFragment pathFragment;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == RestifPackage.Literals.PATH_FRAGMENT__CHILD) {
                    if (notification.getEventType() == Notification.ADD) {
                        pathFragment = (PathFragment) notification.getNewValue();
                        
                        view.getPathFragmentViewOf(pathFragment).showKeyboard();
                        view.getPathFragmentViewOf(pathFragment).clearNameField();
                        restIF.eAdapters().remove(this);
                    }
                }
            }
        });

        String childName = "/{child}";
        ControllerFactory.INSTANCE.getRestTreeController().addChildDynamicFragment(
                view.getRestIF(), parent, childName);
    }

    @Override
    public boolean handleDoubleTap(RestTreeView restTreeView, BaseView<?> target) {
        return false;
    }

    @Override
    public boolean handleTapAndHold(RestTreeView restTreeView, BaseView<?> target) {
        return false;
    }

    @Override
    public void dragAllSelected(RestTreeView restTreeView, Vector3D directionVector) {
        for (BaseView<?> baseView : restTreeView.getSelectedElements()) {
            baseView.translateGlobal(new Vector3D(directionVector));
        }
    }
}
