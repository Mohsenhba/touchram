package ca.mcgill.sel.usecases.ui.scenes.handler.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;
import org.mt4j.sceneManagement.transition.BlendTransition;
import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseModelScene;
import ca.mcgill.sel.usecases.ui.utils.UcModelUtils;
import ca.mcgill.sel.usecases.ui.views.UseCaseDetailView;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;
import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.emf.util.CORECommandStack;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.generator.UseCaseDetailGeneratorFlags;
import ca.mcgill.sel.ram.generator.usecases.UseCaseDetailGenerator;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.RamFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.RamFileBrowser.RamFileBrowserType;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.RamFileBrowserListener;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.components.navigationbar.SlideUpDownTransition;
import ca.mcgill.sel.ram.ui.layouts.AutomaticLayout;
import ca.mcgill.sel.ram.ui.layouts.Layout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory.CurrentMode;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.BasicActionsUtils;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.CompositionView;

/**
 * The handler for the main use case diagram scene.
 * @author rlanguay
 *
 */
public class DisplayUseCaseModelSceneHandler implements IDisplaySceneHandler {

    /**
     * A listener for a confirm popup.
     * Handles the user response on whether to save in case modifications occurred.
     */
    private final class SaveConfirmListener implements ConfirmPopup.SelectionListener {
        private final DisplayUseCaseModelScene scene;

        /**
         * Creates a new instance for the given scene.
         * 
         * @param scene the scene for this listener
         */
        private SaveConfirmListener(DisplayUseCaseModelScene scene) {
            this.scene = scene;
        }

        @Override
        public void optionSelected(int selectedOption) {
            if (selectedOption == ConfirmPopup.YES_OPTION) {
                BasicActionsUtils.saveModel(scene.getUseCaseDiagram(), new FileBrowserListener() {
                    @Override
                    public void modelSaved(File file) {
                        NavigationBar.getInstance().popSection();
                        if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                            DisplayConcernSelectScene prev = (DisplayConcernSelectScene) scene.getPreviousScene();
                            ConcernSelectSceneHandler prevH = (ConcernSelectSceneHandler) prev.getHandler();
                            prevH.switchToPreviousScene(prev);
                            NavigationBar.getInstance().wipeNavigationBar();

                        } else {
                            doSwitchToPreviousScene(scene);
                        }
                    }

                    @Override
                    public void modelLoaded(EObject model) {
                    }
                });
            } else if (selectedOption == ConfirmPopup.NO_OPTION) {

                if (EMFEditUtil.getCommandStack(scene.getUseCaseDiagram()) instanceof CORECommandStack) {
                    CORECommandStack stack = (CORECommandStack) EMFEditUtil.getCommandStack(scene.getUseCaseDiagram());
                    
                    if (stack.canRevertToLastSave()) {
                        stack.goToLastSave();
                    } else {
                        scene.displayPopup(Strings.POPUP_REVERT_IMPOSSIBLE, PopupType.ERROR);
                        return;
                    }
                } else {
                    LoggerUtils.warn(Strings.invalidCommandStackInstance(scene.getArtefact()));
                }
                
                NavigationBar.getInstance().popSection();
                checkForModelRemoval(scene);
                
                if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                    DisplayConcernSelectScene prev = (DisplayConcernSelectScene) scene.getPreviousScene();
                    ConcernSelectSceneHandler prevH = (ConcernSelectSceneHandler) prev.getHandler();
                    prevH.switchToPreviousScene(prev);
                    NavigationBar.getInstance().wipeNavigationBar();

                } else {
                    doSwitchToPreviousScene(scene);
                }
            }
                
        }
    }

    /**
     * The listener for the generate selector.
     */
    private final class GenerateSelectorListener extends AbstractDefaultRamSelectorListener<GenerateOptions> {

        private final DisplayUseCaseModelScene scene;
        private List<UseCase> useCasesToGenerate;

        /**
         * Creates a new generate selector listener.
         *
         *@param scene The scene
         */
        private GenerateSelectorListener(DisplayUseCaseModelScene scene) {
            this.scene = scene;
            MTComponent currentView = scene.getCurrentView();
            if (currentView instanceof UseCaseDiagramView) {
                // Generate for all use cases in the diagram
                UseCaseModel ucm = ((UseCaseDiagramView) currentView).getUseCaseModel();
                useCasesToGenerate = ucm.getUseCases();
            } else if (currentView instanceof UseCaseDetailView) {
                UseCase uc = ((UseCaseDetailView) currentView).getUseCase();
                useCasesToGenerate = new ArrayList<UseCase>();
                useCasesToGenerate.add(uc);
            }
        }

        @Override
        public void elementSelected(
                RamSelectorComponent<GenerateOptions> selector,
                final GenerateOptions element) {
            RamFileBrowser browser = new RamFileBrowser(RamFileBrowserType.FOLDER, "", lastGeneratorSharedFolder);
            browser.setCallbackThreaded(true);
            browser.setCallbackPopupMessage(Strings.POPUP_GENERATING);
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            browser.addFileBrowserListener(new RamFileBrowserListener() {

                @Override
                public void fileSelected(final File path, RamFileBrowser fileBrowser) {
                    try {
                        // Acceleo just prints exceptions during generation on the error console.
                        // Make sure we can write to the folder beforehand.
                        // Any other error that occurs will most likely be a template problem.
                        if (path.isDirectory() && !Files.isWritable(path.toPath())) {
                            throw new IOException("Permission denied. Cannot write to " + path.getAbsolutePath());
                        }
                        
                        ArrayList<String> generatorArguments = new ArrayList<String>();
                        
                        switch (element) {
                            case RICH:
                                generatorArguments.add(UseCaseDetailGeneratorFlags.RICH_TEXT.name());
                                break;
                            case PLAIN:
                                generatorArguments.add(UseCaseDetailGeneratorFlags.PLAIN_TEXT.name());
                                break;
                        }
                        
                        for (UseCase uc : useCasesToGenerate) {
                            UseCaseDetailGenerator useCaseGenerator = new UseCaseDetailGenerator(uc, path,
                                    generatorArguments);
                            useCaseGenerator.doGenerate(null);    
                        }                        
                        
                        RamApp.getApplication().invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                String message = "use case detail(s) in format " + element.toString();
                                scene.displayPopup(new RamPopup(
                                        Strings.popupCodeGenerated(message, path.getAbsolutePath()), true,
                                        PopupType.SUCCESS));
                            }
                        });

                        lastGeneratorSharedFolder = path;
                        // CHECKSTYLE:IGNORE IllegalCatch FOR 1 LINES: Need to catch all types
                    } catch (final Exception e) {
                        e.printStackTrace();
                        RamApp.getApplication().invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                scene.displayPopup(new RamPopup(Strings.POPUP_GENERATION_ERROR + e.getMessage(),
                                        true, PopupType.ERROR));
                            }
                        });
                    }
                }
            });
            browser.display();
        }
    }

    /**
     * Generator target languages or models.
     */
    private enum GenerateOptions {
        RICH(Strings.OPT_GENERATE_UC_RICH),
        PLAIN(Strings.OPT_GENERATE_UC_PLAIN);
        
        private String name;
        
        /**
         * Creates a new literal with the given name.
         * 
         * @param name the name the literal represents
         */
        GenerateOptions(String name) {
            this.name = name;
        }
        
        @Override
        public String toString() {
            return name;
        }
    }
    
    private static File lastGeneratorSharedFolder = new File(GUIConstants.DIRECTORY_MODELS).getAbsoluteFile();
    
    @Override
    public void save(EObject element) {
        if (element instanceof COREConcern) {
            BasicActionsUtils.saveConcern((COREConcern) element);
        } else if (element instanceof UseCaseModel) {
            BasicActionsUtils.saveModel((UseCaseModel) element, null);
        }        
    }

    @Override
    public void undo(EObject element) {
        BasicActionsUtils.undo(element);        
    }

    @Override
    public void redo(EObject element) {
        BasicActionsUtils.redo(element);
        
    }

    @Override
    public void generate(RamAbstractScene<?> scene) {
        DisplayUseCaseModelScene ucScene = (DisplayUseCaseModelScene) scene;
        
        OptionSelectorView<GenerateOptions> selector =
                new OptionSelectorView<GenerateOptions>(GenerateOptions.values());
        RamApp.getActiveScene().addComponent(selector, scene.getMenu().getCenterPointGlobal());

        selector.registerListener(new GenerateSelectorListener(ucScene));
    }

    @Override
    public void loadScene(RamAbstractScene<?> scene) {
        // Transition to the right

        // Ask the user to load a model
        GenericFileBrowser.loadModel("usecasediagram", new FileBrowserListener() {

            @Override
            public void modelLoaded(EObject model) {
                SceneCreationAndChangeFactory.getFactory().navigateToModel(model, CurrentMode.EDIT);
            }

            @Override
            public void modelSaved(File file) {
            }
        });        
    }

    @Override
    public void showValidation(RamAbstractScene<?> scene) {
    }

    @Override
    public void showTracing(RamAbstractScene<?> scene) {
    }

    @Override
    public void back(RamAbstractScene<?> scene) {
        ((DisplayUseCaseModelScene) scene).switchToPreviousView();        
    }

    @Override
    public void switchToCompositionEditMode(RamAbstractScene<?> scene, CompositionView compositionView) {       
    }

    @Override
    public void switchToMenu(RamAbstractScene<?> scene) {
     // to the left!
        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, false));
        // go to SelectAspectScene
        RamApp.getApplication().switchToBackground((DisplayUseCaseModelScene) scene);        
    }

    @Override
    public void weaveAll(RamAbstractScene<?> scene) {
    }

    @Override
    public void weaveStateMachines(RamAbstractScene<?> scene) {
    }

    @Override
    public void weaveAllNoCSPForStateViews(RamAbstractScene<?> scene) {
    }

    @Override
    public void switchToConcern(RamAbstractScene<?> displayAspectScene) {
        DisplayUseCaseModelScene scene = (DisplayUseCaseModelScene) displayAspectScene;
        boolean isSaveNeeded = EMFEditUtil.getCommandStack(scene.getUseCaseDiagram())
                .isSaveNeeded();
        if (isSaveNeeded) {
            showCloseConfirmPopup(scene);
        } else {
            checkForModelRemoval(scene);
            doSwitchToPreviousScene(scene);
        }        
    }

    @Override
    public void closeSplitView(RamAbstractScene<?> displayAspectScene) {
    }

    @Override
    public void layout(RamAbstractScene<?> scene) {
        Layout newLayout = new AutomaticLayout(70);
        newLayout.layout(UcModelUtils.getUseCaseDiagramViewFromApp(RamApp.getApplication()).getContainerLayer(), 
                Layout.LayoutUpdatePhase.FROM_PARENT);        
    }
    
    /**
     * Method called by the collapse environment when going back from a reuse
     * concern scene.
     * 
     * @param scene reused Model scene the user is in
     */
    public void switchBackTo(final RamAbstractScene<?> scene) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(scene.getSceneRootModelObject());
        COREModelUtil.unloadExternalResources(artefact);

        scene.setTransition(new BlendTransition(RamApp.getApplication(), 500));

        RamAbstractScene<?> pA = (RamAbstractScene<?>) scene.getPreviousScene();
//        pA.repushSections();

        SceneCreationAndChangeFactory.getFactory().changeSceneAndUpdate(pA);
        scene.getApplication().destroySceneAfterTransition(scene);
    }
    
    /**
     * Handles hiding/showing notes in Use Case Diagram.
     * 
     * @param scene the affected {@link DisplayAspectScene}
     * @return the new visibility status of notes
     */
    public boolean toggleNotes(DisplayUseCaseModelScene scene) {       
        UseCaseDiagramView view = scene.getUseCaseDiagramView();
        return view.toggleNotes();
    }   
    
    /**
     * Checks whether the artefact needs to be removed from the concern.
     * The artefact is removed, if it was not saved and therefore not contained anywhere.
     *
     * @param scene the current use case diagram scene
     */
    protected static void checkForModelRemoval(DisplayUseCaseModelScene scene) {

        UseCaseModel aspect = scene.getUseCaseDiagram();
        COREArtefact artefact = scene.getArtefact();

        // That is the aspect is not saved at all.
        if (aspect.eResource() == null) {

            // Get all the features realizing it
            List<COREFeature> listOfFeatures = artefact.getScene().getRealizes();
            List<COREFeature> copyOfListOfFeature = new ArrayList<COREFeature>(listOfFeatures);

            // Loop through all the features and remove the realization
            for (COREFeature feature : copyOfListOfFeature) {
                // Do a pre-mature check to see if the feature realizes the scene,
                // Can exist scenario, where the bi directional link might not be true
                if (feature.getRealizedBy().contains(artefact.getScene())) {
                    feature.getRealizedBy().remove(artefact.getScene());
                }
            }
            
            COREConcern concern = artefact.getCoreConcern();

            // Remove from the list of artefacts
            if (concern != null) {
                concern.getArtefacts().remove(artefact);
            }
        }
    }
    
    /**
     * Performs the switching to the concern scene.
     * Unloads the resource and triggers the scene change to the previous scene.
     *
     * @param scene the current aspect scene
     */
    protected void doSwitchToPreviousScene(DisplayUseCaseModelScene scene) {
        // Unload WovenAspect of this aspect when we leave
        COREArtefact artefact = scene.getArtefact();
        COREModelUtil.unloadExternalResources(artefact);
        if (scene.getPreviousScene() instanceof DisplayConcernEditScene) {
            // Temporary workaround until navigation properly takes care of all navigation.
            NavigationBar.getInstance().popSection();
            scene.setTransition(new SlideTransition(RamApp.getApplication(), 500, false));

            SceneCreationAndChangeFactory.getFactory().changeSceneAndUpdate(scene.getPreviousScene());
            scene.getApplication().destroySceneAfterTransition(scene);
        } else {
            scene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 500, true));
            SceneCreationAndChangeFactory.getFactory().changeSceneAndUpdate(scene.getPreviousScene());
            scene.getApplication().destroySceneAfterTransition(scene);
        }
    }
    
    /**
     * Display a popup for the user to decided whether he wants to save the Use Case Diagram or leave the scene.
     *
     * @param scene - The {@link DisplayAspectScene} to consider
     */
    private void showCloseConfirmPopup(final DisplayUseCaseModelScene scene) {
        scene.showCloseConfirmPopup(scene, new SaveConfirmListener(scene));
    }

}
