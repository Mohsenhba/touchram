package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.usecases.ActorMapping;

public class ActorMappingContainerView extends RamRoundedRectangleComponent implements UINotifyChangedListener {
    private ActorMappingView actorMappingView;
    
    private ActorMapping actorMapping;
    
    public ActorMappingContainerView(ActorMapping actorMapping) {
        super(0);
        setNoStroke(false);
        setStrokeWeight(3);
        setNoFill(false);
        setFillColor(Colors.MAPPING_CONTAINER_VIEW_FILL_COLOR);

        actorMappingView = new ActorMappingView(actorMapping);
        this.addChild(actorMappingView);

        this.actorMapping = actorMapping;
        
        setLayout(new VerticalLayout());
        setBuffers(0);
        
        EMFEditUtil.addListenerFor(actorMapping, this);
    }
    
    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(actorMapping, this);
        super.destroy();
    }

    /**
     * Getter for ActorMapping information of the view.
     * 
     * @return {@link ActorMapping}
     */
    public ActorMapping getActorMapping() {
        return actorMapping;
    }
    
    @Override
    public void handleNotification(Notification notification) {
        // TODO Auto-generated method stub
    }

}
