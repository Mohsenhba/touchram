package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.usecases.ActorMapping;

public class ActorMappingView extends RamRectangleComponent implements ActionListener {
    private static final String ACTION_ACTOR_MAPPING_DELETE = "view.actorMapping.delete";

    private static final float ICON_SIZE = Fonts.FONTSIZE_COMPOSITION + 2;
    
    private ActorMapping actorMapping;
    
    /**
     * Button to delete Actor Mapping.
     */
    private RamButton buttonActorMappingDelete;

    /**
     * ActorMapping from element.
     */
    private TextView textActorFromElement;

    /**
     * ActorMapping to element.
     */
    private TextView textActorToElement;

    /**
     * Image for an arrow between mapping elements.
     */
    private RamImageComponent arrow;

    public ActorMappingView(ActorMapping actorMapping) {
        setNoStroke(true);
        setNoFill(false);
        setFillColor(Colors.ACTOR_MAPPING_VIEW_FILL_COLOR);
        setBuffers(0);
        
        this.actorMapping = actorMapping;
        
        // Add delete button
        RamImageComponent deleteClassifierMappingImage = new RamImageComponent(Icons.ICON_DELETE,
                Colors.ICON_DELETE_COLOR);
        deleteClassifierMappingImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        deleteClassifierMappingImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonActorMappingDelete = new RamButton(deleteClassifierMappingImage);
        buttonActorMappingDelete.setActionCommand(ACTION_ACTOR_MAPPING_DELETE);
        buttonActorMappingDelete.addActionListener(this);
        addChild(buttonActorMappingDelete);

        // Add Actor From
        textActorFromElement = new TextView(actorMapping, CorePackage.Literals.CORE_LINK__FROM);
        textActorFromElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        textActorFromElement.setFont(Fonts.FONT_COMPOSITION);
        textActorFromElement.setBufferSize(Cardinal.SOUTH, 0);
        textActorFromElement.setBufferSize(Cardinal.EAST, 0);
        textActorFromElement.setBufferSize(Cardinal.WEST, 0);
        textActorFromElement.setAlignment(Alignment.CENTER_ALIGN);
        textActorFromElement.setPlaceholderText(Strings.PH_SELECT_ACTOR);
        textActorFromElement.setAutoMinimizes(true);
        this.addChild(textActorFromElement);

        // Arrow between Actor From and Actor To
        arrow = new RamImageComponent(Icons.ICON_ARROW_RIGHT, Colors.ICON_ARROW_COLOR);
        arrow.setMinimumSize(ICON_SIZE, ICON_SIZE);
        arrow.setMaximumSize(ICON_SIZE, ICON_SIZE);
        this.addChild(arrow);

        // Add Actor To
        textActorToElement = new TextView(actorMapping, CorePackage.Literals.CORE_LINK__TO);
        textActorToElement.setFont(Fonts.FONT_COMPOSITION);
        textActorToElement.setBufferSize(Cardinal.SOUTH, 0);
        textActorToElement.setBufferSize(Cardinal.EAST, 0);
        textActorToElement.setBufferSize(Cardinal.WEST, 0);
        textActorToElement.setAlignment(Alignment.CENTER_ALIGN);
        textActorToElement.setPlaceholderText(Strings.PH_SELECT_ACTOR);
        textActorToElement.setAutoMinimizes(true);
        textActorToElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        this.addChild(textActorToElement);
        
        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        if (ACTION_ACTOR_MAPPING_DELETE.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getUcMappingContainerViewHandler().deleteActorMapping(actorMapping);
        }
    }
}
