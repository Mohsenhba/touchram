package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public class ActorReferenceTextView extends TextView {
    private boolean actorReferenceRequired;
    
    public ActorReferenceTextView(ActorReferenceText text) {
        this(text, false);
    }
    
    public ActorReferenceTextView(ActorReferenceText text, boolean referenceRequired) {
        super(text, UcPackage.Literals.ACTOR_REFERENCE_TEXT__TEXT);
        this.actorReferenceRequired = referenceRequired;
        // Set the initial text with the replacements
        String textToReplace = text.getText();
        if (textToReplace != null) {
            String displayText = UseCaseTextUtils.replaceActorTokens(text);
            this.setText(displayText);    
        }
    }
    
    @Override
    public void handleNotification(Notification notification) {
        ActorReferenceText data = (ActorReferenceText) this.getData();
        EStructuralFeature feature = this.getFeature();
        String textToReplace = (String) data.eGet(feature);
        if (textToReplace != null) {
            String displayText = UseCaseTextUtils.replaceActorTokens(data);
            this.setText(displayText);    
        }
    }

    public boolean isActorReferenceRequired() {
        return actorReferenceRequired;
    }
}
