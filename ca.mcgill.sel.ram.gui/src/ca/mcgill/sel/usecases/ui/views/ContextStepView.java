package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.ui.utils.UcModelUtils;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;

public class ContextStepView extends StepView {
    
    private ActorReferenceTextView textField;
    
    protected ContextStepView(UseCaseDiagramView useCaseDiagramView, Step represented, boolean readonly) {
        super(useCaseDiagramView, represented, readonly);
    }

    @Override
    protected void buildStepView() {        
        textField = new ActorReferenceTextView(getStep().getStepDescription());        
        textField.setPlaceholderText(Strings.PH_ENTER_STEP_DESCRIPTION);
        textField.setFont(Fonts.DEFAULT_FONT_ITALIC);       
        textField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
        textField.setBufferSize(Cardinal.WEST, 0);
        textField.enableKeyboard(!readonly);
        addChild(textField);
        textField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getActorReferenceTextViewHandler());
    }

}
