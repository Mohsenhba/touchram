package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.StepMapping;

public class FlowMappingContainerView extends RamRoundedRectangleComponent implements UINotifyChangedListener {
    private FlowMappingView flowMappingView;
    
    private FlowMapping flowMapping;
    
    private RamRectangleComponent hideableStepContainer;
    
    public FlowMappingContainerView(FlowMapping flowMapping) {
        super(0);
        setNoStroke(false);
        setStrokeWeight(3);
        setNoFill(false);
        setFillColor(Colors.MAPPING_CONTAINER_VIEW_FILL_COLOR);

        flowMappingView = new FlowMappingView(flowMapping);
        this.addChild(flowMappingView);

        this.flowMapping = flowMapping;
        
        // Container for the steps
        hideableStepContainer = new RamRectangleComponent();
        hideableStepContainer.setLayout(new VerticalLayout(0));
        hideableStepContainer.setFillColor(Colors.STEP_MAPPING_VIEW_FILL_COLOR);
        hideableStepContainer.setNoFill(false);
        hideableStepContainer.setBufferSize(Cardinal.EAST, 0);
        
        this.addChild(hideableStepContainer);
        
        setLayout(new VerticalLayout());
        setBuffers(0);
        
        addAllStepMappings();
        
        EMFEditUtil.addListenerFor(flowMapping, this);
    }
    
    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(flowMapping, this);
        super.destroy();
    }

    /**
     * Getter for FlowMapping information of the view.
     * 
     * @return {@link FlowMapping}
     */
    public FlowMapping getFlowMapping() {
        return flowMapping;
    }
    
    private void addAllStepMappings() {
        EList<COREMapping<?>> coreMappings = flowMapping.getMappings();
        for (COREMapping<?> mapping : coreMappings) {
            if (mapping instanceof StepMapping) {
                addStepMappingView((StepMapping) mapping);
            }
        }
    }
    
    private void addStepMappingView(StepMapping mapping) {
        StepMappingView stepMappingView = new StepMappingView(mapping);
        hideableStepContainer.addChild(stepMappingView);
    }
    
    private void deleteStepMappingView(StepMapping stepMapping) {
        MTComponent[] stepMappingContainerViews = hideableStepContainer.getChildren();
        for (MTComponent view : stepMappingContainerViews) {
            if (view instanceof StepMappingView) {
                StepMappingView stepMappingContainerView = (StepMappingView) view;
                if (stepMappingContainerView.getStepMapping() == stepMapping) {
                    hideableStepContainer.removeChild(stepMappingContainerView);
                }
            }
        }
    }
    
    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == flowMapping) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MAPPING__MAPPINGS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        COREMapping<?> newMapping = (COREMapping<?>) notification.getNewValue();
                        
                        if (newMapping instanceof StepMapping) {
                            StepMapping newStepMapping = (StepMapping) newMapping;
                            addStepMappingView(newStepMapping);
                        }
                        
                        break;

                    case Notification.REMOVE:
                        COREMapping<?> oldMapping = (COREMapping<?>) notification.getOldValue();

                        if (oldMapping instanceof StepMapping) {
                            StepMapping oldStepMapping = (StepMapping) oldMapping;
                            deleteStepMappingView(oldStepMapping);
                        }
                        break;
                }
            }
        }
    }

}
