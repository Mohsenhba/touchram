package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.usecases.FlowMapping;

public class FlowMappingView extends RamRectangleComponent implements ActionListener {
    private static final String ACTION_STEP_MAPPING_ADD = "view.stepMapping.add";
    private static final String ACTION_FLOW_MAPPING_DELETE = "view.useCaseMapping.delete";

    private static final float ICON_SIZE = Fonts.FONTSIZE_COMPOSITION + 2;
    
    private FlowMapping flowMapping;    
    
    /**
     * Button to add Step Mapping.
     */
    private RamButton buttonStepMappingAdd;
    
    /**
     * Button to delete flow Mapping.
     */
    private RamButton buttonFlowMappingDelete;

    /**
     * FlowMapping from element.
     */
    private TextView textFlowFromElement;

    /**
     * FlowMapping to element.
     */
    private TextView textFlowToElement;

    /**
     * Image for an arrow between mapping elements.
     */
    private RamImageComponent arrow;

    public FlowMappingView(FlowMapping flowMapping) {
        setNoStroke(true);
        setNoFill(false);
        setFillColor(Colors.USE_CASE_MAPPING_VIEW_FILL_COLOR);
        setBuffers(0);
        
        this.flowMapping = flowMapping;
        
        // Add delete button
        RamImageComponent deleteFlowMappingImage = new RamImageComponent(Icons.ICON_DELETE,
                Colors.ICON_DELETE_COLOR);
        deleteFlowMappingImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        deleteFlowMappingImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonFlowMappingDelete = new RamButton(deleteFlowMappingImage);
        buttonFlowMappingDelete.setActionCommand(ACTION_FLOW_MAPPING_DELETE);
        buttonFlowMappingDelete.addActionListener(this);
        addChild(buttonFlowMappingDelete);

        // Label
        RamTextComponent stepText = new RamTextComponent(Strings.LABEL_FLOW);
        stepText.setFont(Fonts.FONT_COMPOSITION);
        stepText.setBufferSize(Cardinal.SOUTH, 0);
        stepText.setBufferSize(Cardinal.EAST, 0);
        stepText.setBufferSize(Cardinal.WEST, Fonts.FONTSIZE_COMPOSITION);
        this.addChild(stepText);
        
        // Add Flow From
        textFlowFromElement = new TextView(flowMapping, CorePackage.Literals.CORE_LINK__FROM);
        textFlowFromElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        textFlowFromElement.setFont(Fonts.FONT_COMPOSITION);
        textFlowFromElement.setBufferSize(Cardinal.SOUTH, 0);
        textFlowFromElement.setBufferSize(Cardinal.EAST, 0);
        textFlowFromElement.setBufferSize(Cardinal.WEST, 0);
        textFlowFromElement.setAlignment(Alignment.CENTER_ALIGN);
        textFlowFromElement.setPlaceholderText(Strings.PH_SELECT_FLOW);
        textFlowFromElement.setAutoMinimizes(true);
        this.addChild(textFlowFromElement);

        // Arrow between Flow From and Flow To
        arrow = new RamImageComponent(Icons.ICON_ARROW_RIGHT, Colors.ICON_ARROW_COLOR);
        arrow.setMinimumSize(ICON_SIZE, ICON_SIZE);
        arrow.setMaximumSize(ICON_SIZE, ICON_SIZE);
        this.addChild(arrow);

        // Add Flow To
        textFlowToElement = new TextView(flowMapping, CorePackage.Literals.CORE_LINK__TO);
        textFlowToElement.setFont(Fonts.FONT_COMPOSITION);
        textFlowToElement.setBufferSize(Cardinal.SOUTH, 0);
        textFlowToElement.setBufferSize(Cardinal.EAST, 0);
        textFlowToElement.setBufferSize(Cardinal.WEST, 0);
        textFlowToElement.setAlignment(Alignment.CENTER_ALIGN);
        textFlowToElement.setPlaceholderText(Strings.PH_SELECT_FLOW);
        textFlowToElement.setAutoMinimizes(true);
        textFlowToElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        this.addChild(textFlowToElement);
        
        // Add buttons for adding step mapping
        RamImageComponent stepMappingAddImage = new RamImageComponent(Icons.ICON_STEP_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        stepMappingAddImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        stepMappingAddImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonStepMappingAdd = new RamButton(stepMappingAddImage);
        buttonStepMappingAdd.setActionCommand(ACTION_STEP_MAPPING_ADD);
        buttonStepMappingAdd.addActionListener(this);
        addChild(buttonStepMappingAdd);
        
        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        if (ACTION_FLOW_MAPPING_DELETE.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getUcMappingContainerViewHandler().deleteFlowMapping(flowMapping);
        } else if (ACTION_STEP_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getUcMappingContainerViewHandler().addStepMapping(flowMapping);
        }
    }
}
