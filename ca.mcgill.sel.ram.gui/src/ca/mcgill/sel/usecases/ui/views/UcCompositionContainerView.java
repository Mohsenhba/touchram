package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

public class UcCompositionContainerView extends CompositionContainerView {
    
    /**
     * Constant representing the "add Actor mapping" action.
     */
    public static final String ACTION_ACTOR_MAPPING_ADD = "mapping.ucactor.add";
    
    /**
     * Constant representing the "add Use Case mapping" action.
     */
    public static final String ACTION_USE_CASE_MAPPING_ADD = "mapping.ucusecase.add";  
    
    public UcCompositionContainerView(COREArtefact currentArtefact, boolean isModelReuseContainer, String title) {
        super(currentArtefact, isModelReuseContainer, title);
    }

    @Override
    public CompositionView createCompositionView(COREModelComposition composition, COREReuse reuse) {
        return new UcCompositionView(composition, reuse, this);
    }

    @Override
    public void customizableActionPerformed(ActionEvent event, EObject mappingContainer) {
        String actionCommand = event.getActionCommand();
        
        if (ACTION_ACTOR_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getUcCompositionViewHandler()
                .addActorMapping((COREModelComposition) mappingContainer);
        } else if (ACTION_USE_CASE_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getUcCompositionViewHandler()
                .addUseCaseMapping((COREModelComposition) mappingContainer);
        }        
    }
}
