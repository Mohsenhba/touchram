package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView.Iconified;
import ca.mcgill.sel.ram.ui.views.RamEnd;
import ca.mcgill.sel.ram.ui.views.handler.IRelationshipViewHandler;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.language.controller.ActorController;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseController;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.InheritanceView;

public class InheritanceAssociationViewHandler extends BaseViewHandler implements IRelationshipViewHandler {

    /**
     * The options to display for an association end.
     */
    private enum AssociationOptions implements Iconified {
        DELETE(new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR));

        private RamImageComponent icon;

        /**
         * Creates a new option literal with the given icon.
         *
         * @param icon the icon to use for this option
         */
        AssociationOptions(RamImageComponent icon) {
            this.icon = icon;
        }

        @Override
        public RamImageComponent getIcon() {
            return icon;
        }

    }
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean processDoubleTap(TapEvent tapEvent, RamEnd<?, ?> end) {
        InheritanceView view = (InheritanceView) end.getRelationshipView();
                
        EObject source = view.getFromEnd().getModel();
        EObject target = view.getToEnd().getModel();

        ArrayList<AssociationOptions> availableOptions = 
                new ArrayList<AssociationOptions>();        
        
        availableOptions.add(AssociationOptions.DELETE);
        
        OptionSelectorView<AssociationOptions> selector =
                new OptionSelectorView<AssociationOptions>(availableOptions);

        RamApp.getActiveScene().addComponent(selector, tapEvent.getLocationOnScreen());

        selector.registerListener(new AbstractDefaultRamSelectorListener<AssociationOptions>() {
            @Override
            public void elementSelected(RamSelectorComponent<AssociationOptions> selector, AssociationOptions element) {
                UseCaseController useCaseController = UseCaseControllerFactory.INSTANCE.getUseCaseController();
                ActorController actorController = UseCaseControllerFactory.INSTANCE.getActorController();

                switch (element) {
                    case DELETE:
                        if (source instanceof Actor && target instanceof Actor) {
                            actorController.setGeneralization((Actor) source, null);
                        } else if (source instanceof UseCase && target instanceof UseCase) {
                            useCaseController.setGeneralization((UseCase) source, null);
                        }
                }
            }
        });
        
        return true;
    }

    @Override
    public boolean processTapAndHold(TapAndHoldEvent tapAndHoldEvent, RamEnd<?, ?> end) {
        // TODO Auto-generated method stub
        return false;
    }
}