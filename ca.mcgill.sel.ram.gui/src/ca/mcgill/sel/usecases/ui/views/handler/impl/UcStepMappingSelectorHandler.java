package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.List;

import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.ui.views.StepNamer;

public class UcStepMappingSelectorHandler extends TextViewHandler {
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            final TextView target = (TextView) tapEvent.getTarget(); 
            final StepMapping stepMapping = (StepMapping) target.getData();
            FlowMapping flowMapping = EMFModelUtil.getRootContainerOfType(
                    stepMapping, UcPackage.Literals.FLOW_MAPPING);
            
            Flow flow;
            if (target.getFeature() == CorePackage.Literals.CORE_LINK__TO) {
                flow = flowMapping.getTo();
            } else {
                flow = flowMapping.getFrom();
            }
            
            List<Step> availableOptions = flow.getSteps();
            RamSelectorComponent<Step> selector = new RamSelectorComponent<Step>(availableOptions);
            selector.setMaximumWidth(200);
            selector.setNamer(new StepNamer());

            RamApp.getActiveScene().addComponent(selector, tapEvent.getLocationOnScreen());

            selector.registerListener(new AbstractDefaultRamSelectorListener<Step>() {
                @Override
                public void elementSelected(RamSelectorComponent<Step> selector, Step element) {
                    setValue(target.getData(), target.getFeature(), element);
                    selector.destroy();
                }
            });            

            return true;
        }

        return false;
    }
}
