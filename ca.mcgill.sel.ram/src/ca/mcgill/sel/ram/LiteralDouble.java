/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal Double</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.LiteralDouble#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getLiteralDouble()
 * @model
 * @generated
 */
public interface LiteralDouble extends LiteralSpecification {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The default value is <code>"0.0d"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(double)
	 * @see ca.mcgill.sel.ram.RamPackage#getLiteralDouble_Value()
	 * @model default="0.0d" required="true"
	 * @generated
	 */
	double getValue();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.LiteralDouble#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(double value);

} // LiteralDouble
