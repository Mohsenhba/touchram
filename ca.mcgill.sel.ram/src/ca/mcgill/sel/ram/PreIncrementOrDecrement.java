/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pre Increment Or Decrement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.PreIncrementOrDecrement#getOp <em>Op</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getPreIncrementOrDecrement()
 * @model
 * @generated
 */
public interface PreIncrementOrDecrement extends Unary {
	/**
	 * Returns the value of the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op</em>' attribute.
	 * @see #setOp(String)
	 * @see ca.mcgill.sel.ram.RamPackage#getPreIncrementOrDecrement_Op()
	 * @model
	 * @generated
	 */
	String getOp();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.PreIncrementOrDecrement#getOp <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op</em>' attribute.
	 * @see #getOp()
	 * @generated
	 */
	void setOp(String value);

} // PreIncrementOrDecrement
