/**
 */
package ca.mcgill.sel.ram.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.COREMappingImpl;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.RamPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enum Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EnumMappingImpl extends COREMappingImpl<REnum> implements EnumMapping {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.ENUM_MAPPING;
	}

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public EList<EnumLiteralMapping> getEnumLiteralMappings() {
        Collection<EnumLiteralMapping> collection = EMFModelUtil.collectElementsOfType(this, 
                CorePackage.Literals.CORE_MAPPING__MAPPINGS, RamPackage.Literals.ENUM_LITERAL_MAPPING);
         
        return new BasicEList<EnumLiteralMapping>(collection);
    }

} //EnumMappingImpl
