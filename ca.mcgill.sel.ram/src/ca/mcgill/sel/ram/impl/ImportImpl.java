/**
 */
package ca.mcgill.sel.ram.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.mcgill.sel.ram.ArtifactSignature;
import ca.mcgill.sel.ram.Import;
import ca.mcgill.sel.ram.RamPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Import</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.ImportImpl#getStub <em>Stub</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.ImportImpl#getExternalartifact <em>Externalartifact</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ImportImpl extends NamedElementImpl implements Import {
	/**
	 * The default value of the '{@link #getStub() <em>Stub</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStub()
	 * @generated
	 * @ordered
	 */
	protected static final String STUB_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStub() <em>Stub</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStub()
	 * @generated
	 * @ordered
	 */
	protected String stub = STUB_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExternalartifact() <em>Externalartifact</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalartifact()
	 * @generated
	 * @ordered
	 */
	protected ArtifactSignature externalartifact;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImportImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.IMPORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStub() {
		return stub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStub(String newStub) {
		String oldStub = stub;
		stub = newStub;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.IMPORT__STUB, oldStub, stub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ArtifactSignature getExternalartifact() {
		if (externalartifact != null && externalartifact.eIsProxy()) {
			InternalEObject oldExternalartifact = (InternalEObject)externalartifact;
			externalartifact = (ArtifactSignature)eResolveProxy(oldExternalartifact);
			if (externalartifact != oldExternalartifact) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RamPackage.IMPORT__EXTERNALARTIFACT, oldExternalartifact, externalartifact));
			}
		}
		return externalartifact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtifactSignature basicGetExternalartifact() {
		return externalartifact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExternalartifact(ArtifactSignature newExternalartifact) {
		ArtifactSignature oldExternalartifact = externalartifact;
		externalartifact = newExternalartifact;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.IMPORT__EXTERNALARTIFACT, oldExternalartifact, externalartifact));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RamPackage.IMPORT__STUB:
				return getStub();
			case RamPackage.IMPORT__EXTERNALARTIFACT:
				if (resolve) return getExternalartifact();
				return basicGetExternalartifact();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RamPackage.IMPORT__STUB:
				setStub((String)newValue);
				return;
			case RamPackage.IMPORT__EXTERNALARTIFACT:
				setExternalartifact((ArtifactSignature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RamPackage.IMPORT__STUB:
				setStub(STUB_EDEFAULT);
				return;
			case RamPackage.IMPORT__EXTERNALARTIFACT:
				setExternalartifact((ArtifactSignature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RamPackage.IMPORT__STUB:
				return STUB_EDEFAULT == null ? stub != null : !STUB_EDEFAULT.equals(stub);
			case RamPackage.IMPORT__EXTERNALARTIFACT:
				return externalartifact != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (stub: ");
		result.append(stub);
		result.append(')');
		return result.toString();
	}

} //ImportImpl
