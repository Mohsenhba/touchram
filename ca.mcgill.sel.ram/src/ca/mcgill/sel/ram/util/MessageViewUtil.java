package ca.mcgill.sel.ram.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.AbstractMessageView;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.Gate;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageEnd;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.MessageViewReference;
import ca.mcgill.sel.ram.OpaqueExpression;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Reference;

/**
 * Helper class with convenient static methods for working with message view model objects.
 *
 * @author mschoettle
 * @author emmanuelngch
 */
public final class MessageViewUtil {
    /**
     * Creates a new instance of {@link MessageViewUtil}.
     */
    private MessageViewUtil() {
        // Suppress default constructor.
    }

    /**
     * Creates a new interaction that specifies the given operation.
     * It contains the initial call and a return message if it does not return void.
     *
     * @param operation the operation a message view should be created for
     * @return the {@link Interaction} for the given operation with no behaviour
     */
    public static Interaction createInteraction(Operation operation) {
        RamFactory factory = RamFactory.eINSTANCE;

        // Create interaction.
        Interaction interaction = factory.createInteraction();

        // Create the lifeline.
        Lifeline lifeline = factory.createLifeline();
        interaction.getLifelines().add(lifeline);

        // Create represents.
        Reference represents = factory.createReference();
        represents.setLowerBound(1);
        represents.setName("target");
        represents.setStatic(operation.isStatic());
        represents.setType((Classifier) operation.eContainer());

        interaction.getProperties().add(represents);
        lifeline.setRepresents(represents);

        MessageSort messageSort = null;

        if (operation != null) {
            switch (operation.getOperationType()) {
                case CONSTRUCTOR:
                    messageSort = MessageSort.CREATE_MESSAGE;
                    break;
                case DESTRUCTOR:
                    messageSort = MessageSort.DELETE_MESSAGE;
                    break;
                default:
                    messageSort = MessageSort.SYNCH_CALL;
                    break;
            }
        }

        createInitialMessage(interaction, lifeline, operation, messageSort);
        createInitialMessage(interaction, lifeline, operation, MessageSort.REPLY);

        return interaction;
    }

    /**
     * Creates an initial message in the given interaction calling the given operation.
     * The message is coming from a gate and received by the given lifeline.
     * It is added to the interaction.
     *
     * @param interaction the interaction the message belongs to
     * @param lifeline the lifeline that receives the message call
     * @param operation the operation that is called on the lifeline
     * @param messageSort the kind of the message call
     */
    public static void createInitialMessage(Interaction interaction, Lifeline lifeline, Operation operation,
            MessageSort messageSort) {
        // create gate
        Gate gate = RamFactory.eINSTANCE.createGate();

        String gateName = (messageSort == MessageSort.REPLY) ? "out_" : "in_";
        gateName += operation.getName();

        gate.setName(gateName);
        interaction.getFormalGates().add(gate);

        // create receive event
        MessageOccurrenceSpecification event = RamFactory.eINSTANCE.createMessageOccurrenceSpecification();
        event.getCovered().add(lifeline);
        interaction.getFragments().add(event);

        // create message
        Message message = RamFactory.eINSTANCE.createMessage();
        message.setMessageSort(messageSort);
        message.setSignature(operation);
        interaction.getMessages().add(message);

        // set references
        event.setMessage(message);
        gate.setMessage(message);

        if (messageSort == MessageSort.REPLY) {
            message.setSendEvent(event);
            message.setReceiveEvent(gate);
        } else {
            message.setSendEvent(gate);
            message.setReceiveEvent(event);
        }
    }

    /**
     * Creates a message view for the given operation.
     * The message view contains the initial call, but is otherwise empty.
     *
     * @param operation the {@link Operation} to create a message view for
     * @return the message view for the operation
     */
    public static MessageView createMessageView(Operation operation) {
        MessageView messageView = RamFactory.eINSTANCE.createMessageView();
        messageView.setSpecifies(operation);

        Interaction specification = MessageViewUtil.createInteraction(operation);
        messageView.setSpecification(specification);

        return messageView;
    }

    /**
     * Returns whether a message view for the given operation exists.
     * If the given operation is mapped "from" an operation of another model,
     * it is considered to be defined there, unless that operation is partial.
     *
     * @param aspect the aspect containing the operation and message views
     * @param operation the {@link Operation}
     * @return true, if a {@link MessageView} exists, false otherwise
     */
    public static boolean isMessageViewDefined(Aspect aspect, Operation operation) {
        for (MessageView messageView : getMessageViewsOfType(aspect, MessageView.class)) {
            if (messageView.getSpecifies() == operation) {
                return true;
            }
        }

        /**
         * If the operation is mapped, check whether the mapped from operation has a message view.
         * I.e., the mapping should be a rename and the from operation is therefore not partial.
         */
        Operation fromOperation = RAMModelUtil.getMappedFrom(operation);

        return fromOperation != null
                && isMessageViewDefined((Aspect) EcoreUtil.getRootContainer(fromOperation), fromOperation);
    }

    /**
     * Returns a list of message views from the given aspect
     * that are an instance of the given type. It filters out all message views
     * that do not conform to the type given.
     *
     * @param aspect the aspect that contains message views
     * @param type the class of the message view type to be retrieved
     * @param <T> the type of the message view, i.e., a sub-class of {@link AbstractMessageView}
     * @return a list of message views containing only the given type
     */
    public static <T extends EObject> List<T> getMessageViewsOfType(Aspect aspect, java.lang.Class<T> type) {
        List<T> filteredMessageViews = new ArrayList<T>();

        for (AbstractMessageView messageView : aspect.getMessageViews()) {
            if (type.isInstance(messageView)) {
                @SuppressWarnings("unchecked")
                T typed = (T) messageView;
                filteredMessageViews.add(typed);
            }
        }

        return filteredMessageViews;
    }

    /**
     * Returns the message view that specifies the given operation.
     * Returns null if no message view could be found.
     *
     * @param aspect the aspect that contains the operation
     * @param operation the {@link Operation}
     * @return the {@link MessageView} that specifies the operation, null otherwise
     */
    public static MessageView getMessageViewFor(Aspect aspect, Operation operation) {
        for (MessageView messageView : getMessageViewsOfType(aspect, MessageView.class)) {
            if (messageView.getSpecifies() == operation) {
                return messageView;
            }
        }

        return null;
    }

    /**
     * Returns the message view reference that references the message view for the given operation.
     * Returns null if no message view reference could be found.
     * The operation must be the referenced operation.
     *
     * @param aspect the aspect that contains the message view reference
     * @param operation the {@link Operation}
     * @return the {@link MessageViewReference} that references the message view specifying the operation,
     *         null otherwise
     */
    public static MessageViewReference getMessageViewReferenceFor(Aspect aspect, Operation operation) {
        for (MessageViewReference messageView : getMessageViewsOfType(aspect, MessageViewReference.class)) {
            if (messageView.getReferences().getSpecifies() == operation) {
                return messageView;
            }
        }

        return null;
    }

    /**
     * Finds the initial message the given fragment is part of.
     * The initial message is the one that represents the operation being defined.
     * It contains the local properties of the defined behaviour.
     * Usually this is the first message of a message view, sent from a gate.
     * However, with nested behaviour definition it could be a message somewhere inside the message view.
     *
     * @param fragment the current fragment for which to find the initial message
     * @return the initial message, null if none found
     */
    public static Message findInitialMessage(InteractionFragment fragment) {
        FragmentContainer container = fragment.getContainer();

        if (fragment.getCovered().size() > 0) {
            // CombinedFragments have more than one, but we assume that the initial lifeline was added first.
            Lifeline coveredLifeline = fragment.getCovered().get(0);

            int index = container.getFragments().indexOf(fragment);

            for (int i = index; i >= 0; i--) {
                InteractionFragment currentFragment = container.getFragments().get(i);

                if (currentFragment.getCovered().contains(coveredLifeline)
                        && currentFragment instanceof MessageOccurrenceSpecification) {
                    MessageOccurrenceSpecification messageEnd = (MessageOccurrenceSpecification) currentFragment;
                    Message message = messageEnd.getMessage();

                    if (message != null
                            && message.getReceiveEvent() == messageEnd
                            && !message.isSelfMessage()
                            && message.getMessageSort() != MessageSort.REPLY) {
                        return message;
                    }
                }
            }

            // No message was found so far. If the fragment is within a CombinedFragment,
            // we need to continue the search in its container.
            if (container instanceof InteractionOperand) {
                return findInitialMessage((InteractionFragment) container.eContainer());
            }
        }

        return null;
    }

    /**
     * Returns a list of all combined fragments that the given fragment container "covers".
     * If the container is the owner, an empty list is returned. Otherwise all combined fragments
     * in the containment hierarchy will be returned.
     * The order of the retrieved list is top down from the most combined fragment down to its children etc.,
     * if <code>reverse</code> is <code>false</code>. If <code>true</code> is supplied, the reverse order is returned,
     * i.e., from bottom up.
     *
     * @param owner the {@link Interaction} that contains everything
     * @param container the {@link FragmentContainer} for which to retrieve all combined fragments
     * @param reverse whether the order of combined fragments should be reversed, bottom-up if true, top down otherwise
     * @return a list of all combined fragments the given fragment container "covers"
     */
    public static List<CombinedFragment> getCoveredCombinedFragments(Interaction owner, FragmentContainer container,
            boolean reverse) {
        List<CombinedFragment> result = new ArrayList<CombinedFragment>();

        FragmentContainer currentContainer = container;

        while (owner != currentContainer) {
            CombinedFragment combinedFragment = (CombinedFragment) currentContainer.eContainer();
            // We need a reverse order, so that the parent combined fragment is first considered before its child.
            result.add(0, combinedFragment);

            currentContainer = combinedFragment.getContainer();
        }

        if (reverse) {
            Collections.reverse(result);
        }

        return result;
    }

    /**
     * Returns the provide operation's specification.
     * 
     * @param operation the operation
     * @return the specification or <code>null</code> if it is not defined
     */
    public static Interaction findSpecification(Operation operation) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(operation, RamPackage.Literals.ASPECT);
        MessageView messageView = null;

        if (aspect != null) {
            messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        }

        return messageView == null ? null : messageView.getSpecification();
    }

    /**
     * Returns the root {@link Interaction} for the given container.
     * If the container is of type {@link Interaction}, the same container is returned.
     * Otherwise, the containenment hierarchy is used to locate the interaction.
     * 
     * @param container the fragment container
     * @return the root {@link Interaction} for the given container, <code>null</code> if none found
     */
    public static Interaction getInteraction(FragmentContainer container) {
        Interaction result = null;

        if (container instanceof Interaction) {
            result = (Interaction) container;
        } else {
            result = EMFModelUtil.getRootContainerOfType(container, RamPackage.Literals.INTERACTION);
        }

        return result;
    }

    /**
     * Returns a new {@link OpaqueExpression} with default language.
     *
     * @return create {@link OpaqueExpression} with default language.
     */
    public static OpaqueExpression createOpaqueExpression() {
        OpaqueExpression specification = RamFactory.eINSTANCE.createOpaqueExpression();

        return specification;
    }
    
    /**
     * Finds the lower boundary (inclusive) of where a fragment can be moved to within the given container.
     * If it's within a regular message view, the lower boundary is the receive event of the initial message.
     * If the fragment is within an operand, the lower boundary is 0.
     * 
     * @param container the {@link FragmentContainer} for which to find the lower bound for
     * @return the lowest index the fragment can be moved to
     * @see #findUpperBound(FragmentContainer)
     */
    public static int findLowerBound(FragmentContainer container) {
        int result = 0;
        
        if (container.getFragments().size() > 0) {
            InteractionFragment firstFragment = container.getFragments().get(0);
            
            Message initialMessage = MessageViewUtil.findInitialMessage(firstFragment);
            
            if (initialMessage.getReceiveEvent() == firstFragment) {
                result = 1;
            }
        }
        
        return result;
    }

    /**
     * Finds the upper boundary (exclusive) of where the a fragment can be moved to within the given container.
     * If it's within a regular message view, the upper boundary is the send event of the reply message.
     * If it's within an operand, the upper boundary is the last possible index within the operand.
     * The last possible index is either the size + 1 or, if the operand has a reply message, the size.
     * 
     * @param container the {@link FragmentContainer} for which to find the upper bound for
     * @return the highest index the fragment can be moved to
     * @see MessageViewUtil#findLowerBound(FragmentContainer)
     */
    public static int findUpperBound(FragmentContainer container) {
        int result = container.getFragments().size();
        
        if (!container.getFragments().isEmpty() && container instanceof InteractionOperand) {
            InteractionFragment lastFragment = container.getFragments().get(result - 1);
            
            if (lastFragment instanceof MessageOccurrenceSpecification) {
                MessageOccurrenceSpecification end = (MessageOccurrenceSpecification) lastFragment;
                
                if (end.getMessage().getMessageSort() != MessageSort.REPLY) {
                    result++;
                }
            }
        }
        
        if (container.getFragments().isEmpty()) {
            result++;
        }
        
        
        return result;
    }
    
    /**
     * Returns the view index after the view index of the given fragment. In case of message ends it depends on the
     * message kind.
     *
     * @param container the {@link FragmentContainer} containing the fragments
     * @param fragment the preceding {@link InteractionFragment} for which to find the following view index
     * @return the view index after the view index of the given fragment
     */
    public static int getIndexAfter(FragmentContainer container, InteractionFragment fragment) {
        int index = container.getFragments().indexOf(fragment) + 1;

        // When there is a create message on the first lifeline a fragment may not be found.
        // No index (-1) will be found.
        // Assume that the new index will be 1.
        if (fragment == null) {
            index = 1;
        } else if (fragment instanceof MessageOccurrenceSpecification) {
            MessageOccurrenceSpecification messageEnd = (MessageOccurrenceSpecification) fragment;

            // If the end is not the receive event, we need to find the last event.
            if (messageEnd.getMessage().getSendEvent() == messageEnd) {
                // If it is the send event and part of a reply message,
                // we want the index after the receive event (index + 2).
                if (messageEnd.getMessage().getMessageSort() == MessageSort.REPLY) {
                    index++;
                } else {
                    Lifeline lifeline = messageEnd.getCovered().get(0);

                    /**
                     * Possible lifelines are consecutive lifelines that the behaviour is covered on going from the
                     * current message end.
                     */
                    Set<Lifeline> possibleLifelines = new HashSet<Lifeline>();
                    possibleLifelines.add(lifeline);
                    possibleLifelines.add(getLifeline(messageEnd.getMessage().getReceiveEvent()));

                    for (int i = index; i < container.getFragments().size(); i++) {
                        InteractionFragment currentFragment = container.getFragments().get(i);

                        /**
                         * Certain cases need to be considered. 1. The fragment covers the same lifeline, so it comes
                         * after, then the current index is the correct one. 2. Otherwise we need to check if the
                         * covered lifeline is a possible one. a) In case of send events the receive events lifeline is
                         * added, if the send event covers a possible lifeline. Otherwise we found a message that does
                         * not belong to the behaviour and we found the next index. b) For all other fragments we only
                         * need to check if the covered lifeline is valid, otherwise we found one outside and therefore
                         * the next index.
                         */
                        if (lifeline.getCoveredBy().contains(currentFragment)) {
                            index = i;
                            break;
                        } else {
                            if (currentFragment instanceof MessageOccurrenceSpecification) {
                                MessageOccurrenceSpecification event = (MessageOccurrenceSpecification) currentFragment;

                                if (event.getMessage().getSendEvent() == event) {
                                    if (!possibleLifelines.contains(event.getCovered().get(0))) {
                                        break;
                                    } else {
                                        possibleLifelines.add(getLifeline(event.getMessage().getReceiveEvent()));
                                    }
                                }
                            } else {
                                if (!possibleLifelines.contains(currentFragment.getCovered().get(0))) {
                                    break;
                                }
                            }
                        }

                        index++;
                    }
                }
            }
        }

        return index;
    }

    /**
     * Returns the lifeline the given message end covers.
     *
     * @param messageEnd the {@link MessageEnd} to get the covered lifeline for
     * @return the lifeline that is covered by the given message end
     */
    private static Lifeline getLifeline(MessageEnd messageEnd) {
        MessageOccurrenceSpecification event = (MessageOccurrenceSpecification) messageEnd;
        return event.getCovered().get(0);
    }

}
