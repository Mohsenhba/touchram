package ca.mcgill.sel.restif.language.controller;

/**
 * A factory to obtain all controllers.
 * 
 * @author Bowen
 */
public final class ControllerFactory {
    
    /**
     * The singleton instance of this factory.
     */
    public static final ControllerFactory INSTANCE = new ControllerFactory();
    
    private RestTreeController restTreeController;
    
    /**
     * Creates a new instance of {@link ControllerFactory}.
     */
    private ControllerFactory() {}
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.restif.RestIF}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.restif.RestIF}s
     */
    public RestTreeController getRestTreeController() {
        if (restTreeController == null) {
            restTreeController = new RestTreeController();
        }
        
        return restTreeController;
    }
}
