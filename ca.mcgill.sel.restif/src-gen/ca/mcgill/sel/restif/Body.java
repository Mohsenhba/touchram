/**
 */
package ca.mcgill.sel.restif;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Body</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getBody()
 * @model
 * @generated
 */
public interface Body extends Parameter {
} // Body
