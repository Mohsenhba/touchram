/**
 */
package ca.mcgill.sel.restif.impl;

import ca.mcgill.sel.restif.Body;
import ca.mcgill.sel.restif.RestifPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BodyImpl extends ParameterImpl implements Body {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected BodyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RestifPackage.Literals.BODY;
    }

} //BodyImpl
