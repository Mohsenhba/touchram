/**
 */
package ca.mcgill.sel.restif.impl;

import ca.mcgill.sel.restif.Parameter;
import ca.mcgill.sel.restif.RestifPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ParameterImpl extends MinimalEObjectImpl.Container implements Parameter {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ParameterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RestifPackage.Literals.PARAMETER;
    }

} //ParameterImpl
