/**
 */
package ca.mcgill.sel.ucm;

import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.ucm.UCMFactory
 * @model kind="package"
 * @generated
 */
public interface UCMPackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "ucm";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "http://cs.mcgill.ca/sel/ucm/1.0";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "ucm";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    UCMPackage eINSTANCE = ca.mcgill.sel.ucm.impl.UCMPackageImpl.init();

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.UCMModelElementImpl <em>Model Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.UCMModelElementImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getUCMModelElement()
     * @generated
     */
    int UCM_MODEL_ELEMENT = 10;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MODEL_ELEMENT__NAME = CorePackage.CORE_MODEL_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MODEL_ELEMENT__PARTIALITY = CorePackage.CORE_MODEL_ELEMENT__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MODEL_ELEMENT__VISIBILITY = CorePackage.CORE_MODEL_ELEMENT__VISIBILITY;

    /**
     * The number of structural features of the '<em>Model Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MODEL_ELEMENT_FEATURE_COUNT = CorePackage.CORE_MODEL_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.PathNodeImpl <em>Path Node</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.PathNodeImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getPathNode()
     * @generated
     */
    int PATH_NODE = 5;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PATH_NODE__NAME = UCM_MODEL_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PATH_NODE__PARTIALITY = UCM_MODEL_ELEMENT__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PATH_NODE__VISIBILITY = UCM_MODEL_ELEMENT__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PATH_NODE__SUCC = UCM_MODEL_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PATH_NODE__PRED = UCM_MODEL_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PATH_NODE__COMP_REF = UCM_MODEL_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Path Node</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PATH_NODE_FEATURE_COUNT = UCM_MODEL_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.AndForkImpl <em>And Fork</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.AndForkImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getAndFork()
     * @generated
     */
    int AND_FORK = 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_FORK__NAME = PATH_NODE__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_FORK__PARTIALITY = PATH_NODE__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_FORK__VISIBILITY = PATH_NODE__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_FORK__SUCC = PATH_NODE__SUCC;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_FORK__PRED = PATH_NODE__PRED;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_FORK__COMP_REF = PATH_NODE__COMP_REF;

    /**
     * The number of structural features of the '<em>And Fork</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_FORK_FEATURE_COUNT = PATH_NODE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.AndJoinImpl <em>And Join</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.AndJoinImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getAndJoin()
     * @generated
     */
    int AND_JOIN = 1;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_JOIN__NAME = PATH_NODE__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_JOIN__PARTIALITY = PATH_NODE__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_JOIN__VISIBILITY = PATH_NODE__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_JOIN__SUCC = PATH_NODE__SUCC;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_JOIN__PRED = PATH_NODE__PRED;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_JOIN__COMP_REF = PATH_NODE__COMP_REF;

    /**
     * The number of structural features of the '<em>And Join</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int AND_JOIN_FEATURE_COUNT = PATH_NODE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.OrForkImpl <em>Or Fork</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.OrForkImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getOrFork()
     * @generated
     */
    int OR_FORK = 2;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_FORK__NAME = PATH_NODE__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_FORK__PARTIALITY = PATH_NODE__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_FORK__VISIBILITY = PATH_NODE__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_FORK__SUCC = PATH_NODE__SUCC;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_FORK__PRED = PATH_NODE__PRED;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_FORK__COMP_REF = PATH_NODE__COMP_REF;

    /**
     * The number of structural features of the '<em>Or Fork</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_FORK_FEATURE_COUNT = PATH_NODE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.OrJoinImpl <em>Or Join</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.OrJoinImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getOrJoin()
     * @generated
     */
    int OR_JOIN = 3;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_JOIN__NAME = PATH_NODE__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_JOIN__PARTIALITY = PATH_NODE__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_JOIN__VISIBILITY = PATH_NODE__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_JOIN__SUCC = PATH_NODE__SUCC;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_JOIN__PRED = PATH_NODE__PRED;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_JOIN__COMP_REF = PATH_NODE__COMP_REF;

    /**
     * The number of structural features of the '<em>Or Join</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OR_JOIN_FEATURE_COUNT = PATH_NODE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.EmptyPointImpl <em>Empty Point</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.EmptyPointImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getEmptyPoint()
     * @generated
     */
    int EMPTY_POINT = 4;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPTY_POINT__NAME = PATH_NODE__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPTY_POINT__PARTIALITY = PATH_NODE__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPTY_POINT__VISIBILITY = PATH_NODE__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPTY_POINT__SUCC = PATH_NODE__SUCC;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPTY_POINT__PRED = PATH_NODE__PRED;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPTY_POINT__COMP_REF = PATH_NODE__COMP_REF;

    /**
     * The feature id for the '<em><b>Direction</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPTY_POINT__DIRECTION = PATH_NODE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Empty Point</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPTY_POINT_FEATURE_COUNT = PATH_NODE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.ResponsibilityRefImpl <em>Responsibility Ref</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.ResponsibilityRefImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getResponsibilityRef()
     * @generated
     */
    int RESPONSIBILITY_REF = 6;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_REF__NAME = PATH_NODE__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_REF__PARTIALITY = PATH_NODE__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_REF__VISIBILITY = PATH_NODE__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_REF__SUCC = PATH_NODE__SUCC;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_REF__PRED = PATH_NODE__PRED;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_REF__COMP_REF = PATH_NODE__COMP_REF;

    /**
     * The feature id for the '<em><b>Resp Def</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_REF__RESP_DEF = PATH_NODE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Responsibility Ref</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_REF_FEATURE_COUNT = PATH_NODE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.ResponsibilityImpl <em>Responsibility</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.ResponsibilityImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getResponsibility()
     * @generated
     */
    int RESPONSIBILITY = 7;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY__NAME = UCM_MODEL_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY__PARTIALITY = UCM_MODEL_ELEMENT__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY__VISIBILITY = UCM_MODEL_ELEMENT__VISIBILITY;

    /**
     * The feature id for the '<em><b>Resps Refs</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY__RESPS_REFS = UCM_MODEL_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Responsibility</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESPONSIBILITY_FEATURE_COUNT = UCM_MODEL_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.StartPointImpl <em>Start Point</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.StartPointImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getStartPoint()
     * @generated
     */
    int START_POINT = 8;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int START_POINT__NAME = PATH_NODE__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int START_POINT__PARTIALITY = PATH_NODE__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int START_POINT__VISIBILITY = PATH_NODE__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int START_POINT__SUCC = PATH_NODE__SUCC;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int START_POINT__PRED = PATH_NODE__PRED;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int START_POINT__COMP_REF = PATH_NODE__COMP_REF;

    /**
     * The number of structural features of the '<em>Start Point</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int START_POINT_FEATURE_COUNT = PATH_NODE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.NodeConnectionImpl <em>Node Connection</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.NodeConnectionImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getNodeConnection()
     * @generated
     */
    int NODE_CONNECTION = 9;

    /**
     * The feature id for the '<em><b>Condition</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NODE_CONNECTION__CONDITION = 0;

    /**
     * The feature id for the '<em><b>Source</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NODE_CONNECTION__SOURCE = 1;

    /**
     * The feature id for the '<em><b>Target</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NODE_CONNECTION__TARGET = 2;

    /**
     * The number of structural features of the '<em>Node Connection</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NODE_CONNECTION_FEATURE_COUNT = 3;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.UCMMapImpl <em>Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.UCMMapImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getUCMMap()
     * @generated
     */
    int UCM_MAP = 11;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__NAME = CorePackage.CORE_MODEL__NAME;

    /**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__MODEL_REUSES = CorePackage.CORE_MODEL__MODEL_REUSES;

    /**
     * The feature id for the '<em><b>Model Elements</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__MODEL_ELEMENTS = CorePackage.CORE_MODEL__MODEL_ELEMENTS;

    /**
     * The feature id for the '<em><b>Realizes</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__REALIZES = CorePackage.CORE_MODEL__REALIZES;

    /**
     * The feature id for the '<em><b>Core Concern</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__CORE_CONCERN = CorePackage.CORE_MODEL__CORE_CONCERN;

    /**
     * The feature id for the '<em><b>Connections</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__CONNECTIONS = CorePackage.CORE_MODEL_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__NODES = CorePackage.CORE_MODEL_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Resps</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__RESPS = CorePackage.CORE_MODEL_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Comps</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__COMPS = CorePackage.CORE_MODEL_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Comp Refs</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP__COMP_REFS = CorePackage.CORE_MODEL_FEATURE_COUNT + 4;

    /**
     * The number of structural features of the '<em>Map</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UCM_MAP_FEATURE_COUNT = CorePackage.CORE_MODEL_FEATURE_COUNT + 5;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.ComponentRefImpl <em>Component Ref</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.ComponentRefImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getComponentRef()
     * @generated
     */
    int COMPONENT_REF = 12;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_REF__NAME = UCM_MODEL_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_REF__PARTIALITY = UCM_MODEL_ELEMENT__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_REF__VISIBILITY = UCM_MODEL_ELEMENT__VISIBILITY;

    /**
     * The feature id for the '<em><b>Comp Def</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_REF__COMP_DEF = UCM_MODEL_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Children</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_REF__CHILDREN = UCM_MODEL_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Parent</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_REF__PARENT = UCM_MODEL_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Nodes</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_REF__NODES = UCM_MODEL_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The number of structural features of the '<em>Component Ref</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_REF_FEATURE_COUNT = UCM_MODEL_ELEMENT_FEATURE_COUNT + 4;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.ComponentImpl <em>Component</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.ComponentImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getComponent()
     * @generated
     */
    int COMPONENT = 13;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT__NAME = UCM_MODEL_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT__PARTIALITY = UCM_MODEL_ELEMENT__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT__VISIBILITY = UCM_MODEL_ELEMENT__VISIBILITY;

    /**
     * The feature id for the '<em><b>Comp Refs</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT__COMP_REFS = UCM_MODEL_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Children</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT__CHILDREN = UCM_MODEL_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Parents</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT__PARENTS = UCM_MODEL_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Kind</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT__KIND = UCM_MODEL_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The number of structural features of the '<em>Component</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPONENT_FEATURE_COUNT = UCM_MODEL_ELEMENT_FEATURE_COUNT + 4;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.ConditionImpl <em>Condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.ConditionImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getCondition()
     * @generated
     */
    int CONDITION = 14;

    /**
     * The feature id for the '<em><b>Expression</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION__EXPRESSION = 0;

    /**
     * The number of structural features of the '<em>Condition</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.impl.EndPointImpl <em>End Point</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.impl.EndPointImpl
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getEndPoint()
     * @generated
     */
    int END_POINT = 15;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int END_POINT__NAME = PATH_NODE__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int END_POINT__PARTIALITY = PATH_NODE__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int END_POINT__VISIBILITY = PATH_NODE__VISIBILITY;

    /**
     * The feature id for the '<em><b>Succ</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int END_POINT__SUCC = PATH_NODE__SUCC;

    /**
     * The feature id for the '<em><b>Pred</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int END_POINT__PRED = PATH_NODE__PRED;

    /**
     * The feature id for the '<em><b>Comp Ref</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int END_POINT__COMP_REF = PATH_NODE__COMP_REF;

    /**
     * The number of structural features of the '<em>End Point</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int END_POINT_FEATURE_COUNT = PATH_NODE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.ucm.ComponentKind <em>Component Kind</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.ucm.ComponentKind
     * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getComponentKind()
     * @generated
     */
    int COMPONENT_KIND = 16;


    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.AndFork <em>And Fork</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>And Fork</em>'.
     * @see ca.mcgill.sel.ucm.AndFork
     * @generated
     */
    EClass getAndFork();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.AndJoin <em>And Join</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>And Join</em>'.
     * @see ca.mcgill.sel.ucm.AndJoin
     * @generated
     */
    EClass getAndJoin();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.OrFork <em>Or Fork</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Or Fork</em>'.
     * @see ca.mcgill.sel.ucm.OrFork
     * @generated
     */
    EClass getOrFork();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.OrJoin <em>Or Join</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Or Join</em>'.
     * @see ca.mcgill.sel.ucm.OrJoin
     * @generated
     */
    EClass getOrJoin();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.EmptyPoint <em>Empty Point</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Empty Point</em>'.
     * @see ca.mcgill.sel.ucm.EmptyPoint
     * @generated
     */
    EClass getEmptyPoint();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.ucm.EmptyPoint#isDirection <em>Direction</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Direction</em>'.
     * @see ca.mcgill.sel.ucm.EmptyPoint#isDirection()
     * @see #getEmptyPoint()
     * @generated
     */
    EAttribute getEmptyPoint_Direction();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.PathNode <em>Path Node</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Path Node</em>'.
     * @see ca.mcgill.sel.ucm.PathNode
     * @generated
     */
    EClass getPathNode();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.ucm.PathNode#getSucc <em>Succ</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Succ</em>'.
     * @see ca.mcgill.sel.ucm.PathNode#getSucc()
     * @see #getPathNode()
     * @generated
     */
    EReference getPathNode_Succ();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.ucm.PathNode#getPred <em>Pred</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Pred</em>'.
     * @see ca.mcgill.sel.ucm.PathNode#getPred()
     * @see #getPathNode()
     * @generated
     */
    EReference getPathNode_Pred();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.ucm.PathNode#getCompRef <em>Comp Ref</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Comp Ref</em>'.
     * @see ca.mcgill.sel.ucm.PathNode#getCompRef()
     * @see #getPathNode()
     * @generated
     */
    EReference getPathNode_CompRef();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.ResponsibilityRef <em>Responsibility Ref</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Responsibility Ref</em>'.
     * @see ca.mcgill.sel.ucm.ResponsibilityRef
     * @generated
     */
    EClass getResponsibilityRef();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.ucm.ResponsibilityRef#getRespDef <em>Resp Def</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Resp Def</em>'.
     * @see ca.mcgill.sel.ucm.ResponsibilityRef#getRespDef()
     * @see #getResponsibilityRef()
     * @generated
     */
    EReference getResponsibilityRef_RespDef();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.Responsibility <em>Responsibility</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Responsibility</em>'.
     * @see ca.mcgill.sel.ucm.Responsibility
     * @generated
     */
    EClass getResponsibility();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.ucm.Responsibility#getRespsRefs <em>Resps Refs</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Resps Refs</em>'.
     * @see ca.mcgill.sel.ucm.Responsibility#getRespsRefs()
     * @see #getResponsibility()
     * @generated
     */
    EReference getResponsibility_RespsRefs();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.StartPoint <em>Start Point</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Start Point</em>'.
     * @see ca.mcgill.sel.ucm.StartPoint
     * @generated
     */
    EClass getStartPoint();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.NodeConnection <em>Node Connection</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Node Connection</em>'.
     * @see ca.mcgill.sel.ucm.NodeConnection
     * @generated
     */
    EClass getNodeConnection();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.ucm.NodeConnection#getCondition <em>Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Condition</em>'.
     * @see ca.mcgill.sel.ucm.NodeConnection#getCondition()
     * @see #getNodeConnection()
     * @generated
     */
    EReference getNodeConnection_Condition();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.ucm.NodeConnection#getSource <em>Source</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Source</em>'.
     * @see ca.mcgill.sel.ucm.NodeConnection#getSource()
     * @see #getNodeConnection()
     * @generated
     */
    EReference getNodeConnection_Source();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.ucm.NodeConnection#getTarget <em>Target</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Target</em>'.
     * @see ca.mcgill.sel.ucm.NodeConnection#getTarget()
     * @see #getNodeConnection()
     * @generated
     */
    EReference getNodeConnection_Target();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.UCMModelElement <em>Model Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Model Element</em>'.
     * @see ca.mcgill.sel.ucm.UCMModelElement
     * @generated
     */
    EClass getUCMModelElement();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.UCMMap <em>Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Map</em>'.
     * @see ca.mcgill.sel.ucm.UCMMap
     * @generated
     */
    EClass getUCMMap();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.ucm.UCMMap#getConnections <em>Connections</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Connections</em>'.
     * @see ca.mcgill.sel.ucm.UCMMap#getConnections()
     * @see #getUCMMap()
     * @generated
     */
    EReference getUCMMap_Connections();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.ucm.UCMMap#getNodes <em>Nodes</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Nodes</em>'.
     * @see ca.mcgill.sel.ucm.UCMMap#getNodes()
     * @see #getUCMMap()
     * @generated
     */
    EReference getUCMMap_Nodes();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.ucm.UCMMap#getResps <em>Resps</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Resps</em>'.
     * @see ca.mcgill.sel.ucm.UCMMap#getResps()
     * @see #getUCMMap()
     * @generated
     */
    EReference getUCMMap_Resps();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.ucm.UCMMap#getComps <em>Comps</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Comps</em>'.
     * @see ca.mcgill.sel.ucm.UCMMap#getComps()
     * @see #getUCMMap()
     * @generated
     */
    EReference getUCMMap_Comps();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.ucm.UCMMap#getCompRefs <em>Comp Refs</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Comp Refs</em>'.
     * @see ca.mcgill.sel.ucm.UCMMap#getCompRefs()
     * @see #getUCMMap()
     * @generated
     */
    EReference getUCMMap_CompRefs();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.ComponentRef <em>Component Ref</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Component Ref</em>'.
     * @see ca.mcgill.sel.ucm.ComponentRef
     * @generated
     */
    EClass getComponentRef();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.ucm.ComponentRef#getCompDef <em>Comp Def</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Comp Def</em>'.
     * @see ca.mcgill.sel.ucm.ComponentRef#getCompDef()
     * @see #getComponentRef()
     * @generated
     */
    EReference getComponentRef_CompDef();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.ucm.ComponentRef#getChildren <em>Children</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Children</em>'.
     * @see ca.mcgill.sel.ucm.ComponentRef#getChildren()
     * @see #getComponentRef()
     * @generated
     */
    EReference getComponentRef_Children();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.ucm.ComponentRef#getParent <em>Parent</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Parent</em>'.
     * @see ca.mcgill.sel.ucm.ComponentRef#getParent()
     * @see #getComponentRef()
     * @generated
     */
    EReference getComponentRef_Parent();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.ucm.ComponentRef#getNodes <em>Nodes</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Nodes</em>'.
     * @see ca.mcgill.sel.ucm.ComponentRef#getNodes()
     * @see #getComponentRef()
     * @generated
     */
    EReference getComponentRef_Nodes();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.Component <em>Component</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Component</em>'.
     * @see ca.mcgill.sel.ucm.Component
     * @generated
     */
    EClass getComponent();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.ucm.Component#getCompRefs <em>Comp Refs</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Comp Refs</em>'.
     * @see ca.mcgill.sel.ucm.Component#getCompRefs()
     * @see #getComponent()
     * @generated
     */
    EReference getComponent_CompRefs();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.ucm.Component#getChildren <em>Children</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Children</em>'.
     * @see ca.mcgill.sel.ucm.Component#getChildren()
     * @see #getComponent()
     * @generated
     */
    EReference getComponent_Children();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.ucm.Component#getParents <em>Parents</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Parents</em>'.
     * @see ca.mcgill.sel.ucm.Component#getParents()
     * @see #getComponent()
     * @generated
     */
    EReference getComponent_Parents();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.ucm.Component#getKind <em>Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Kind</em>'.
     * @see ca.mcgill.sel.ucm.Component#getKind()
     * @see #getComponent()
     * @generated
     */
    EAttribute getComponent_Kind();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.Condition <em>Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Condition</em>'.
     * @see ca.mcgill.sel.ucm.Condition
     * @generated
     */
    EClass getCondition();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.ucm.Condition#getExpression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Expression</em>'.
     * @see ca.mcgill.sel.ucm.Condition#getExpression()
     * @see #getCondition()
     * @generated
     */
    EAttribute getCondition_Expression();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.ucm.EndPoint <em>End Point</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>End Point</em>'.
     * @see ca.mcgill.sel.ucm.EndPoint
     * @generated
     */
    EClass getEndPoint();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.ucm.ComponentKind <em>Component Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Component Kind</em>'.
     * @see ca.mcgill.sel.ucm.ComponentKind
     * @generated
     */
    EEnum getComponentKind();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    UCMFactory getUCMFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each operation of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.AndForkImpl <em>And Fork</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.AndForkImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getAndFork()
         * @generated
         */
        EClass AND_FORK = eINSTANCE.getAndFork();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.AndJoinImpl <em>And Join</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.AndJoinImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getAndJoin()
         * @generated
         */
        EClass AND_JOIN = eINSTANCE.getAndJoin();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.OrForkImpl <em>Or Fork</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.OrForkImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getOrFork()
         * @generated
         */
        EClass OR_FORK = eINSTANCE.getOrFork();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.OrJoinImpl <em>Or Join</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.OrJoinImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getOrJoin()
         * @generated
         */
        EClass OR_JOIN = eINSTANCE.getOrJoin();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.EmptyPointImpl <em>Empty Point</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.EmptyPointImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getEmptyPoint()
         * @generated
         */
        EClass EMPTY_POINT = eINSTANCE.getEmptyPoint();

        /**
         * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute EMPTY_POINT__DIRECTION = eINSTANCE.getEmptyPoint_Direction();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.PathNodeImpl <em>Path Node</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.PathNodeImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getPathNode()
         * @generated
         */
        EClass PATH_NODE = eINSTANCE.getPathNode();

        /**
         * The meta object literal for the '<em><b>Succ</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference PATH_NODE__SUCC = eINSTANCE.getPathNode_Succ();

        /**
         * The meta object literal for the '<em><b>Pred</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference PATH_NODE__PRED = eINSTANCE.getPathNode_Pred();

        /**
         * The meta object literal for the '<em><b>Comp Ref</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference PATH_NODE__COMP_REF = eINSTANCE.getPathNode_CompRef();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.ResponsibilityRefImpl <em>Responsibility Ref</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.ResponsibilityRefImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getResponsibilityRef()
         * @generated
         */
        EClass RESPONSIBILITY_REF = eINSTANCE.getResponsibilityRef();

        /**
         * The meta object literal for the '<em><b>Resp Def</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference RESPONSIBILITY_REF__RESP_DEF = eINSTANCE.getResponsibilityRef_RespDef();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.ResponsibilityImpl <em>Responsibility</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.ResponsibilityImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getResponsibility()
         * @generated
         */
        EClass RESPONSIBILITY = eINSTANCE.getResponsibility();

        /**
         * The meta object literal for the '<em><b>Resps Refs</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference RESPONSIBILITY__RESPS_REFS = eINSTANCE.getResponsibility_RespsRefs();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.StartPointImpl <em>Start Point</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.StartPointImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getStartPoint()
         * @generated
         */
        EClass START_POINT = eINSTANCE.getStartPoint();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.NodeConnectionImpl <em>Node Connection</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.NodeConnectionImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getNodeConnection()
         * @generated
         */
        EClass NODE_CONNECTION = eINSTANCE.getNodeConnection();

        /**
         * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference NODE_CONNECTION__CONDITION = eINSTANCE.getNodeConnection_Condition();

        /**
         * The meta object literal for the '<em><b>Source</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference NODE_CONNECTION__SOURCE = eINSTANCE.getNodeConnection_Source();

        /**
         * The meta object literal for the '<em><b>Target</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference NODE_CONNECTION__TARGET = eINSTANCE.getNodeConnection_Target();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.UCMModelElementImpl <em>Model Element</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.UCMModelElementImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getUCMModelElement()
         * @generated
         */
        EClass UCM_MODEL_ELEMENT = eINSTANCE.getUCMModelElement();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.UCMMapImpl <em>Map</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.UCMMapImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getUCMMap()
         * @generated
         */
        EClass UCM_MAP = eINSTANCE.getUCMMap();

        /**
         * The meta object literal for the '<em><b>Connections</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference UCM_MAP__CONNECTIONS = eINSTANCE.getUCMMap_Connections();

        /**
         * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference UCM_MAP__NODES = eINSTANCE.getUCMMap_Nodes();

        /**
         * The meta object literal for the '<em><b>Resps</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference UCM_MAP__RESPS = eINSTANCE.getUCMMap_Resps();

        /**
         * The meta object literal for the '<em><b>Comps</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference UCM_MAP__COMPS = eINSTANCE.getUCMMap_Comps();

        /**
         * The meta object literal for the '<em><b>Comp Refs</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference UCM_MAP__COMP_REFS = eINSTANCE.getUCMMap_CompRefs();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.ComponentRefImpl <em>Component Ref</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.ComponentRefImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getComponentRef()
         * @generated
         */
        EClass COMPONENT_REF = eINSTANCE.getComponentRef();

        /**
         * The meta object literal for the '<em><b>Comp Def</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPONENT_REF__COMP_DEF = eINSTANCE.getComponentRef_CompDef();

        /**
         * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPONENT_REF__CHILDREN = eINSTANCE.getComponentRef_Children();

        /**
         * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPONENT_REF__PARENT = eINSTANCE.getComponentRef_Parent();

        /**
         * The meta object literal for the '<em><b>Nodes</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPONENT_REF__NODES = eINSTANCE.getComponentRef_Nodes();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.ComponentImpl <em>Component</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.ComponentImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getComponent()
         * @generated
         */
        EClass COMPONENT = eINSTANCE.getComponent();

        /**
         * The meta object literal for the '<em><b>Comp Refs</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPONENT__COMP_REFS = eINSTANCE.getComponent_CompRefs();

        /**
         * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPONENT__CHILDREN = eINSTANCE.getComponent_Children();

        /**
         * The meta object literal for the '<em><b>Parents</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPONENT__PARENTS = eINSTANCE.getComponent_Parents();

        /**
         * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute COMPONENT__KIND = eINSTANCE.getComponent_Kind();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.ConditionImpl <em>Condition</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.ConditionImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getCondition()
         * @generated
         */
        EClass CONDITION = eINSTANCE.getCondition();

        /**
         * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONDITION__EXPRESSION = eINSTANCE.getCondition_Expression();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.impl.EndPointImpl <em>End Point</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.impl.EndPointImpl
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getEndPoint()
         * @generated
         */
        EClass END_POINT = eINSTANCE.getEndPoint();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.ucm.ComponentKind <em>Component Kind</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.ucm.ComponentKind
         * @see ca.mcgill.sel.ucm.impl.UCMPackageImpl#getComponentKind()
         * @generated
         */
        EEnum COMPONENT_KIND = eINSTANCE.getComponentKind();

    }

} //UCMPackage
