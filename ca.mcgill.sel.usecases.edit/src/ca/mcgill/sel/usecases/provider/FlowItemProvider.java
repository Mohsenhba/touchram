/**
 */
package ca.mcgill.sel.usecases.provider;


import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.usecases.Flow} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FlowItemProvider 
    extends NamedElementItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public FlowItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addConclusionTypePropertyDescriptor(object);
			addConclusionStepPropertyDescriptor(object);
			addPostConditionPropertyDescriptor(object);
			addReferencedStepsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

    /**
	 * This adds a property descriptor for the Conclusion Type feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addConclusionTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Flow_conclusionType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Flow_conclusionType_feature", "_UI_Flow_type"),
				 UcPackage.Literals.FLOW__CONCLUSION_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Conclusion Step feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addConclusionStepPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Flow_conclusionStep_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Flow_conclusionStep_feature", "_UI_Flow_type"),
				 UcPackage.Literals.FLOW__CONCLUSION_STEP,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Post Condition feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addPostConditionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Flow_postCondition_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Flow_postCondition_feature", "_UI_Flow_type"),
				 UcPackage.Literals.FLOW__POST_CONDITION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Referenced Steps feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addReferencedStepsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Flow_referencedSteps_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Flow_referencedSteps_feature", "_UI_Flow_type"),
				 UcPackage.Literals.FLOW__REFERENCED_STEPS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UcPackage.Literals.FLOW__STEPS);
			childrenFeatures.add(UcPackage.Literals.FLOW__ALTERNATE_FLOWS);
			childrenFeatures.add(UcPackage.Literals.FLOW__PRECONDITION_STEP);
		}
		return childrenFeatures;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public String getText(Object object) {
        Flow flow = (Flow)object;
        String label = flow.getName() + "(" + UseCaseTextUtils.getFlowStepNumberText(flow) + ")";
        return label == null || label.length() == 0 ?
            getString("_UI_Flow_type") :
            getString("_UI_Flow_type") + " " + label;
    }


    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Flow.class)) {
			case UcPackage.FLOW__CONCLUSION_TYPE:
			case UcPackage.FLOW__POST_CONDITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case UcPackage.FLOW__STEPS:
			case UcPackage.FLOW__ALTERNATE_FLOWS:
			case UcPackage.FLOW__PRECONDITION_STEP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__STEPS,
				 UcFactory.eINSTANCE.createCommunicationStep()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__STEPS,
				 UcFactory.eINSTANCE.createUseCaseReferenceStep()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__STEPS,
				 UcFactory.eINSTANCE.createContextStep()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__STEPS,
				 UcFactory.eINSTANCE.createExtensionPoint()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__STEPS,
				 UcFactory.eINSTANCE.createAnythingStep()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__ALTERNATE_FLOWS,
				 UcFactory.eINSTANCE.createFlow()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__PRECONDITION_STEP,
				 UcFactory.eINSTANCE.createCommunicationStep()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__PRECONDITION_STEP,
				 UcFactory.eINSTANCE.createUseCaseReferenceStep()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__PRECONDITION_STEP,
				 UcFactory.eINSTANCE.createContextStep()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__PRECONDITION_STEP,
				 UcFactory.eINSTANCE.createExtensionPoint()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.FLOW__PRECONDITION_STEP,
				 UcFactory.eINSTANCE.createAnythingStep()));
	}

    /**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == UcPackage.Literals.FLOW__STEPS ||
			childFeature == UcPackage.Literals.FLOW__PRECONDITION_STEP;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
