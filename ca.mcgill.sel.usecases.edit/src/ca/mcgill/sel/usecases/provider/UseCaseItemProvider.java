/**
 */
package ca.mcgill.sel.usecases.provider;


import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.usecases.MappableElement;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.usecases.UseCase} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UseCaseItemProvider extends MappableElementItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public UseCaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPrimaryActorsPropertyDescriptor(object);
			addSecondaryActorsPropertyDescriptor(object);
			addIntentionPropertyDescriptor(object);
			addMultiplicityPropertyDescriptor(object);
			addLevelPropertyDescriptor(object);
			addAbstractPropertyDescriptor(object);
			addGeneralizationPropertyDescriptor(object);
			addIncludedUseCasesPropertyDescriptor(object);
			addExtendedUseCasePropertyDescriptor(object);
			addSelectionConditionPropertyDescriptor(object);
			addSelectionConditionTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

    /**
	 * This adds a property descriptor for the Primary Actors feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addPrimaryActorsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_primaryActors_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_primaryActors_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__PRIMARY_ACTORS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Secondary Actors feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addSecondaryActorsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_secondaryActors_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_secondaryActors_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__SECONDARY_ACTORS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Intention feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addIntentionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_intention_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_intention_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__INTENTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Multiplicity feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addMultiplicityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_multiplicity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_multiplicity_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__MULTIPLICITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Level feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addLevelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_level_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_level_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__LEVEL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Abstract feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addAbstractPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_abstract_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_abstract_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__ABSTRACT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Generalization feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addGeneralizationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_generalization_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_generalization_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__GENERALIZATION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Included Use Cases feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addIncludedUseCasesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_includedUseCases_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_includedUseCases_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__INCLUDED_USE_CASES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Extended Use Case feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addExtendedUseCasePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_extendedUseCase_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_extendedUseCase_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__EXTENDED_USE_CASE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Selection Condition feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addSelectionConditionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_selectionCondition_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_selectionCondition_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__SELECTION_CONDITION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Selection Condition Type feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addSelectionConditionTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_selectionConditionType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_selectionConditionType_feature", "_UI_UseCase_type"),
				 UcPackage.Literals.USE_CASE__SELECTION_CONDITION_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

    /**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UcPackage.Literals.USE_CASE__MAIN_SUCCESS_SCENARIO);
			childrenFeatures.add(UcPackage.Literals.USE_CASE__USE_CASE_INTENTION);
			childrenFeatures.add(UcPackage.Literals.USE_CASE__USE_CASE_MULTIPLICITY);
		}
		return childrenFeatures;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

    /**
	 * This returns UseCase.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UseCase"));
	}

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public String getText(Object object) {
        MappableElement mappableElement = (MappableElement) object;
        
        String label = ((MappableElement)object).getName();
        
        if (mappableElement.getPartiality() == COREPartialityType.PUBLIC) {
            label = "|" + label;
        } else if (mappableElement.getPartiality() == COREPartialityType.CONCERN) {
            label = "\u00A6" + label;
        }
        
        return label == null || label.length() == 0 ?
            getString("_UI_UseCase_type") :
            getString("_UI_UseCase_type") + " " + label;
    }


    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UseCase.class)) {
			case UcPackage.USE_CASE__PRIMARY_ACTORS:
			case UcPackage.USE_CASE__SECONDARY_ACTORS:
			case UcPackage.USE_CASE__INTENTION:
			case UcPackage.USE_CASE__MULTIPLICITY:
			case UcPackage.USE_CASE__LEVEL:
			case UcPackage.USE_CASE__ABSTRACT:
			case UcPackage.USE_CASE__GENERALIZATION:
			case UcPackage.USE_CASE__INCLUDED_USE_CASES:
			case UcPackage.USE_CASE__SELECTION_CONDITION_TYPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case UcPackage.USE_CASE__MAIN_SUCCESS_SCENARIO:
			case UcPackage.USE_CASE__USE_CASE_INTENTION:
			case UcPackage.USE_CASE__USE_CASE_MULTIPLICITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.USE_CASE__MAIN_SUCCESS_SCENARIO,
				 UcFactory.eINSTANCE.createFlow()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.USE_CASE__USE_CASE_INTENTION,
				 UcFactory.eINSTANCE.createActorReferenceText()));

		newChildDescriptors.add
			(createChildParameter
				(UcPackage.Literals.USE_CASE__USE_CASE_MULTIPLICITY,
				 UcFactory.eINSTANCE.createActorReferenceText()));
	}

    /**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == UcPackage.Literals.USE_CASE__USE_CASE_INTENTION ||
			childFeature == UcPackage.Literals.USE_CASE__USE_CASE_MULTIPLICITY;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
