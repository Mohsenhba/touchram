package ca.mcgill.sel.usecases.language.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.MappableElement;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;

/**
 * A controller for actions on a use case.
 * @author rlanguay
 *
 */
public class UseCaseController extends BaseController {
    
    /**
     * Sets the primary actor on a use case.
     * @param actor The actor.
     * @param useCase The use case.
     */
    public void addPrimaryActor(Actor actor, UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(useCase);
        CompoundCommand command = new CompoundCommand();
        
        // If the actor is already a secondary actor, remove it from that set first.
        if (useCase.getSecondaryActors().contains(actor)) {
            command.append(createRemoveSecondaryActorCommand(editingDomain, useCase, actor));
        }
        
        Command setCommand = AddCommand.create(editingDomain, useCase, UcPackage.Literals.USE_CASE__PRIMARY_ACTORS, 
                actor, useCase.getPrimaryActors().size());
        command.append (setCommand);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Adds a secondary actor to a use case.
     * @param actor The actor.
     * @param useCase The use case.
     */
    public void addSecondaryActor(Actor actor, UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(useCase);
        CompoundCommand command = new CompoundCommand();
        
        // If the actor is already the primary actor, remove it first.
        if (useCase.getPrimaryActors().contains(actor)) {
            command.append(createRemovePrimaryActorCommand(editingDomain, useCase, actor));
        }
        
        Command addCommand = AddCommand.create(
                editingDomain, useCase, UcPackage.Literals.USE_CASE__SECONDARY_ACTORS, actor, useCase.getSecondaryActors().size());
        command.append (addCommand);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Clears the primary actor from a use case.
     * @param actor The actor.
     * @param useCase The use case.
     */
    public void removePrimaryActor(Actor actor, UseCase useCase) {
        UseCaseModel ucd = (UseCaseModel) useCase.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        Command command = createRemovePrimaryActorCommand(editingDomain, useCase, actor);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Removes a secondary actor from a use case.
     * @param actor The actor.
     * @param useCase The use case.
     */
    public void removeSecondaryActor(Actor actor, UseCase useCase) {
        UseCaseModel ucd = (UseCaseModel) useCase.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        Command command = createRemoveSecondaryActorCommand(editingDomain, useCase, actor);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Sets the generalization relationship for a use case.
     * @param useCase The use case.
     * @param generalizationUseCase The more general use case.
     */
    public void setGeneralization(UseCase useCase, UseCase generalizationUseCase) {
        doSet(useCase, UcPackage.Literals.USE_CASE__GENERALIZATION, generalizationUseCase);
    }
    
    /**
     * Used by the UI to create required flows in older use cases.
     * @param useCase The use case.
     */
    public void createFlows(UseCase useCase) {
        UseCaseModel ucd = (UseCaseModel) useCase.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        CompoundCommand command = new CompoundCommand();
        
        Flow mainSuccessScenario = UcFactory.eINSTANCE.createFlow();

        command.append(SetCommand.create(editingDomain, useCase, 
                UcPackage.Literals.USE_CASE__MAIN_SUCCESS_SCENARIO, mainSuccessScenario));
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Add a use case to the list of included use cases.
     * @param useCase The use case.
     * @param useCaseToInclude The use case to include.
     */
    public void addIncludedUseCase(UseCase useCase, UseCase useCaseToInclude) {
        doAdd(useCase, UcPackage.Literals.USE_CASE__INCLUDED_USE_CASES, useCaseToInclude);
    }
    
    /**
     * Remove a use case to the list of included use cases.
     * @param useCase The use case.
     * @param useCaseToInclude The use case to remove.
     */
    public void removeIncludedUseCase(UseCase useCase, UseCase useCaseToRemove) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(useCase);
        Command command = RemoveCommand.create(
                editingDomain, useCase, UcPackage.Literals.USE_CASE__INCLUDED_USE_CASES, useCaseToRemove);
        doExecute(editingDomain, command);
    }
    
    /**
     * Switch the abstract flag on a use case.
     * @param useCase The use case.
     */
    public void switchAbstract(UseCase useCase) {
        doSwitch(useCase, UcPackage.Literals.USE_CASE__ABSTRACT);
    }
    
    /**
     * Changes the partiality of a {@link MappableElement}.
     * 
     * @param owner - The use case we want the partiality to be changed
     * @param corePartiality - The new value of the classifier partiality
     */
    public void changeUseCasePartiality(UseCase owner, COREPartialityType corePartiality) {
        changeMappableElementPartiality(owner, corePartiality);
    }
}
