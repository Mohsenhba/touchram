package ca.mcgill.sel.usecases.language.controller;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.core.controller.util.COREReferenceUtil;
import ca.mcgill.sel.usecases.*;
import ca.mcgill.sel.usecases.util.UcModelUtil;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

/**
 * Controller defining actions on use case diagrams.
 * @author rlanguay
 *
 */
public class UseCaseModelController extends BaseController {
    /**
     * Label for main success scenario.
     */
    private static final String MAIN_SUCCESS_SCENARIO = "Main Success Scenario";
    
    /**
     * Creates a new instance of {@link UseCaseModelController}.
     */
    protected UseCaseModelController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    /**
     * Creates a new actor as well as its layout element.
     *
     * @param owner the {@link UseCaseDiagram} the actor should be added to
     * @param name the name of the actor
     * @param x the x position of the class
     * @param y the y position of the class 
     */
    public void createNewActor(UseCaseModel owner, String name, float x, float y) {
        Actor newActor = UcFactory.eINSTANCE.createActor();
        newActor.setName(name);

        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addClassCommand = AddCommand.create(editingDomain, owner, UcPackage.Literals.USE_CASE_MODEL__ACTORS,
                newActor);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newActor, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        // If the layout element is added first, when the structural view gets notified (about a new class)
        // access to the layout element is already possible.
        compoundCommand.append(addClassCommand);
        compoundCommand.append(changeCoreVisibilityCommand(editingDomain, newActor, owner, COREVisibilityType.PUBLIC));
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Creates a new use case as well as its layout element.
     *
     * @param owner the {@link UseCaseDiagram} the actor should be added to
     * @param name the name of the use case
     * @param x the x position of the class
     * @param y the y position of the class
     */
    public void createNewUseCase(UseCaseModel owner, String name, float x, float y) {
        UseCase newUseCase = UcFactory.eINSTANCE.createUseCase();
        newUseCase.setName(name);
        
        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);        

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addClassCommand = AddCommand.create(editingDomain, owner, UcPackage.Literals.USE_CASE_MODEL__USE_CASES,
                newUseCase);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newUseCase, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // If the layout element is added first, when the structural view gets notified (about a new class)
        // access to the layout element is already possible.
        initializeUseCaseCommand(newUseCase, compoundCommand);
        compoundCommand.append(addClassCommand);
        compoundCommand.append(changeCoreVisibilityCommand(editingDomain, newUseCase, owner, COREVisibilityType.PUBLIC));
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Creates a new note.
     *
     * @param owner the {@link UseCaseDiagram} the note should be added to
     * @param x the x position of the note
     * @param y the y position of the note
     */
    public void createNewNote(UseCaseModel cd, float x, float y) {
        //Create note
        Note newNote = UcFactory.eINSTANCE.createNote();

        //Create layout element
        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);
        
        //Get editing domain
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);
        
        //Create Commands
        Command addNoteCommand = AddCommand.create(editingDomain, cd, UcPackage.Literals.USE_CASE_MODEL__NOTES,
                newNote);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, cd, newNote, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addNoteCommand);
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
        cd.getNotes().add(newNote);       
    }
    
    /**
     * Creates a note link between the note and the given element.
     *
     * @param owner the {@link ClassDiagram} the association should be added to
     * @param note the {@link Note} linked from
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void createAnnotation(UseCaseModel owner, Note note, NamedElement annotatedElement) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        if(!note.getNotedElement().contains(annotatedElement)) {
            Command addNotedElementCommand = AddCommand.create(editingDomain, note,
                    UcPackage.Literals.NOTE__NOTED_ELEMENT, annotatedElement);
            // execute command
            doExecute(editingDomain, addNotedElementCommand);
        }
    }

    /**
     * Moves a set of named elements in a diagram.
     * @param ucd The use case diagram.
     * @param positionMap The position map to use.
     */
    public void moveNamedElements(
            UseCaseModel ucd, Map<NamedElement, LayoutElement> positionMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);

        if (positionMap.size() > 0) {
            // Create compound command.
            CompoundCommand moveClassifiersCommand = new CompoundCommand();
    
            for (Entry<NamedElement, LayoutElement> entry : positionMap.entrySet()) {
                // get all required values
                NamedElement namedElement = entry.getKey();
                float x = entry.getValue().getX();
                float y = entry.getValue().getY();
    
                Command classifierMoveCommand = createMoveCommand(editingDomain, ucd, namedElement, x, y);
                moveClassifiersCommand.append(classifierMoveCommand);
            }
    
            doExecute(editingDomain, moveClassifiersCommand);
        }
        
    }

    /**
     * Moves a set of non-named elements in a diagram.
     * @param ucd The use case diagram.
     * @param nonClassifierPositionMap The position map to use.
     */
    public void moveNonNamedElements(
            UseCaseModel ucd, Map<EObject, LayoutElement> nonClassifierPositionMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);

        if (nonClassifierPositionMap.size() > 0) {
            // Create compound command.
            CompoundCommand moveClassifiersCommand = new CompoundCommand();
    
            for (Entry<EObject, LayoutElement> entry : nonClassifierPositionMap.entrySet()) {
                // get all required values
                EObject object = entry.getKey();
                float x = entry.getValue().getX();
                float y = entry.getValue().getY();
    
                Command classifierMoveCommand = createMoveCommand(editingDomain, ucd, object, x, y);
                moveClassifiersCommand.append(classifierMoveCommand);
            }
    
            doExecute(editingDomain, moveClassifiersCommand);
        }
        
    }
    
    /**
     * Removes the given actor and its associated layout element.
     *
     * @param actor the actor that should be removed
     */
    public void removeActor(Actor actor) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(actor);
        UseCaseModel ucd = (UseCaseModel) actor.eContainer();
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Create commands for removing the {@link CORECIElement}
        Command c = deleteAllInterfaceElements(actor);
        if (c != null) {
            compoundCommand.append(c);
        }
        
        // Manage the use cases associated to this actor
        for (UseCase uc : UcModelUtil.getUseCasesAssociatedToActor(actor)) {
            if (uc.getPrimaryActors().contains(actor)) {
                compoundCommand.append(createRemovePrimaryActorCommand(editingDomain, uc, actor));
            } else if (uc.getSecondaryActors().contains(actor)) {
                compoundCommand.append(createRemoveSecondaryActorCommand(editingDomain, uc, actor));
            }
        }
        
        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, ucd, actor);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing note.
        Command removeUseCaseCommand = RemoveCommand.create(editingDomain, actor);
        compoundCommand.append(removeUseCaseCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes the given use case and its associated layout element.
     *
     * @param useCase the use case that should be removed
     */
    public void removeUseCase(UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(useCase);
        UseCaseModel ucd = (UseCaseModel) useCase.eContainer();
        
        CompoundCommand compoundCommand = new CompoundCommand();
        // Create commands for removing the {@link CORECIElement}
        Command c = deleteAllInterfaceElements(useCase);
        if (c != null) {
            compoundCommand.append(c);
        }
        
        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, ucd, useCase);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing note.
        Command removeUseCaseCommand = RemoveCommand.create(editingDomain, useCase);
        compoundCommand.append(removeUseCaseCommand);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Removes the given note and its associated layout element
     *
     * @param note the note that should be removed
     */
    public void removeNote(Note note) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(note);
        UseCaseModel ucd = (UseCaseModel) note.eContainer();
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, ucd, note);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing note.
        Command removeNoteCommand = RemoveCommand.create(editingDomain, note);
        compoundCommand.append(removeNoteCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Remove a note link between the note and the given class.
     *
     * @param note the {@link Note} linked from
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void removeAnnotation(Note note, NamedElement annotatedElement) {
        UseCaseModel ucd = (UseCaseModel) note.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        Command removeNotedElementCommand = RemoveCommand.create(editingDomain, note,
                UcPackage.Literals.NOTE__NOTED_ELEMENT, annotatedElement);
        
        // execute command
        doExecute(editingDomain, removeNotedElementCommand);
    }
    
    /**
     * Remove all the annotations of the given note
     *
     * @param note the {@link Note} linked from
     */
    public void removeAnnotations(Note note) {
        UseCaseModel ucd = (UseCaseModel) note.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        if (note.getNotedElement().size() > 0) {
            for (NamedElement element : note.getNotedElement()) {
                Command removeNotedElementCommand = RemoveCommand.create(editingDomain, note,
                        UcPackage.Literals.NOTE__NOTED_ELEMENT, element);
                compoundCommand.append(removeNotedElementCommand);
            }
                
            // execute command
            doExecute(editingDomain, compoundCommand);
        }
    }
    
    /**
     * Imports an actor into the current use case model and creates a mapping.
     * The goal is to allow the modeler to extend the actor or use it in his/her model.
     *
     * @param owner the {@link UseCaseModel} the actor should be added to
     * @param actorToImport the actor to import
     * @param x the x position of the actor
     * @param y the y position of the actor
     */
    public void importActor(UseCaseModel owner, Actor actorToImport, float x, float y) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        Actor newActor = 
                COREReferenceUtil.localizeElement(editingDomain, compoundCommand, owner, actorToImport);

        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);
        compoundCommand.append(createAddLayoutElementCommand(editingDomain,
                owner, newActor, layoutElement));
        
        doExecute(editingDomain, compoundCommand);    
    }
    
    /**
     * Imports a useCase into the current use case model and creates a mapping.
     * The goal is to allow the modeler to extend the useCase or use it in his/her model.
     *
     * @param owner the {@link UseCaseModel} the useCase should be added to
     * @param useCaseToImport the useCase to import
     * @param x the x position of the useCase
     * @param y the y position of the useCase
     */
    public void importUseCase(UseCaseModel owner, UseCase useCaseToImport, float x, float y) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        UseCase newUseCase = 
                COREReferenceUtil.localizeElement(editingDomain, compoundCommand, owner, useCaseToImport);

        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);
        compoundCommand.append(createAddLayoutElementCommand(editingDomain,
                owner, newUseCase, layoutElement));
        
        doExecute(editingDomain, compoundCommand);    
    }
    
    /**
     * Initializes a use case, to allow editing of details.
     * @param useCase The use case.
     */
    public void initializeUseCase(UseCase useCase) {
        EditingDomain domain = EMFEditUtil.getEditingDomain(useCase);
        CompoundCommand command = new CompoundCommand();
        
        initializeUseCaseCommand(useCase, command);
        doExecute(domain, command);
    }
    
    /**
     * Migrate actor text (with implicit actor IDs) to the new metamodel construct.
     * TODO: remove when backward compatibility is no longer required.
     * @param model The model to migrate.
     */
    public void migrateActorReferenceTexts(UseCaseModel model) {
        EditingDomain domain = EMFEditUtil.getEditingDomain(model);
        CompoundCommand command = new CompoundCommand();
        
        for (UseCase uc : model.getUseCases()) {
            Set<Actor> actors = UcModelUtil.getAvailableActors(uc);
            uc.setUseCaseIntention(parseActorReferenceText(uc.getIntention(), actors, model, domain, command));
            uc.setUseCaseMultiplicity(parseActorReferenceText(uc.getMultiplicity(), actors, model, domain, command));
            
            for (Flow f : UcModelUtil.getFlows(uc)) {
                Step preconditionStep = f.getPreconditionStep();
                if (preconditionStep != null) {
                    preconditionStep.setStepDescription(
                            parseActorReferenceText(preconditionStep.getStepText(), actors, model, domain, command));
                }
                
                for (Step s : f.getSteps()) {
                    s.setStepDescription(parseActorReferenceText(s.getStepText(), actors, model, domain, command));
                }
            }
        }
        
        for (Condition c : model.getConditions()) {
            Set<Actor> actors = new HashSet<Actor>(model.getActors());
            c.setConditionText(parseActorReferenceText(c.getText(), actors, model, domain, command));
        }
        
        doExecute(domain, command);
    }
    
    private ActorReferenceText parseActorReferenceText(String text, Set<Actor> actors, 
            UseCaseModel model, EditingDomain editingDomain, CompoundCommand command) {
        ActorReferenceText textObject = UcFactory.eINSTANCE.createActorReferenceText();
        
        if (text == null) {
            return textObject;
        }
        
        int currentIndex = 0;
        for (Actor a : actors) {
            if (text.contains(EMFModelUtil.getObjectEId(a))) {
                Actor localActor = COREReferenceUtil.localizeElement(editingDomain, command, model, a);
                command.append(AddCommand.create(editingDomain, textObject, 
                        UcPackage.Literals.ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES, localActor));
                text = text.replace(EMFModelUtil.getObjectEId(a), 
                        String.format(UseCaseTextUtils.ACTOR_TOKEN, currentIndex));
                currentIndex++;
            }
        }
        
        textObject.setText(text);
        return textObject;
    }
    
    private void initializeUseCaseCommand(UseCase useCase, CompoundCommand command) {
        EditingDomain domain = EMFEditUtil.getEditingDomain(useCase);
        ActorReferenceText intention = UcFactory.eINSTANCE.createActorReferenceText();
        command.append(SetCommand.create(domain, useCase, UcPackage.Literals.USE_CASE__USE_CASE_INTENTION, intention));
        
        ActorReferenceText multiplicity = UcFactory.eINSTANCE.createActorReferenceText();
        command.append(SetCommand.create(domain, useCase, UcPackage.Literals.USE_CASE__USE_CASE_MULTIPLICITY, 
                multiplicity));
        
        Flow mainSuccessScenario = UcFactory.eINSTANCE.createFlow();
        mainSuccessScenario.setName(MAIN_SUCCESS_SCENARIO);
        command.append(SetCommand.create(domain, useCase, UcPackage.Literals.USE_CASE__MAIN_SUCCESS_SCENARIO, 
                mainSuccessScenario));
    }
}
