package ca.mcgill.sel.usecases.language.weaver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.language.weaver.COREModelWeaver;
import ca.mcgill.sel.core.language.weaver.COREWeaver;
import ca.mcgill.sel.core.language.weaver.util.WeavingInformation;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.AnythingStep;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.util.UcModelUtil;

/**
 * Takes care of updating the references of elements of any {@link org.eclipse.emf.ecore.EObject} of use case models.
 * based on the weaving information provided by the use case model weaver.
 * @author rlanguay
 *
 */
public class UseCaseModelWeaver extends COREModelWeaver<UseCaseModel> {

    @Override
    public UseCaseModel createEmptyModel() {
        UseCaseModel ucm = UcModelUtil.createUseCaseDiagram("newUseCaseModel");
        return ucm;
    }
    
    @Override
    public UseCaseModel weaveModel(COREExternalArtefact targetArtefact, UseCaseModel target,
            COREModelComposition modelComposition, WeavingInformation weavingInfo, boolean topdown) {
        // Set the name of the woven model to the name of the target artefact
        target.setName(targetArtefact.getName());
        target.setSystemName(target.getName());
        
        UseCaseModel sourceUCM = (UseCaseModel) ((COREExternalArtefact) modelComposition.getSource())
                .getRootModelElement();
        
        // First iterate over the use cases in the source and either merge or copy them
        for (Iterator<UseCase> iterator = sourceUCM.getUseCases().iterator(); iterator.hasNext(); ) {
            UseCase useCaseFromSource = iterator.next();
            
            System.out.print("Handling " + useCaseFromSource.getName() + ", which is ");
            if (weavingInfo.wasWoven(useCaseFromSource)) {
                // The use case was mapped, so we need to merge
                List<UseCase> mappedToTargetUseCases = weavingInfo.getToElements(useCaseFromSource);
                System.out.println(" mapped to: " + mappedToTargetUseCases);
                for (UseCase targetUseCase : mappedToTargetUseCases) {
                    if (topdown && useCaseFromSource.getPartiality() == COREPartialityType.NONE) {
                        targetUseCase.setName(useCaseFromSource.getName());
                        targetUseCase.setUseCaseIntention(EcoreUtil.copy(useCaseFromSource.getUseCaseIntention()));
                        targetUseCase.setUseCaseMultiplicity(EcoreUtil.copy(useCaseFromSource.getUseCaseMultiplicity()));
                    }
                    
                    weaveFlows(targetUseCase, useCaseFromSource, weavingInfo, modelComposition, topdown);
                    
                    // Weave inheritance
                    // Although this code will set the supertype reference to point to the super use case in
                    // the source model, the reference updater will take care of changing the reference to
                    // the use case in the target model eventually
                    if (targetUseCase.getGeneralization() == null && useCaseFromSource.getGeneralization() != null) {
                        targetUseCase.setGeneralization(useCaseFromSource.getGeneralization());
                    }
                }
            } else {
                // The use case was not mapped, so we can just copy it over
                System.out.println("not mapped.");
                UseCase modelElementCopy = COREWeaver.copyModelElement(useCaseFromSource, targetArtefact, 
                        weavingInfo, modelComposition);
                target.getUseCases().add(modelElementCopy);
            }
        }
        
        // Then do the same for the actors
        for (Iterator<Actor> iterator = sourceUCM.getActors().iterator(); iterator.hasNext(); ) {
            Actor actorFromSource = iterator.next();
            
            System.out.print("Handling " + actorFromSource.getName() + ", which is ");
            if (!weavingInfo.wasWoven(actorFromSource)) {
                // Copy the actor from the source to the target
                System.out.println("not mapped.");
                Actor modelElementCopy = COREWeaver.copyModelElement(actorFromSource, targetArtefact,
                        weavingInfo, modelComposition);
                target.getActors().add(modelElementCopy);
            } else {
                List<Actor> mappedToTargetActors = weavingInfo.getToElements(actorFromSource);
                System.out.println(" mapped to: " + mappedToTargetActors);
                
                for (Actor targetActor : mappedToTargetActors) {
                    if (targetActor.getGeneralization() == null && actorFromSource.getGeneralization() != null) {
                        targetActor.setGeneralization(actorFromSource.getGeneralization());
                    }
                }
            }
        }
        
        // Also add all conditions from the source
        for (Iterator<Condition> iterator = sourceUCM.getConditions().iterator(); iterator.hasNext(); ) {
            Condition conditionFromSource = iterator.next();
            
            System.out.print("Handling " + conditionFromSource.toString() + ", which is ");
            if (!weavingInfo.wasWoven(conditionFromSource)) {
                // Copy the condition from the source to the target
                System.out.println("not mapped.");
                Condition modelElementCopy = COREWeaver.copyModelElement(conditionFromSource, targetArtefact,
                        weavingInfo, modelComposition);
                target.getConditions().add(modelElementCopy);
            } else {
                List<Condition> mappedToTargetConditions = weavingInfo.getToElements(conditionFromSource);
                System.out.println(" mapped to: " + mappedToTargetConditions);
            }
        }
        
        // now update any references that still refer to the external source model
        UseCaseModelReferenceUpdater updater = UseCaseModelReferenceUpdater.getInstance();
        updater.update(target, weavingInfo);
        
        return target;
    }

    /**
     * This is the main method to weave flows in use cases.
     * @param target The target use case.
     * @param source The source use case.
     * @param weavingInfo The weaving information.
     * @param modelComposition The model composition.
     * @param topdown Flag for top-down or bottom-up weaving.
     */
    public static void weaveFlows(final UseCase target, final UseCase source,
            final WeavingInformation weavingInfo, COREModelComposition modelComposition, final boolean topdown) {
        if (target.getMainSuccessScenario() == null) {
            target.setUseCaseIntention(EcoreUtil.copy(source.getUseCaseIntention()));
            target.setUseCaseMultiplicity(EcoreUtil.copy(source.getUseCaseMultiplicity()));
            
            // Copy all the flows from the source to the target
            for (Flow sourceFlow : UcModelUtil.getFlows(source)) {
                copySourceFlowToTarget(sourceFlow, target, weavingInfo);
            }
            
            return;
        }
        
        HashSet<Step> processedSourceSteps = new HashSet<Step>();
        
        for (Flow sourceFlow : UcModelUtil.getFlows(source)) {
            if (weavingInfo.wasWoven(sourceFlow)) {
                // This source flow was mapped in the composition
                Flow targetFlow = weavingInfo.getFirstToElement(sourceFlow);
                
                Step prevMappedStep = null;
                List<Step> stepsBetweenMapped = new ArrayList<>();
                
                int currentIndex = 0;
                if (topdown) {
                    // For top-down weaving, we want the referenced steps of the target
                    // to be replaced by the source
                    if (!targetFlow.isMainSuccessScenario() ) {
                        targetFlow.getReferencedSteps().clear();
                        targetFlow.getReferencedSteps().addAll(sourceFlow.getReferencedSteps());    
                    }                    
                    
                    // In top-down weaving, we want to iterate through the source flow
                    // since this is where the ordering has been defined
                    int numIterations = sourceFlow.getSteps().size();
                    for (int i = 0; i < numIterations; i++) {
                        Step sourceStep = sourceFlow.getSteps().get(i);
                        
                        // If the source step is an anything step, we skip forward through the target
                        // until we reach an unmapped step
                        if (sourceStep instanceof AnythingStep) {
                            Step currentTargetStep = targetFlow.getSteps().get(currentIndex);
                            Step nextMappedStep = getNextMappedStep(weavingInfo, targetFlow, currentTargetStep);
                            
                            while (currentTargetStep != nextMappedStep && 
                                    currentIndex < targetFlow.getSteps().size() - 1) {
                                currentIndex++;
                                currentTargetStep = targetFlow.getSteps().get(currentIndex);
                            }
                            
                            continue;
                        }
                        
                        if (weavingInfo.wasWoven(sourceStep)) {
                            // This step was mapped, which means we need to replace the version in the target
                            // with the one from the source
                            Step mappedTargetStep = weavingInfo.getFirstToElement(sourceStep);
                            currentIndex = targetFlow.getSteps().indexOf(mappedTargetStep);                            
                            Step newTargetStep = addStepToTarget(weavingInfo, targetFlow, sourceStep, currentIndex);
                            replaceStepReferences(sourceStep, newTargetStep);
                            targetFlow.getSteps().remove(currentIndex+1);                            
                        } else {
                            // This step was not mapped. We just add it to the target at our current position
                            addStepToTarget(weavingInfo, targetFlow, sourceStep, currentIndex);    
                        }
                        
                        processedSourceSteps.add(sourceStep);
                        currentIndex++;
                    }
                } else {
                    int initialTargetSize = targetFlow.getSteps().size();
                    for (int i = 0; i < initialTargetSize; i++) {
                        
                        Step targetStep = targetFlow.getSteps().get(currentIndex);
                        
                        if (targetStep instanceof AnythingStep) {
                            // This pattern matches all of the steps in the source
                            // between the mappings
                            Step nextMappedStep = getNextMappedStep(weavingInfo, targetFlow, targetStep);
                            Step prevMappedSourceStep = prevMappedStep == null ? null : 
                                weavingInfo.getFromElements(prevMappedStep).get(0);
                            Step nextMappedSourceStep = nextMappedStep == null ? null : 
                                weavingInfo.getFromElements(nextMappedStep).get(0);
                            
                            List<Step> sourceSteps = UcModelUtil.getStepsBetween(sourceFlow, prevMappedSourceStep, 
                                    nextMappedSourceStep, false);
                            
                            for (Step s : sourceSteps) {
                                addStepToTarget(weavingInfo, targetFlow, s, currentIndex);
                                processedSourceSteps.add(s);
                                currentIndex++;
                            }
                            
                            targetFlow.getSteps().remove(targetStep);
                            currentIndex++;
                            continue;
                        }
                        
                        List<Step> fromElements = weavingInfo.getFromElements(targetStep);
                        if (fromElements.size() > 0) {
                            // Step was mapped
                            Step mappedSourceStep = fromElements.get(0);
                            List<Step> mappedTargetSteps = weavingInfo.getToElements(mappedSourceStep);
                            
                            if (mappedTargetSteps.size() > 1) {
                                // The source step is mapped to multiple target steps
                                // We mark the source step as processed (so we add its alternates), but we do not add it
                                processedSourceSteps.add(mappedSourceStep);
                            } else if (stepsBetweenMapped.size() == 0) {
                                // If no steps between in the target, we add from the source
                                // If there were, an Anything step is required to inject from the source
                                Step prevMappedSourceStep = prevMappedStep == null ? null : 
                                    weavingInfo.getFromElements(prevMappedStep).get(0);
                                List<Step> sourceSteps = UcModelUtil.getStepsBetween(sourceFlow, prevMappedSourceStep, 
                                        mappedSourceStep, false);
                                
                                for (Step s : sourceSteps) {
                                    addStepToTarget(weavingInfo, targetFlow, s, currentIndex);
                                    processedSourceSteps.add(s);
                                    currentIndex++;
                                }
                            }                        
                            
                            // Set the target step as previously mapped, and clear the steps between
                            prevMappedStep = targetStep;
                            stepsBetweenMapped.clear();
                        } else {
                            // Step was not mapped; add it to the set of steps between mapped steps
                            stepsBetweenMapped.add(targetStep);
                        }
                        
                        currentIndex++;
                    }
                }                
            } else {
                // Unmapped source flows: if a referenced step has been previously processed,
                // then we need to add this flow to the target as well
                boolean shouldBeCopied = false;
                for (Step refStep : sourceFlow.getReferencedSteps()) {
                    if (processedSourceSteps.contains(refStep)) {
                        shouldBeCopied = true;
                        break;
                    }
                }
                
                if (shouldBeCopied) {              
                    copySourceFlowToTarget(sourceFlow, target, weavingInfo);
                }
            }
        }        
    }

    /**
     * Adds a specific step from a source flow to a target flow.
     * @param weavingInfo The weaving info (will be updated to include the new copy).
     * @param targetFlow The target flow.
     * @param s The step to insert.
     * @param index The position to insert at.
     * @return The copy of the step. This is what has been added to the target flow.
     */
    private static Step addStepToTarget(WeavingInformation weavingInfo, Flow targetFlow, Step s, int index) {
        Step copy = EcoreUtil.copy(s);
        if (index >= 0 || index >= targetFlow.getSteps().size()) {
            targetFlow.getSteps().add(index, copy);   
        } else {
            targetFlow.getSteps().add(copy);
        }
        
        if (!weavingInfo.wasWoven(s)) {
            weavingInfo.add(s, copy);    
        }
        
        return copy;
    }
    
    /**
     * Gets the next mapped step in a target flow.
     * @param weavingInfo The weaving information.
     * @param targetFlow The flow to scan.
     * @param currentStep The current step being examined.
     * @return The next mapped step in the flow, or null if there is none.
     */
    private static Step getNextMappedStep(WeavingInformation weavingInfo, Flow targetFlow, Step currentStep) {
        int index = targetFlow.getSteps().indexOf(currentStep);
        index++;
        for (; index < targetFlow.getSteps().size(); index++) {
            Step s = targetFlow.getSteps().get(index);
            List<Step> from = weavingInfo.getFromElements(s);
            if (from.size() > 0) {
               return s;
            }
        }
        
        return null;
    }
    
    /**
     * Copy an entire flow from the source to the target.
     * @param sourceFlow The source flow to copy.
     * @param targetUseCase The target use case.
     * @param weavingInfo The weaving information (will be updated to include the new copies).
     */
    private static void copySourceFlowToTarget(Flow sourceFlow, UseCase targetUseCase,
            WeavingInformation weavingInfo) {
        Flow copy = EcoreUtil.copy(sourceFlow);
        
        if (sourceFlow.isMainSuccessScenario()) {
            targetUseCase.setMainSuccessScenario(copy);
        } else {
            Flow sourceParentFlow = (Flow) sourceFlow.eContainer();
            Flow originalTargetParentFlow = weavingInfo.getFirstToElement(sourceParentFlow);
            Flow targetParentFlow = UcModelUtil.getFlowByName(targetUseCase, originalTargetParentFlow.getName());
            targetParentFlow.getAlternateFlows().add(copy);
        }

        weavingInfo.add(sourceFlow, copy);
        
        for (int i = 0; i < sourceFlow.getSteps().size(); i++) {
            weavingInfo.add(sourceFlow.getSteps().get(i), copy.getSteps().get(i));
        }
    }
    
    /**
     * Replace a step with another. Used in top-down weaving.
     * @param oldStep The old step.
     * @param newStep The new step.
     */
    private static void replaceStepReferences(Step oldStep, Step newStep) {
        List<EObject> referencedObjects = EMFModelUtil.findObjectsThatCrossReference(oldStep);
        
        for (EObject refObject : referencedObjects) {
            // This has to be a flow, according to the definition of the metamodel
            // But just in case...
            if (refObject instanceof Flow) {
                Flow refFlow = (Flow) refObject;
                
                // Check any of the possible links (exclusind containment, as this cannot apply)
                if (Objects.equals(oldStep, refFlow.getConclusionStep())) {
                    refFlow.setConclusionStep(newStep);
                }
                
                List<Step> referencedSteps = refFlow.getReferencedSteps();
                for (int index = 0; index < referencedSteps.size(); index++) {
                    Step referencedStep = referencedSteps.get(index);
                    if (Objects.equals(oldStep, referencedStep)) {
                        referencedSteps.remove(index);
                        if (!referencedSteps.contains(newStep)) {
                            referencedSteps.add(index, newStep);
                        }
                    }
                }
            }
        }
    }
}
