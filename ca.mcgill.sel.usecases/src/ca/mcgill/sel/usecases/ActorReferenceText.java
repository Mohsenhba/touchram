/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor Reference Text</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.ActorReferenceText#getText <em>Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.ActorReferenceText#getActorReferences <em>Actor References</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getActorReferenceText()
 * @model
 * @generated
 */
public interface ActorReferenceText extends EObject {
	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see ca.mcgill.sel.usecases.UcPackage#getActorReferenceText_Text()
	 * @model
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.ActorReferenceText#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

	/**
	 * Returns the value of the '<em><b>Actor References</b></em>' reference list.
	 * The list contents are of type {@link ca.mcgill.sel.usecases.Actor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actor References</em>' reference list.
	 * @see ca.mcgill.sel.usecases.UcPackage#getActorReferenceText_ActorReferences()
	 * @model
	 * @generated
	 */
	EList<Actor> getActorReferences();

} // ActorReferenceText
