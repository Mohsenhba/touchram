/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Anything Step</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getAnythingStep()
 * @model
 * @generated
 */
public interface AnythingStep extends Step {
} // AnythingStep
