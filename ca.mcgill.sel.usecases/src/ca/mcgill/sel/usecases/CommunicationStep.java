/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.CommunicationStep#getDirection <em>Direction</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getCommunicationStep()
 * @model
 * @generated
 */
public interface CommunicationStep extends Step {
    /**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link ca.mcgill.sel.usecases.Direction}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see ca.mcgill.sel.usecases.Direction
	 * @see #setDirection(Direction)
	 * @see ca.mcgill.sel.usecases.UcPackage#getCommunicationStep_Direction()
	 * @model
	 * @generated
	 */
    Direction getDirection();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.CommunicationStep#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see ca.mcgill.sel.usecases.Direction
	 * @see #getDirection()
	 * @generated
	 */
    void setDirection(Direction value);

} // CommunicationStep
