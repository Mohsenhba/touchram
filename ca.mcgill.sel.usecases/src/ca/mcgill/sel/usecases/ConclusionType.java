/**
 */
package ca.mcgill.sel.usecases;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Conclusion Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcPackage#getConclusionType()
 * @model
 * @generated
 */
public enum ConclusionType implements Enumerator {
    /**
	 * The '<em><b>Success</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #SUCCESS_VALUE
	 * @generated
	 * @ordered
	 */
    SUCCESS(0, "Success", "Success"),

    /**
	 * The '<em><b>Failure</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #FAILURE_VALUE
	 * @generated
	 * @ordered
	 */
    FAILURE(1, "Failure", "Failure"),

    /**
	 * The '<em><b>Step</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #STEP_VALUE
	 * @generated
	 * @ordered
	 */
    STEP(2, "Step", "Step"), /**
	 * The '<em><b>Abandon</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #ABANDON_VALUE
	 * @generated
	 * @ordered
	 */
    ABANDON(3, "Abandon", "Abandon"), /**
	 * The '<em><b>Where Interrupted</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #WHERE_INTERRUPTED_VALUE
	 * @generated
	 * @ordered
	 */
    WHERE_INTERRUPTED(4, "WhereInterrupted", "WhereInterrupted");

    /**
	 * The '<em><b>Success</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #SUCCESS
	 * @model name="Success"
	 * @generated
	 * @ordered
	 */
    public static final int SUCCESS_VALUE = 0;

    /**
	 * The '<em><b>Failure</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #FAILURE
	 * @model name="Failure"
	 * @generated
	 * @ordered
	 */
    public static final int FAILURE_VALUE = 1;

    /**
	 * The '<em><b>Step</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #STEP
	 * @model name="Step"
	 * @generated
	 * @ordered
	 */
    public static final int STEP_VALUE = 2;

    /**
	 * The '<em><b>Abandon</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #ABANDON
	 * @model name="Abandon"
	 * @generated
	 * @ordered
	 */
    public static final int ABANDON_VALUE = 3;

    /**
	 * The '<em><b>Where Interrupted</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #WHERE_INTERRUPTED
	 * @model name="WhereInterrupted"
	 * @generated
	 * @ordered
	 */
    public static final int WHERE_INTERRUPTED_VALUE = 4;

    /**
	 * An array of all the '<em><b>Conclusion Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private static final ConclusionType[] VALUES_ARRAY =
        new ConclusionType[] {
			SUCCESS,
			FAILURE,
			STEP,
			ABANDON,
			WHERE_INTERRUPTED,
		};

    /**
	 * A public read-only list of all the '<em><b>Conclusion Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public static final List<ConclusionType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
	 * Returns the '<em><b>Conclusion Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
    public static ConclusionType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ConclusionType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

    /**
	 * Returns the '<em><b>Conclusion Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
    public static ConclusionType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ConclusionType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

    /**
	 * Returns the '<em><b>Conclusion Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
    public static ConclusionType get(int value) {
		switch (value) {
			case SUCCESS_VALUE: return SUCCESS;
			case FAILURE_VALUE: return FAILURE;
			case STEP_VALUE: return STEP;
			case ABANDON_VALUE: return ABANDON;
			case WHERE_INTERRUPTED_VALUE: return WHERE_INTERRUPTED;
		}
		return null;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private final int value;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private final String name;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private final String literal;

    /**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private ConclusionType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public int getValue() {
	  return value;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getName() {
	  return name;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getLiteral() {
	  return literal;
	}

    /**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String toString() {
		return literal;
	}
    
} //ConclusionType
