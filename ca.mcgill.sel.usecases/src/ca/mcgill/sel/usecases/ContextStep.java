/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Context Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.ContextStep#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getContextStep()
 * @model
 * @generated
 */
public interface ContextStep extends Step {
    /**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link ca.mcgill.sel.usecases.ContextType}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see ca.mcgill.sel.usecases.ContextType
	 * @see #setType(ContextType)
	 * @see ca.mcgill.sel.usecases.UcPackage#getContextStep_Type()
	 * @model
	 * @generated
	 */
    ContextType getType();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.ContextStep#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see ca.mcgill.sel.usecases.ContextType
	 * @see #getType()
	 * @generated
	 */
    void setType(ContextType value);

} // ContextStep
