/**
 */
package ca.mcgill.sel.usecases;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Flow Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcPackage#getFlowType()
 * @model
 * @generated
 */
public enum FlowType implements Enumerator {
    /**
	 * The '<em><b>Basic</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #BASIC_VALUE
	 * @generated
	 * @ordered
	 */
    BASIC(0, "Basic", "Basic"),

    /**
	 * The '<em><b>Specific</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #SPECIFIC_VALUE
	 * @generated
	 * @ordered
	 */
    SPECIFIC(1, "Specific", "Specific"),

    /**
	 * The '<em><b>Bounded</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #BOUNDED_VALUE
	 * @generated
	 * @ordered
	 */
    BOUNDED(2, "Bounded", "Bounded"), /**
	 * The '<em><b>Global</b></em>' literal object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #GLOBAL_VALUE
	 * @generated
	 * @ordered
	 */
    GLOBAL(3, "Global", "Global");

    /**
	 * The '<em><b>Basic</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #BASIC
	 * @model name="Basic"
	 * @generated
	 * @ordered
	 */
    public static final int BASIC_VALUE = 0;

    /**
	 * The '<em><b>Specific</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #SPECIFIC
	 * @model name="Specific"
	 * @generated
	 * @ordered
	 */
    public static final int SPECIFIC_VALUE = 1;

    /**
	 * The '<em><b>Bounded</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #BOUNDED
	 * @model name="Bounded"
	 * @generated
	 * @ordered
	 */
    public static final int BOUNDED_VALUE = 2;

    /**
	 * The '<em><b>Global</b></em>' literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #GLOBAL
	 * @model name="Global"
	 * @generated
	 * @ordered
	 */
    public static final int GLOBAL_VALUE = 3;

    /**
	 * An array of all the '<em><b>Flow Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private static final FlowType[] VALUES_ARRAY =
        new FlowType[] {
			BASIC,
			SPECIFIC,
			BOUNDED,
			GLOBAL,
		};

    /**
	 * A public read-only list of all the '<em><b>Flow Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public static final List<FlowType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
	 * Returns the '<em><b>Flow Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
    public static FlowType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FlowType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

    /**
	 * Returns the '<em><b>Flow Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
    public static FlowType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FlowType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

    /**
	 * Returns the '<em><b>Flow Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
    public static FlowType get(int value) {
		switch (value) {
			case BASIC_VALUE: return BASIC;
			case SPECIFIC_VALUE: return SPECIFIC;
			case BOUNDED_VALUE: return BOUNDED;
			case GLOBAL_VALUE: return GLOBAL;
		}
		return null;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private final int value;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private final String name;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private final String literal;

    /**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private FlowType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public int getValue() {
	  return value;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getName() {
	  return name;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getLiteral() {
	  return literal;
	}

    /**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String toString() {
		return literal;
	}
    
} //FlowType
