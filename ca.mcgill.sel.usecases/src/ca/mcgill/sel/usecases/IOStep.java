/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IO Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.IOStep#getMessage <em>Message</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.IOStep#getActor <em>Actor</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.IOStep#getDirection <em>Direction</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getIOStep()
 * @model
 * @generated
 */
public interface IOStep extends Step {
    /**
     * Returns the value of the '<em><b>Message</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Message</em>' attribute.
     * @see #setMessage(String)
     * @see ca.mcgill.sel.usecases.UcPackage#getIOStep_Message()
     * @model
     * @generated
     */
    String getMessage();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.IOStep#getMessage <em>Message</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Message</em>' attribute.
     * @see #getMessage()
     * @generated
     */
    void setMessage(String value);

    /**
     * Returns the value of the '<em><b>Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Actor</em>' reference.
     * @see #setActor(Actor)
     * @see ca.mcgill.sel.usecases.UcPackage#getIOStep_Actor()
     * @model
     * @generated
     */
    Actor getActor();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.IOStep#getActor <em>Actor</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Actor</em>' reference.
     * @see #getActor()
     * @generated
     */
    void setActor(Actor value);

    /**
     * Returns the value of the '<em><b>Direction</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.usecases.Direction}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Direction</em>' attribute.
     * @see ca.mcgill.sel.usecases.Direction
     * @see #setDirection(Direction)
     * @see ca.mcgill.sel.usecases.UcPackage#getIOStep_Direction()
     * @model
     * @generated
     */
    Direction getDirection();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.IOStep#getDirection <em>Direction</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Direction</em>' attribute.
     * @see ca.mcgill.sel.usecases.Direction
     * @see #getDirection()
     * @generated
     */
    void setDirection(Direction value);

} // IOStep
