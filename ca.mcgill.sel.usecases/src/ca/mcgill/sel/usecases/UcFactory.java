/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcPackage
 * @generated
 */
public interface UcFactory extends EFactory {
    /**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    UcFactory eINSTANCE = ca.mcgill.sel.usecases.impl.UcFactoryImpl.init();

    /**
	 * Returns a new object of class '<em>Actor</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Actor</em>'.
	 * @generated
	 */
    Actor createActor();

    /**
	 * Returns a new object of class '<em>Use Case</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Case</em>'.
	 * @generated
	 */
    UseCase createUseCase();

    /**
	 * Returns a new object of class '<em>Layout</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Layout</em>'.
	 * @generated
	 */
    Layout createLayout();

    /**
	 * Returns a new object of class '<em>Layout Element</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Layout Element</em>'.
	 * @generated
	 */
    LayoutElement createLayoutElement();

    /**
	 * Returns a new object of class '<em>Use Case Model</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Case Model</em>'.
	 * @generated
	 */
    UseCaseModel createUseCaseModel();

    /**
	 * Returns a new object of class '<em>Note</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Note</em>'.
	 * @generated
	 */
    Note createNote();

    /**
	 * Returns a new object of class '<em>Flow</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Flow</em>'.
	 * @generated
	 */
    Flow createFlow();

    /**
	 * Returns a new object of class '<em>Communication Step</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Step</em>'.
	 * @generated
	 */
    CommunicationStep createCommunicationStep();

    /**
	 * Returns a new object of class '<em>Use Case Reference Step</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Case Reference Step</em>'.
	 * @generated
	 */
    UseCaseReferenceStep createUseCaseReferenceStep();

    /**
	 * Returns a new object of class '<em>Context Step</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Context Step</em>'.
	 * @generated
	 */
    ContextStep createContextStep();

    /**
	 * Returns a new object of class '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Condition</em>'.
	 * @generated
	 */
    Condition createCondition();

    /**
	 * Returns a new object of class '<em>Extension Point</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extension Point</em>'.
	 * @generated
	 */
    ExtensionPoint createExtensionPoint();

    /**
	 * Returns a new object of class '<em>Actor Mapping</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Actor Mapping</em>'.
	 * @generated
	 */
    ActorMapping createActorMapping();

    /**
	 * Returns a new object of class '<em>Use Case Mapping</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Case Mapping</em>'.
	 * @generated
	 */
    UseCaseMapping createUseCaseMapping();

    /**
	 * Returns a new object of class '<em>Step Mapping</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Step Mapping</em>'.
	 * @generated
	 */
    StepMapping createStepMapping();

    /**
	 * Returns a new object of class '<em>Anything Step</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Anything Step</em>'.
	 * @generated
	 */
    AnythingStep createAnythingStep();

    /**
	 * Returns a new object of class '<em>Mappable Element</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mappable Element</em>'.
	 * @generated
	 */
    MappableElement createMappableElement();

    /**
	 * Returns a new object of class '<em>Flow Mapping</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Flow Mapping</em>'.
	 * @generated
	 */
    FlowMapping createFlowMapping();

    /**
	 * Returns a new object of class '<em>Actor Reference Text</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return a new object of class '<em>Actor Reference Text</em>'.
	 * @generated
	 */
    ActorReferenceText createActorReferenceText();

    /**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
    UcPackage getUcPackage();

} //UcFactory
