/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getPrimaryActors <em>Primary Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getSecondaryActors <em>Secondary Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getIntention <em>Intention</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getLevel <em>Level</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getGeneralization <em>Generalization</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getIncludedUseCases <em>Included Use Cases</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getExtendedUseCase <em>Extended Use Case</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getMainSuccessScenario <em>Main Success Scenario</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getSelectionCondition <em>Selection Condition</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getSelectionConditionType <em>Selection Condition Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getUseCaseIntention <em>Use Case Intention</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getUseCaseMultiplicity <em>Use Case Multiplicity</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase()
 * @model
 * @generated
 */
public interface UseCase extends MappableElement {
    /**
	 * Returns the value of the '<em><b>Primary Actors</b></em>' reference list.
	 * The list contents are of type {@link ca.mcgill.sel.usecases.Actor}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Primary Actors</em>' reference list.
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_PrimaryActors()
	 * @model
	 * @generated
	 */
    EList<Actor> getPrimaryActors();

    /**
	 * Returns the value of the '<em><b>Secondary Actors</b></em>' reference list.
	 * The list contents are of type {@link ca.mcgill.sel.usecases.Actor}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Secondary Actors</em>' reference list.
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_SecondaryActors()
	 * @model
	 * @generated
	 */
    EList<Actor> getSecondaryActors();

    /**
	 * Returns the value of the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Intention</em>' attribute.
	 * @see #setIntention(String)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Intention()
	 * @model
	 * @generated
	 */
    String getIntention();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getIntention <em>Intention</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Intention</em>' attribute.
	 * @see #getIntention()
	 * @generated
	 */
    void setIntention(String value);

    /**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' attribute.
	 * @see #setMultiplicity(String)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Multiplicity()
	 * @model
	 * @generated
	 */
    String getMultiplicity();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getMultiplicity <em>Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' attribute.
	 * @see #getMultiplicity()
	 * @generated
	 */
    void setMultiplicity(String value);

    /**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The default value is <code>"UserGoal"</code>.
	 * The literals are from the enumeration {@link ca.mcgill.sel.usecases.Level}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see ca.mcgill.sel.usecases.Level
	 * @see #setLevel(Level)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Level()
	 * @model default="UserGoal"
	 * @generated
	 */
    Level getLevel();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see ca.mcgill.sel.usecases.Level
	 * @see #getLevel()
	 * @generated
	 */
    void setLevel(Level value);

    /**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Abstract()
	 * @model required="true"
	 * @generated
	 */
    boolean isAbstract();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
    void setAbstract(boolean value);

    /**
	 * Returns the value of the '<em><b>Generalization</b></em>' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Generalization</em>' reference.
	 * @see #setGeneralization(UseCase)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_Generalization()
	 * @model
	 * @generated
	 */
    UseCase getGeneralization();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getGeneralization <em>Generalization</em>}' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generalization</em>' reference.
	 * @see #getGeneralization()
	 * @generated
	 */
    void setGeneralization(UseCase value);

    /**
	 * Returns the value of the '<em><b>Included Use Cases</b></em>' reference list.
	 * The list contents are of type {@link ca.mcgill.sel.usecases.UseCase}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Included Use Cases</em>' reference list.
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_IncludedUseCases()
	 * @model
	 * @generated
	 */
    EList<UseCase> getIncludedUseCases();

    /**
	 * Returns the value of the '<em><b>Extended Use Case</b></em>' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Use Case</em>' reference.
	 * @see #setExtendedUseCase(UseCase)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_ExtendedUseCase()
	 * @model
	 * @generated
	 */
    UseCase getExtendedUseCase();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getExtendedUseCase <em>Extended Use Case</em>}' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extended Use Case</em>' reference.
	 * @see #getExtendedUseCase()
	 * @generated
	 */
    void setExtendedUseCase(UseCase value);

    /**
	 * Returns the value of the '<em><b>Main Success Scenario</b></em>' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Success Scenario</em>' containment reference.
	 * @see #setMainSuccessScenario(Flow)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_MainSuccessScenario()
	 * @model containment="true"
	 * @generated
	 */
    Flow getMainSuccessScenario();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getMainSuccessScenario <em>Main Success Scenario</em>}' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Success Scenario</em>' containment reference.
	 * @see #getMainSuccessScenario()
	 * @generated
	 */
    void setMainSuccessScenario(Flow value);

    /**
	 * Returns the value of the '<em><b>Selection Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection Condition</em>' reference.
	 * @see #setSelectionCondition(Condition)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_SelectionCondition()
	 * @model
	 * @generated
	 */
    Condition getSelectionCondition();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getSelectionCondition <em>Selection Condition</em>}' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection Condition</em>' reference.
	 * @see #getSelectionCondition()
	 * @generated
	 */
    void setSelectionCondition(Condition value);

    /**
	 * Returns the value of the '<em><b>Selection Condition Type</b></em>' attribute.
	 * The literals are from the enumeration {@link ca.mcgill.sel.usecases.SelectionConditionType}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection Condition Type</em>' attribute.
	 * @see ca.mcgill.sel.usecases.SelectionConditionType
	 * @see #setSelectionConditionType(SelectionConditionType)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_SelectionConditionType()
	 * @model
	 * @generated
	 */
    SelectionConditionType getSelectionConditionType();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getSelectionConditionType <em>Selection Condition Type</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection Condition Type</em>' attribute.
	 * @see ca.mcgill.sel.usecases.SelectionConditionType
	 * @see #getSelectionConditionType()
	 * @generated
	 */
    void setSelectionConditionType(SelectionConditionType value);

    /**
	 * Returns the value of the '<em><b>Use Case Intention</b></em>' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Case Intention</em>' containment reference.
	 * @see #setUseCaseIntention(ActorReferenceText)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_UseCaseIntention()
	 * @model containment="true"
	 * @generated
	 */
    ActorReferenceText getUseCaseIntention();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getUseCaseIntention <em>Use Case Intention</em>}' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Case Intention</em>' containment reference.
	 * @see #getUseCaseIntention()
	 * @generated
	 */
    void setUseCaseIntention(ActorReferenceText value);

    /**
	 * Returns the value of the '<em><b>Use Case Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Case Multiplicity</em>' containment reference.
	 * @see #setUseCaseMultiplicity(ActorReferenceText)
	 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_UseCaseMultiplicity()
	 * @model containment="true"
	 * @generated
	 */
    ActorReferenceText getUseCaseMultiplicity();

    /**
	 * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getUseCaseMultiplicity <em>Use Case Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Case Multiplicity</em>' containment reference.
	 * @see #getUseCaseMultiplicity()
	 * @generated
	 */
    void setUseCaseMultiplicity(ActorReferenceText value);

} // UseCase
