/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ActorMapping;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actor Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ActorMappingImpl extends COREMappingImpl<Actor> implements ActorMapping {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActorMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UcPackage.Literals.ACTOR_MAPPING;
	}

} //ActorMappingImpl
