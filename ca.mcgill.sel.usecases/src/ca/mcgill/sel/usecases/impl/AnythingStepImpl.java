/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.AnythingStep;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Anything Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AnythingStepImpl extends StepImpl implements AnythingStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnythingStepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UcPackage.Literals.ANYTHING_STEP;
	}

} //AnythingStepImpl
