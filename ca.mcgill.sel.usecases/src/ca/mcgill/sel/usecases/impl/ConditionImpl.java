/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.ConditionImpl#getText <em>Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.ConditionImpl#getConditionText <em>Condition Text</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionImpl extends MinimalEObjectImpl.Container implements Condition {
    /**
	 * The default value of the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
    protected static final String TEXT_EDEFAULT = "";

    /**
	 * The cached value of the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
    protected String text = TEXT_EDEFAULT;

    /**
	 * The cached value of the '{@link #getConditionText() <em>Condition Text</em>}' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getConditionText()
	 * @generated
	 * @ordered
	 */
    protected ActorReferenceText conditionText;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected ConditionImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return UcPackage.Literals.CONDITION;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getText() {
		return text;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void setText(String newText) {
		String oldText = text;
		text = newText;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.CONDITION__TEXT, oldText, text));
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ActorReferenceText getConditionText() {
		return conditionText;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public NotificationChain basicSetConditionText(ActorReferenceText newConditionText, NotificationChain msgs) {
		ActorReferenceText oldConditionText = conditionText;
		conditionText = newConditionText;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UcPackage.CONDITION__CONDITION_TEXT, oldConditionText, newConditionText);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void setConditionText(ActorReferenceText newConditionText) {
		if (newConditionText != conditionText) {
			NotificationChain msgs = null;
			if (conditionText != null)
				msgs = ((InternalEObject)conditionText).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UcPackage.CONDITION__CONDITION_TEXT, null, msgs);
			if (newConditionText != null)
				msgs = ((InternalEObject)newConditionText).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UcPackage.CONDITION__CONDITION_TEXT, null, msgs);
			msgs = basicSetConditionText(newConditionText, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.CONDITION__CONDITION_TEXT, newConditionText, newConditionText));
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UcPackage.CONDITION__CONDITION_TEXT:
				return basicSetConditionText(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UcPackage.CONDITION__TEXT:
				return getText();
			case UcPackage.CONDITION__CONDITION_TEXT:
				return getConditionText();
		}
		return super.eGet(featureID, resolve, coreType);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UcPackage.CONDITION__TEXT:
				setText((String)newValue);
				return;
			case UcPackage.CONDITION__CONDITION_TEXT:
				setConditionText((ActorReferenceText)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void eUnset(int featureID) {
		switch (featureID) {
			case UcPackage.CONDITION__TEXT:
				setText(TEXT_EDEFAULT);
				return;
			case UcPackage.CONDITION__CONDITION_TEXT:
				setConditionText((ActorReferenceText)null);
				return;
		}
		super.eUnset(featureID);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UcPackage.CONDITION__TEXT:
				return TEXT_EDEFAULT == null ? text != null : !TEXT_EDEFAULT.equals(text);
			case UcPackage.CONDITION__CONDITION_TEXT:
				return conditionText != null;
		}
		return super.eIsSet(featureID);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (text: ");
		result.append(text);
		result.append(')');
		return result.toString();
	}

} //ConditionImpl
