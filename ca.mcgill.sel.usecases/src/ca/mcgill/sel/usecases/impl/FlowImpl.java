/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.ConclusionType;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.FlowType;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flow</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.FlowImpl#getSteps <em>Steps</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.FlowImpl#getAlternateFlows <em>Alternate Flows</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.FlowImpl#getConclusionType <em>Conclusion Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.FlowImpl#getConclusionStep <em>Conclusion Step</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.FlowImpl#getPostCondition <em>Post Condition</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.FlowImpl#getReferencedSteps <em>Referenced Steps</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.FlowImpl#getPreconditionStep <em>Precondition Step</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FlowImpl extends NamedElementImpl implements Flow {
    /**
	 * The cached value of the '{@link #getSteps() <em>Steps</em>}' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getSteps()
	 * @generated
	 * @ordered
	 */
    protected EList<Step> steps;

    /**
	 * The cached value of the '{@link #getAlternateFlows() <em>Alternate Flows</em>}' containment reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getAlternateFlows()
	 * @generated
	 * @ordered
	 */
    protected EList<Flow> alternateFlows;

    /**
	 * The default value of the '{@link #getConclusionType() <em>Conclusion Type</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getConclusionType()
	 * @generated
	 * @ordered
	 */
    protected static final ConclusionType CONCLUSION_TYPE_EDEFAULT = ConclusionType.SUCCESS;

    /**
	 * The cached value of the '{@link #getConclusionType() <em>Conclusion Type</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getConclusionType()
	 * @generated
	 * @ordered
	 */
    protected ConclusionType conclusionType = CONCLUSION_TYPE_EDEFAULT;

    /**
	 * The cached value of the '{@link #getConclusionStep() <em>Conclusion Step</em>}' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getConclusionStep()
	 * @generated
	 * @ordered
	 */
    protected Step conclusionStep;

    /**
	 * The cached value of the '{@link #getPostCondition() <em>Post Condition</em>}' reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getPostCondition()
	 * @generated
	 * @ordered
	 */
    protected Condition postCondition;

    /**
	 * The cached value of the '{@link #getReferencedSteps() <em>Referenced Steps</em>}' reference list.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getReferencedSteps()
	 * @generated
	 * @ordered
	 */
    protected EList<Step> referencedSteps;

    /**
	 * The cached value of the '{@link #getPreconditionStep() <em>Precondition Step</em>}' containment reference.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getPreconditionStep()
	 * @generated
	 * @ordered
	 */
    protected Step preconditionStep;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected FlowImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return UcPackage.Literals.FLOW;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EList<Step> getSteps() {
		if (steps == null) {
			steps = new EObjectContainmentEList<Step>(Step.class, this, UcPackage.FLOW__STEPS);
		}
		return steps;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EList<Flow> getAlternateFlows() {
		if (alternateFlows == null) {
			alternateFlows = new EObjectContainmentEList<Flow>(Flow.class, this, UcPackage.FLOW__ALTERNATE_FLOWS);
		}
		return alternateFlows;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ConclusionType getConclusionType() {
		return conclusionType;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void setConclusionType(ConclusionType newConclusionType) {
		ConclusionType oldConclusionType = conclusionType;
		conclusionType = newConclusionType == null ? CONCLUSION_TYPE_EDEFAULT : newConclusionType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.FLOW__CONCLUSION_TYPE, oldConclusionType, conclusionType));
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Step getConclusionStep() {
		if (conclusionStep != null && conclusionStep.eIsProxy()) {
			InternalEObject oldConclusionStep = (InternalEObject)conclusionStep;
			conclusionStep = (Step)eResolveProxy(oldConclusionStep);
			if (conclusionStep != oldConclusionStep) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.FLOW__CONCLUSION_STEP, oldConclusionStep, conclusionStep));
			}
		}
		return conclusionStep;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public Step basicGetConclusionStep() {
		return conclusionStep;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void setConclusionStep(Step newConclusionStep) {
		Step oldConclusionStep = conclusionStep;
		conclusionStep = newConclusionStep;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.FLOW__CONCLUSION_STEP, oldConclusionStep, conclusionStep));
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Condition getPostCondition() {
		if (postCondition != null && postCondition.eIsProxy()) {
			InternalEObject oldPostCondition = (InternalEObject)postCondition;
			postCondition = (Condition)eResolveProxy(oldPostCondition);
			if (postCondition != oldPostCondition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.FLOW__POST_CONDITION, oldPostCondition, postCondition));
			}
		}
		return postCondition;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public Condition basicGetPostCondition() {
		return postCondition;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void setPostCondition(Condition newPostCondition) {
		Condition oldPostCondition = postCondition;
		postCondition = newPostCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.FLOW__POST_CONDITION, oldPostCondition, postCondition));
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EList<Step> getReferencedSteps() {
		if (referencedSteps == null) {
			referencedSteps = new EObjectResolvingEList<Step>(Step.class, this, UcPackage.FLOW__REFERENCED_STEPS);
		}
		return referencedSteps;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Step getPreconditionStep() {
		return preconditionStep;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public NotificationChain basicSetPreconditionStep(Step newPreconditionStep, NotificationChain msgs) {
		Step oldPreconditionStep = preconditionStep;
		preconditionStep = newPreconditionStep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UcPackage.FLOW__PRECONDITION_STEP, oldPreconditionStep, newPreconditionStep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void setPreconditionStep(Step newPreconditionStep) {
		if (newPreconditionStep != preconditionStep) {
			NotificationChain msgs = null;
			if (preconditionStep != null)
				msgs = ((InternalEObject)preconditionStep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UcPackage.FLOW__PRECONDITION_STEP, null, msgs);
			if (newPreconditionStep != null)
				msgs = ((InternalEObject)newPreconditionStep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UcPackage.FLOW__PRECONDITION_STEP, null, msgs);
			msgs = basicSetPreconditionStep(newPreconditionStep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.FLOW__PRECONDITION_STEP, newPreconditionStep, newPreconditionStep));
	}

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public boolean isMainSuccessScenario() {
        return this.eContainer() instanceof UseCase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public FlowType getFlowType() {
        List<Step> referencedSteps = this.getReferencedSteps();
        if (this.isMainSuccessScenario()) {
            return FlowType.BASIC;  
        } else if (referencedSteps.size() == 1) {
            return FlowType.SPECIFIC;
        } else if (referencedSteps.size() > 1) {
            return FlowType.BOUNDED;
        } else {
            return FlowType.GLOBAL;
        }
    }

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UcPackage.FLOW__STEPS:
				return ((InternalEList<?>)getSteps()).basicRemove(otherEnd, msgs);
			case UcPackage.FLOW__ALTERNATE_FLOWS:
				return ((InternalEList<?>)getAlternateFlows()).basicRemove(otherEnd, msgs);
			case UcPackage.FLOW__PRECONDITION_STEP:
				return basicSetPreconditionStep(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UcPackage.FLOW__STEPS:
				return getSteps();
			case UcPackage.FLOW__ALTERNATE_FLOWS:
				return getAlternateFlows();
			case UcPackage.FLOW__CONCLUSION_TYPE:
				return getConclusionType();
			case UcPackage.FLOW__CONCLUSION_STEP:
				if (resolve) return getConclusionStep();
				return basicGetConclusionStep();
			case UcPackage.FLOW__POST_CONDITION:
				if (resolve) return getPostCondition();
				return basicGetPostCondition();
			case UcPackage.FLOW__REFERENCED_STEPS:
				return getReferencedSteps();
			case UcPackage.FLOW__PRECONDITION_STEP:
				return getPreconditionStep();
		}
		return super.eGet(featureID, resolve, coreType);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UcPackage.FLOW__STEPS:
				getSteps().clear();
				getSteps().addAll((Collection<? extends Step>)newValue);
				return;
			case UcPackage.FLOW__ALTERNATE_FLOWS:
				getAlternateFlows().clear();
				getAlternateFlows().addAll((Collection<? extends Flow>)newValue);
				return;
			case UcPackage.FLOW__CONCLUSION_TYPE:
				setConclusionType((ConclusionType)newValue);
				return;
			case UcPackage.FLOW__CONCLUSION_STEP:
				setConclusionStep((Step)newValue);
				return;
			case UcPackage.FLOW__POST_CONDITION:
				setPostCondition((Condition)newValue);
				return;
			case UcPackage.FLOW__REFERENCED_STEPS:
				getReferencedSteps().clear();
				getReferencedSteps().addAll((Collection<? extends Step>)newValue);
				return;
			case UcPackage.FLOW__PRECONDITION_STEP:
				setPreconditionStep((Step)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void eUnset(int featureID) {
		switch (featureID) {
			case UcPackage.FLOW__STEPS:
				getSteps().clear();
				return;
			case UcPackage.FLOW__ALTERNATE_FLOWS:
				getAlternateFlows().clear();
				return;
			case UcPackage.FLOW__CONCLUSION_TYPE:
				setConclusionType(CONCLUSION_TYPE_EDEFAULT);
				return;
			case UcPackage.FLOW__CONCLUSION_STEP:
				setConclusionStep((Step)null);
				return;
			case UcPackage.FLOW__POST_CONDITION:
				setPostCondition((Condition)null);
				return;
			case UcPackage.FLOW__REFERENCED_STEPS:
				getReferencedSteps().clear();
				return;
			case UcPackage.FLOW__PRECONDITION_STEP:
				setPreconditionStep((Step)null);
				return;
		}
		super.eUnset(featureID);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UcPackage.FLOW__STEPS:
				return steps != null && !steps.isEmpty();
			case UcPackage.FLOW__ALTERNATE_FLOWS:
				return alternateFlows != null && !alternateFlows.isEmpty();
			case UcPackage.FLOW__CONCLUSION_TYPE:
				return conclusionType != CONCLUSION_TYPE_EDEFAULT;
			case UcPackage.FLOW__CONCLUSION_STEP:
				return conclusionStep != null;
			case UcPackage.FLOW__POST_CONDITION:
				return postCondition != null;
			case UcPackage.FLOW__REFERENCED_STEPS:
				return referencedSteps != null && !referencedSteps.isEmpty();
			case UcPackage.FLOW__PRECONDITION_STEP:
				return preconditionStep != null;
		}
		return super.eIsSet(featureID);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (conclusionType: ");
		result.append(conclusionType);
		result.append(')');
		return result.toString();
	}

} //FlowImpl
