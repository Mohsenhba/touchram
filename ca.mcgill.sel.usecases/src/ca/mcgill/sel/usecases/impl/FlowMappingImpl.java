/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flow Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FlowMappingImpl extends COREMappingImpl<Flow> implements FlowMapping {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FlowMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UcPackage.Literals.FLOW_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<StepMapping> getStepMappings() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

} //FlowMappingImpl
