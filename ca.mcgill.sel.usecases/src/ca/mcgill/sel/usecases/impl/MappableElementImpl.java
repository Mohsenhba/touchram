/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.core.COREPartialityType;

import ca.mcgill.sel.usecases.MappableElement;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mappable Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.MappableElementImpl#getPartiality <em>Partiality</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MappableElementImpl extends NamedElementImpl implements MappableElement {
	/**
	 * The default value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartiality()
	 * @generated
	 * @ordered
	 */
	protected static final COREPartialityType PARTIALITY_EDEFAULT = COREPartialityType.NONE;

	/**
	 * The cached value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartiality()
	 * @generated
	 * @ordered
	 */
	protected COREPartialityType partiality = PARTIALITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MappableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UcPackage.Literals.MAPPABLE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COREPartialityType getPartiality() {
		return partiality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPartiality(COREPartialityType newPartiality) {
		COREPartialityType oldPartiality = partiality;
		partiality = newPartiality == null ? PARTIALITY_EDEFAULT : newPartiality;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.MAPPABLE_ELEMENT__PARTIALITY, oldPartiality, partiality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UcPackage.MAPPABLE_ELEMENT__PARTIALITY:
				return getPartiality();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UcPackage.MAPPABLE_ELEMENT__PARTIALITY:
				setPartiality((COREPartialityType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UcPackage.MAPPABLE_ELEMENT__PARTIALITY:
				setPartiality(PARTIALITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UcPackage.MAPPABLE_ELEMENT__PARTIALITY:
				return partiality != PARTIALITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (partiality: ");
		result.append(partiality);
		result.append(')');
		return result.toString();
	}

} //MappableElementImpl
