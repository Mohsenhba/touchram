/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.*;

import java.util.Map;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UcFactoryImpl extends EFactoryImpl implements UcFactory {
    /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public static UcFactory init() {
		try {
			UcFactory theUcFactory = (UcFactory)EPackage.Registry.INSTANCE.getEFactory(UcPackage.eNS_URI);
			if (theUcFactory != null) {
				return theUcFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UcFactoryImpl();
	}

    /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public UcFactoryImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UcPackage.ACTOR: return createActor();
			case UcPackage.USE_CASE: return createUseCase();
			case UcPackage.LAYOUT: return createLayout();
			case UcPackage.CONTAINER_MAP: return (EObject)createContainerMap();
			case UcPackage.ELEMENT_MAP: return (EObject)createElementMap();
			case UcPackage.LAYOUT_ELEMENT: return createLayoutElement();
			case UcPackage.USE_CASE_MODEL: return createUseCaseModel();
			case UcPackage.NOTE: return createNote();
			case UcPackage.FLOW: return createFlow();
			case UcPackage.COMMUNICATION_STEP: return createCommunicationStep();
			case UcPackage.USE_CASE_REFERENCE_STEP: return createUseCaseReferenceStep();
			case UcPackage.CONTEXT_STEP: return createContextStep();
			case UcPackage.CONDITION: return createCondition();
			case UcPackage.EXTENSION_POINT: return createExtensionPoint();
			case UcPackage.ACTOR_MAPPING: return createActorMapping();
			case UcPackage.USE_CASE_MAPPING: return createUseCaseMapping();
			case UcPackage.STEP_MAPPING: return createStepMapping();
			case UcPackage.ANYTHING_STEP: return createAnythingStep();
			case UcPackage.MAPPABLE_ELEMENT: return createMappableElement();
			case UcPackage.FLOW_MAPPING: return createFlowMapping();
			case UcPackage.ACTOR_REFERENCE_TEXT: return createActorReferenceText();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case UcPackage.LEVEL:
				return createLevelFromString(eDataType, initialValue);
			case UcPackage.DIRECTION:
				return createDirectionFromString(eDataType, initialValue);
			case UcPackage.CONTEXT_TYPE:
				return createContextTypeFromString(eDataType, initialValue);
			case UcPackage.CONCLUSION_TYPE:
				return createConclusionTypeFromString(eDataType, initialValue);
			case UcPackage.FLOW_TYPE:
				return createFlowTypeFromString(eDataType, initialValue);
			case UcPackage.SELECTION_CONDITION_TYPE:
				return createSelectionConditionTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case UcPackage.LEVEL:
				return convertLevelToString(eDataType, instanceValue);
			case UcPackage.DIRECTION:
				return convertDirectionToString(eDataType, instanceValue);
			case UcPackage.CONTEXT_TYPE:
				return convertContextTypeToString(eDataType, instanceValue);
			case UcPackage.CONCLUSION_TYPE:
				return convertConclusionTypeToString(eDataType, instanceValue);
			case UcPackage.FLOW_TYPE:
				return convertFlowTypeToString(eDataType, instanceValue);
			case UcPackage.SELECTION_CONDITION_TYPE:
				return convertSelectionConditionTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Actor createActor() {
		ActorImpl actor = new ActorImpl();
		return actor;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public UseCase createUseCase() {
		UseCaseImpl useCase = new UseCaseImpl();
		return useCase;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Layout createLayout() {
		LayoutImpl layout = new LayoutImpl();
		return layout;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public Map.Entry<EObject, EMap<EObject, LayoutElement>> createContainerMap() {
		ContainerMapImpl containerMap = new ContainerMapImpl();
		return containerMap;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public Map.Entry<EObject, LayoutElement> createElementMap() {
		ElementMapImpl elementMap = new ElementMapImpl();
		return elementMap;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public LayoutElement createLayoutElement() {
		LayoutElementImpl layoutElement = new LayoutElementImpl();
		return layoutElement;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public UseCaseModel createUseCaseModel() {
		UseCaseModelImpl useCaseModel = new UseCaseModelImpl();
		return useCaseModel;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Note createNote() {
		NoteImpl note = new NoteImpl();
		return note;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Flow createFlow() {
		FlowImpl flow = new FlowImpl();
		return flow;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public CommunicationStep createCommunicationStep() {
		CommunicationStepImpl communicationStep = new CommunicationStepImpl();
		return communicationStep;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public UseCaseReferenceStep createUseCaseReferenceStep() {
		UseCaseReferenceStepImpl useCaseReferenceStep = new UseCaseReferenceStepImpl();
		return useCaseReferenceStep;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ContextStep createContextStep() {
		ContextStepImpl contextStep = new ContextStepImpl();
		return contextStep;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Condition createCondition() {
		ConditionImpl condition = new ConditionImpl();
		return condition;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ExtensionPoint createExtensionPoint() {
		ExtensionPointImpl extensionPoint = new ExtensionPointImpl();
		return extensionPoint;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ActorMapping createActorMapping() {
		ActorMappingImpl actorMapping = new ActorMappingImpl();
		return actorMapping;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public UseCaseMapping createUseCaseMapping() {
		UseCaseMappingImpl useCaseMapping = new UseCaseMappingImpl();
		return useCaseMapping;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public StepMapping createStepMapping() {
		StepMappingImpl stepMapping = new StepMappingImpl();
		return stepMapping;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public AnythingStep createAnythingStep() {
		AnythingStepImpl anythingStep = new AnythingStepImpl();
		return anythingStep;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public MappableElement createMappableElement() {
		MappableElementImpl mappableElement = new MappableElementImpl();
		return mappableElement;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public FlowMapping createFlowMapping() {
		FlowMappingImpl flowMapping = new FlowMappingImpl();
		return flowMapping;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ActorReferenceText createActorReferenceText() {
		ActorReferenceTextImpl actorReferenceText = new ActorReferenceTextImpl();
		return actorReferenceText;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public Level createLevelFromString(EDataType eDataType, String initialValue) {
		Level result = Level.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public String convertLevelToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public Direction createDirectionFromString(EDataType eDataType, String initialValue) {
		Direction result = Direction.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public String convertDirectionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public ContextType createContextTypeFromString(EDataType eDataType, String initialValue) {
		ContextType result = ContextType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public String convertContextTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public ConclusionType createConclusionTypeFromString(EDataType eDataType, String initialValue) {
		ConclusionType result = ConclusionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public String convertConclusionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public FlowType createFlowTypeFromString(EDataType eDataType, String initialValue) {
		FlowType result = FlowType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public String convertFlowTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public SelectionConditionType createSelectionConditionTypeFromString(EDataType eDataType, String initialValue) {
		SelectionConditionType result = SelectionConditionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public String convertSelectionConditionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public UcPackage getUcPackage() {
		return (UcPackage)getEPackage();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
    @Deprecated
    public static UcPackage getPackage() {
		return UcPackage.eINSTANCE;
	}

} //UcFactoryImpl
