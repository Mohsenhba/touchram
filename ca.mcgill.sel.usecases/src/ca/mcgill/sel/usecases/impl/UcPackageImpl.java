/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ActorMapping;
import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.AnythingStep;
import ca.mcgill.sel.usecases.CommunicationStep;
import ca.mcgill.sel.usecases.ConclusionType;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.ContextStep;
import ca.mcgill.sel.usecases.ContextType;
import ca.mcgill.sel.usecases.Direction;
import ca.mcgill.sel.usecases.ExtensionPoint;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.FlowType;
import ca.mcgill.sel.usecases.Layout;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.Level;
import ca.mcgill.sel.usecases.MappableElement;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.Note;
import ca.mcgill.sel.usecases.SelectionConditionType;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseMapping;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;
import ca.mcgill.sel.usecases.util.UcValidator;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UcPackageImpl extends EPackageImpl implements UcPackage {
    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass actorEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass namedElementEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass useCaseEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass layoutEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass containerMapEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass elementMapEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass layoutElementEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass useCaseModelEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass noteEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass stepEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass flowEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass communicationStepEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass useCaseReferenceStepEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass contextStepEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass conditionEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass extensionPointEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass actorMappingEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass useCaseMappingEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass stepMappingEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass anythingStepEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass mappableElementEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass flowMappingEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EClass actorReferenceTextEClass = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EEnum levelEEnum = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EEnum directionEEnum = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EEnum contextTypeEEnum = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EEnum conclusionTypeEEnum = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EEnum flowTypeEEnum = null;

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private EEnum selectionConditionTypeEEnum = null;

    /**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.mcgill.sel.usecases.UcPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
    private UcPackageImpl() {
		super(eNS_URI, UcFactory.eINSTANCE);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private static boolean isInited = false;

    /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link UcPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
    public static UcPackage init() {
		if (isInited) return (UcPackage)EPackage.Registry.INSTANCE.getEPackage(UcPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredUcPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		UcPackageImpl theUcPackage = registeredUcPackage instanceof UcPackageImpl ? (UcPackageImpl)registeredUcPackage : new UcPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theUcPackage.createPackageContents();

		// Initialize created meta-data
		theUcPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theUcPackage,
			 new EValidator.Descriptor() {
				 @Override
				 public EValidator getEValidator() {
					 return UcValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theUcPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UcPackage.eNS_URI, theUcPackage);
		return theUcPackage;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getActor() {
		return actorEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getActor_LowerBound() {
		return (EAttribute)actorEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getActor_UpperBound() {
		return (EAttribute)actorEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getActor_Generalization() {
		return (EReference)actorEClass.getEStructuralFeatures().get(2);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getActor_Abstract() {
		return (EAttribute)actorEClass.getEStructuralFeatures().get(3);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getNamedElement() {
		return namedElementEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getUseCase() {
		return useCaseEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_PrimaryActors() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_SecondaryActors() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getUseCase_Intention() {
		return (EAttribute)useCaseEClass.getEStructuralFeatures().get(2);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getUseCase_Multiplicity() {
		return (EAttribute)useCaseEClass.getEStructuralFeatures().get(3);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getUseCase_Level() {
		return (EAttribute)useCaseEClass.getEStructuralFeatures().get(4);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getUseCase_Abstract() {
		return (EAttribute)useCaseEClass.getEStructuralFeatures().get(5);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_Generalization() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(6);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_IncludedUseCases() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(7);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_ExtendedUseCase() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(8);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_MainSuccessScenario() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(9);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_SelectionCondition() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(10);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getUseCase_SelectionConditionType() {
		return (EAttribute)useCaseEClass.getEStructuralFeatures().get(11);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_UseCaseIntention() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(12);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCase_UseCaseMultiplicity() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(13);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getLayout() {
		return layoutEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getLayout_Containers() {
		return (EReference)layoutEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getContainerMap() {
		return containerMapEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getContainerMap_Key() {
		return (EReference)containerMapEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getContainerMap_Value() {
		return (EReference)containerMapEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getElementMap() {
		return elementMapEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getElementMap_Key() {
		return (EReference)elementMapEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getElementMap_Value() {
		return (EReference)elementMapEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getLayoutElement() {
		return layoutElementEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getLayoutElement_X() {
		return (EAttribute)layoutElementEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getLayoutElement_Y() {
		return (EAttribute)layoutElementEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getUseCaseModel() {
		return useCaseModelEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCaseModel_Layout() {
		return (EReference)useCaseModelEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCaseModel_Actors() {
		return (EReference)useCaseModelEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCaseModel_UseCases() {
		return (EReference)useCaseModelEClass.getEStructuralFeatures().get(2);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCaseModel_Notes() {
		return (EReference)useCaseModelEClass.getEStructuralFeatures().get(3);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCaseModel_Conditions() {
		return (EReference)useCaseModelEClass.getEStructuralFeatures().get(4);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getUseCaseModel_SystemName() {
		return (EAttribute)useCaseModelEClass.getEStructuralFeatures().get(5);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getNote() {
		return noteEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getNote_NotedElement() {
		return (EReference)noteEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getNote_Content() {
		return (EAttribute)noteEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getStep() {
		return stepEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getStep_StepText() {
		return (EAttribute)stepEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getStep_StepDescription() {
		return (EReference)stepEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getFlow() {
		return flowEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getFlow_Steps() {
		return (EReference)flowEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getFlow_AlternateFlows() {
		return (EReference)flowEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getFlow_ConclusionType() {
		return (EAttribute)flowEClass.getEStructuralFeatures().get(2);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getFlow_ConclusionStep() {
		return (EReference)flowEClass.getEStructuralFeatures().get(3);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getFlow_PostCondition() {
		return (EReference)flowEClass.getEStructuralFeatures().get(4);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getFlow_ReferencedSteps() {
		return (EReference)flowEClass.getEStructuralFeatures().get(5);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getFlow_PreconditionStep() {
		return (EReference)flowEClass.getEStructuralFeatures().get(6);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getCommunicationStep() {
		return communicationStepEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getCommunicationStep_Direction() {
		return (EAttribute)communicationStepEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getUseCaseReferenceStep() {
		return useCaseReferenceStepEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getUseCaseReferenceStep_Usecase() {
		return (EReference)useCaseReferenceStepEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getContextStep() {
		return contextStepEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getContextStep_Type() {
		return (EAttribute)contextStepEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getCondition() {
		return conditionEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getCondition_Text() {
		return (EAttribute)conditionEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getCondition_ConditionText() {
		return (EReference)conditionEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getExtensionPoint() {
		return extensionPointEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getActorMapping() {
		return actorMappingEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getUseCaseMapping() {
		return useCaseMappingEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getStepMapping() {
		return stepMappingEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getAnythingStep() {
		return anythingStepEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getMappableElement() {
		return mappableElementEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getMappableElement_Partiality() {
		return (EAttribute)mappableElementEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getFlowMapping() {
		return flowMappingEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EClass getActorReferenceText() {
		return actorReferenceTextEClass;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EAttribute getActorReferenceText_Text() {
		return (EAttribute)actorReferenceTextEClass.getEStructuralFeatures().get(0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EReference getActorReferenceText_ActorReferences() {
		return (EReference)actorReferenceTextEClass.getEStructuralFeatures().get(1);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EEnum getLevel() {
		return levelEEnum;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EEnum getDirection() {
		return directionEEnum;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EEnum getContextType() {
		return contextTypeEEnum;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EEnum getConclusionType() {
		return conclusionTypeEEnum;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EEnum getFlowType() {
		return flowTypeEEnum;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public EEnum getSelectionConditionType() {
		return selectionConditionTypeEEnum;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public UcFactory getUcFactory() {
		return (UcFactory)getEFactoryInstance();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private boolean isCreated = false;

    /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		actorEClass = createEClass(ACTOR);
		createEAttribute(actorEClass, ACTOR__LOWER_BOUND);
		createEAttribute(actorEClass, ACTOR__UPPER_BOUND);
		createEReference(actorEClass, ACTOR__GENERALIZATION);
		createEAttribute(actorEClass, ACTOR__ABSTRACT);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		useCaseEClass = createEClass(USE_CASE);
		createEReference(useCaseEClass, USE_CASE__PRIMARY_ACTORS);
		createEReference(useCaseEClass, USE_CASE__SECONDARY_ACTORS);
		createEAttribute(useCaseEClass, USE_CASE__INTENTION);
		createEAttribute(useCaseEClass, USE_CASE__MULTIPLICITY);
		createEAttribute(useCaseEClass, USE_CASE__LEVEL);
		createEAttribute(useCaseEClass, USE_CASE__ABSTRACT);
		createEReference(useCaseEClass, USE_CASE__GENERALIZATION);
		createEReference(useCaseEClass, USE_CASE__INCLUDED_USE_CASES);
		createEReference(useCaseEClass, USE_CASE__EXTENDED_USE_CASE);
		createEReference(useCaseEClass, USE_CASE__MAIN_SUCCESS_SCENARIO);
		createEReference(useCaseEClass, USE_CASE__SELECTION_CONDITION);
		createEAttribute(useCaseEClass, USE_CASE__SELECTION_CONDITION_TYPE);
		createEReference(useCaseEClass, USE_CASE__USE_CASE_INTENTION);
		createEReference(useCaseEClass, USE_CASE__USE_CASE_MULTIPLICITY);

		layoutEClass = createEClass(LAYOUT);
		createEReference(layoutEClass, LAYOUT__CONTAINERS);

		containerMapEClass = createEClass(CONTAINER_MAP);
		createEReference(containerMapEClass, CONTAINER_MAP__KEY);
		createEReference(containerMapEClass, CONTAINER_MAP__VALUE);

		elementMapEClass = createEClass(ELEMENT_MAP);
		createEReference(elementMapEClass, ELEMENT_MAP__KEY);
		createEReference(elementMapEClass, ELEMENT_MAP__VALUE);

		layoutElementEClass = createEClass(LAYOUT_ELEMENT);
		createEAttribute(layoutElementEClass, LAYOUT_ELEMENT__X);
		createEAttribute(layoutElementEClass, LAYOUT_ELEMENT__Y);

		useCaseModelEClass = createEClass(USE_CASE_MODEL);
		createEReference(useCaseModelEClass, USE_CASE_MODEL__LAYOUT);
		createEReference(useCaseModelEClass, USE_CASE_MODEL__ACTORS);
		createEReference(useCaseModelEClass, USE_CASE_MODEL__USE_CASES);
		createEReference(useCaseModelEClass, USE_CASE_MODEL__NOTES);
		createEReference(useCaseModelEClass, USE_CASE_MODEL__CONDITIONS);
		createEAttribute(useCaseModelEClass, USE_CASE_MODEL__SYSTEM_NAME);

		noteEClass = createEClass(NOTE);
		createEReference(noteEClass, NOTE__NOTED_ELEMENT);
		createEAttribute(noteEClass, NOTE__CONTENT);

		stepEClass = createEClass(STEP);
		createEAttribute(stepEClass, STEP__STEP_TEXT);
		createEReference(stepEClass, STEP__STEP_DESCRIPTION);

		flowEClass = createEClass(FLOW);
		createEReference(flowEClass, FLOW__STEPS);
		createEReference(flowEClass, FLOW__ALTERNATE_FLOWS);
		createEAttribute(flowEClass, FLOW__CONCLUSION_TYPE);
		createEReference(flowEClass, FLOW__CONCLUSION_STEP);
		createEReference(flowEClass, FLOW__POST_CONDITION);
		createEReference(flowEClass, FLOW__REFERENCED_STEPS);
		createEReference(flowEClass, FLOW__PRECONDITION_STEP);

		communicationStepEClass = createEClass(COMMUNICATION_STEP);
		createEAttribute(communicationStepEClass, COMMUNICATION_STEP__DIRECTION);

		useCaseReferenceStepEClass = createEClass(USE_CASE_REFERENCE_STEP);
		createEReference(useCaseReferenceStepEClass, USE_CASE_REFERENCE_STEP__USECASE);

		contextStepEClass = createEClass(CONTEXT_STEP);
		createEAttribute(contextStepEClass, CONTEXT_STEP__TYPE);

		conditionEClass = createEClass(CONDITION);
		createEAttribute(conditionEClass, CONDITION__TEXT);
		createEReference(conditionEClass, CONDITION__CONDITION_TEXT);

		extensionPointEClass = createEClass(EXTENSION_POINT);

		actorMappingEClass = createEClass(ACTOR_MAPPING);

		useCaseMappingEClass = createEClass(USE_CASE_MAPPING);

		stepMappingEClass = createEClass(STEP_MAPPING);

		anythingStepEClass = createEClass(ANYTHING_STEP);

		mappableElementEClass = createEClass(MAPPABLE_ELEMENT);
		createEAttribute(mappableElementEClass, MAPPABLE_ELEMENT__PARTIALITY);

		flowMappingEClass = createEClass(FLOW_MAPPING);

		actorReferenceTextEClass = createEClass(ACTOR_REFERENCE_TEXT);
		createEAttribute(actorReferenceTextEClass, ACTOR_REFERENCE_TEXT__TEXT);
		createEReference(actorReferenceTextEClass, ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES);

		// Create enums
		levelEEnum = createEEnum(LEVEL);
		directionEEnum = createEEnum(DIRECTION);
		contextTypeEEnum = createEEnum(CONTEXT_TYPE);
		conclusionTypeEEnum = createEEnum(CONCLUSION_TYPE);
		flowTypeEEnum = createEEnum(FLOW_TYPE);
		selectionConditionTypeEEnum = createEEnum(SELECTION_CONDITION_TYPE);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private boolean isInitialized = false;

    /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		actorEClass.getESuperTypes().add(this.getMappableElement());
		useCaseEClass.getESuperTypes().add(this.getMappableElement());
		useCaseModelEClass.getESuperTypes().add(this.getNamedElement());
		flowEClass.getESuperTypes().add(this.getNamedElement());
		communicationStepEClass.getESuperTypes().add(this.getStep());
		useCaseReferenceStepEClass.getESuperTypes().add(this.getStep());
		contextStepEClass.getESuperTypes().add(this.getStep());
		extensionPointEClass.getESuperTypes().add(this.getStep());
		EGenericType g1 = createEGenericType(theCorePackage.getCOREMapping());
		EGenericType g2 = createEGenericType(this.getActor());
		g1.getETypeArguments().add(g2);
		actorMappingEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(theCorePackage.getCOREMapping());
		g2 = createEGenericType(this.getUseCase());
		g1.getETypeArguments().add(g2);
		useCaseMappingEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(theCorePackage.getCOREMapping());
		g2 = createEGenericType(this.getStep());
		g1.getETypeArguments().add(g2);
		stepMappingEClass.getEGenericSuperTypes().add(g1);
		anythingStepEClass.getESuperTypes().add(this.getStep());
		mappableElementEClass.getESuperTypes().add(this.getNamedElement());
		g1 = createEGenericType(theCorePackage.getCOREMapping());
		g2 = createEGenericType(this.getFlow());
		g1.getETypeArguments().add(g2);
		flowMappingEClass.getEGenericSuperTypes().add(g1);

		// Initialize classes and features; add operations and parameters
		initEClass(actorEClass, Actor.class, "Actor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActor_LowerBound(), ecorePackage.getEInt(), "lowerBound", "1", 0, 1, Actor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActor_UpperBound(), ecorePackage.getEInt(), "upperBound", "-1", 0, 1, Actor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActor_Generalization(), this.getActor(), null, "generalization", null, 0, 1, Actor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActor_Abstract(), ecorePackage.getEBoolean(), "abstract", "false", 1, 1, Actor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(useCaseEClass, UseCase.class, "UseCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUseCase_PrimaryActors(), this.getActor(), null, "primaryActors", null, 0, -1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_SecondaryActors(), this.getActor(), null, "secondaryActors", null, 0, -1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCase_Intention(), ecorePackage.getEString(), "intention", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCase_Multiplicity(), ecorePackage.getEString(), "multiplicity", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCase_Level(), this.getLevel(), "level", "UserGoal", 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCase_Abstract(), ecorePackage.getEBoolean(), "abstract", null, 1, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_Generalization(), this.getUseCase(), null, "generalization", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_IncludedUseCases(), this.getUseCase(), null, "includedUseCases", null, 0, -1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_ExtendedUseCase(), this.getUseCase(), null, "extendedUseCase", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_MainSuccessScenario(), this.getFlow(), null, "mainSuccessScenario", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_SelectionCondition(), this.getCondition(), null, "selectionCondition", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCase_SelectionConditionType(), this.getSelectionConditionType(), "selectionConditionType", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_UseCaseIntention(), this.getActorReferenceText(), null, "useCaseIntention", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_UseCaseMultiplicity(), this.getActorReferenceText(), null, "useCaseMultiplicity", null, 0, 1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(layoutEClass, Layout.class, "Layout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLayout_Containers(), this.getContainerMap(), null, "containers", null, 1, -1, Layout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(containerMapEClass, Map.Entry.class, "ContainerMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContainerMap_Key(), ecorePackage.getEObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContainerMap_Value(), this.getElementMap(), null, "value", null, 1, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(elementMapEClass, Map.Entry.class, "ElementMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getElementMap_Key(), ecorePackage.getEObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementMap_Value(), this.getLayoutElement(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(layoutElementEClass, LayoutElement.class, "LayoutElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLayoutElement_X(), ecorePackage.getEFloat(), "x", "0.0", 0, 1, LayoutElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLayoutElement_Y(), ecorePackage.getEFloat(), "y", null, 0, 1, LayoutElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(useCaseModelEClass, UseCaseModel.class, "UseCaseModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUseCaseModel_Layout(), this.getLayout(), null, "layout", null, 0, 1, UseCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCaseModel_Actors(), this.getActor(), null, "actors", null, 0, -1, UseCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCaseModel_UseCases(), this.getUseCase(), null, "useCases", null, 0, -1, UseCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCaseModel_Notes(), this.getNote(), null, "notes", null, 0, -1, UseCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCaseModel_Conditions(), this.getCondition(), null, "conditions", null, 0, -1, UseCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCaseModel_SystemName(), ecorePackage.getEString(), "systemName", null, 0, 1, UseCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(noteEClass, Note.class, "Note", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNote_NotedElement(), this.getNamedElement(), null, "notedElement", null, 0, -1, Note.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNote_Content(), ecorePackage.getEString(), "content", null, 0, 1, Note.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stepEClass, Step.class, "Step", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStep_StepText(), ecorePackage.getEString(), "stepText", "", 0, 1, Step.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStep_StepDescription(), this.getActorReferenceText(), null, "stepDescription", null, 1, 1, Step.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(flowEClass, Flow.class, "Flow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFlow_Steps(), this.getStep(), null, "steps", null, 0, -1, Flow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFlow_AlternateFlows(), this.getFlow(), null, "alternateFlows", null, 0, -1, Flow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFlow_ConclusionType(), this.getConclusionType(), "conclusionType", null, 0, 1, Flow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFlow_ConclusionStep(), this.getStep(), null, "conclusionStep", null, 0, 1, Flow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFlow_PostCondition(), this.getCondition(), null, "postCondition", null, 0, 1, Flow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFlow_ReferencedSteps(), this.getStep(), null, "referencedSteps", null, 0, -1, Flow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFlow_PreconditionStep(), this.getStep(), null, "preconditionStep", null, 0, 1, Flow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(flowEClass, ecorePackage.getEBoolean(), "isMainSuccessScenario", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(flowEClass, this.getFlowType(), "getFlowType", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(communicationStepEClass, CommunicationStep.class, "CommunicationStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCommunicationStep_Direction(), this.getDirection(), "direction", null, 0, 1, CommunicationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(useCaseReferenceStepEClass, UseCaseReferenceStep.class, "UseCaseReferenceStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUseCaseReferenceStep_Usecase(), this.getUseCase(), null, "usecase", null, 1, 1, UseCaseReferenceStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(contextStepEClass, ContextStep.class, "ContextStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getContextStep_Type(), this.getContextType(), "type", null, 0, 1, ContextStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionEClass, Condition.class, "Condition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCondition_Text(), ecorePackage.getEString(), "text", "", 0, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCondition_ConditionText(), this.getActorReferenceText(), null, "conditionText", null, 1, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(extensionPointEClass, ExtensionPoint.class, "ExtensionPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(actorMappingEClass, ActorMapping.class, "ActorMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(useCaseMappingEClass, UseCaseMapping.class, "UseCaseMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(useCaseMappingEClass, this.getFlowMapping(), "getFlowMappings", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(stepMappingEClass, StepMapping.class, "StepMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(anythingStepEClass, AnythingStep.class, "AnythingStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mappableElementEClass, MappableElement.class, "MappableElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMappableElement_Partiality(), theCorePackage.getCOREPartialityType(), "partiality", null, 0, 1, MappableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(flowMappingEClass, FlowMapping.class, "FlowMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(flowMappingEClass, this.getStepMapping(), "getStepMappings", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(actorReferenceTextEClass, ActorReferenceText.class, "ActorReferenceText", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActorReferenceText_Text(), ecorePackage.getEString(), "text", null, 0, 1, ActorReferenceText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActorReferenceText_ActorReferences(), this.getActor(), null, "actorReferences", null, 0, -1, ActorReferenceText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(levelEEnum, Level.class, "Level");
		addEEnumLiteral(levelEEnum, Level.SUMMARY);
		addEEnumLiteral(levelEEnum, Level.USER_GOAL);
		addEEnumLiteral(levelEEnum, Level.SUBFUNCTION);

		initEEnum(directionEEnum, Direction.class, "Direction");
		addEEnumLiteral(directionEEnum, Direction.INPUT);
		addEEnumLiteral(directionEEnum, Direction.OUTPUT);

		initEEnum(contextTypeEEnum, ContextType.class, "ContextType");
		addEEnumLiteral(contextTypeEEnum, ContextType.INTERNAL);
		addEEnumLiteral(contextTypeEEnum, ContextType.EXTERNAL);
		addEEnumLiteral(contextTypeEEnum, ContextType.CONTROL_FLOW);
		addEEnumLiteral(contextTypeEEnum, ContextType.TIMEOUT);

		initEEnum(conclusionTypeEEnum, ConclusionType.class, "ConclusionType");
		addEEnumLiteral(conclusionTypeEEnum, ConclusionType.SUCCESS);
		addEEnumLiteral(conclusionTypeEEnum, ConclusionType.FAILURE);
		addEEnumLiteral(conclusionTypeEEnum, ConclusionType.STEP);
		addEEnumLiteral(conclusionTypeEEnum, ConclusionType.ABANDON);
		addEEnumLiteral(conclusionTypeEEnum, ConclusionType.WHERE_INTERRUPTED);

		initEEnum(flowTypeEEnum, FlowType.class, "FlowType");
		addEEnumLiteral(flowTypeEEnum, FlowType.BASIC);
		addEEnumLiteral(flowTypeEEnum, FlowType.SPECIFIC);
		addEEnumLiteral(flowTypeEEnum, FlowType.BOUNDED);
		addEEnumLiteral(flowTypeEEnum, FlowType.GLOBAL);

		initEEnum(selectionConditionTypeEEnum, SelectionConditionType.class, "SelectionConditionType");
		addEEnumLiteral(selectionConditionTypeEEnum, SelectionConditionType.NONE);
		addEEnumLiteral(selectionConditionTypeEEnum, SelectionConditionType.ENVIRONMENT_INPUT);
		addEEnumLiteral(selectionConditionTypeEEnum, SelectionConditionType.SYSTEM_STATE_CONDITION);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/OCL/Import
		createImportAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

    /**
	 * Initializes the annotations for <b>http://www.eclipse.org/OCL/Import</b>.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void createImportAnnotations() {
		String source = "http://www.eclipse.org/OCL/Import";
		addAnnotation
		  (this,
		   source,
		   new String[] {
		   });
	}

    /**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			   "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			   "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });
		addAnnotation
		  (namedElementEClass,
		   source,
		   new String[] {
			   "constraints", "validName"
		   });
	}

    /**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";
		addAnnotation
		  (namedElementEClass,
		   source,
		   new String[] {
			   "validName", "Tuple {\n\tmessage : String = \'Name of elements may not be empty\',\n\tstatus : Boolean = self.name <> \'\'\n}.status"
		   });
	}

} //UcPackageImpl
