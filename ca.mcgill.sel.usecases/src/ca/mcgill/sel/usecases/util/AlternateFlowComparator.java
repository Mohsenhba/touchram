package ca.mcgill.sel.usecases.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.FlowType;

/**
 * Comparator allowing us to compare the step indexes of two alternate flows.
 * @author rlanguay
 *
 */
public class AlternateFlowComparator implements Comparator<Flow> {
    private Flow parentFlow;
    public AlternateFlowComparator(Flow parentFlow) {
        this.parentFlow = parentFlow;
    }
    
    @Override
    public int compare(Flow f1, Flow f2) {
        // Sorted sets can call a flow against itself...
        if (f1 == f2) {
            return 0;
        }
        
        // Basic flows: if either one is basic, that one comes first.
        
        if (f1.getFlowType() == FlowType.BASIC) {
            return -1;
        } else if (f2.getFlowType() == FlowType.BASIC) {
            return 1;
        }
        
        String s1 = UseCaseTextUtils.getFlowStepNumberText(f1, parentFlow);
        String s2 = UseCaseTextUtils.getFlowStepNumberText(f2, parentFlow);
        
        String[] components1 = s1.split("[.]");
        String[] components2 = s2.split("[.]");
        
        // Check in left-right order of depth
        for (int i = 0; i < components1.length; i++) {
            String component1 = components1[i].replaceAll("[()]", "");
            String component2 = components2[i].replaceAll("[()]", "");
            
            String letter1 = getLettersFromString(component1);
            String letter2 = getLettersFromString(component2);
            
            List<Integer> steps1 = getStepIndexes(component1.replace(letter1, ""));
            List<Integer> steps2 = getStepIndexes(component2.replace(letter2,  ""));
            
            // First compare the integers involved in this section
            for (int j = 0; j < steps1.size(); j++) {
               if (j >= steps2.size()) {
                   return 1;
               }
               
               if (Integer.compare(steps1.get(j), steps2.get(j)) == 0) {
                   continue;
               } else {
                   return Integer.compare(steps1.get(j), steps2.get(j));
               }
            }
            
            // Then compare the letters
            if (letter1.compareTo(letter2) != 0) {
                return letter1.compareTo(letter2);
            }
        }
        
        // To get here, the two strings have to be exactly the same
        // The only time this should happen is when comparing the last flow with a new extension
        return 0;
    }

    private List<Integer> getStepIndexes(String s) {
        String[] split = s.split(", ");
        List<Integer> numbers = new ArrayList<Integer>();
        for (String fragment : split) {
           try {
               if (fragment.contains("-")) {
                   // Range of two integers
                   String[] rangeEnds = fragment.split("-");
                   for (String rangeEnd : rangeEnds) {
                       numbers.addAll(getStepIndexes(rangeEnd));
                   }
               } else {
                   // Single integer
                   numbers.add(Integer.parseInt(fragment));    
               }
               
               
           } catch (NumberFormatException e) {
               continue;
           }
        }
        
        return numbers;
    }
    
    private String getLettersFromString(String s) {
        // The letters are always at the right end.
        String letters = "";
        
        for (int i = s.length() - 1; i >= 0; i--) {
            if (Character.isAlphabetic(s.charAt(i))) {
                letters = s.charAt(i) + letters;
            } else {
                break;
            }
        }
        
        return letters;
    }
}
