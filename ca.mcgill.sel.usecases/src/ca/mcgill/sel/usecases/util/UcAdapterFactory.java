/**
 */
package ca.mcgill.sel.usecases.util;

import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.usecases.*;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcPackage
 * @generated
 */
public class UcAdapterFactory extends AdapterFactoryImpl {
    /**
	 * The cached model package.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected static UcPackage modelPackage;

    /**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public UcAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = UcPackage.eINSTANCE;
		}
	}

    /**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
    @Override
    public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

    /**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected UcSwitch<Adapter> modelSwitch =
        new UcSwitch<Adapter>() {
			@Override
			public Adapter caseActor(Actor object) {
				return createActorAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseUseCase(UseCase object) {
				return createUseCaseAdapter();
			}
			@Override
			public Adapter caseLayout(Layout object) {
				return createLayoutAdapter();
			}
			@Override
			public Adapter caseContainerMap(Map.Entry<EObject, EMap<EObject, LayoutElement>> object) {
				return createContainerMapAdapter();
			}
			@Override
			public Adapter caseElementMap(Map.Entry<EObject, LayoutElement> object) {
				return createElementMapAdapter();
			}
			@Override
			public Adapter caseLayoutElement(LayoutElement object) {
				return createLayoutElementAdapter();
			}
			@Override
			public Adapter caseUseCaseModel(UseCaseModel object) {
				return createUseCaseModelAdapter();
			}
			@Override
			public Adapter caseNote(Note object) {
				return createNoteAdapter();
			}
			@Override
			public Adapter caseStep(Step object) {
				return createStepAdapter();
			}
			@Override
			public Adapter caseFlow(Flow object) {
				return createFlowAdapter();
			}
			@Override
			public Adapter caseCommunicationStep(CommunicationStep object) {
				return createCommunicationStepAdapter();
			}
			@Override
			public Adapter caseUseCaseReferenceStep(UseCaseReferenceStep object) {
				return createUseCaseReferenceStepAdapter();
			}
			@Override
			public Adapter caseContextStep(ContextStep object) {
				return createContextStepAdapter();
			}
			@Override
			public Adapter caseCondition(Condition object) {
				return createConditionAdapter();
			}
			@Override
			public Adapter caseExtensionPoint(ExtensionPoint object) {
				return createExtensionPointAdapter();
			}
			@Override
			public Adapter caseActorMapping(ActorMapping object) {
				return createActorMappingAdapter();
			}
			@Override
			public Adapter caseUseCaseMapping(UseCaseMapping object) {
				return createUseCaseMappingAdapter();
			}
			@Override
			public Adapter caseStepMapping(StepMapping object) {
				return createStepMappingAdapter();
			}
			@Override
			public Adapter caseAnythingStep(AnythingStep object) {
				return createAnythingStepAdapter();
			}
			@Override
			public Adapter caseMappableElement(MappableElement object) {
				return createMappableElementAdapter();
			}
			@Override
			public Adapter caseFlowMapping(FlowMapping object) {
				return createFlowMappingAdapter();
			}
			@Override
			public Adapter caseActorReferenceText(ActorReferenceText object) {
				return createActorReferenceTextAdapter();
			}
			@Override
			public <T> Adapter caseCOREModelElementComposition(COREModelElementComposition<T> object) {
				return createCOREModelElementCompositionAdapter();
			}
			@Override
			public <T> Adapter caseCORELink(CORELink<T> object) {
				return createCORELinkAdapter();
			}
			@Override
			public <T> Adapter caseCOREMapping(COREMapping<T> object) {
				return createCOREMappingAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

    /**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
    @Override
    public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.Actor
	 * @generated
	 */
    public Adapter createActorAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.NamedElement
	 * @generated
	 */
    public Adapter createNamedElementAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.UseCase <em>Use Case</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.UseCase
	 * @generated
	 */
    public Adapter createUseCaseAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.Layout <em>Layout</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.Layout
	 * @generated
	 */
    public Adapter createLayoutAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Container Map</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
    public Adapter createContainerMapAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Element Map</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
    public Adapter createElementMapAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.LayoutElement <em>Layout Element</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.LayoutElement
	 * @generated
	 */
    public Adapter createLayoutElementAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.UseCaseModel <em>Use Case Model</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.UseCaseModel
	 * @generated
	 */
    public Adapter createUseCaseModelAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.Note <em>Note</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.Note
	 * @generated
	 */
    public Adapter createNoteAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.Step <em>Step</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.Step
	 * @generated
	 */
    public Adapter createStepAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.Flow <em>Flow</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.Flow
	 * @generated
	 */
    public Adapter createFlowAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.CommunicationStep <em>Communication Step</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.CommunicationStep
	 * @generated
	 */
    public Adapter createCommunicationStepAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.UseCaseReferenceStep <em>Use Case Reference Step</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.UseCaseReferenceStep
	 * @generated
	 */
    public Adapter createUseCaseReferenceStepAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.ContextStep <em>Context Step</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.ContextStep
	 * @generated
	 */
    public Adapter createContextStepAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.Condition
	 * @generated
	 */
    public Adapter createConditionAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.ExtensionPoint <em>Extension Point</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.ExtensionPoint
	 * @generated
	 */
    public Adapter createExtensionPointAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.ActorMapping <em>Actor Mapping</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.ActorMapping
	 * @generated
	 */
    public Adapter createActorMappingAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.UseCaseMapping <em>Use Case Mapping</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.UseCaseMapping
	 * @generated
	 */
    public Adapter createUseCaseMappingAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.StepMapping <em>Step Mapping</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.StepMapping
	 * @generated
	 */
    public Adapter createStepMappingAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.AnythingStep <em>Anything Step</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.AnythingStep
	 * @generated
	 */
    public Adapter createAnythingStepAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.MappableElement <em>Mappable Element</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.MappableElement
	 * @generated
	 */
    public Adapter createMappableElementAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.FlowMapping <em>Flow Mapping</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.FlowMapping
	 * @generated
	 */
    public Adapter createFlowMappingAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.usecases.ActorReferenceText <em>Actor Reference Text</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.usecases.ActorReferenceText
	 * @generated
	 */
    public Adapter createActorReferenceTextAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREModelElementComposition <em>CORE Model Element Composition</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.core.COREModelElementComposition
	 * @generated
	 */
    public Adapter createCOREModelElementCompositionAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORELink <em>CORE Link</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.core.CORELink
	 * @generated
	 */
    public Adapter createCORELinkAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREMapping <em>CORE Mapping</em>}'.
	 * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.mcgill.sel.core.COREMapping
	 * @generated
	 */
    public Adapter createCOREMappingAdapter() {
		return null;
	}

    /**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
    public Adapter createEObjectAdapter() {
		return null;
	}

} //UcAdapterFactory
