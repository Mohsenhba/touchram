/**
 */
package ca.mcgill.sel.usecases.util;

import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.usecases.*;

import java.util.Map;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcPackage
 * @generated
 */
public class UcSwitch<T1> extends Switch<T1> {
    /**
	 * The cached model package
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected static UcPackage modelPackage;

    /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public UcSwitch() {
		if (modelPackage == null) {
			modelPackage = UcPackage.eINSTANCE;
		}
	}

    /**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

    /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
    @Override
    protected T1 doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case UcPackage.ACTOR: {
				Actor actor = (Actor)theEObject;
				T1 result = caseActor(actor);
				if (result == null) result = caseMappableElement(actor);
				if (result == null) result = caseNamedElement(actor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T1 result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.USE_CASE: {
				UseCase useCase = (UseCase)theEObject;
				T1 result = caseUseCase(useCase);
				if (result == null) result = caseMappableElement(useCase);
				if (result == null) result = caseNamedElement(useCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.LAYOUT: {
				Layout layout = (Layout)theEObject;
				T1 result = caseLayout(layout);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.CONTAINER_MAP: {
				@SuppressWarnings("unchecked") Map.Entry<EObject, EMap<EObject, LayoutElement>> containerMap = (Map.Entry<EObject, EMap<EObject, LayoutElement>>)theEObject;
				T1 result = caseContainerMap(containerMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.ELEMENT_MAP: {
				@SuppressWarnings("unchecked") Map.Entry<EObject, LayoutElement> elementMap = (Map.Entry<EObject, LayoutElement>)theEObject;
				T1 result = caseElementMap(elementMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.LAYOUT_ELEMENT: {
				LayoutElement layoutElement = (LayoutElement)theEObject;
				T1 result = caseLayoutElement(layoutElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.USE_CASE_MODEL: {
				UseCaseModel useCaseModel = (UseCaseModel)theEObject;
				T1 result = caseUseCaseModel(useCaseModel);
				if (result == null) result = caseNamedElement(useCaseModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.NOTE: {
				Note note = (Note)theEObject;
				T1 result = caseNote(note);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.STEP: {
				Step step = (Step)theEObject;
				T1 result = caseStep(step);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.FLOW: {
				Flow flow = (Flow)theEObject;
				T1 result = caseFlow(flow);
				if (result == null) result = caseNamedElement(flow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.COMMUNICATION_STEP: {
				CommunicationStep communicationStep = (CommunicationStep)theEObject;
				T1 result = caseCommunicationStep(communicationStep);
				if (result == null) result = caseStep(communicationStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.USE_CASE_REFERENCE_STEP: {
				UseCaseReferenceStep useCaseReferenceStep = (UseCaseReferenceStep)theEObject;
				T1 result = caseUseCaseReferenceStep(useCaseReferenceStep);
				if (result == null) result = caseStep(useCaseReferenceStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.CONTEXT_STEP: {
				ContextStep contextStep = (ContextStep)theEObject;
				T1 result = caseContextStep(contextStep);
				if (result == null) result = caseStep(contextStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.CONDITION: {
				Condition condition = (Condition)theEObject;
				T1 result = caseCondition(condition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.EXTENSION_POINT: {
				ExtensionPoint extensionPoint = (ExtensionPoint)theEObject;
				T1 result = caseExtensionPoint(extensionPoint);
				if (result == null) result = caseStep(extensionPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.ACTOR_MAPPING: {
				ActorMapping actorMapping = (ActorMapping)theEObject;
				T1 result = caseActorMapping(actorMapping);
				if (result == null) result = caseCOREMapping(actorMapping);
				if (result == null) result = caseCORELink(actorMapping);
				if (result == null) result = caseCOREModelElementComposition(actorMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.USE_CASE_MAPPING: {
				UseCaseMapping useCaseMapping = (UseCaseMapping)theEObject;
				T1 result = caseUseCaseMapping(useCaseMapping);
				if (result == null) result = caseCOREMapping(useCaseMapping);
				if (result == null) result = caseCORELink(useCaseMapping);
				if (result == null) result = caseCOREModelElementComposition(useCaseMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.STEP_MAPPING: {
				StepMapping stepMapping = (StepMapping)theEObject;
				T1 result = caseStepMapping(stepMapping);
				if (result == null) result = caseCOREMapping(stepMapping);
				if (result == null) result = caseCORELink(stepMapping);
				if (result == null) result = caseCOREModelElementComposition(stepMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.ANYTHING_STEP: {
				AnythingStep anythingStep = (AnythingStep)theEObject;
				T1 result = caseAnythingStep(anythingStep);
				if (result == null) result = caseStep(anythingStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.MAPPABLE_ELEMENT: {
				MappableElement mappableElement = (MappableElement)theEObject;
				T1 result = caseMappableElement(mappableElement);
				if (result == null) result = caseNamedElement(mappableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.FLOW_MAPPING: {
				FlowMapping flowMapping = (FlowMapping)theEObject;
				T1 result = caseFlowMapping(flowMapping);
				if (result == null) result = caseCOREMapping(flowMapping);
				if (result == null) result = caseCORELink(flowMapping);
				if (result == null) result = caseCOREModelElementComposition(flowMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UcPackage.ACTOR_REFERENCE_TEXT: {
				ActorReferenceText actorReferenceText = (ActorReferenceText)theEObject;
				T1 result = caseActorReferenceText(actorReferenceText);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Actor</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Actor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseActor(Actor object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseNamedElement(NamedElement object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Use Case</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseUseCase(UseCase object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Layout</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Layout</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseLayout(Layout object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Container Map</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Container Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseContainerMap(Map.Entry<EObject, EMap<EObject, LayoutElement>> object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Element Map</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseElementMap(Map.Entry<EObject, LayoutElement> object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Layout Element</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Layout Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseLayoutElement(LayoutElement object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Use Case Model</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use Case Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseUseCaseModel(UseCaseModel object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Note</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Note</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseNote(Note object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Step</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseStep(Step object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Flow</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flow</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseFlow(Flow object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Step</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseCommunicationStep(CommunicationStep object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Use Case Reference Step</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use Case Reference Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseUseCaseReferenceStep(UseCaseReferenceStep object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Context Step</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Context Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseContextStep(ContextStep object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseCondition(Condition object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Extension Point</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extension Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseExtensionPoint(ExtensionPoint object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Actor Mapping</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Actor Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseActorMapping(ActorMapping object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Use Case Mapping</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use Case Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseUseCaseMapping(UseCaseMapping object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Step Mapping</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Step Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseStepMapping(StepMapping object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Anything Step</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anything Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseAnythingStep(AnythingStep object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Mappable Element</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mappable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseMappableElement(MappableElement object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Flow Mapping</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flow Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseFlowMapping(FlowMapping object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>Actor Reference Text</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Actor Reference Text</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public T1 caseActorReferenceText(ActorReferenceText object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>CORE Model Element Composition</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CORE Model Element Composition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public <T> T1 caseCOREModelElementComposition(COREModelElementComposition<T> object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>CORE Link</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CORE Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public <T> T1 caseCORELink(CORELink<T> object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>CORE Mapping</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CORE Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
    public <T> T1 caseCOREMapping(COREMapping<T> object) {
		return null;
	}

    /**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
    @Override
    public T1 defaultCase(EObject object) {
		return null;
	}

} //UcSwitch
