/**
 */
package ca.mcgill.sel.usecases.util;

import ca.mcgill.sel.usecases.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcPackage
 * @generated
 */
public class UcValidator extends EObjectValidator {
    /**
	 * The cached model package
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public static final UcValidator INSTANCE = new UcValidator();

    /**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
    public static final String DIAGNOSTIC_SOURCE = "ca.mcgill.sel.usecases";

    /**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

    /**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

    /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public UcValidator() {
		super();
	}

    /**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EPackage getEPackage() {
	  return UcPackage.eINSTANCE;
	}

    /**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case UcPackage.ACTOR:
				return validateActor((Actor)value, diagnostics, context);
			case UcPackage.NAMED_ELEMENT:
				return validateNamedElement((NamedElement)value, diagnostics, context);
			case UcPackage.USE_CASE:
				return validateUseCase((UseCase)value, diagnostics, context);
			case UcPackage.LAYOUT:
				return validateLayout((Layout)value, diagnostics, context);
			case UcPackage.CONTAINER_MAP:
				return validateContainerMap((Map.Entry<?, ?>)value, diagnostics, context);
			case UcPackage.ELEMENT_MAP:
				return validateElementMap((Map.Entry<?, ?>)value, diagnostics, context);
			case UcPackage.LAYOUT_ELEMENT:
				return validateLayoutElement((LayoutElement)value, diagnostics, context);
			case UcPackage.USE_CASE_MODEL:
				return validateUseCaseModel((UseCaseModel)value, diagnostics, context);
			case UcPackage.NOTE:
				return validateNote((Note)value, diagnostics, context);
			case UcPackage.STEP:
				return validateStep((Step)value, diagnostics, context);
			case UcPackage.FLOW:
				return validateFlow((Flow)value, diagnostics, context);
			case UcPackage.COMMUNICATION_STEP:
				return validateCommunicationStep((CommunicationStep)value, diagnostics, context);
			case UcPackage.USE_CASE_REFERENCE_STEP:
				return validateUseCaseReferenceStep((UseCaseReferenceStep)value, diagnostics, context);
			case UcPackage.CONTEXT_STEP:
				return validateContextStep((ContextStep)value, diagnostics, context);
			case UcPackage.CONDITION:
				return validateCondition((Condition)value, diagnostics, context);
			case UcPackage.EXTENSION_POINT:
				return validateExtensionPoint((ExtensionPoint)value, diagnostics, context);
			case UcPackage.ACTOR_MAPPING:
				return validateActorMapping((ActorMapping)value, diagnostics, context);
			case UcPackage.USE_CASE_MAPPING:
				return validateUseCaseMapping((UseCaseMapping)value, diagnostics, context);
			case UcPackage.STEP_MAPPING:
				return validateStepMapping((StepMapping)value, diagnostics, context);
			case UcPackage.ANYTHING_STEP:
				return validateAnythingStep((AnythingStep)value, diagnostics, context);
			case UcPackage.MAPPABLE_ELEMENT:
				return validateMappableElement((MappableElement)value, diagnostics, context);
			case UcPackage.FLOW_MAPPING:
				return validateFlowMapping((FlowMapping)value, diagnostics, context);
			case UcPackage.ACTOR_REFERENCE_TEXT:
				return validateActorReferenceText((ActorReferenceText)value, diagnostics, context);
			case UcPackage.LEVEL:
				return validateLevel((Level)value, diagnostics, context);
			case UcPackage.DIRECTION:
				return validateDirection((Direction)value, diagnostics, context);
			case UcPackage.CONTEXT_TYPE:
				return validateContextType((ContextType)value, diagnostics, context);
			case UcPackage.CONCLUSION_TYPE:
				return validateConclusionType((ConclusionType)value, diagnostics, context);
			case UcPackage.FLOW_TYPE:
				return validateFlowType((FlowType)value, diagnostics, context);
			case UcPackage.SELECTION_CONDITION_TYPE:
				return validateSelectionConditionType((SelectionConditionType)value, diagnostics, context);
			default:
				return true;
		}
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateActor(Actor actor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(actor, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(actor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(actor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(actor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(actor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(actor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(actor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(actor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(actor, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamedElement_validName(actor, diagnostics, context);
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateNamedElement(NamedElement namedElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(namedElement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(namedElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(namedElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(namedElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(namedElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(namedElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(namedElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(namedElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(namedElement, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamedElement_validName(namedElement, diagnostics, context);
		return result;
	}

    /**
	 * The cached validation expression for the validName constraint of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected static final String NAMED_ELEMENT__VALID_NAME__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Name of elements may not be empty',\n" +
		"\tstatus : Boolean = self.name <> ''\n" +
		"}.status";

    /**
	 * Validates the validName constraint of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateNamedElement_validName(NamedElement namedElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(UcPackage.Literals.NAMED_ELEMENT,
				 namedElement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "validName",
				 NAMED_ELEMENT__VALID_NAME__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateUseCase(UseCase useCase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(useCase, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(useCase, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(useCase, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(useCase, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(useCase, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(useCase, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(useCase, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(useCase, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(useCase, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamedElement_validName(useCase, diagnostics, context);
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateLayout(Layout layout, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(layout, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateContainerMap(Map.Entry<?, ?> containerMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)containerMap, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateElementMap(Map.Entry<?, ?> elementMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)elementMap, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateLayoutElement(LayoutElement layoutElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(layoutElement, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateUseCaseModel(UseCaseModel useCaseModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(useCaseModel, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(useCaseModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(useCaseModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(useCaseModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(useCaseModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(useCaseModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(useCaseModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(useCaseModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(useCaseModel, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamedElement_validName(useCaseModel, diagnostics, context);
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateNote(Note note, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(note, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateStep(Step step, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(step, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateFlow(Flow flow, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(flow, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(flow, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(flow, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(flow, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(flow, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(flow, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(flow, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(flow, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(flow, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamedElement_validName(flow, diagnostics, context);
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateCommunicationStep(CommunicationStep communicationStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(communicationStep, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateUseCaseReferenceStep(UseCaseReferenceStep useCaseReferenceStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(useCaseReferenceStep, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateContextStep(ContextStep contextStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(contextStep, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateCondition(Condition condition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(condition, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateExtensionPoint(ExtensionPoint extensionPoint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(extensionPoint, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateActorMapping(ActorMapping actorMapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(actorMapping, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateUseCaseMapping(UseCaseMapping useCaseMapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(useCaseMapping, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateStepMapping(StepMapping stepMapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(stepMapping, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateAnythingStep(AnythingStep anythingStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(anythingStep, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateMappableElement(MappableElement mappableElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(mappableElement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(mappableElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(mappableElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(mappableElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(mappableElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(mappableElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(mappableElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(mappableElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(mappableElement, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamedElement_validName(mappableElement, diagnostics, context);
		return result;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateFlowMapping(FlowMapping flowMapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(flowMapping, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateActorReferenceText(ActorReferenceText actorReferenceText, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(actorReferenceText, diagnostics, context);
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateLevel(Level level, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateDirection(Direction direction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateContextType(ContextType contextType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateConclusionType(ConclusionType conclusionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateFlowType(FlowType flowType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public boolean validateSelectionConditionType(SelectionConditionType selectionConditionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

    /**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //UcValidator
